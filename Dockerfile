FROM registry.cern.ch/docker.io/library/node:18.13-alpine AS build

ARG build_env
WORKDIR /usr/src/app

COPY . .

RUN apk update && apk add python3 build-base && npm install
RUN if [ "$build_env" == "prod" ]; then npm run build; \
    elif [ "$build_env" == "qa" ]; then npm run build:qa; \
    elif [ "$build_env" == "dev" ]; then npm run build:dev; fi

FROM registry.cern.ch/docker.io/library/node:18.13-alpine

WORKDIR /usr/src/app

COPY --from=build /usr/src/app/build ./build

RUN npm install -g serve

EXPOSE 3000

CMD ["serve", "-s", "build", "-l", "3000"]
