env-nginx:
	docker-compose -f docker-compose.nginx.yml up --remove-orphans
.PHONY: env-nginx

destroy-env-nginx:
	docker-compose -f docker-compose.nginx.yml down --volumes
	docker-compose -f docker-compose.nginx.yml rm -f
.PHONY: destroy-env-nginx

stop-env-nginx:
	docker-compose -f docker-compose.nginx.yml down --volumes
.PHONY: stop-env-nginx