self.addEventListener('push', function (event) {
  if (event.data) {
    var options = {
      icon: '/favicon/favicon-32x32.png',
      badge: '/favicon/android-chrome-192x192.png',
    };

    let title = 'CERN Notification';
    try {
      const blob = event.data.json(); 
      options.body = blob.message;
      if (blob.title) title = blob.title;
      if (blob.image) options.image = blob.image;
      if (blob.url) options.data = {url: blob.url};
    } catch {}

    // fallback if no json blob in message
    if (!options.body) options.body = event.data.text();

    event.waitUntil(self.registration.showNotification(title, options));
  } 
});

self.addEventListener('notificationclick', function (event) {
  if (event.notification.data && event.notification.data.url) {
    event.notification.close();
    event.waitUntil(clients.openWindow(event.notification.data.url));
  } else console.log('Url undefined.');
});
