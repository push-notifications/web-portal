## System requirements

- [Nodejs](https://nodejs.org/en/)
- [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

## Start web portal application

Using Node v18.13.0

1. In the root folder run
  ```
  npm install
  ```
2. After installing all the dependencies, you should be able to run the project
  ```
  npm start
  ```

## Test docker image locally

We can build the docker image to test the production build of the system:
```
docker build . -t web-portal
```
Once it is built, we can run the container with the app listening on `http://localhost:3000`:
```
docker run -p 3000:3000 web-portal
```

## Testing locally push registration
Push registration requires SSL and a valid cert, even on localhost.

## Troubleshooting
