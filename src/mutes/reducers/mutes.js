import {
  GET_MUTES,
  GET_MUTES_SUCCESS,
  GET_MUTES_FAILURE,
  GET_MUTES_PAGE_ERROR,
  CREATE_MUTE,
  CREATE_MUTE_SUCCESS,
  CREATE_MUTE_FAILURE,
  DELETE_MUTE,
  DELETE_MUTE_SUCCESS,
  DELETE_MUTE_FAILURE,
  UPDATE_MUTE,
  UPDATE_MUTE_SUCCESS,
  UPDATE_MUTE_FAILURE,
} from 'mutes/actions/mutes';

const INITIAL_STATE = {
  global: [],
  channel: [],
  error: null,
  pageError: null,
  loading: false,
  loadingCreate: false,
  loadingUpdate: false,
  loadingDelete: false,
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_MUTES:
      return {...state, loading: true, error: null, pageError: null};
    case GET_MUTES_SUCCESS:
      return {
        ...state,
        global: action.payload.globalMutes || [],
        channel: action.payload.channelMutes || [],
        loading: false,
      };
    case GET_MUTES_FAILURE:
      return {...state, error: action.payload, loading: false};
    case GET_MUTES_PAGE_ERROR:
      return {...state, pageError: action.payload, loading: false};

    case CREATE_MUTE:
      return {...state, loadingCreate: true, error: null, pageError: null};
    case CREATE_MUTE_SUCCESS: {
      const isChannelMute = !!action.payload.target;
      if (!isChannelMute) {
        return {...state, loadingCreate: false, global: [...state.global, action.payload]};
      }
      return {...state, loadingCreate: false, channel: [...state.channel, action.payload]};
    }
    case CREATE_MUTE_FAILURE:
      return {...state, error: action.payload, loadingCreate: false};

    case DELETE_MUTE:
      return {...state, loadingDelete: true, error: null, pageError: null};
    case DELETE_MUTE_SUCCESS:
      return {
        ...state,
        global: state.global.filter(p => p.id !== action.payload),
        channel: state.channel.filter(p => p.id !== action.payload),
        loadingDelete: false,
      };
    case DELETE_MUTE_FAILURE:
      return {...state, error: action.payload, loadingDelete: false};

    case UPDATE_MUTE:
      return {...state, loadingUpdate: true, error: null, pageError: null};
    case UPDATE_MUTE_SUCCESS: {
      const isChannelMute = !!action.payload.target;
      if (!isChannelMute) {
        // object replace avoiding mutate
        return {
          ...state,
          loadingUpdate: false,
          global: state.global.map(item => {
            if (item.id === action.payload.id) {
              // item to replace - return an updated value
              return {
                ...action.payload,
              };
            }
            // the rest keep it as-is
            return item;
          }),
        };
      }
      // object replace avoiding mutate
      return {
        ...state,
        loadingUpdate: false,
        channel: state.channel.map(item => {
          if (item.id === action.payload.id) {
            // item to replace - return an updated value
            return {
              ...action.payload,
            };
          }
          // the rest keep it as-is
          return item;
        }),
      };
    }

    case UPDATE_MUTE_FAILURE:
      return {...state, error: action.payload, loadingUpdate: false};

    default:
      return state;
  }
}
