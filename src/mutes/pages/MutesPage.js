import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Header, Divider, Segment} from 'semantic-ui-react';
import {Helmet} from 'react-helmet';

import MutesList from 'mutes/components/MutesList';
import * as mutesActions from 'mutes/actions/mutes';
import ErrorComponent from 'utils/error-component';

const TITLE = `${process.env.REACT_APP_NAME} | Mutes`;
function MutesPage({globalMutes, channelMutes, getMutes, loading, error}) {
  useEffect(() => {
    getMutes(null, true);
  }, [getMutes]);

  return error ? (
    <ErrorComponent error={error} />
  ) : (
    <div
      style={{
        width: 933,
        marginTop: 100,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginBottom: 50,
      }}
    >
      <Helmet>
        <title>{TITLE}</title>
      </Helmet>
      <Header as="h3">
        Mutes
        <Header.Subheader>
          Register your global and specific channel Mutes to disable notifications temporarily or
          permanently
        </Header.Subheader>
      </Header>
      <Divider />

      <Segment basic loading={loading}>
        <Header as="h4">Global Mutes / Do Not Disturb</Header>
        <MutesList mutes={globalMutes} global allowDelete allowEdit />
        <Divider hidden section />
        <Header as="h4">Channel Mutes</Header>
        <MutesList mutes={channelMutes} allowDelete allowEdit allowAdd={false} />
      </Segment>
    </div>
  );
}

MutesPage.propTypes = {
  globalMutes: PropTypes.arrayOf(PropTypes.object).isRequired,
  channelMutes: PropTypes.arrayOf(PropTypes.object).isRequired,
  getMutes: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
};

const mapStateToProps = state => {
  return {
    globalMutes: state.mutes.global,
    channelMutes: state.mutes.channel,
    loading: state.preferences.loading,
    error: state.preferences.pageError,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getMutes: mutesActions.getMutes,
      ...mutesActions,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(MutesPage);
