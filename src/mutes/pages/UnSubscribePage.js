import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Header, Divider, Segment, Form, Button, Message} from 'semantic-ui-react';
import {Helmet} from 'react-helmet';
import {useParams} from 'react-router-dom';

import * as mutesActions from 'mutes/actions/mutes';
import ErrorComponent from 'utils/error-component';
import * as showSnackBarActionCreators from 'common/actions/Snackbar';

const TITLE = `${process.env.REACT_APP_NAME} | Unsubscribe`;

function UnSubscribePage({createUnSubscribe, error, showSnackbar, isAuthenticated}) {
  const {encryptedEmail, email} = useParams();

  async function handleUnsubscribe() {
    const response = await createUnSubscribe(
      {type: 'PERMANENT'},
      encryptedEmail,
      decodeURIComponent(email)
    );
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while unsubscribing',
        'error'
      );
    } else {
      showSnackbar('Email was successfully unsubscribed', 'success');
    }
  }

  return error ? (
    <ErrorComponent error={error} />
  ) : (
    <div
      style={{
        width: 933,
        marginTop: 100,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginBottom: 50,
      }}
    >
      <Helmet>
        <title>{TITLE}</title>
      </Helmet>
      <Header as="h3">
        Unsubscribe
        <Header.Subheader>
          Unsubscribe to disable notifications permanently for all channels in the CERN
          Notifications Service
        </Header.Subheader>
      </Header>
      <Divider />

      <Segment basic>
        <Form onSubmit={handleUnsubscribe}>
          <Form.Group inline>
            <Form.Input readOnly value={decodeURIComponent(email)} width={6} />
            <Button type="submit" primary>
              Unsubscribe
            </Button>
          </Form.Group>
        </Form>
      </Segment>

      <Segment basic>
        <Message warning>
          <Message.Header>Unsubscribing from all channels</Message.Header>
          <p>
            All emails and notifications for all channels will stop. If you'd prefer to select which
            channel(s) you'd like to keep receiving,
            {!isAuthenticated ? ' please Sign In to the service and ' : ' '} manage your
            subscriptions.
          </p>
        </Message>
      </Segment>
    </div>
  );
}

const mapStatetoProps = state => {
  return {
    isAuthenticated: state.auth.loggedIn,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      createUnSubscribe: mutesActions.createUnSubscribe,
      ...showSnackBarActionCreators,
    },
    dispatch
  );
};

export default connect(mapStatetoProps, mapDispatchToProps)(UnSubscribePage);
