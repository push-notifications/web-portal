import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Button, Table} from 'semantic-ui-react';

import * as mutesActions from 'mutes/actions/mutes';
import AddMute from 'mutes/components/AddMute';
import * as snackBarActions from 'common/actions/Snackbar';
import MuteHelp from 'help/components/MuteHelp';

const baseColumns = [
  {id: 'targetId', label: 'Channel', width: 4},
  {id: 'type', label: 'Mute', width: 4},
  {id: 'start', label: 'Start Date', width: 3},
  {id: 'end', label: 'End Date', width: 3},
];

function MutesList({
  mutes,
  global,
  allowAdd,
  allowDelete,
  allowEdit,
  deleteMuteById,
  showSnackbar,
  loadingDelete,
}) {
  const [deleteId, setDeleteId] = useState(null);

  async function handleDelete(mute) {
    setDeleteId(mute.id);
    const response = await deleteMuteById(mute.id);
    if (response?.error) {
      setDeleteId(null);
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while deleting the mute setting',
        'error'
      );
    } else {
      setDeleteId(null);
      showSnackbar('Mute setting removed successfully', 'success');
    }
  }

  const shouldShowColumn = column =>
    column.label !== 'Channel' || (!global && column.label === 'Channel');

  return (
    <>
      <Table striped>
        <Table.Header>
          <Table.Row>
            {baseColumns.map(
              column =>
                shouldShowColumn(column) && (
                  <Table.HeaderCell key={column.id}>{column.label}</Table.HeaderCell>
                )
            )}
            <Table.HeaderCell width={2} />
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {mutes.map(mute => {
            return (
              <Table.Row key={mute.id}>
                {/* eslint-disable-next-line react/no-unescaped-entities */}
                {!global && <Table.Cell>{mute.target ? mute.target.name : ''}</Table.Cell>}
                <Table.Cell style={{textTransform: 'capitalize'}}>
                  {mute.type.toLowerCase()}
                </Table.Cell>
                <Table.Cell>{mute.start ? new Date(mute.start).toLocaleString() : ''}</Table.Cell>
                <Table.Cell>{mute.end ? new Date(mute.end).toLocaleString() : ''}</Table.Cell>
                <Table.Cell singleLine={true}>
                  {allowEdit && <AddMute key={mute.id} editMute={mute} />}
                  {allowDelete && (
                    <Button
                      disabled={mute.id === deleteId && loadingDelete}
                      loading={mute.id === deleteId && loadingDelete}
                      inverted
                      color="red"
                      icon="trash"
                      onClick={() => handleDelete(mute)}
                      size="mini"
                    />
                  )}
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
      {allowAdd && (
        <>
          <AddMute global={global} />
          <MuteHelp activeIndex={0} />
        </>
      )}
    </>
  );
}

MutesList.propTypes = {
  mutes: PropTypes.arrayOf(PropTypes.object).isRequired,
  global: PropTypes.bool,
  channelId: PropTypes.string,
  allowAdd: PropTypes.bool,
  allowDelete: PropTypes.bool,
  allowEdit: PropTypes.bool,
  deleteMuteById: PropTypes.func.isRequired,
  showSnackbar: PropTypes.func.isRequired,
  loadingDelete: PropTypes.bool,
  loadingUpdate: PropTypes.bool,
};

MutesList.defaultProps = {
  global: false,
  channelId: null,
  allowAdd: true,
  allowDelete: false,
  allowEdit: false,
  loadingDelete: false,
  loadingUpdate: false,
};

const mapStateToProps = state => {
  return {
    loadingDelete: state.mutes.loadingDelete,
    loadingUpdate: state.mutes.loadingUpdate,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      deleteMuteById: mutesActions.deleteMuteById,
      showSnackbar: snackBarActions.showSnackbar,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(MutesList);
