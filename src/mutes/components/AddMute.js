import React, {useState, useCallback} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Form, Modal, Button, Label, Segment, Radio, Message, Popup} from 'semantic-ui-react';
import {DateTimeInput} from 'semantic-ui-calendar-react';

import MuteHelp from 'help/components/MuteHelp';
import * as mutesActions from 'mutes/actions/mutes';
import * as showSnackBarActionCreators from 'common/actions/Snackbar';

const AddMute = ({
  createMute,
  updateMute,
  deleteMuteById,
  showSnackbar,
  global,
  loading,
  editMute,
  isChannelList,
  channelId,
  channelName,
  disabled,
}) => {
  const initialChannelIdInput = () => {
    if (global) return null;
    if (editMute) return editMute.target?.id || null;

    return channelId || null;
  };

  // Initialize the form as Empty for Add, or with values for Edit
  const [channelIdInput, setChannelIdInput] = useState(initialChannelIdInput());
  const [start, setStart] = useState(editMute ? editMute.start : '');
  const [end, setEnd] = useState(editMute ? editMute.end : '');
  const [type, setType] = useState(editMute ? editMute.type : 'PERMANENT');

  const [modalOpen, setModalOpen] = useState(false);

  const resetFormValues = () => {
    setChannelIdInput(initialChannelIdInput());
    setType('PERMANENT');
    setStart('');
    setEnd('');
  };

  const resetEditFormValues = () => {
    // Initialize the form as Empty for Add, or with values for Edit
    setChannelIdInput(initialChannelIdInput());
    setStart(editMute.start);
    setEnd(editMute.end);
  };

  // Trick: Get a ref to a div inside the form and attach the date picker to it so it renders on top of the modal
  const [formRef, setFormRef] = useState(null);
  const calendarFormRef = useCallback(node => {
    if (node !== null) {
      setFormRef(node);
    }
  }, []);

  async function handleSubmitUpdateMute() {
    const response = await updateMute(editMute.id, {
      id: editMute.id,
      target: channelIdInput,
      type: type,
      start: start,
      end: end,
    });
    if (response?.error) {
      showSnackbar(
        response?.payload?.response?.message ||
          'An error occurred while updating your mute setting',
        'error'
      );
    } else {
      showSnackbar('The mute setting has been updated successfully', 'success');
      setModalOpen(false);
    }
  }

  async function handleSubmitCreateMute() {
    const response = await createMute({
      target: channelIdInput,
      type,
      start,
      end,
    });
    if (response?.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while adding your mute setting',
        'error'
      );
    } else {
      showSnackbar('The mute setting has been created successfully', 'success');
      resetFormValues();
      setModalOpen(false);
    }
  }

  async function handleSubmit() {
    if (editMute) {
      await handleSubmitUpdateMute();
    } else {
      await handleSubmitCreateMute();
    }
  }

  function onClose() {
    setModalOpen(false);
    if (editMute) resetEditFormValues();
    else resetFormValues();
  }

  async function handleDelete(mute) {
    const response = await deleteMuteById(mute.id);
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while deleting the mute setting',
        'error'
      );
    } else {
      showSnackbar('Mute setting removed successfully', 'success');
    }
    setModalOpen(false);
  }

  function triggerButton() {
    // Channel List buttons
    if (isChannelList) {
      if (global) {
        return (
          <Popup
            trigger={
              <Button
                icon={editMute ? 'alarm mute' : 'alarm'}
                basic
                color={editMute && 'red'}
                onClick={() => setModalOpen(true)}
                fluid
                content="Do Not Disturb"
                labelPosition="left"
              />
            }
            position="top center"
            content="Mute everything"
          />
        );
      } else {
        return (
          <Popup
            trigger={
              <span>
                <Button
                  disabled={!!disabled}
                  basic
                  size="mini"
                  icon={{name: editMute ? 'volume off' : 'volume up'}}
                  color={editMute && 'red'}
                  content={editMute ? 'Mute Active' : 'Mute'}
                  labelPosition="left"
                  onClick={() => setModalOpen(true)}
                />
              </span>
            }
            position="top center"
            content={
              disabled ? 'Do Not Disturb is active' : editMute ? 'Unmute Channel' : 'Mute Channel'
            }
          />
        );
      }
    }

    // Mute page buttons
    if (editMute) return <Button icon="pencil" onClick={() => setModalOpen(true)} size="mini" />;

    return (
      <Button primary onClick={() => setModalOpen(true)}>
        Add Mute
      </Button>
    );
  }

  return (
    <Modal
      as={Form}
      onSubmit={handleSubmit}
      trigger={triggerButton()}
      open={modalOpen}
      onClose={onClose}
    >
      <Modal.Header>
        {editMute ? 'Edit' : 'Add'}{' '}
        {channelId ? 'Mute for channel' : 'Global Mute / Do Not Disturb'}
      </Modal.Header>
      <Segment basic>
        <Message
          color="orange"
          icon="warning sign"
          content="Mute will stop delivering ALL content from channel(s) to ALL
                  your configured devices (mail, notifications, etc.).
                  Content will NOT be delivered later and will NOT be delayed."
        />
        <Form.Field>
          {!global && channelIdInput && (
            <>
              <Label color="blue" ribbon className="form-ribbon">
                Channel
              </Label>
              <label>{channelName ? channelName : editMute?.target?.name}</label>
              <Form.Input
                readOnly
                placeholder="Channel id"
                value={channelIdInput}
                onChange={(e, d) => setChannelIdInput(d.value)}
                disabled
              />
            </>
          )}
        </Form.Field>

        <Label color="blue" ribbon className="form-ribbon">
          Mute Type
        </Label>
        <Form.Group grouped>
          <Form.Field
            control={Radio}
            label="Permanent: all content will be muted until this mute setting is manually removed"
            value="PERMANENT"
            checked={type === 'PERMANENT'}
            onChange={() => setType('PERMANENT')}
          />
          <Form.Field
            control={Radio}
            label="Date range: all content will be muted during the specified date range only"
            value="RANGE"
            checked={type === 'RANGE'}
            onChange={() => setType('RANGE')}
          />
        </Form.Group>

        {type === 'RANGE' && (
          <>
            <Label color="blue" ribbon className="form-ribbon">
              Mute Range
            </Label>
            <Form.Group grouped>
              <div ref={calendarFormRef}>
                <label>Select a date/time range in which this mute applies.</label>
                <Form.Group width="equals">
                  <Form.Group style={{marginLeft: 10, marginRight: 10}}>
                    <DateTimeInput
                      dateFormat="YYYY-MM-DD"
                      timeFormat="24"
                      name="startDateTime"
                      placeholder="Start date and time"
                      value={start}
                      iconPosition="left"
                      onChange={(object, data) => {
                        setStart(data.value);
                      }}
                      mountNode={formRef}
                      closable={true}
                    />
                  </Form.Group>
                  <Form.Group>
                    <DateTimeInput
                      dateFormat="YYYY-MM-DD"
                      timeFormat="24"
                      name="startEndTime"
                      placeholder="End date and time"
                      value={end}
                      iconPosition="left"
                      onChange={(object, data) => {
                        setEnd(data.value);
                      }}
                      mountNode={formRef}
                      closable={true}
                    />
                  </Form.Group>
                </Form.Group>
              </div>
            </Form.Group>
          </>
        )}
      </Segment>

      <Modal.Actions>
        {isChannelList && <MuteHelp activeIndex={0} />}
        <Button type="reset" onClick={onClose}>
          Cancel
        </Button>
        {editMute && isChannelList && (
          <Button
            type="button"
            inverted
            color="red"
            icon="trash"
            onClick={() => handleDelete(editMute)}
          />
        )}
        <Button type="submit" primary disabled={loading} loading={loading}>
          Submit
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

export default connect(
  state => ({
    loading: state.mutes.loadingCreate || state.mutes.loadingUpdate,
  }),
  dispatch =>
    bindActionCreators(
      {
        createMute: mutesActions.createMute,
        updateMute: mutesActions.updateMute,
        deleteMuteById: mutesActions.deleteMuteById,
        ...showSnackBarActionCreators,
      },
      dispatch
    )
)(AddMute);
