import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const GET_MUTES = 'GET_MUTES';
export const GET_MUTES_SUCCESS = 'GET_MUTES_SUCCESS';
export const GET_MUTES_FAILURE = 'GET_MUTES_FAILURE';
export const GET_MUTES_PAGE_ERROR = 'GET_MUTES_PAGE_ERROR';

export const CREATE_MUTE = 'CREATE_MUTE';
export const CREATE_MUTE_SUCCESS = 'CREATE_MUTE_SUCCESS';
export const CREATE_MUTE_FAILURE = 'CREATE_MUTE_FAILURE';

export const DELETE_MUTE = 'DELETE_MUTE';
export const DELETE_MUTE_SUCCESS = 'DELETE_MUTE_SUCCESS';
export const DELETE_MUTE_FAILURE = 'DELETE_MUTE_FAILURE';

export const UPDATE_MUTE = 'UPDATE_MUTE';
export const UPDATE_MUTE_SUCCESS = 'UPDATE_MUTE_SUCCESS';
export const UPDATE_MUTE_FAILURE = 'UPDATE_MUTE_FAILURE';

export const getMutes = (channelId, pageError) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/mutes/${channelId || ''}`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [GET_MUTES, GET_MUTES_SUCCESS, pageError ? GET_MUTES_PAGE_ERROR : GET_MUTES_FAILURE],
  },
});

export const createMute = mute => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/mutes`,
    method: 'POST',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({mute}),
    types: [CREATE_MUTE, CREATE_MUTE_SUCCESS, CREATE_MUTE_FAILURE],
  },
});

export const deleteMuteById = muteId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/mutes/${muteId}`,
    method: 'DELETE',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [DELETE_MUTE, DELETE_MUTE_SUCCESS, DELETE_MUTE_FAILURE],
  },
});

export const updateMute = (muteId, mute) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/mutes/${muteId}`,
    method: 'PUT',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({mute}),
    types: [UPDATE_MUTE, UPDATE_MUTE_SUCCESS, UPDATE_MUTE_FAILURE],
  },
});

export const createUnSubscribe = (mute, blob, email) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/public/unsubscribe/${blob}/${email}`,
    method: 'POST',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({mute}),
    types: [CREATE_MUTE, CREATE_MUTE_SUCCESS, CREATE_MUTE_FAILURE],
  },
});
