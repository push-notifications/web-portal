import React from 'react';
import {Redirect} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as logoutActionCreators from 'auth/actions/Logout';

const LogoutPage = ({logout}) => {
  logout();
  return <Redirect to="/" />;
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(logoutActionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(LogoutPage);
