import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import qs from 'qs';
import * as authRoutes from 'auth/routes';

class RedirectPage extends Component {
  componentDidMount = () => {
    const queryParams = qs.parse(this.props.urlQuery.slice(1));
    if (queryParams.code) {
      this.props.login(queryParams.code);
    }
  };

  render() {
    const lastURLvisited = localStorage.getItem('lastURLvisited');
    if (this.props.isAuthenticated) {
      return <Redirect to="/" />;
    }
    return <Redirect to={lastURLvisited ? lastURLvisited : authRoutes.loginRoute.path} />;
  }
}

export default RedirectPage;
