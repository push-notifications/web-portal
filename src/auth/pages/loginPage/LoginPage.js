import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';

import {Dimmer, Loader} from 'semantic-ui-react';

import {buildAuthorizeUrl} from 'auth/utils/authUtils';
import './LoginPage.scss';

/**
 * Redirects the user to the Oauth authorization URL
 */
const loginUser = () => {
  const authorizeUrl = buildAuthorizeUrl();
  window.location.href = authorizeUrl;
};

export class LoginPage extends Component {
  render() {
    if (this.props.isAuthenticated) {
      return <Redirect exact to="/" />;
    }

    if (this.props.loginInProgress) {
      return (
        <Dimmer active inverted>
          <Loader content="Loading your profile" />
        </Dimmer>
      );
    }

    loginUser();
    return (
      <Dimmer active inverted>
        <Loader content="Loading your profile" />
      </Dimmer>
    );
  }
}

export default LoginPage;
