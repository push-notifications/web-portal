export const LOGOUT = 'LOGOUT_REQUEST';

export const logout = () => ({
  type: LOGOUT,
});
