import {RSAA} from 'redux-api-middleware';

export const LOGIN = 'LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

export const login = code => {
  return {
    [RSAA]: {
      endpoint: `${process.env.REACT_APP_BASE_URL}/login`,
      method: 'POST',
      body: JSON.stringify({code, service: 'web'}),
      credentials: 'include',
      headers: {'Content-Type': 'application/json'},
      types: [LOGIN, LOGIN_SUCCESS, LOGIN_FAILURE],
    },
  };
};
