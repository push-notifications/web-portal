import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {isAuthenticated} from 'auth/utils/authUtils';
import {bindActionCreators} from 'redux';

import * as loginActionCreators from 'auth/actions/Login';
import RedirectPage from 'auth/pages/redirectPage/RedirectPage';

function mapStateToProps({errors, router}) {
  return {
    errors,
    isAuthenticated: isAuthenticated(),
    urlQuery: router.location.search,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...loginActionCreators,
    },
    dispatch
  );
}

export const RedirectPageContainer = connect(mapStateToProps, mapDispatchToProps)(RedirectPage);

export default withRouter(RedirectPageContainer);
