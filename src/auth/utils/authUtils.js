import jwtDecode from 'jwt-decode';
import BuildUrl from 'build-url';

/**
 * Gets the access token csrf from the cookies
 * @returns {*|{}} The cookie value
 */
export function getAccessToken() {
  return localStorage.getItem('accessToken');
}

export function getDecodedToken() {
  return jwtDecode(getAccessToken());
}

/**
 * Checks if the access token csrf cookie is present (not expired) or not present (expired)
 * @returns {boolean} (true|false)
 */
export function isAccessTokenExpired() {
  if (!getAccessToken()) return true;
  return getDecodedToken().exp < new Date().getTime() / 1000;
}

/**
 * Gets the refresh token csrf from the cookies
 * @returns {*|{}} The cookie value
 */
export function getRefreshToken() {
  return localStorage.getItem('refreshToken');
}

/**
 * Delete access and refresh token from the cookies
 * @returns {boolean} (true|false)
 */
export function deleteSavedTokens() {
  localStorage.removeItem('accessToken');
  localStorage.removeItem('refreshToken');
  return true;
}

/**
 * Checks if the user is authenticated on the application. Both tokens must be present.
 * @returns {boolean} (true|false)
 */
export function isAuthenticated() {
  return getAccessToken() !== null && getRefreshToken() !== null;
}

/**
 * Adds a X-CSRF-TOKEN with the access token attribute to the headers
 * @param headers dict with http headers
 * @returns {function(*): {'X-CSRF-TOKEN': (*|{})}} A function that returns a dict with
 * the new headers
 */
export function withAuth(headers = {}) {
  return state => ({
    ...headers,
    Authorization: `Bearer ${getAccessToken()}`,
  });
}

/**
 * Adds a X-CSRF-TOKEN with the refresh token attribute to the headers
 * @param headers dict with http headers
 * @returns {function(*): {'X-CSRF-TOKEN': (*|{})}} A function that returns a dict with
 * the new headers
 */
export function withRefresh(headers = {}) {
  return state => ({
    ...headers,
    Authorization: `Bearer ${getRefreshToken()}`,
  });
}

export function buildAuthorizeUrl() {
  localStorage.setItem('lastURLvisited', window.location.pathname);
  const config = {
    client_id: process.env.REACT_APP_OAUTH_CLIENT_ID,
    url: process.env.REACT_APP_AUTHORIZATION_URL,
    redirect_url: process.env.REACT_APP_OAUTH_REDIRECT_URL,
    response_type: 'code',
  };
  return BuildUrl(config.url, {
    queryParams: {
      client_id: config.client_id,
      response_type: config.response_type,
      redirect_uri: config.redirect_url,
    },
  });
}

export const isAdmin = roles => roles.includes(process.env.REACT_APP_SUPPORTER_ROLE);
export const isInternal = roles => roles.includes(process.env.REACT_APP_INTERNAL_ROLE);
