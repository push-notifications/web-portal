import {LOGIN, LOGIN_SUCCESS, LOGIN_FAILURE} from 'auth/actions/Login';
import {getDecodedToken, isAuthenticated} from 'auth/utils/authUtils';
import {
  REFRESH_TOKEN,
  REFRESH_TOKEN_SUCCESS,
  REFRESH_TOKEN_FAILURE,
} from 'auth/actions/RefreshToken';
import {LOGOUT} from 'auth/actions/Logout';

const initialState = {
  loggedIn: isAuthenticated(),
  loginInProgress: false,
  error: null,
  roles: isAuthenticated() ? getDecodedToken().cern_roles : [],
};

const checkUnauthorizedAction = action => {
  if (
    typeof action.payload === 'object' &&
    action.payload.toString().includes('401 - Unauthorized')
  ) {
    localStorage.removeItem('accessToken');
  }
};

/**
 * Reducer function for the authentication actions
 *
 * @param state Authentication state
 * @param action
 * @returns {{refresh: boolean, loggedIn: boolean, loginInProgress: boolean, errors: {}}}
 */
export default (state = initialState, action) => {
  checkUnauthorizedAction(action);
  switch (action.type) {
    case LOGIN:
    case REFRESH_TOKEN:
      return {
        ...state,
        loginInProgress: true,
        roles: [],
      };
    case LOGIN_SUCCESS:
    case REFRESH_TOKEN_SUCCESS:
      localStorage.setItem('accessToken', action.payload.accessToken);
      localStorage.setItem('refreshToken', action.payload.refreshToken);

      return {
        ...state,
        loggedIn: isAuthenticated(),
        loginInProgress: false,
        errors: {},
        roles: getDecodedToken().cern_roles,
      };
    case LOGIN_FAILURE:
    case REFRESH_TOKEN_FAILURE:
      localStorage.removeItem('accessToken');
      localStorage.removeItem('refreshToken');
      return {
        ...state,
        loginInProgress: false,
        loggedIn: false,
        errors: action.payload.response || {
          non_field_errors: action.payload.statusText,
        },
        roles: [],
      };
    case LOGOUT:
      localStorage.removeItem('accessToken');
      localStorage.removeItem('refreshToken');
      return {
        ...state,
        loggedIn: false,
        errors: {},
        roles: [],
      };
    default:
      return state;
  }
};
