import React, {useEffect, useState} from 'react';
import {Helmet} from 'react-helmet';
import {Grid, Menu, Container, Header} from 'semantic-ui-react';

import AboutChannels from 'about/components/AboutChannels';
import AboutContact from 'about/components/AboutContact';
import AboutAPI from 'about/components/AboutAPI';
import AboutDevices from 'about/components/AboutDevices';
import AboutPreferences from 'about/components/AboutPreferences';
import AboutMutes from 'about/components/AboutMutes';
import AboutNotifications from 'about/components/AboutNotifications';
import AboutProjectTimeline from 'about/components/AboutProjectTimeline';
import AboutMembers from 'about/components/AboutMembers';
import {Link, useLocation} from 'react-router-dom';
import {connect} from 'react-redux';
import AboutIntegrations from '../components/AboutIntegrations';
import AboutFavorites from 'about/components/AboutFavorites';
import AboutLifecycle from 'about/components/AboutLifecycle';

const TITLE = `${process.env.REACT_APP_NAME} | About`;
const SECTIONS = Object.freeze({
  CHANNELS: '#channels',
  FAVORITES: '#favorites',
  CONTACTS: '#contacts',
  API: '#api',
  DEVICES: '#devices',
  PREFERENCES: '#preferences',
  MUTES: '#mutes',
  NOTIFICATIONS: '#notifications',
  TIMELINE: '#timeline',
  MEMBERS: '#members',
  INTEGRATIONS: '#integrations',
  MIGRATIONS: '#migrations',
  LIFECYCLE: '#lifecycle',
});

const AboutPage = () => {
  const location = useLocation();
  const [activeSection, setActiveSection] = useState(location.hash || SECTIONS.CONTACTS);

  useEffect(() => {
    setActiveSection(location.hash || SECTIONS.CONTACTS);
  }, [location]);

  const handleSectionClick = (e, {to}) => {
    setActiveSection(to);
  };

  return (
    <Container style={{margin: '50px 100px 100px 100px'}}>
      <Helmet>
        <title>{TITLE}</title>
      </Helmet>
      <Header as="h1">CERN Notifications service description</Header>
      <p>Service description and helpful information</p>

      <Grid
        stackable
        style={{
          marginTop: 25,
        }}
      >
        <Grid.Column width={4}>
          <Menu vertical style={{width: 'unset'}}>
            <Menu.Item>
              <Menu.Header>Service Description</Menu.Header>

              <Menu.Menu>
                <Menu.Item
                  as={Link}
                  to={SECTIONS.CONTACTS}
                  name="Contact Info"
                  active={activeSection === SECTIONS.CONTACTS}
                  onClick={handleSectionClick}
                />
                <Menu.Item
                  name="Privacy Notice"
                  href="https://cern.service-now.com/service-portal?id=privacy_policy&se=notifications-service&notice=notifications-service"
                  target="_blank"
                  rel="noopener noreferrer"
                />
              </Menu.Menu>
            </Menu.Item>

            <Menu.Item>
              <Menu.Header>Service Overview</Menu.Header>

              <Menu.Menu>
                <Menu.Item
                  as={Link}
                  name="Channels"
                  to={SECTIONS.CHANNELS}
                  active={activeSection === SECTIONS.CHANNELS}
                  onClick={handleSectionClick}
                />
                <Menu.Item
                  as={Link}
                  name="Favorites"
                  to={SECTIONS.FAVORITES}
                  active={activeSection === SECTIONS.FAVORITES}
                  onClick={handleSectionClick}
                />
                <Menu.Item
                  as={Link}
                  to={SECTIONS.MEMBERS}
                  name="Members"
                  active={activeSection === SECTIONS.MEMBERS}
                  onClick={handleSectionClick}
                />
                <Menu.Item
                  as={Link}
                  to={SECTIONS.DEVICES}
                  name="Devices"
                  active={activeSection === SECTIONS.DEVICES}
                  onClick={handleSectionClick}
                />
                <Menu.Item
                  as={Link}
                  to={SECTIONS.PREFERENCES}
                  name="Preferences"
                  active={activeSection === SECTIONS.PREFERENCES}
                  onClick={handleSectionClick}
                />
                <Menu.Item
                  as={Link}
                  to={SECTIONS.MUTES}
                  name="Unsubscribe and Mutes"
                  active={activeSection === SECTIONS.MUTES}
                  onClick={handleSectionClick}
                />
                <Menu.Item
                  as={Link}
                  to={SECTIONS.NOTIFICATIONS}
                  name="Notifications"
                  active={activeSection === SECTIONS.NOTIFICATIONS}
                  onClick={handleSectionClick}
                />
                <Menu.Item
                  as={Link}
                  to={SECTIONS.INTEGRATIONS}
                  name="Integrations"
                  active={activeSection === SECTIONS.INTEGRATIONS}
                  onClick={handleSectionClick}
                />
                <Menu.Item
                  as={Link}
                  to={SECTIONS.API}
                  name="API"
                  active={activeSection === SECTIONS.API}
                  onClick={handleSectionClick}
                />
                <Menu.Item
                  as={Link}
                  to={SECTIONS.TIMELINE}
                  name="Project Timeline"
                  active={activeSection === SECTIONS.TIMELINE}
                  onClick={handleSectionClick}
                />
                <Menu.Item
                  as={Link}
                  to={SECTIONS.LIFECYCLE}
                  name="Notification Lifecycle"
                  active={activeSection === SECTIONS.LIFECYCLE}
                  onClick={handleSectionClick}
                />
              </Menu.Menu>
            </Menu.Item>
          </Menu>
        </Grid.Column>

        <Grid.Column stretched width={12}>
          {activeSection === SECTIONS.CHANNELS && <AboutChannels />}
          {activeSection === SECTIONS.FAVORITES && <AboutFavorites />}
          {activeSection === SECTIONS.CONTACTS && <AboutContact />}
          {activeSection === SECTIONS.API && <AboutAPI />}
          {activeSection === SECTIONS.DEVICES && <AboutDevices />}
          {activeSection === SECTIONS.PREFERENCES && <AboutPreferences />}
          {activeSection === SECTIONS.MUTES && <AboutMutes />}
          {activeSection === SECTIONS.NOTIFICATIONS && <AboutNotifications />}
          {activeSection === SECTIONS.TIMELINE && <AboutProjectTimeline />}
          {activeSection === SECTIONS.MEMBERS && <AboutMembers />}
          {activeSection === SECTIONS.INTEGRATIONS && <AboutIntegrations />}
          {activeSection === SECTIONS.LIFECYCLE && <AboutLifecycle />}
        </Grid.Column>
      </Grid>
    </Container>
  );
};

export default connect(null, null)(AboutPage);
