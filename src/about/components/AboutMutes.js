import React from 'react';
import {Container, Divider, Header, Image, List} from 'semantic-ui-react';

const AboutMutes = () => {
  return (
    <Container>
      <Header as="h2">Mutes</Header>
      <Divider />
      <p>
        CERN Notifications service allows a flexible and tailored preference system that users can
        configure to define very complex rules to select the best device and method to receive
        notifications. However, users might want to stop receiving completely notifications, either
        globally or for specific channels. Register your global and specific channel Mutes to
        disable notifications temporarily or permanently. Note: Mutes do not affect your
        subscriptions to channels, it simply turns off all notifications.
      </p>
      <List bulleted relaxed>
        <List.Item>
          <List.Header>Global Mute / Do Not Disturb</List.Header>
          <List.Description>
            All notifications will be stopped, for all existing channels, but also any new channels
            the user might be added to.
          </List.Description>
        </List.Item>
        <List.Item>
          <List.Header>Channel Mute</List.Header>
          <List.Description>
            Notifications will be stopped for the specified channel.
          </List.Description>
        </List.Item>
      </List>
      <Divider horizontal style={{margin: '32px 0px'}}>
        <Header as="h3">Mutes section</Header>
      </Divider>
      <p>
        All of the user's Mutes (Global and Channel), can be seen and managed in the Mutes page.
      </p>
      <p>
        <Image
          src={`${process.env.PUBLIC_URL}images/about_images/mutes_page.png`}
          fluid
          rounded
          spaced
          bordered
          centered
        />
      </p>
      <p>
        Mutes can be set from the Channel page or from this page. When adding a new Mute the user is
        presented a window on which the Mute can be configured. Selecting the "Date Range" option
        allows the user to refine the Mute duration using the two presented calendars.
      </p>
      <p>
        <Image
          src={`${process.env.PUBLIC_URL}images/about_images/add_mute.png`}
          rounded
          centered
          size="huge"
        />
      </p>
      <p>
        There is also a "Do Not Disturb" button built right into the main Channels page which
        functions as a shortcut to set a Global mute for all Channels, on all of the user's devices.
        <br />
        When a global mute is active the button appears highlighted in red.
      </p>
      <Image
        src={`${process.env.PUBLIC_URL}images/about_images/do_not_disturb.png`}
        rounded
        bordered
        centered
        size="medium"
      />
      <Divider horizontal style={{margin: '32px 0px'}}>
        <Header as="h3">Unsubscribing</Header>
      </Divider>
      <p>
        When receiving a notification by email, the user can click the "Unsubscribe" link at the
        bottom to manage whether or not to continue receiving further Notification emails.
      </p>
      <p>
        <Image
          src={`${process.env.PUBLIC_URL}images/about_images/email_unsub.png`}
          size="large"
          rounded
          bordered
          centered
        />
      </p>
      <p>
        The link will forward to a landing page allowing the user to unsubscribe. If the user is
        logged in or has an account with the platform it will generate a global permanent mute,
        which the user will see in the mutes page.
      </p>
      <p>
        <Image
          src={`${process.env.PUBLIC_URL}images/about_images/unsub_landing.png`}
          fluid
          rounded
          spaced
          bordered
          centered
        />
      </p>
    </Container>
  );
};

export default AboutMutes;
