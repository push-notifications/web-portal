import React from 'react';
import {Divider, Image, Header, List, Container, Message} from 'semantic-ui-react';

const AboutPreferences = () => {
  return (
    <Container>
      <Header as="h2">Notification preferences</Header>
      <Divider />
      <p>
        CERN Notifications service allows a flexible and tailored preference system that users can
        configure to define very complex rules to select the best device and method to receive
        notifications. The rules defined on the preferences are checked and executed for every
        notification that is sent to you. Rules are applied on a hierarchical order, first channel
        specific preferences and then Global preferences, the first rule that matches is executed
        and the rest rules are discarded.
      </p>
      <p>There are two context levels for defining notification preferences rules:</p>
      <List bulleted relaxed>
        <List.Item>
          <List.Header>Channel Preferences</List.Header>
          <List.Description>
            These are preferences that apply only to a specific channel. All preferences that match
            a notification will be applied.
          </List.Description>
        </List.Item>
        <List.Item>
          <List.Header>Global Preferences</List.Header>
          <List.Description>
            Apply to any channel and are executed unless they are disabled in the Channel
            Preferences page.
          </List.Description>
        </List.Item>
      </List>
      <Divider horizontal>
        <Header as="h4">Preference rules options</Header>
      </Divider>
      <p>The preference rules are composed by the following parts:</p>
      <List bulleted relaxed>
        <List.Item>
          <List.Header>Name</List.Header>
          <List.Description>Descriptive name of the rule</List.Description>
        </List.Item>
        <List.Item>
          <List.Header>Priority</List.Header>
          <List.Description>
            (Low, Normal, Important) multi-option that selects the rule to be executed for any
            notification with any of the selected Notification levels
          </List.Description>
        </List.Item>
        <List.Item>
          <List.Header>Time</List.Header>
          <List.Description>
            Time windows where you want this rule to be active. Out of this time window the rule
            won’t be executed.
          </List.Description>
        </List.Item>
        <List.Item>
          <List.Header>Frequency</List.Header>
          <List.Description>
            <List bulleted relaxed>
              <List.Item>
                <List.Header>Live</List.Header>
                <List.Description>
                  Notification is sent to the selected device as soon as it’s processed by the
                  service. You can choose any of the previously defined devices.
                </List.Description>
                <Message warning compact>
                  Duplicated matching preferences will lead to duplicates being received.
                </Message>
              </List.Item>
              <List.Item>
                <List.Header>Daily / Weekly / Monthly</List.Header>
                <List.Description>
                  An aggregated e-mail format (newsletter style) grouping a summary of your
                  notifications.
                </List.Description>
                <Message info compact>
                  All daily / weekly / monthly preferences are aggregated per time. Duplicates are
                  filtered.
                </Message>
              </List.Item>
            </List>
          </List.Description>
        </List.Item>
      </List>
      <Header as="h4">Example</Header>
      <p>
        This configuration allows any important notification sent to this channel during the whole
        day to be sent directly live to a Firefox browser as a Push notification. Any other
        notification will fallback to the default global preference and thus it will be received by
        email.
      </p>

      <Image
        src={`${process.env.PUBLIC_URL}/images/about_images/add_preference.png`}
        fluid
        rounded
        spaced
        bordered
        centered
      />
    </Container>
  );
};

export default AboutPreferences;
