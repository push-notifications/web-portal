import React from 'react';
import {Divider, List, Container, Header} from 'semantic-ui-react';

const AboutProjectTimeline = () => {
  return (
    <Container>
      <Header as="h2">Project Timeline</Header>
      <Divider />
      <List bulleted relaxed>
        <List.Item>Q1 2021 Prototype opened internally in CDA</List.Item>
        <List.Item>Q2 2021 Pilot service opened to IT</List.Item>
        <List.Item>Q3 2021 Pilot service opened to CERN</List.Item>
        <List.Item>
          Service entered production status and available for CERN-wide usage 2022 Q1.
          Keep track of service updates and new features by following the{' '}
          <a href="https://notifications.web.cern.ch/channels/1f0155ad-d7f9-431e-9b7f-a2128dd2cec2/notifications">
            Notifications Service News channel
          </a>
          .
        </List.Item>
      </List>
    </Container>
  );
};

export default AboutProjectTimeline;
