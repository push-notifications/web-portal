import React from 'react';
import {Divider, Image, List, Container, Header} from 'semantic-ui-react';

const AboutNotifications = () => {
  return (
    <Container>
      <Header as="h2">Notifications</Header>
      <Divider />
      <Header as="h3">Channel configurations</Header>
      <p>
        By default, only administrators of a channel are able to send notifications. Channel members
        are also able to do so if said option is enabled.
      </p>
      <Image
        src={`${process.env.PUBLIC_URL}/images/about_images/notifications_members_option.png`}
        fluid
        bordered
        centered
        rounded
        spaced
        style={{marginBottom: '1rem'}}
      />
      <p>
        Enabling direct notifications to members allows to specify to which users or groups a
        notification can be sent, instead of sending it to all the members of the channel.
      </p>
      <Divider hidden />
      <Header as="h3">Options</Header>
      <Header as="h4">Send notifications options</Header>
      <p>Notifications are formed by the following fields:</p>
      <List bulleted relaxed>
        <List.Item>
          <List.Header>Title</List.Header>
          <List.Description>The title of the notification</List.Description>
        </List.Item>
        <List.Item>
          <List.Header>Content</List.Header>
          <List.Description>Notification content. Content might contain HTML code</List.Description>
        </List.Item>
        <List.Item>
          <List.Header>Priority</List.Header>
          <List.Description>
            In order to categorize the notifications sent through the service, and allow a fine
            grain preference rules. There are different levels for the notifications:
            <List bulleted relaxed>
              <List.Item>Low</List.Item>
              <List.Item>Normal</List.Item>
              <List.Item>Important</List.Item>
              <List.Item>
                [Blocked] Critical
                <br />
                Only allowed for very specific channels after approval. Critical notifications
                bypass all preferences and are direct to all users on all available devices.
                Available after approval, request via{' '}
                <a
                  href="https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&fe=notifications"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  ticket
                </a>
                .
              </List.Item>
            </List>
          </List.Description>
        </List.Item>
      </List>
      <Header as="h4">Advanced options</Header>
      <List>
        <List.Description>
          <List bulleted relaxed>
            <List.Item>
              <List.Header>Target URL</List.Header>
              <List.Description>
                Url the user will be redirected when clicking on the notification
              </List.Description>
            </List.Item>
            <List.Item>
              <List.Header>Image URL</List.Header>
              <List.Description>
                Preview image to be presented in the Push notifications when platform supports it.
              </List.Description>
            </List.Item>
            <List.Item>
              <List.Header>Scheduled Notifications</List.Header>
              <List.Description>
                <p>
                  Users can schedule when the notification should be sent. Under the{' '}
                  <code>Advanced Options</code>, in the <code>Scheduled send</code> section, users
                  can pick the day, and one of the predefined time slots for the notification
                  schedule.
                </p>
                <Image
                  src={`${process.env.PUBLIC_URL}/images/about_images/schedule_section.png`}
                  rounded
                  bordered
                  spaced
                  style={{marginBottom: '1rem'}}
                />
                <br />
                Scheduled notifications are also shown when viewing the list of notifications of a
                channel if the user is:
                <List bulleted relaxed>
                  <List.Item>The author of the notification</List.Item>
                  <List.Item>Channel admin</List.Item>
                </List>
              </List.Description>
            </List.Item>
            <List.Item>
              <List.Header>Direct Notification</List.Header>
              <List.Description>
                If the direct notifications to members is <b>Enabled</b> for the channel, it is
                possible to scope the users from the channel that will receive this notification.
                There are two ways to specify the users that will be notified:
                <br />
                <List bulleted relaxed>
                  <List.Item>Specifying the users by their emails or logins</List.Item>
                  <List.Item>
                    Specifying the grappa groups that contain the users to notify
                  </List.Item>
                </List>
                <br />
                <p>
                  In case of a user belonging to more than one of the added grappa groups and/or is
                  added as channel member to send a direct notification, the user will be notified
                  only once.
                </p>
                There are also two different modes to customize to whom to send the notification:
                <List bulleted relaxed>
                  <List.Item>
                    <b>All specified users or groups</b>: This mode notifies everyone on the grappa
                    groups added and/or member of the channel added to send a direct notification.
                    Specified users or groups that are not part of the channel will be added as
                    channel members.
                  </List.Item>
                  <List.Item>
                    <p>
                      <b>Intersection between specified users or groups and channel members ONLY</b>
                      : This mode will send the notification only to the users that belong to the
                      channel AND that belong to the grappa groups added and/or members of the
                      channel added to send a direct notification. The rest of the users of the
                      grappa groups that are not channel members will not be notified. Furthermore,
                      if a user is added as member of the channel to send a direct notification but
                      the user is not, indeed, member of the channel, the user will not be notified.
                    </p>
                    <p>
                      <Image
                        src={`${process.env.PUBLIC_URL}/images/about_images/venn_diagram.png`}
                        size="medium"
                        bordered
                        rounded
                        centered
                        style={{marginBottom: '1rem'}}
                      />
                    </p>
                  </List.Item>
                </List>
                <br />
                When to use each mode?
                <br />
                <b>Intersection</b> between specified users or groups and channel members mode is
                useful to notify a subset of users from a channel, while
                <b> to all </b> specified users or groups can be used if you want to make sure that
                the notification reaches everyone and you may not be sure if they are on the channel
                yet, or if you want to automate the process of adding users to a channel dynamically
                while sending notifications.
                <br />
              </List.Description>
            </List.Item>
          </List>
        </List.Description>
      </List>
      <Header as="h3">Drafts Information</Header>
      <List bulleted relaxed>
        <List.Item>
          When composing notifications, after a couple of seconds of inactivity, the user's current
          content will be saved.
          <p>
            <Image src="/images/about_images/draft_saved.png" rounded spaced bordered />
          </p>
        </List.Item>
        <List.Item>
          When returning to a notification compose, the user's draft will be loaded.
          <p>
            <Image src="/images/about_images/draft_loaded.png" rounded spaced bordered />
          </p>
        </List.Item>
      </List>
      <p>
        This draft is not channel specific and will be loaded for any of the user's notifications.
        The user can check draft status on the top right corner of the composer.
      </p>
    </Container>
  );
};

export default AboutNotifications;
