import React from 'react';
import {Divider, Image, Container, Header, Button, Icon} from 'semantic-ui-react';

const AboutFavorites = () => {
  return (
    <Container>
      <Header as="h2">Favorites</Header>
      <Divider />
      <p>
        Users can mark or unmark channels as <code>Favorites</code> by toggling the icons{' '}
        <Icon name="star" color="grey" /> and <Icon name="star" color="yellow" />.
      </p>

      <Image
        src={`${process.env.PUBLIC_URL}images/about_images/fav_chan_ex1.png`}
        rounded
        spaced
        fluid
        bordered
        centered
      />
      <Divider hidden />
      <p>
        Clicking the{' '}
        <Button disabled className="button-example">
          Favorites &nbsp; &nbsp; &nbsp;
          <Icon name="star" color="yellow" />
        </Button>
        button will list only the user's favorite channels.
      </p>
      <Image
        src={`${process.env.PUBLIC_URL}images/about_images/fav_chan_ex2.png`}
        rounded
        spaced
        fluid
        bordered
        centered
      />
    </Container>
  );
};

export default AboutFavorites;
