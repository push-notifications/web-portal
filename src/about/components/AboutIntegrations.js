import React from 'react';
import {Divider, Container, Header} from 'semantic-ui-react';

const AboutIntegrations = () => {
  return (
    <Container>
      <Header as="h2">Integrations</Header>
      <Divider />
      <p>
        Integrations allow connecting the Notifications Service to some external services, mainly to
        push content to Notifications Service Channels easily.
      </p>

      <Header as="h3">RSS Feeds to Notifications Service</Header>
      <p>
        A Channel owner can populate Channel content from RSS feeds using the Notifications Service
        API and some simple scripts.
        <br />
        Samples can be found in the{' '}
        <a
          href="https://gitlab.cern.ch/push-notifications/notifications-samples"
          target="_blank"
          rel="noopener noreferrer"
        >
          Notifications Samples Repository
        </a>{' '}
        (<tt>rss-to-notifications</tt>).
      </p>

      <Header as="h3">Other content sources to Notifications Service</Header>
      <p>
        A Channel owner can populate Channel content from any kind of source using the Notifications
        Service API and some simple scripts.
        <br />
        Samples can be found in the{' '}
        <a
          href="https://gitlab.cern.ch/push-notifications/notifications-samples"
          target="_blank"
          rel="noopener noreferrer"
        >
          Notifications Samples Repository.
        </a>
      </p>

      <Header as="h3">More examples</Header>
      <p>
        More on this topic in the{' '}
        <a
          href="https://gitlab.cern.ch/push-notifications/notifications-samples"
          target="_blank"
          rel="noopener noreferrer"
        >
          Notifications Samples Repository.
        </a>
      </p>
    </Container>
  );
};

export default AboutIntegrations;
