import React from 'react';
import {Divider, Container, Header} from 'semantic-ui-react';

const AboutLifecycle = () => {
  return (
    <Container>
      <Header as="h2">Notification Lifecycle</Header>
      <Divider />
      <Header as="h3">Archival</Header>
      <p>
        By default, Notifications are deleted after 13 months from the Notifications Service,
        they're no longer in the Notifications Service storage and are not accessible through the
        Notifications UI or API. Channels owners can turn on the Archive feature, meaning
        notifications are archived to the{' '}
        <a href="https://cern.ch/notifications-archives" target="_blank" rel="noopener noreferrer">
          Notification Archives
        </a>{' '}
        after 13 months, and stay accessible.
      </p>
      <p>
        Be aware that archival can be enabled or disabled but{' '}
        <b>removal from the Notifications Service can't be prevented</b>.
      </p>
      <p>
        Direct notifications, both for users and groups, are never archived for privacy reasons.
        These notifications will be deleted after 13 months from our service.
      </p>
      <Header as="h4">Configuration</Header>
      <p>
        Configuration can be done at the bottom of the <code>Edit Channel</code> section of the
        Manage channel panel.
      </p>
      <Header as="h4">Access</Header>
      <p>Channel archived is accessible from the Channel Notifications page.</p>
    </Container>
  );
};

export default AboutLifecycle;
