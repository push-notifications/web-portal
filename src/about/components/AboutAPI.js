import React from 'react';
import {Divider, Container, Header, Message} from 'semantic-ui-react';

const AboutAPI = () => {
  return (
    <Container>
      <Header as="h2">API-Access</Header>
      <Divider />
      <Header as="h3">API Key (available in Edit Channel)</Header>
      A Channel API Key can be generated to use in the send notification API in the Advanced
      Settings tab of the Channel Management page.
      <br />
      Once generated, don&apos;t forget to enable this permission: &apos;
      <strong>Allow scripts with the API Key to send notifications</strong>&apos; on the Edit
      Channel tab.
      <br />
      If you need to programmatically send notifications, use the API Key in the HTTP header when
      calling the API with <code>POST</code> and a Notification object as below:
      <Message>
        <pre>
          {`POST ${process.env.REACT_APP_BASE_URL}/notifications
Content-Type: application/json
Authorization: Bearer '<api-key>'
`}
          {JSON.stringify(
            {
              target: 'channel id that you can find in your browser url bar',
              summary: 'Notification title',
              priority: 'LOW|NORMAL|IMPORTANT',
              body: 'Notification content',
            },
            null,
            2
          )}
        </pre>
      </Message>
      <br />
      Curl example for a direct notification:
      <Message>
        <pre>{`curl -X POST ${process.env.REACT_APP_BASE_URL}/notifications ${'\\'}
  -H "Content-Type: application/json" ${'\\'}
  -H "Authorization: Bearer <api-key>" ${'\\'}
  --insecure ${'\\'}
  --data '{
        "target": 'channel id that you can find in your browser url bar'
        "summary":"Test direct notification",
        "priority":"NORMAL",
        "body":"<p>This is a test direct notification sent via the API</p>",
        "private": true,
        "targetUsers": [{"email": ""some-member@some.domain"}, ...],
        "targetGroups": [{"groupIdentifier": "some-group"}, ...],
        "intersection": true,
  }' `}</pre>
      </Message>
      Curl example for a simple (not direct) notification:
      <Message>
        <pre>{`curl -X POST ${process.env.REACT_APP_BASE_URL}/notifications ${'\\'}
  -H "Content-Type: application/json" ${'\\'}
  -H "Authorization: Bearer <api-key>" ${'\\'}
  --insecure ${'\\'}
  --data '{
        "target": 'channel id that you can find in your browser url bar'
        "summary":"Test notification",
        "priority":"NORMAL",
        "body":"<p>This is a test notification sent via the API</p>"
  }'
       `}</pre>
      </Message>
      Notes:
      <br />
      <tt>targetUsers</tt>: can contain only user objects with an email address,
      <br />
      <tt>targetGroups</tt>: can contain only group objects with a groupIdentifier containing the
      name,
      <br />
      <tt>intersection</tt>: if <code>true</code>, the notification will be sent only to the
      intersection between specified users or groups and channel members. If <code>false</code>, the
      notification will be sent to all specified users or groups (users or groups will be added as
      channel members if needed).{' '}
      <a href={`${process.env.PUBLIC_URL}/help#notifications`}>
        Read more in the notifications section
      </a>
      .<Header as="h3">Swagger Documentation</Header>
      <p>
        <a href="https://notifications-qa.web.cern.ch/swagger">Swagger UI</a> is available on the QA
        environment. (Intranet only).
        <br />
        Users should subscribe to the <b>notifications-qa-users</b> e-group to have access to
        receive notifications in QA.
      </p>
    </Container>
  );
};

export default AboutAPI;
