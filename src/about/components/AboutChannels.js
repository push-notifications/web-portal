import React from 'react';
import {Divider, Image, Header, Container, Icon, Message, List} from 'semantic-ui-react';

const AboutChannels = () => {
  return (
    <Container>
      <Header as="h2">Channels</Header>
      <Divider />
      <p>
        This is the basis of the service, all the notifications are organized in channels that can
        have a number of members. All the members of a channel will receive the notification that
        was sent through the channel.
      </p>
      <p>
        <Image src="/images/about_images/channel_page.png" fluid rounded spaced bordered centered />
      </p>
      <p>There are different levels of visibility for channels:</p>
      <Header as="h5">
        <Icon color="grey" name="world" />
        Public
      </Header>
      Channels are listed for both anonymous and authenticated users. Notifications sent on{' '}
      <code>public</code> channels are also accessible to anyone. Authenticated users can subscribe
      to the channel to receive notifications on their preference devices.
      <Header as="h5">
        <Image size="tiny" centered src="/images/cern-logo-grey.svg" />
        CERN Internal
      </Header>
      Not visible for anonymous users and thus only listed to <strong>any</strong> authenticated
      user. <code>CERN Internal</code> channels allow both modes, self-subscription and/or members
      management. Notifications sent using this type of channel are only visible by channel members.
      <Header as="h5">
        <Icon color="grey" name="lock" />
        Restricted
      </Header>
      The most private and restricted of the channel types. <code>Restricted</code> channels are not
      listed in the channel list by default and they are only visible to members, thus, they don’t
      allow a self-subscription model and membership of the channel must be defined with the{' '}
      <code>members management</code> options described below. Like <code>CERN Internal</code>
      channels notifications are only sent to members.
      <Header as="h4">Channel Type</Header>
      <List bulleted relaxed>
        <List.Item>
          <List.Header>Official</List.Header>
          <List.Description>
            Official channels will be kept after their owner's departure, reassigned to the
            supervisor.
          </List.Description>
        </List.Item>
        <List.Item>
          <List.Header>Personal</List.Header>
          <List.Description>
            Personal channels will be deleted after their owner's departure.
          </List.Description>
        </List.Item>
      </List>
      <Header as="h4">Categories and Tags</Header>
      <List bulleted relaxed>
        <List.Item>
          <List.Header>Categories</List.Header>
          <List.Description>
            Categories are limited to one per channel, and unlike Tags, these cannot be created by
            the users. Categories will need to be requested and approved, and will provide means of
            aggregation for different services/topics. To request a new category submit a{' '}
            <a href="https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=notifications-service">
              request via ticket
            </a>
            .
            <br />
            Notification emails from a channel with a Category will display its category name title.
            <br />
            More features TBD.
          </List.Description>
        </List.Item>
        <Image
          src="/images/about_images/category_email.png"
          size="large"
          rounded
          bordered
          centered
        />
        <List.Item>
          <List.Header>Tags</List.Header>
          <List.Description>
            Tags are used to help classify and filter channels. Tags are not the same as Categories,
            a user can add many Tags to one Channel. Tags will also be useful for personalized
            notifications, channel discovery and suggestion, among other features.
            <br />
            When adding Tags to a Channel, the user can pick from a list of existing Tags, or create
            a new more appropriate one.
            <br />A channel's Tags can be checked on the channel list by hovering the{' '}
            <Icon name="tags" style={{color: '#ccc'}} /> symbol.
          </List.Description>
        </List.Item>
      </List>
      <p>
        <Image
          src="/images/about_images/cats_and_tags.png"
          fluid
          rounded
          spaced
          bordered
          centered
        />
        <Image
          src="/images/about_images/listed_channel_tags_cat.png"
          size="large"
          rounded
          bordered
          centered
        />
      </p>
      <Divider horizontal style={{marginTop: 32}}>
        <Header as="h3">Advanced Configurations</Header>
      </Divider>
      <Header as="h4">Archive</Header>
      <p>
        Channels can be configured to archive regularly any notification into a more long
        preservation solution. This option can be configured in the <code>Edit Channel</code>{' '}
        section of the <code>Manage</code> channel panel. More information about{' '}
        <code>Archive</code> can be consulted{' '}
        <a href="/help#lifecycle" target="_blank">
          here
        </a>
        .
      </p>
      <Header as="h4">Email-Gateway</Header>
      <p>
        An Email Gateway component is available to accept emails as input for channel notifications.
        In order to activate this feature you need to go to the <code>Edit Channel</code> section of
        the <code>Manage</code> channel panel as admin of a channel.
      </p>
      <p>
        To enable it, you need to input at least one e-mail address from where you will be sending
        notifications to this channel. Only defined email addresses will be allowed to do so.
      </p>
      <p>
        In addition the following e-mail address syntax should be used when sending the notification
      </p>
      <Message compact>
        <code>
          {process.env.REACT_APP_EMAIL_GATEWAY_USER}+<strong>&lt;channel-name-slug&gt;</strong>+
          <strong>&lt;level&gt;</strong>
          @dovecotmta.cern.ch
        </code>
      </Message>
      <p>Example, to send an normal notification to channel it-dep-cda-wf send the email to:</p>
      <code>
        {process.env.REACT_APP_EMAIL_GATEWAY_USER}+it-dep-cda-wf+normal@dovecotmta.cern.ch
      </code>
      <Header as="h4">Migrating mailing lists</Header>
      <p>
        Legacy e-groups are also supported, allowing to add a channel email address as member of an
        egroup. You'll need to give permission to that e-group via the Edit Channel Section.
      </p>
      <p>
        Read more on this topic in the{' '}
        <a href={`${process.env.PUBLIC_URL}/help#integrations`}>Integrations</a> section.
      </p>
    </Container>
  );
};

export default AboutChannels;
