import React from 'react';
import {Divider, Header, Icon, List, Accordion, Image, Container} from 'semantic-ui-react';

const AboutDevices = () => {
  const level1Panels = [
    {
      key: 'panel-1a',
      title: 'MacOS',
      content: {
        content: (
          <>
            <Header as="h5">Browser support</Header>
            <List horizontal>
              <List.Item>
                <Icon name="safari" />
                Safari
              </List.Item>
              <List.Item>
                <Icon name="chrome" />
                Chrome
              </List.Item>
              <List.Item>
                <Icon name="firefox" />
                Firefox
              </List.Item>
            </List>

            <Header as="h5">System Settings</Header>
            <List bulleted>
              <List.Item>
                Make sure notifications are enabled and authorized for the selected browser in{' '}
                <i>
                  <Icon name="apple" />
                  &#8594; System Preferences &#8594; Notifications
                </i>
                .
              </List.Item>
            </List>

            <Header as="h5">Browser Settings</Header>
            <List bulleted>
              <List.Item>
                <List.Header>
                  <Icon name="safari" />
                  Safari:
                </List.Header>
                <List.List>
                  <List.Item>
                    In <i>Preferences &#8594; Websites tab &#8594; Notifications</i>: Tick the box{' '}
                    <i>Allow websites to ask for permission to send notifications</i>
                  </List.Item>
                  <List.Item>
                    Make sure the CERN Notifications site is not in the block list.
                  </List.Item>
                </List.List>
              </List.Item>
              <List.Item>
                <List.Header>
                  <Icon name="chrome" />
                  Chrome:
                </List.Header>
                <List.List>
                  <List.Item>
                    In{' '}
                    <i>
                      Settings &#8594; Privacy and security tab &#8594; Site Settings &#8594;
                      Notifications &#8594; Authorizations
                    </i>
                    : Enable <i>Sites can ask to send notifications</i>.
                  </List.Item>
                  <List.Item>
                    Make sure the CERN Notifications site is not in the block list.
                  </List.Item>
                </List.List>
              </List.Item>
              <List.Item>
                <List.Header>
                  <Icon name="firefox" />
                  Firefox:
                </List.Header>
                <List.List>
                  <List.Item>
                    <i>Options &#8594; Notifications parameters</i>: Ensure the box{' '}
                    <i>Block new requests..</i> is not ticked.
                  </List.Item>
                  <List.Item>
                    Make sure the CERN Notifications site is not in the block list in
                    <i>
                      Preferences &#8594; Privacy & security tab &#8594; Permissions &#8594;
                      Notifications
                    </i>
                  </List.Item>
                </List.List>
              </List.Item>
            </List>
          </>
        ),
      },
    },
    {
      key: 'panel-1b',
      title: 'Windows',
      content: {
        content: (
          <>
            <Header as="h5">Browser support</Header>
            <List horizontal>
              <List.Item>
                <Icon name="chrome" />
                Chrome
              </List.Item>
              <List.Item>
                <Icon name="firefox" />
                Firefox
              </List.Item>
              <List.Item>
                <Icon name="edge" />
                Edge (with no guarantees, currently upgrading to a complete new version)
              </List.Item>
            </List>

            <Header as="h5">System Settings</Header>
            <List bulleted>
              <List.Item>
                Make sure notifications are enabled and authorized for the selected browser in{' '}
                <i>
                  <Icon name="windows" />
                  &#8594; Settings &#8594; Notifications & Actions
                </i>
                .
              </List.Item>
            </List>

            <Header as="h5">Browser Settings</Header>
            <List bulleted>
              <List.Item>
                <List.Header>
                  <Icon name="chrome" />
                  Chrome:
                </List.Header>
                <List.List>
                  <List.Item>
                    In{' '}
                    <i>
                      Settings &#8594; Privacy and security tab &#8594; Site Settings &#8594;
                      Notifications &#8594; Authorizations
                    </i>
                    : Enable <i>Sites can ask to send notifications</i>.
                  </List.Item>
                  <List.Item>
                    Make sure the CERN Notifications site is not in the block list.
                  </List.Item>
                </List.List>
              </List.Item>
              <List.Item>
                <List.Header>
                  <Icon name="firefox" />
                  Firefox:
                </List.Header>
                <List.List>
                  <List.Item>
                    <i>Options &#8594; Notifications parameters</i>: Ensure the box{' '}
                    <i>Block new requests..</i> is not ticked.
                  </List.Item>
                  <List.Item>
                    Make sure the CERN Notifications site is not in the block list in
                    <i>
                      Preferences &#8594; Privacy & security tab &#8594; Permissions &#8594;
                      Notifications
                    </i>
                  </List.Item>
                  <List.Item>
                    <i>Options &#8594; Privacy & security tab</i>: Ensure the box{' '}
                    <i>Delete cookies and site data when Firefox is closed</i> is not ticked.
                  </List.Item>
                </List.List>
              </List.Item>
            </List>
          </>
        ),
      },
    },
    {
      key: 'panel-1c',
      title: 'Linux',
      content: {
        content: (
          <>
            <Header as="h5">Browser support</Header>
            <List horizontal>
              <List.Item>
                <Icon name="chrome" />
                Chrome
              </List.Item>
              <List.Item>
                <Icon name="firefox" />
                Firefox
              </List.Item>
            </List>

            <Header as="h5">Browser Settings</Header>
            <List bulleted>
              <List.Item>
                <List.Header>
                  <Icon name="chrome" />
                  Chrome:
                </List.Header>
                <List.List>
                  <List.Item>
                    In{' '}
                    <i>
                      Settings &#8594; Privacy and security tab &#8594; Site Settings &#8594;
                      Notifications &#8594; Authorizations
                    </i>
                    : Enable <i>Sites can ask to send notifications</i>.
                  </List.Item>
                  <List.Item>
                    Make sure the CERN Notifications site is not in the block list.
                  </List.Item>
                </List.List>
              </List.Item>
              <List.Item>
                <List.Header>
                  <Icon name="firefox" />
                  Firefox:
                </List.Header>
                <List.List>
                  <List.Item>
                    <i>Options &#8594; Notifications parameters</i>: Ensure the box{' '}
                    <i>Block new requests..</i> is not ticked.
                  </List.Item>
                  <List.Item>
                    Make sure the CERN Notifications site is not in the block list in
                    <i>
                      Preferences &#8594; Privacy & security tab &#8594; Permissions &#8594;
                      Notifications
                    </i>
                  </List.Item>
                  <List.Item>
                    <i>Options &#8594; Privacy & security tab</i>: Ensure the box{' '}
                    <i>Delete cookies and site data when Firefox is closed</i> is not ticked.
                  </List.Item>
                </List.List>
              </List.Item>
            </List>
          </>
        ),
      },
    },
    {
      key: 'panel-1d',
      title: 'Android',
      content: {
        content: (
          <>
            <Header as="h5">Browser support</Header>
            <List horizontal>
              <List.Item>
                <Icon name="chrome" />
                Chrome
              </List.Item>
              <List.Item>
                <Icon name="firefox" />
                Firefox
              </List.Item>
            </List>
            <Header as="h5">System Settings</Header>
            <List bulleted>
              <List.Item>
                <i>
                  <Icon name="android" />
                </i>
                &#8594; Settings &#8594; Apps & Notifications
                <List.List>
                  <List.Item>
                    Make sure notifications are enabled and authorized for <i>Chrome</i> or{' '}
                    <i>Firefox</i>
                  </List.Item>
                  <List.Item>
                    Select <i>notifications.web.cern.ch</i> properties and make sure you have
                    selected <i>Important</i>, <i>Display on screen</i> and the other settings
                    according to your preferences.
                  </List.Item>
                </List.List>
              </List.Item>
            </List>
          </>
        ),
      },
    },
    {
      key: 'panel-1e',
      title: 'iOS',
      content: {
        content: (
          <>
            <Header as="h5">Support</Header>
            <List bulleted>
              <List.Item>
                Unfortunately, Apple does not support Push Notifications on iOS (iPhones) browsers,
                even on third party browsers like Chrome.
              </List.Item>
            </List>
          </>
        ),
      },
    },
  ];

  return (
    <Container>
      <Header as="h2">Device management</Header>
      <Divider />
      <p>
        Under device management users can control the different devices that can be used on their
        <code>Notification preferences</code>. Currently supported devices include:
      </p>
      <Header as="h4">
        Email
        <Header.Subheader>Default Device</Header.Subheader>
      </Header>
      <p>
        This is pre-created to any user using the platform and is the only default pre-defined
        delivery method based on e-mails to the user’s primary email address. Additional
        <code> Email</code> devices can be configured afterwards.
      </p>
      <Header as="h4">
        App
        <Header.Subheader>Default Device</Header.Subheader>
      </Header>
      <p>
        This device represents possible user Applications. Right now only one App - Mattermost is
        supported. This device is created for each user by default, and can be used for delivery for
        any type of notification.
      </p>
      <Header as="h4">
        Phone
        <Header.Subheader>Default Device</Header.Subheader>
      </Header>
      <p>
        Like Email, this is pre-created to any user using the platform. CERN Notifications uses the
        phone number registered on CERN Phone. The Phone device will be updated according to the
        users AD entry upon login. Delivery to this device is limited, for the time being its use
        will be limited to delivery of{' '}
        <a href="/help#notifications" target="_blank">
          Critical notifications
        </a>
        . Other factors also limit delivery through this device, such as length of the message,
        which is 140 charaters at most.
      </p>
      <Header as="h4"> Browser</Header>
      <p>
        Wou can register your device’s browser (computer or mobile) to receive notifications. The
        notifications will be presented as a Browser Notifications pop-up window depending of the OS
        platform.
      </p>
      <Header as="h4">
        App
        <Header.Subheader>In development</Header.Subheader>
      </Header>
      <p>
        Will allow registration of native applications displaying notifications on a variety of
        devices and operating systems. Feature not yet available.
      </p>
      <p>
        <Image
          src={`${process.env.PUBLIC_URL}images/about_images/devices_list.png`}
          rounded
          spaced
          fluid
          bordered
          centered
        />
      </p>
      <Divider horizontal style={{marginTop: 32}}>
        <Header as="h3">Adding devices</Header>
      </Divider>
      <Header as="h5">Mail device</Header>
      <List bulleted>
        <List.Item>
          By default, you have a MAIL device, targeting your email address, and used in default
          preferences.
        </List.Item>
      </List>
      <Header as="h5">Adding a new browser for notifications</Header>
      <List bulleted>
        <List.Item>
          Add browsers on different devices for different preferences: e.g. Send important
          notifications to your mobile.
        </List.Item>
        <List.Item>
          To enable the browser you currently use, just click the <i>Add Device</i> button then
          follow the steps.
        </List.Item>
        <List.Item>
          Once added, you can test notifications by clicking the <i>Test me</i> button. If nothing
          happens, follow the help below depending on your browser and operating system.
        </List.Item>
      </List>
      <Accordion panels={level1Panels} fluid styled />
    </Container>
  );
};

export default AboutDevices;
