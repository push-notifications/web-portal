import React from 'react';
import {Divider, Image, Container, Header, Button} from 'semantic-ui-react';
import './AboutMembers.scss';

const AboutMembers = () => {
  return (
    <Container>
      <Header as="h2">Members managements/subscriptions</Header>
      <Divider />
      <p>
        As described before there are two modes of joining a channel and be able to receive
        notifications sent to this particular channel.
      </p>

      <Header as="h4">Self-subscription</Header>
      <p>
        {' '}
        Valid for <code>public</code> and <code>CERN Internal</code> channel types. Authenticated
        users can opt-in for channel subscription by selecting their prefered channels in the list
        and click the <Button icon="bell" size="mini" disabled className="button-example" />
        icon. Unsubscription of channels is also possible.
      </p>

      <Image
        src={`${process.env.PUBLIC_URL}images/about_images/channels_example.png`}
        rounded
        spaced
        fluid
        bordered
        centered
      />
      <Header as="h4">Members management panel</Header>

      <p>
        Under <Button icon="settings" size="mini" disabled className="button-example" />
        <code>Manage</code> option of the channel. Only accessible to Channel owners or
        administrators. <code>Members management panel</code> allows to define the membership of the
        channel based on both individual user accounts or any existing group on the{' '}
        <a href="https://groups-portal.web.cern.ch/" target="_blank" rel="noopener noreferrer">
          Grappa service
        </a>
        .
      </p>
    </Container>
  );
};

export default AboutMembers;
