import React from 'react';
import {Divider, Container, Header} from 'semantic-ui-react';

const AboutContact = () => {
  return (
    <Container>
      <Header as="h2">Context and contact info</Header>
      <Divider />
      <p>
        CERN Notifications service is provided by the IT department as a new communication mechanism
        within the organization. It can provide an alternative option to mailing list, simple
        newsletters, monitoring alerts, …
      </p>
      <p>Report any issues or questions at our Mattermost channel or Service Now:</p>
      <a
        href="https://mattermost.web.cern.ch/it-dep/channels/notifications"
        target="_blank"
        rel="noopener noreferrer"
      >
        https://mattermost.web.cern.ch/it-dep/channels/notifications
      </a>
      <br />
      <br />
      <a
        href="https://cern.service-now.com/service-portal?id=functional_element&name=notifications"
        target="_blank"
        rel="noopener noreferrer"
      >
        Service Now
      </a>
    </Container>
  );
};

export default AboutContact;
