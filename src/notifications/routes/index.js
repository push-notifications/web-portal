import NotificationsPage from 'notifications/pages/NotificationsPage/NotificationsPage';

const notificationsRoute = {
  id: 'notifications',
  path: '/notifications',
  icon: 'inbox',
  text: 'Notifications',
  component: NotificationsPage,
};

export default notificationsRoute;
