import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils.js';
import qs from 'qs';

// Get notifications
export const GET_NOTIFICATIONS = 'GET_NOTIFICATIONS';
export const GET_NOTIFICATIONS_SUCCESS = 'GET_NOTIFICATIONS_SUCCESS';
export const GET_NOTIFICATIONS_FAILURE = 'GET_NOTIFICATIONS_FAILURE';

export const SET_GET_NOTIFICATIONS_QUERY = 'SET_GET_NOTIFICATIONS_QUERY';

export const getNotifications = (channelId, query) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}/notifications?${qs.stringify(
      query
    )}`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [
      {
        type: GET_NOTIFICATIONS,
        payload: (action, state) => channelId,
      },
      GET_NOTIFICATIONS_SUCCESS,
      GET_NOTIFICATIONS_FAILURE,
    ],
  },
});

export const setGetNotificationsQuery = query => {
  return {
    type: SET_GET_NOTIFICATIONS_QUERY,
    payload: query,
  };
};
