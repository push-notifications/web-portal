import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

// Get notifications target group
export const GET_TARGET_GROUPS = 'GET_TARGET_GROUPS';
export const GET_TARGET_GROUPS_SUCCESS = 'GET_TARGET_GROUPS_SUCCESS';
export const GET_TARGET_GROUPS_FAILURE = 'GET_TARGET_GROUPS_FAILURE';

export const getNotificationTargetGroups = (notificationId, query) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/notifications/${notificationId}/targets/groups`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [GET_TARGET_GROUPS, GET_TARGET_GROUPS_SUCCESS, GET_TARGET_GROUPS_FAILURE],
  },
});
