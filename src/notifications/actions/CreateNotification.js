import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils.js';
// Create new notification
export const CREATE_NOTIFICATION = 'CREATE_NOTIFICATION';
export const CREATE_NOTIFICATION_SUCCESS = 'CREATE_NOTIFICATION_SUCCESS';
export const CREATE_NOTIFICATION_FAILURE = 'CREATE_NOTIFICATION_FAILURE';

const SOURCE_TYPE = 'WEB';

export const createNotification = notification => {
  return {
    [RSAA]: {
      endpoint: `${process.env.REACT_APP_BASE_URL}/notifications`,
      method: 'POST',
      body: JSON.stringify({...notification, source: SOURCE_TYPE}),
      credentials: 'include',
      headers: withAuth({'Content-Type': 'application/json'}),
      types: [CREATE_NOTIFICATION, CREATE_NOTIFICATION_SUCCESS, CREATE_NOTIFICATION_FAILURE],
    },
  };
};
