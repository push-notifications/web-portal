import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

// Get notifications target users
export const GET_TARGET_USERS = 'GET_TARGET_USERS';
export const GET_TARGET_USERS_SUCCESS = 'GET_TARGET_USERS_SUCCESS';
export const GET_TARGET_USERS_FAILURE = 'GET_TARGET_USERS_FAILURE';

export const getNotificationTargetUsers = (notificationId, query) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/notifications/${notificationId}/targets/users`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [GET_TARGET_USERS, GET_TARGET_USERS_SUCCESS, GET_TARGET_USERS_FAILURE],
  },
});
