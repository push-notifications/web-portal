import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils.js';
import qs from 'qs';

// Get notifications
export const GET_NOTIFICATIONS_ADMIN = 'GET_NOTIFICATIONS_ADMIN';
export const GET_NOTIFICATIONS_ADMIN_SUCCESS = 'GET_NOTIFICATIONS_ADMIN_SUCCESS';
export const GET_NOTIFICATIONS_ADMIN_FAILURE = 'GET_NOTIFICATIONS_ADMIN_FAILURE';

export const SET_GET_NOTIFICATIONS_QUERY_ADMIN = 'SET_GET_NOTIFICATIONS_QUERY_ADMIN';

export const getNotifications = (channelId, query) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/notifications/${channelId}?${qs.stringify(query)}`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [
      {
        type: GET_NOTIFICATIONS_ADMIN,
        payload: (action, state) => channelId,
      },
      GET_NOTIFICATIONS_ADMIN_SUCCESS,
      GET_NOTIFICATIONS_ADMIN_FAILURE,
    ],
  },
});

export const setGetNotificationsQuery = query => {
  return {
    type: SET_GET_NOTIFICATIONS_QUERY_ADMIN,
    payload: query,
  };
};
