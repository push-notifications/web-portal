import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils.js';

// Get Draft
export const GET_DRAFT = 'GET_DRAFT';
export const GET_DRAFT_SUCCESS = 'GET_DRAFT_SUCCESS';
export const GET_DRAFT_FAILURE = 'GET_DRAFT_FAILURE';
// Save Draft
export const SAVE_DRAFT = 'SAVE_DRAFT';
export const SAVE_DRAFT_SUCCESS = 'SAVE_DRAFT_SUCCESS';
export const SAVE_DRAFT_FAILURE = 'SAVE_DRAFT_FAILURE';
// Delete Draft
export const DELETE_DRAFT = 'DELETE_DRAFT';
export const DELETE_DRAFT_SUCCESS = 'DELETE_DRAFT_SUCCESS';
export const DELETE_DRAFT_FAILURE = 'DELETE_DRAFT_FAILURE';

export const getDraft = () => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/usersettings/draft`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [GET_DRAFT, GET_DRAFT_SUCCESS, GET_DRAFT_FAILURE],
  },
});

export const saveDraft = draft => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/usersettings/draft`,
    method: 'POST',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({draft}),
    types: [SAVE_DRAFT, SAVE_DRAFT_SUCCESS, SAVE_DRAFT_FAILURE],
  },
});

export const deleteDraft = () => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/usersettings/draft`,
    method: 'DELETE',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [DELETE_DRAFT, DELETE_DRAFT_SUCCESS, DELETE_DRAFT_FAILURE],
  },
});
