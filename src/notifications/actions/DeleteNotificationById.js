import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils.js';

// Delete notification
export const DELETE_NOTIFICATION = 'DELETE_NOTIFICATION';
export const DELETE_NOTIFICATION_SUCCESS = 'DELETE_NOTIFICATION_SUCCESS';
export const DELETE_NOTIFICATION_FAILURE = 'DELETE_NOTIFICATION_FAILURE';

export const deleteNotificationById = notificationId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/notifications/${notificationId}`,
    method: 'DELETE',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [DELETE_NOTIFICATION, DELETE_NOTIFICATION_SUCCESS, DELETE_NOTIFICATION_FAILURE],
  },
});
