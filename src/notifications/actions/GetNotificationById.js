import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils.js';

// Get notifications
export const GET_NOTIFICATION_BY_ID = 'GET_NOTIFICATION_BY_ID';
export const GET_NOTIFICATION_BY_ID_SUCCESS = 'GET_NOTIFICATION_BY_ID_SUCCESS';
export const GET_NOTIFICATION_BY_ID_FAILURE = 'GET_NOTIFICATION_BY_ID_FAILURE';

export const SET_NOTIFICATION = 'SET_NOTIFICATION';

export const getNotificationById = (notificationId, visibility) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/notifications/${notificationId}`,
    method: 'GET',
    credentials: 'include',
    headers:
      visibility === 'PUBLIC'
        ? {'Content-Type': 'application/json'}
        : withAuth({'Content-Type': 'application/json'}),
    types: [GET_NOTIFICATION_BY_ID, GET_NOTIFICATION_BY_ID_SUCCESS, GET_NOTIFICATION_BY_ID_FAILURE],
  },
});

export const setNotification = () => {
  return {
    type: SET_NOTIFICATION,
  };
};
