export const OPEN_CREATE_NOTIFICATION_FORM = 'OPEN_CREATE_NOTIFICATION_FORM';
export const CLOSE_CREATE_NOTIFICATION_FORM = 'CLOSE_CREATE_NOTIFICATION_FORM';

export const openCreateNotificationsForm = function () {
  return {
    type: 'OPEN_CREATE_NOTIFICATION_FORM',
  };
};

export const closeCreateNotificationForm = function () {
  return {
    type: 'CLOSE_CREATE_NOTIFICATION_FORM',
  };
};
