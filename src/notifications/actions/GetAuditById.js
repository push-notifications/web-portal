import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

// Get audit by ID
export const GET_AUDIT_BY_ID = 'GET_AUDIT_BY_ID';
export const GET_AUDIT_BY_ID_SUCCESS = 'GET_AUDIT_BY_ID_SUCCESS';
export const GET_AUDIT_BY_ID_FAILURE = 'GET_AUDIT_BY_ID_FAILURE';

export const getAuditById = id => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/notifications/${id}/audit`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [GET_AUDIT_BY_ID, GET_AUDIT_BY_ID_SUCCESS, GET_AUDIT_BY_ID_FAILURE],
  },
});
