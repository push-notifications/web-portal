import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {useParams, useLocation, useHistory} from 'react-router-dom';
import {
  Divider,
  Header,
  Icon,
  Menu,
  Segment,
  Label,
  Popup,
  Container,
  Dropdown,
  Button,
} from 'semantic-ui-react';
import {Helmet} from 'react-helmet';
import Linkify from 'linkify-react';

import * as preferencesActions from 'preferences/actions/preferences';
import PreferencesList from 'preferences/components/PreferencesList';
import NotificationsList from 'notifications/components/NotificationsList/NotificationsList';
import SearchNotificationComponent from 'notifications/components/SearchNotificationComponent/SearchNotificationComponent';
import SendNotificationComponent from 'notifications/components/SendNotificationComponent/SendNotificationComponent';
import {getChannelById} from 'channels/actions/GetChannelById';
import {getPublicChannelById} from 'channels/actions/GetPublicChannelById';
import ErrorComponent from 'utils/error-component';
import './NotificationsPage.scss';
import {channelFlagTypes} from 'common/types/ChannelFlagTypes';
import * as noContentActionCreators from 'channels/actions/NoContentActionsUpdate';
import NoPreferencesWarningBanner from 'preferences/components/NoPreferencesWarningBanner/NoPreferencesWarningBanner';
import * as subscribeToChannelActionCreators from '../../../channels/actions/SubscribeToChannel';
import * as unsubscribeFromChannelActionCreators from '../../../channels/actions/UnsubscribeFromChannel';
import {showSnackbar} from '../../../common/actions/Snackbar';

const NotificationsPage = ({
  isAuthenticated,
  preferences,
  getPreferences,
  loadingPreferences,
  loadingNotifications,
  channel,
  getChannel,
  getPublicChannel,
  error,
  subscribeToChannel,
  unsubscribeFromChannel,
  loadingSubscribe,
  loadingUnsubscribe,
  loadingChannel,
  updateSubscribeToChannel,
}) => {
  const {channelId} = useParams();
  const location = useLocation();
  const [activeItem, setActiveItem] = useState('Notifications');
  const history = useHistory();

  const isEveryGlobalPreferenceDisabled = () => {
    const isChannelInDisabledChannels = preference =>
      preference.disabledChannels.some(c => c.id === channelId);

    return preferences?.global.every(isChannelInDisabledChannels);
  };

  const handleReport = reportData => {
    window.open(
      // eslint-disable-next-line max-len
      `https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&fe=notifications&short_description=${reportData.text}%20report%20for%20channel%20"${channel.name}"%20(Channel%20ID:%20${channel.id})`
    );
  };

  if (location.section) {
    setActiveItem(location.section);
    location.section = null;
  }

  const handleItemClick = (e, {name}) => setActiveItem(name);
  const TITLE = `${process.env.REACT_APP_NAME} |`;

  useEffect(() => {
    if (isAuthenticated) getChannel(channelId);
    else getPublicChannel(channelId);
  }, [isAuthenticated, channelId, getChannel, getPublicChannel]);

  useEffect(() => {
    if (isAuthenticated) getPreferences(channelId);
  }, [isAuthenticated, getPreferences, channelId]);

  const channelCategory = () => {
    return channel.category && <Label color="grey" size="large" content={channel.category.name} />;
  };

  const channelTags = () => {
    return (
      channel.tags &&
      channel.tags.length > 0 && (
        <Label.Group tag style={{display: 'inline-block'}}>
          {channel.tags.map(data => (
            <Label key={data.id} content={data.name} />
          ))}
        </Label.Group>
      )
    );
  };

  const channelCanSendPrivate = () => {
    return (
      isAuthenticated &&
      channel.sendPrivate && (
        <Popup
          style={{marginLeft: 5}}
          trigger={<Label className="label--info" content="Direct notifications" icon="inbox" />}
          content="This channel can send you direct notifications."
        />
      )
    );
  };

  const channelIsMandatory = () => {
    return (
      isAuthenticated &&
      channel.channelFlags &&
      channel.channelFlags.includes(channelFlagTypes.MANDATORY) && (
        <Popup
          style={{marginLeft: 5}}
          trigger={<Label className="label--info" content="Mandatory" icon="shield" />}
          content="This channel is mandatory. Unsubscribe is not possible, it can however be muted if needed."
        />
      )
    );
  };

  const channelIsCritical = () => {
    return (
      isAuthenticated &&
      channel.channelFlags &&
      channel.channelFlags.includes(channelFlagTypes.CRITICAL) && (
        <Popup
          style={{marginLeft: 5}}
          trigger={<Label className="label--info" content="Critical" icon="exclamation" />}
          content="This channel can send critical (emergency) notifications on all your devices."
        />
      )
    );
  };

  const hasNoValidPreferences = () => {
    return (
      preferences.channel.length === 0 &&
      (preferences.global.length === 0 || isEveryGlobalPreferenceDisabled())
    );
  };

  const channelHeader = () => {
    return (
      <Header.Content>
        {channel.name} {channelCategory()} {channelTags()}
      </Header.Content>
    );
  };

  const headerIcon = () => {
    switch (activeItem) {
      case 'Notifications':
        return 'envelope';
      case 'Send notification':
        return 'send';
      case 'Preferences':
        return 'setting';
      default:
        return '';
    }
  };

  const reportChannel = () => {
    return (
      <div className="report-dropdown">
        <Popup
          position="top center"
          content="Report this channel"
          trigger={
            <Dropdown button pointing="top right" direction="left" icon="flag outline">
              <Dropdown.Menu>
                <Dropdown.Header className="Header">What would you like to report?</Dropdown.Header>
                <Dropdown.Divider />
                <Dropdown.Item
                  value="categorization"
                  text="Inappropriate categorization"
                  onClick={(evt, data) => {
                    handleReport(data);
                  }}
                />
                <Dropdown.Item
                  value="name"
                  text="Inappropriate name"
                  onClick={(evt, data) => {
                    handleReport(data);
                  }}
                />
                <Dropdown.Item
                  value="inactive"
                  text="Inactive channel"
                  onClick={(evt, data) => {
                    handleReport(data);
                  }}
                />
                <Dropdown.Item
                  value="other"
                  text="Other reason"
                  onClick={(evt, data) => {
                    handleReport(data);
                  }}
                />
              </Dropdown.Menu>
            </Dropdown>
          }
        />
      </div>
    );
  };

  async function handleUnsubscribe(channel) {
    const response = await unsubscribeFromChannel(channel.id);
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message ||
          'An error occurred while unsubscribing from channel',
        'error'
      );
    } else {
      updateSubscribeToChannel(channel.id, false);

      showSnackbar(`Unsubscribed from channel ${channel.name} successfully`, 'success');
    }
  }

  async function handleSubscribe(channel) {
    const response = await subscribeToChannel(channel.id);
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while subscribing to channel',
        'error'
      );
    } else {
      updateSubscribeToChannel(channel.id, true);

      showSnackbar(`Subscribed channel  ${channel.name} successfully`, 'success');
    }
  }

  const toggleSubscribe = () => {
    if (loadingChannel || channel.manage) {
      return;
    }

    if (channel.subscribed) {
      return (
        <Popup
          trigger={
            <Button
              disabled={loadingUnsubscribe}
              loading={loadingUnsubscribe}
              size="mini"
              onClick={() => handleUnsubscribe(channel)}
              content="Unsubscribe"
            />
          }
          position="top center"
          content="Unsubscribe"
        />
      );
    }

    return (
      <Popup
        trigger={
          <Button
            disabled={loadingSubscribe}
            loading={loadingSubscribe}
            color="blue"
            size="mini"
            onClick={() => handleSubscribe(channel)}
            content="Subscribe"
          />
        }
        position="top center"
        content="Subscribe"
      />
    );
  };

  return error ? (
    <ErrorComponent error={error} />
  ) : (
    <div className="notifications-list">
      {activeItem === 'Notifications' ? (
        <Helmet>
          <title>{TITLE} Notifications</title>
        </Helmet>
      ) : (
        <Helmet>
          <title>{TITLE} Preference</title>
        </Helmet>
      )}

      <Header as="h2" style={{marginTop: '0'}}>
        <div style={{float: 'right', display: 'flex'}}>
          {channel.manage && (
            <Button
              icon="settings"
              onClick={() => {
                history.push(`/channels/${channel.id}`);
              }}
            />
          )}
          {isAuthenticated && toggleSubscribe()}
          {reportChannel()}
        </div>
        <Icon name={headerIcon()} />
        {channelHeader()}
      </Header>
      <Container>
        <Header>
          <Header.Subheader className="channel-description">
            <Linkify options={{target: '_blank'}}>{channel.description}</Linkify>
          </Header.Subheader>
        </Header>
        {channelCanSendPrivate()}
        {channelIsMandatory()}
        {channelIsCritical()}
      </Container>

      <Menu pointing secondary color="blue">
        <Menu.Item
          name="Notifications"
          active={activeItem === 'Notifications'}
          onClick={handleItemClick}
        />
        {isAuthenticated && channel.send && (
          <Menu.Item
            name="Send notification"
            active={activeItem === 'Send notification'}
            onClick={handleItemClick}
          />
        )}
        {isAuthenticated && (
          <Menu.Item
            name="Preferences"
            active={activeItem === 'Preferences'}
            onClick={handleItemClick}
          />
        )}
      </Menu>
      {activeItem === 'Notifications' && (
        <Segment basic loading={loadingNotifications}>
          <SearchNotificationComponent />
          <NotificationsList archive={channel.archive} visibility={channel.visibility} />
        </Segment>
      )}
      {activeItem === 'Preferences' && (
        <Segment basic loading={loadingPreferences}>
          {hasNoValidPreferences() && <NoPreferencesWarningBanner />}
          <Header as="h4">Global Preferences</Header>
          <Divider />
          <PreferencesList
            preferences={preferences.global}
            allowAdd={false}
            channelId={channelId}
            global
            allowToggleEnable
          />
          <Header as="h4">Channel Preferences</Header>
          <Divider />
          <PreferencesList
            preferences={preferences.channel}
            channelId={channelId}
            allowDelete
            allowEdit
          />
        </Segment>
      )}
      {activeItem === 'Send notification' && (
        <Segment basic>
          <SendNotificationComponent channel={channel} />
        </Segment>
      )}
    </div>
  );
};

NotificationsPage.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  preferences: PropTypes.object.isRequired,
  getPreferences: PropTypes.func.isRequired,
  loadingNotifications: PropTypes.bool.isRequired,
  loadingPreferences: PropTypes.bool.isRequired,
  channel: PropTypes.shape({
    name: PropTypes.string,
  }).isRequired,
  loadingSubscribe: PropTypes.bool.isRequired,
  loadingUnsubscribe: PropTypes.bool.isRequired,
  unsubscribeFromChannel: PropTypes.func.isRequired,
  subscribeToChannel: PropTypes.func.isRequired,
  loadingChannel: PropTypes.bool.isRequired,
  updateSubscribeToChannel: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  preferences: state.preferences,
  isAuthenticated: state.auth.loggedIn,
  loadingNotifications: state.notifications.notificationsList.loading,
  loadingPreferences: state.preferences.loading,
  channel: state.channels.channel.channel,
  loadingChannel: state.channels.channel.loading,
  error: state.channels.channel.error,
  loadingUnsubscribe: state.channels.channelsList.loadingUnsubscribe,
  loadingSubscribe: state.channels.channelsList.loadingSubscribe,
});

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(
      {
        getPreferences: preferencesActions.getPreferences,
        getChannel: getChannelById,
        getPublicChannel: getPublicChannelById,
      },
      dispatch
    ),
    ...bindActionCreators(subscribeToChannelActionCreators, dispatch),
    ...bindActionCreators(unsubscribeFromChannelActionCreators, dispatch),
    ...bindActionCreators(noContentActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NotificationsPage);
