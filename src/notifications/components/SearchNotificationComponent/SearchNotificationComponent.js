import React, {useState} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as getNotificationsActionCreators from 'notifications/actions/GetNotifications';
import SearchComponent from '../../../common/components/search/SearchComponent';

const SearchNotificationComponent = ({getNotificationsQuery, setGetNotificationsQuery}) => {
  const [text, setText] = useState(getNotificationsQuery.searchText);

  const onSearch = () => {
    setGetNotificationsQuery({...getNotificationsQuery, searchText: text});
  };

  return <SearchComponent value={text} setValue={setText} onClick={onSearch} />;
};

const mapStateToProps = state => {
  return {
    getNotificationsQuery: state.notifications.notificationsList.getNotificationsQuery,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(getNotificationsActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchNotificationComponent);
