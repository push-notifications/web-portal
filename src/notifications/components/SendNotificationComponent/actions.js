export const RESET_NOTIFICATION = 'RESET_NOTIFICATION';
export const SET_NOTIFICATION = 'SET_NOTIFICATION';
export const INIT_EDITOR_DATA = 'INIT_EDITOR_DATA';
export const EDITOR_DATA_CHANGED = 'EDITOR_DATA_CHANGED';

export const resetNotification = () => {
  return {
    type: RESET_NOTIFICATION,
  };
};

export const setNotification = notification => {
  return {
    type: SET_NOTIFICATION,
    value: notification,
  };
};

export const initEditorData = data => {
  return {
    type: INIT_EDITOR_DATA,
    value: data,
  };
};

export const editorDataChanged = () => {
  return {
    type: EDITOR_DATA_CHANGED,
  };
};
