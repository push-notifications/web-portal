import React, {useCallback, useEffect, useReducer, useState} from 'react';
import {
  Accordion,
  Button,
  Dropdown,
  Form,
  Icon,
  Label,
  Message,
  Radio,
  Segment,
} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {useParams, withRouter} from 'react-router-dom';
import debounce from 'lodash/debounce';
import {DateInput} from 'semantic-ui-calendar-react';
import moment from 'moment-timezone';

import * as showSnackBarActionCreators from 'common/actions/Snackbar';
import {createNotification} from 'notifications/actions/CreateNotification';
import {deleteDraft, getDraft, saveDraft} from 'notifications/actions/Draft';
import {notificationPriorityTypes} from 'common/types/NotificationPriorityTypes';
import SendNotificationHelp from 'help/components/SendNotificationHelp';
import NotificationPreview from 'notifications/components/NotificationDisplay/NotificationPreview';
import {channelFlagTypes} from 'common/types/ChannelFlagTypes';
import {
  DATE_FORMAT,
  DATETIME_FORMAT,
  isTimeInPast,
  TIME_FORMAT,
  TIMEZONE_CH,
} from '../../../utils/datetime';
import './SendNotificationComponent.scss';
import CKEditor from './CKEditor';
import {
  editorDataChanged as editorDataChangedAction,
  initEditorData as initEditorDataAction,
  resetNotification as resetNotificationAction,
  setNotification as setNotificationAction,
} from './actions';

import reducer from './reducer';
import {resetGetNotificationsQuery} from 'notifications/actions/GetPublicNotifications';

const SendNotificationComponent = ({
  createNotification,
  history,
  loading,
  showSnackbar,
  getDraft,
  saveDraft,
  deleteDraft,
  draft,
  loadingGetDraft,
  error,
  channel,
  resetGetNotificationsQuery,
}) => {
  const {channelId} = useParams();

  const REDUCER_INITIAL_STATE = {
    loadingCKEditorChange: false,
    notification: {
      target: channelId,
      summary: '',
      priority: notificationPriorityTypes.NORMAL,
      body: '',
      imgUrl: '',
      link: '',
      sendAt: null,
      targetUsers: null,
      targetGroups: null,
      intersection: false,
    },
    editorData: '',
    editorDataChange: '',
  };
  const [
    {editorData, editorDataChange, loadingCKEditorChange, notification},
    dispatch,
  ] = useReducer(reducer, REDUCER_INITIAL_STATE);
  const [showPreview, setShowPreview] = useState(false);
  const [loadedDraft, setLoadedDraft] = useState(false);
  const [savedDraft, setSavedDraft] = useState(false);
  const [sendAtDate, setSendAtDate] = useState('');
  const [sendAtTime, setSendAtTime] = useState('');
  const [scheduleOptions, setScheduleOptions] = useState([]);
  const [advancedOptionsOpen, setDefaultAdvancedOptionsOpen] = useState(-1);
  const [strTargetUsers, setStrTargetUsers] = useState('');
  const [strTargetGroups, setStrTargetGroups] = useState('');
  const [showIndicoURLMessage, setShowIndicoURLMessage] = useState(false);

  const handleTitleClick = (e, {index}) => {
    setDefaultAdvancedOptionsOpen(advancedOptionsOpen === index ? -1 : index);
  };

  const debouncedSaveDraft = useCallback(
    debounce(draftNotification => {
      saveDraft(draftNotification);
      setLoadedDraft(false);
      setSavedDraft(true);
    }, 2000),
    []
  );

  const saveNotification = useCallback(
    n => {
      dispatch(setNotificationAction(n));
      setSavedDraft(false);
      debouncedSaveDraft({...n});
    },
    [debouncedSaveDraft]
  );

  useEffect(() => {
    getDraft();
  }, [getDraft]);

  useEffect(() => {
    const {link} = notification;
    if (
      link &&
      (link.match(/^(https?:\/\/)?indico.cern.ch\/e\/.*/) ||
        link.match(/^(https?:\/\/)?indi.to\/.*$/))
    ) {
      setShowIndicoURLMessage(true);
      return;
    }

    setShowIndicoURLMessage(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [notification.link, setShowIndicoURLMessage]);

  useEffect(() => {
    if (loadingCKEditorChange) {
      saveNotification({...notification, body: editorDataChange});
      dispatch(editorDataChangedAction());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loadingCKEditorChange, editorDataChange, saveNotification]);

  useEffect(() => {
    if (error) {
      showSnackbar(error.response?.message || 'An error occurred.', 'error');
    }
  }, [error, showSnackbar]);

  useEffect(() => {
    if (!draft) {
      return;
    }

    const getSendAtFromDraft = () => {
      if (!draft.sendAt) {
        return null;
      }

      const sendAt = moment(draft.sendAt, moment.ISO_8601).tz(TIMEZONE_CH); // service stores in utc
      if (sendAt.isAfter(moment.tz(TIMEZONE_CH))) {
        return sendAt;
      }

      return null;
    };

    const sendAt = getSendAtFromDraft(draft);
    if (sendAt) {
      setSendAtTime(sendAt.format(TIME_FORMAT));
      setSendAtDate(sendAt.format(DATE_FORMAT));
    }

    saveNotification({
      ...notification,
      summary: draft.summary || '',
      priority: draft.priority || notificationPriorityTypes.NORMAL,
      body: draft.body || '',
      imgUrl: draft.imgUrl || '',
      link: draft.link || '',
      sendAt,
    });

    dispatch(initEditorDataAction(draft.body || ''));

    if (sendAt || draft.link || draft.imgUrl) {
      setDefaultAdvancedOptionsOpen(0);
    }

    setLoadedDraft(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [draft, saveNotification]);

  useEffect(() => {
    const scheduleDescriptions = {
      '09:00:00': 'Morning (9am)',
      '13:00:00': 'Lunch time (1pm)',
      '17:00:00': 'Afternoon (5pm)',
      '21:00:00': 'Night time (9pm)',
    };

    const getScheduleOptions = isToday => {
      const buildScheduleOption = value => ({
        key: value,
        value,
        text: scheduleDescriptions[value],
        disabled: isToday && isTimeInPast(value),
      });
      return [
        buildScheduleOption('09:00:00'),
        buildScheduleOption('13:00:00'),
        buildScheduleOption('17:00:00'),
        buildScheduleOption('21:00:00'),
      ];
    };
    const sendAtDateCH = moment.tz(sendAtDate, DATE_FORMAT, TIMEZONE_CH);
    const todayCH = moment.tz(TIMEZONE_CH);
    const isToday = !!sendAtDate && sendAtDateCH.isSame(todayCH, 'day');
    setScheduleOptions(getScheduleOptions(isToday));

    if (isToday && sendAtTime && isTimeInPast(sendAtTime)) {
      setSendAtTime('');
    }
  }, [sendAtDate, sendAtTime]);

  const updateSendAt = (date, time) => {
    const calcSendAt = () => {
      if (!date || !time) {
        return null;
      }

      return moment.tz(`${date} ${time}`, DATETIME_FORMAT, TIMEZONE_CH);
    };

    const sendAt = calcSendAt();
    saveNotification({...notification, sendAt});
  };

  const updateField = (e, d) => {
    saveNotification({...notification, [d.name]: d.value || d.checked});
  };

  const updateSendAtTime = (e, d) => {
    setSendAtTime(d.value);
    updateSendAt(sendAtDate, d.value);
  };

  const updateSendAtDate = (e, d) => {
    setSendAtDate(d.value);
    updateSendAt(d.value, sendAtTime);
  };

  const resetNotification = () => {
    dispatch(resetNotificationAction());
    dispatch(initEditorDataAction(''));
    setSendAtDate('');
    setSendAtTime('');
    setSavedDraft(false);
    setLoadedDraft(false);
    setStrTargetUsers('');
    setStrTargetGroups('');
    deleteDraft();
  };

  const validateContent = content => {
    if (content === '') {
      showSnackbar('Content field cannot be empty.', 'error');
      return false;
    }
    return true;
  };
  const validateScheduled = () => {
    if (sendAtDate && sendAtTime === '') {
      showSnackbar('You have to choose a delayed time', 'error');
      return false;
    }
    const sendAtCH = moment.tz(`${notification.sendAt}`, DATETIME_FORMAT, TIMEZONE_CH);
    const nowCH = moment.tz(TIMEZONE_CH);
    if (sendAtCH.isSameOrBefore(nowCH)) {
      showSnackbar('The scheduled date/time cannot be in the past.', 'error');
      return false;
    }
    return true;
  };

  const advancedOptions = [
    {
      key: 'advanced',
      title: 'Advanced Options',
      content: {
        content: (
          <>
            <Label color="blue" ribbon className="form-ribbon">
              Associated Url
            </Label>
            <Form.Input
              label="The URL associated with this notification: Website, Newdle, Indico event, etc."
              name="link"
              placeholder="Target url when notification is clicked"
              value={notification.link}
              onChange={updateField}
            />
            {showIndicoURLMessage && (
              <Message
                info
                content="Indico short URL detected. Use the regular Indico URLs for full features,
                 ie. https://indico.cern.ch/event/xxxxxxx/"
              />
            )}
            <Label color="blue" ribbon className="form-ribbon">
              Image Url
            </Label>
            <Form.Input
              label="Specify an image url to display as preview when possible (for push notifications, optional)"
              name="imgUrl"
              placeholder="Url of image to display, should not be protected."
              value={notification.imgUrl}
              onChange={updateField}
            />
            <Label color="blue" ribbon className="form-ribbon">
              Schedule send (Europe/Zurich)
            </Label>
            <Form.Group inline>
              <DateInput
                dateFormat={DATE_FORMAT}
                name="sendAtDate"
                placeholder="Schedule date"
                iconPosition="left"
                value={sendAtDate}
                onChange={updateSendAtDate}
                closable
                minDate={moment.tz(TIMEZONE_CH)}
              />
              <label className="daily-frequency-time-label">at </label>
              <Dropdown
                inline
                header="Select time to send"
                options={scheduleOptions}
                placeholder="Choose a time"
                name="sendAtTime"
                value={sendAtTime}
                disabled={!sendAtDate}
                onChange={updateSendAtTime}
                selectOnNavigation={false}
                selectOnBlur={false}
                closeOnChange
              />
            </Form.Group>
            {channel?.sendPrivate && (
              <>
                <Label color="blue" ribbon className="form-ribbon">
                  Direct Notification
                </Label>
                <Form.Group grouped>
                  <Message
                    info
                    size="small"
                    header="All channel administrators have access to all notifications (both content and targets), including direct notifications.."
                    content="Note: Only Channel owner and administrators are allowed to send direct notifications."
                  />
                  <Form.Field
                    control={Radio}
                    label="Notification will be sent ONLY to intersection between specified users or groups and channel members."
                    value="true"
                    checked={notification.intersection}
                    onChange={() => saveNotification({...notification, intersection: true})}
                  />
                  <Form.Field
                    control={Radio}
                    label="Notification will be sent to all specified users or groups. Users or groups will be added as channel members if needed."
                    value="false"
                    checked={notification.intersection === false}
                    onChange={() => saveNotification({...notification, intersection: false})}
                  />

                  <Form.Input
                    label="Specify users to send a direct notification to some channel members"
                    name="targetUsers"
                    placeholder="Emails or logins of users, semi-colon separated (i.e.: user1;user2;user3)."
                    value={strTargetUsers}
                    onChange={(e, d) => setStrTargetUsers(d.value)}
                  />
                  <Form.Input
                    label="Specify grappa groups to send direct notification to some channel members"
                    name="targetGroups"
                    placeholder="Grappa groups of users, semi-colon separated (i.e.: group1;group2;group3)."
                    value={strTargetGroups}
                    onChange={(e, d) => setStrTargetGroups(d.value)}
                  />
                </Form.Group>
              </>
            )}
          </>
        ),
      },
    },
  ];

  async function handleSubmit() {
    setShowPreview(true);
  }

  async function handleSubmitValidated(send) {
    setShowPreview(false);
    if (!validateContent(notification.body)) return;
    if (!send) return;
    if (!validateScheduled()) return;

    // Construct arrays of target users and groups
    if (channel.sendPrivate) {
      notification.targetUsers = strTargetUsers
        .split(';')
        .filter(i => i)
        .map(user => {
          return {email: user};
        });
      notification.targetGroups = strTargetGroups
        .split(';')
        .filter(i => i)
        .map(group => {
          return {groupIdentifier: group};
        });
    }
    const response = await createNotification(notification);
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message ||
          'An error occurred while creating your notification. Please try again or open a ticket if the issue persists.',
        'error'
      );
    } else {
      showSnackbar('The notification has been created successfully', 'success');
      resetNotification();
      resetGetNotificationsQuery();

      setTimeout(() => {
        history.push({
          pathname: `/channels/${channelId}/notifications`,
          section: 'Notifications',
        });
      }, 1000);
    }
  }

  return (
    <Segment>
      {loadedDraft && (
        <Label basic size="tiny" floating className="draft-label" style={{width: 100}}>
          <Icon name="save" />
          Draft loaded
        </Label>
      )}
      {savedDraft && (
        <Label basic size="tiny" floating className="draft-label" style={{width: 90}}>
          <Icon name="save" />
          Draft saved
        </Label>
      )}
      <Form onSubmit={handleSubmit} loading={loadingGetDraft}>
        <Label color="blue" ribbon className="form-ribbon">
          Title *
        </Label>
        <Form.Input
          required
          name="summary"
          placeholder="Notification title"
          value={notification.summary}
          onChange={updateField}
          autoFocus
        />

        <Label color="blue" ribbon className="form-ribbon">
          Content *
        </Label>
        <Form.Field>
          <CKEditor
            editorUrl="https://cdn.ckeditor.com/4.21.0/full-all/ckeditor.js"
            config={{
              autoGrow_onStartup: true,
              contentsCss: ['https://cdn.ckeditor.com/4.21.0/full-all/contents.css'],
              extraPlugins: 'autogrow,image2,emoji',
              removePlugins: 'exportpdf,image',
              // PasteFromWord extra options
              forcePasteAsPlainText: false,
              pasteFromWordRemoveFontStyles: false,
              pasteFromWordRemoveStyles: false,
              extraAllowedContent: 'p(mso*,Normal)',
              pasteFilter: null,
              allowedContent: true,

              toolbarGroups: [
                {name: 'forms', groups: ['forms']},
                '/',
                {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                {name: 'links', groups: ['links']},
                {
                  name: 'paragraph',
                  groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'],
                },
                {name: 'insert', groups: ['insert']},
                '/',
                {name: 'styles', groups: ['styles']},
                {name: 'colors', groups: ['colors']},
                {name: 'tools', groups: ['tools']},
                {name: 'others', groups: ['others']},
                {name: 'clipboard', groups: ['clipboard', 'undo']},
                {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
                {name: 'document', groups: ['mode', 'document', 'doctools']},
                {name: 'about', groups: ['about']},
              ],

              removeButtons:
                'Print,Preview,NewPage,Save,Templates,Cut,Copy,Paste,PasteText,Checkbox,Form,Radio,TextField,' +
                'Textarea,ImageButton,HiddenField,Select,Button,Iframe,Flash,PageBreak,SelectAll,ShowBlocks,' +
                'CreateDiv,BidiRtl,BidiLtr,Language,CopyFormatting,Copy,PasteFromWord',
            }}
            dispatchEvent={dispatch}
            type="classic"
            data={editorData}
            uniqueName="body-editor"
          />
        </Form.Field>
        <Label color="blue" ribbon className="form-ribbon">
          Priority *
        </Label>
        <Form.Group grouped>
          {channel?.channelFlags && channel.channelFlags.includes(channelFlagTypes.CRITICAL) && (
            <Form.Field
              control={Radio}
              label="Critical (Emergencies ONLY: sends the notification to all user devices bypassing preferences)"
              name="notificationPriorityGroup"
              value={notificationPriorityTypes.CRITICAL}
              checked={notification.priority === notificationPriorityTypes.CRITICAL}
              onClick={() =>
                saveNotification({...notification, priority: notificationPriorityTypes.CRITICAL})
              }
            />
          )}
          <Form.Field
            control={Radio}
            label="Important"
            name="notificationPriorityGroup"
            value={notificationPriorityTypes.IMPORTANT}
            checked={notification.priority === notificationPriorityTypes.IMPORTANT}
            onClick={() =>
              saveNotification({...notification, priority: notificationPriorityTypes.IMPORTANT})
            }
          />
          <Form.Field
            control={Radio}
            label="Normal"
            name="notificationPriorityGroup"
            value={notificationPriorityTypes.NORMAL}
            checked={notification.priority === notificationPriorityTypes.NORMAL}
            onClick={() =>
              saveNotification({...notification, priority: notificationPriorityTypes.NORMAL})
            }
          />
          <Form.Field
            control={Radio}
            label="Low"
            name="notificationPriorityGroup"
            value={notificationPriorityTypes.LOW}
            checked={notification.priority === notificationPriorityTypes.LOW}
            onClick={() =>
              saveNotification({...notification, priority: notificationPriorityTypes.LOW})
            }
          />
        </Form.Group>
        <Accordion
          panels={advancedOptions}
          activeIndex={advancedOptionsOpen}
          onTitleClick={handleTitleClick}
        />
        <Form.Group inline>
          <Form.Button disabled={loading} primary loading={loading}>
            Submit
          </Form.Button>
          <Button type="reset" onClick={resetNotification}>
            Reset
          </Button>
          <SendNotificationHelp />
        </Form.Group>
      </Form>

      <NotificationPreview
        notification={notification}
        modalOpen={showPreview}
        onCloseModal={handleSubmitValidated}
      />
    </Segment>
  );
};

const mapStateToProps = state => ({
  loading: state.notifications.newNotification.loading,
  draft: state.notifications.draft.draft,
  loadingGetDraft: state.notifications.draft.loadingGet,
  error: state.notifications.draft.error,
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      createNotification,
      getDraft,
      saveDraft,
      deleteDraft,
      resetGetNotificationsQuery,
      ...showSnackBarActionCreators,
    },
    dispatch
  );
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SendNotificationComponent));
