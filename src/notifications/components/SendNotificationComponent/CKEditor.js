import React, {useMemo} from 'react';
import {useCKEditor} from 'ckeditor4-react';

const {useEffect, useState} = React;

/* eslint-disable react/prop-types */
function CKEditor({editorUrl, config, debug, type, data, dispatchEvent, uniqueName}) {
  const [element, setElement] = useState();

  const {editor} = useCKEditor({
    editorUrl,
    config,
    element,
    type,
    debug,
    dispatchEvent,
    subscribeTo: ['change'],
  });

  const setEditorData = useMemo(() => {
    if (editor) {
      /* eslint-disable-next-line */
      return new CKEDITOR.tools.buffers.throttle(500, data => {
        if (editor) {
          editor.setData(data);
        }
      }).input;
    }
  }, [editor]);
  /**
   * Sets editor data if it comes from a different source.
   */
  useEffect(() => {
    if (setEditorData) {
      setEditorData(data);
    }
  }, [data, setEditorData]);

  return <div id={uniqueName} ref={setElement} />;
}

export default CKEditor;
