import {CKEditorEventAction} from 'ckeditor4-react';
import {
  EDITOR_DATA_CHANGED,
  INIT_EDITOR_DATA,
  RESET_NOTIFICATION,
  SET_NOTIFICATION,
} from './actions';
import {notificationPriorityTypes} from '../../../common/types/NotificationPriorityTypes';

const reducer = (state, action) => {
  switch (action.type) {
    case RESET_NOTIFICATION:
      return {
        ...state,
        notification: {
          ...state.notification,
          summary: '',
          priority: notificationPriorityTypes.NORMAL,
          body: '',
          imgUrl: '',
          link: '',
          sendAt: null,
          targetUsers: null,
          targetGroups: null,
          intersection: false,
        },
        loadingCKEditorChange: false,
        editorData: '',
        editorDataChange: '',
      };
    case SET_NOTIFICATION:
      return {
        ...state,
        notification: action.value,
      };
    case INIT_EDITOR_DATA:
      return {
        ...state,
        editorData: action.value,
      };
    case CKEditorEventAction.change:
      return {
        ...state,
        editorDataChange: action.payload.editor.getData(),
        loadingCKEditorChange: true,
      };
    case EDITOR_DATA_CHANGED:
      return {
        ...state,
        loadingCKEditorChange: false,
        editorDataChange: '',
      };
    default:
      return state;
  }
};

export default reducer;
