import React, {useState} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as getNotificationsActionCreators from 'notifications/actions/GetNotificationsAdmin';
import SearchComponent from '../../../common/components/search/SearchComponent';

const SearchNotificationAdminComponent = ({getNotificationsQuery, setGetNotificationsQuery}) => {
  const [text, setText] = useState(getNotificationsQuery.searchText);

  const onSearch = () => {
    setGetNotificationsQuery({...getNotificationsQuery, searchText: text});
  };

  return <SearchComponent value={text} setValue={setText} onClick={onSearch} />;
};

const mapStateToProps = state => {
  return {
    getNotificationsQuery: state.notifications.notificationsListAdmin.getNotificationsQuery,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({...getNotificationsActionCreators}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchNotificationAdminComponent);
