import React from 'react';
import {Item, Button, Message, Dimmer, Container, Icon, Popup} from 'semantic-ui-react';

import * as he from 'he';

import NotificationIcon from 'utils/notification-type-icon';

import {getDisplayDatetime, isTimeInFuture} from '../../../utils/datetime';
import './NotificationsList.scss';
import IndicoEventButton from '../IndicoEventButton/IndicoEventButton';

const NotificationsListItem = ({notification, setShowNotification}) => {
  const isInFuture = notification.sendAt && isTimeInFuture(notification.sendAt);

  const bodyPreview = body => {
    const preview = body.replace(/(<([^>]+)>)/gi, '');
    // Using he.decode to decode html special chars
    const shortBody = preview && `${he.decode(preview)}`;
    if (shortBody.length > 100) return `${shortBody.substring(0, 100)}...`;
    return shortBody;
  };

  return (
    <React.Fragment key={notification.id}>
      {isInFuture && (
        <Container attached="top" className="notification-scheduled-list">
          <Message
            info
            icon={
              <Icon.Group size="large">
                <Icon name="envelope" />
                <Icon corner name="clock" />
              </Icon.Group>
            }
            size="small"
            content={`Send scheduled for ${getDisplayDatetime(notification.sendAt)}`}
          />
        </Container>
      )}
      <Dimmer.Dimmable
        as={Item}
        dimmed={isInFuture}
        className={isInFuture ? 'notification-scheduled-list' : 'notification-list'}
      >
        {notification.imgUrl && <Item.Image size="tiny" src={notification.imgUrl} />}

        <Item.Content>
          <Item.Header>
            {notification.private && (
              <Popup trigger={<Icon name="paper plane outline" />} content="Direct notification" />
            )}
            <NotificationIcon priority={notification.priority} />
            <Button className="tertiarynohover" onClick={() => setShowNotification(notification)}>
              {notification.summary}
            </Button>
          </Item.Header>
          <Item.Description>{bodyPreview(notification.body)}</Item.Description>
          <Item.Extra>
            {notification.sentAt ? getDisplayDatetime(notification.sentAt) : ''}
            {isInFuture ? getDisplayDatetime(notification.sendAt) : ''}
          </Item.Extra>
        </Item.Content>
        <IndicoEventButton notification={notification} />
        <Dimmer inverted onClick={() => setShowNotification(notification)} active={isInFuture} />
      </Dimmer.Dimmable>
    </React.Fragment>
  );
};

export default NotificationsListItem;
