import React, {useEffect, useState} from 'react';
import {Grid, Item, Pagination, Icon} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {useParams} from 'react-router-dom';

import NotificationDisplay from 'notifications/components/NotificationDisplay/NotificationDisplay';
import * as getNotificationsActionCreators from 'notifications/actions/GetNotifications';
import * as getNotificationByIdActionCreators from 'notifications/actions/GetNotificationById';
import * as getPublicNotificationsActionCreators from 'notifications/actions/GetPublicNotifications';
import './NotificationsList.scss';
import NotificationsListItem from './NotificationsListItem';

const NotificationsList = props => {
  const {
    notifications,
    getNotifications,
    getNotificationsQuery,
    isAuthenticated,
    getPublicNotifications,
    setGetNotificationsQuery,
    getNotificationById,
    notification,
    count,
    archive,
    visibility,
  } = props;
  const {channelId} = useParams();
  const notificationId = window.location.pathname.split('notifications/')[1];
  const [showNotification, setShowNotification] = useState(null);
  const activePage = getNotificationsQuery.skip / getNotificationsQuery.take + 1;
  const totalPages = Math.ceil(count / getNotificationsQuery.take);

  const closeNotificationModal = () => {
    window.history.pushState({}, document.title, `/channels/${channelId}/notifications`);
    setShowNotification(null);
  };

  useEffect(() => {
    if (isAuthenticated) getNotifications(channelId, getNotificationsQuery);
    else getPublicNotifications(channelId, getNotificationsQuery);
  }, [getNotifications, channelId, getNotificationsQuery, getPublicNotifications, isAuthenticated]);

  useEffect(() => {
    if (notificationId) {
      getNotificationById(notificationId, visibility);
    }
  }, [getNotificationById, notificationId, visibility, isAuthenticated]);

  useEffect(() => {
    setShowNotification(notification);
  }, [notification]);

  if (notifications.length === 0) {
    return (
      <Grid padded>
      <Grid.Row centered>
        <p>The are no notifications in this channel</p>;
      </Grid.Row>
      {archive && (
          <Grid.Row centered>
            <Icon name="archive" color="blue" />
            <a
              href={`${process.env.REACT_APP_ARCHIVE_URL}/data/${channelId}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              Notifications's Archive
            </a>
          </Grid.Row>
        )}
      </Grid>
    )
  }

  return (
    <>
      <NotificationDisplay
        channelId={channelId}
        notification={showNotification}
        onCloseModal={() => closeNotificationModal()}
      />
      <Grid padded>
        <Grid.Row>
          <Grid.Column>
            <Item.Group divided relaxed>
              {notifications.map(notification => (
                <NotificationsListItem
                  key={notification.id}
                  notification={notification}
                  setShowNotification={setShowNotification}
                />
              ))}
            </Item.Group>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row centered>
          <Pagination
            activePage={activePage}
            onPageChange={(e, {activePage}) =>
              setGetNotificationsQuery({
                ...getNotificationsQuery,
                skip: (activePage - 1) * getNotificationsQuery.take,
              })
            }
            totalPages={totalPages}
          />
        </Grid.Row>
        {archive && (
          <Grid.Row centered>
            <Icon name="archive" color="blue" />
            <a
              href={`${process.env.REACT_APP_ARCHIVE_URL}/data/${channelId}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              Notifications's Archive
            </a>
          </Grid.Row>
        )}
      </Grid>
    </>
  );
};

const mapStatetoProps = state => {
  return {
    isAuthenticated: state.auth.loggedIn,
    notifications: state.notifications.notificationsList.notifications,
    count: state.notifications.notificationsList.count,
    getNotificationsQuery: state.notifications.notificationsList.getNotificationsQuery,
    notification: state.notifications.notification.notification,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(getNotificationByIdActionCreators, dispatch),
    ...bindActionCreators(getNotificationsActionCreators, dispatch),
    ...bindActionCreators(getPublicNotificationsActionCreators, dispatch),
  };
};

export default connect(mapStatetoProps, mapDispatchToProps)(NotificationsList);
