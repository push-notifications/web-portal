import React, {useEffect} from 'react';
import {Pagination, Segment, Table} from 'semantic-ui-react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {useParams} from 'react-router-dom';

import SearchNotificationAdminComponent from 'notifications/components/SearchNotificationAdminComponent/SearchNotificationAdminComponent';
import * as getNotificationsActionCreators from 'notifications/actions/GetNotificationsAdmin';

const columns = [
  {id: 'summary', numeric: false, disablePadding: true, label: 'Title'},
  {id: 'sentAt', numeric: false, disablePadding: false, label: 'Priority'},
  {id: 'sentAt', numeric: false, disablePadding: false, label: 'Created at'},
];

const NotificationsListAdmin = props => {
  const {
    notifications,
    getNotifications,
    getNotificationsQuery,
    loading,
    setGetNotificationsQuery,
    count,
  } = props;
  const {channelId} = useParams();

  useEffect(() => {
    getNotifications(channelId, getNotificationsQuery);
  }, [getNotifications, channelId, getNotificationsQuery]);

  return (
    <Segment basic loading={loading}>
      <SearchNotificationAdminComponent />
      <Table striped>
        <Table.Header>
          <Table.Row>
            {columns.map(c => (
              <Table.HeaderCell key={c.id}>{c.label}</Table.HeaderCell>
            ))}
            {/* <Table.HeaderCell>Notifications sent</Table.HeaderCell>
            <Table.HeaderCell>Daily summaries</Table.HeaderCell>
            <Table.HeaderCell>Weekly summaries</Table.HeaderCell> */}
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {notifications.map(notification => {
            return (
              <Table.Row key={notification.id}>
                <Table.Cell>{notification.summary}</Table.Cell>
                <Table.Cell>{notification.priority}</Table.Cell>
                <Table.Cell>{new Date(notification.sentAt).toLocaleString()}</Table.Cell>
                {/* <Table.Cell>12</Table.Cell>
                <Table.Cell>3</Table.Cell>
                <Table.Cell>0</Table.Cell> */}
              </Table.Row>
            );
          })}
        </Table.Body>
        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="3" textAlign="center">
              <Pagination
                activePage={getNotificationsQuery.skip / getNotificationsQuery.take + 1}
                onPageChange={(e, {activePage}) =>
                  setGetNotificationsQuery({
                    ...getNotificationsQuery,
                    skip: (activePage - 1) * getNotificationsQuery.take,
                  })
                }
                totalPages={Math.ceil(count / getNotificationsQuery.take)}
              />
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
    </Segment>
  );
};

const mapStatetoProps = state => {
  return {
    notifications: state.notifications.notificationsListAdmin.notifications,
    loading: state.notifications.notificationsListAdmin.loading,
    getNotificationsQuery: state.notifications.notificationsListAdmin.getNotificationsQuery,
    count: state.notifications.notificationsListAdmin.count,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(getNotificationsActionCreators, dispatch);
};

export default connect(mapStatetoProps, mapDispatchToProps)(NotificationsListAdmin);
