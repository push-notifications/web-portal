import React, {useEffect, useState} from 'react';
import {
  Modal,
  Button,
  Segment,
  Message,
  Icon,
  Confirm,
  Accordion,
  Label,
  Loader,
  Divider,
} from 'semantic-ui-react';

import * as getAuditByIdActionCreator from 'notifications/actions/GetAuditById';
import * as deleteNotificationByIdCreator from 'notifications/actions/DeleteNotificationById';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as showSnackBarActionCreator from 'common/actions/Snackbar';
import {getDisplayDatetime, isTimeAfter, isTimeInFuture} from '../../../utils/datetime';

import * as getNotificationTargetUsersActionCreator from 'notifications/actions/GetNotificationTargetUsers';
import * as getNotificationTargetGroupsActionCreator from 'notifications/actions/GetNotificationTargetGroups';

import NotificationComponent from './NotificationComponent';
import './NotificationDisplay.scss';
import {isAdmin} from 'auth/utils/authUtils';
import * as setNotificationActionCreator from 'notifications/actions/GetNotificationById';

const NotificationDisplay = ({
  notification,
  onCloseModal,
  showSnackbar,
  channelId,
  manage,
  getAuditById,
  loadingAudit,
  deleteNotificationById,
  setNotification,
  targetGroups,
  targetUsers,
  getNotificationTargetUsers,
  getNotificationTargetGroups,
  targetUsersCount,
  targetGroupsCount,
  loadingTargetUsers,
  loadingTargetGroups,
  roles,
}) => {
  const [openRemoveConfirmation, setOpenRemoveConfirmation] = useState(false);
  const [openNotificationTargets, setOpenNotificationTargets] = useState(false);

  useEffect(() => {
    if (openNotificationTargets && notification) {
      getNotificationTargetUsers(notification.id);
      getNotificationTargetGroups(notification.id);
    }
  }, [
    getNotificationTargetUsers,
    getNotificationTargetGroups,
    openNotificationTargets,
    notification,
  ]);

  useEffect(() => {
    return () => {
      setNotification();
    };
  }, [setNotification]);

  async function copyNotificationLinkToClipboard() {
    navigator.clipboard.writeText(
      `${window.location.origin}/channels/${channelId}/notifications/${notification.id}/`
    );
    showSnackbar("Notification's link copied to your clipboard", 'info');
  }

  async function downloadAuditReport() {
    const response = await getAuditById(notification.id);
    if (response.error) {
      const msg =
        response.payload.status === 404
          ? 'Audit not yet available. Please try again soon.'
          : 'An error occurred while downloading audit information.';
      showSnackbar(msg, 'error');
      return;
    }

    const url = window.URL.createObjectURL(
      new Blob([JSON.stringify(response.payload)], {type: 'application/json'})
    );
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', `notifications-audit-${notification?.id}.json`);
    document.body.appendChild(link);
    link.click();
    link.parentNode.removeChild(link);
    showSnackbar('The audit has been downloaded successfully', 'success');
  }

  async function deleteNotification(id) {
    const response = await deleteNotificationById(id);
    if (response?.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while deleting the notification',
        'error'
      );
    } else {
      showSnackbar('The notification has been deleted successfully', 'success');
      setOpenRemoveConfirmation(false);
      window.location.reload();
    }
  }

  return (
    <Modal open={!!notification} onClose={onCloseModal}>
      {notification?.sendAt && isTimeInFuture(notification.sendAt) && (
        <Segment basic style={{margin: 0, paddingBottom: 0}}>
          <Message
            info
            icon={
              <Icon.Group size="large">
                <Icon name="envelope" />
                <Icon corner name="clock" />
              </Icon.Group>
            }
            size="small"
            header={`Send scheduled for ${getDisplayDatetime(notification.sendAt)}`}
          />
        </Segment>
      )}
      {notification?.private && (
        <Segment basic style={{margin: 0, paddingBottom: 0}}>
          <Message
            color="teal"
            info
            icon={
              <Icon.Group size="large">
                <Icon name="paper plane outline" />
              </Icon.Group>
            }
            size="small"
            header="This notification is targeted"
          />
        </Segment>
      )}
      <Modal.Content>
        <Modal.Description>
          <NotificationComponent notification={notification} />
        </Modal.Description>
      </Modal.Content>
      {notification?.private && (manage || isAdmin(roles)) && (
        <Modal.Actions className="modal-actions-targets">
          <Accordion fluid>
            <Accordion.Title
              align="left"
              active={openNotificationTargets}
              onClick={() => setOpenNotificationTargets(!openNotificationTargets)}
            >
              <Icon name="dropdown" />
              {notification.intersection ? 'Intersection' : ''} Targets
            </Accordion.Title>
            <Accordion.Content align="left" active={openNotificationTargets}>
              {(targetUsersCount > targetUsers.length ||
                targetGroupsCount > targetGroups.length) && (
                <Message
                  icon={
                    <Icon.Group size="large">
                      <Icon name="warning circle" />
                    </Icon.Group>
                  }
                  color="orange"
                  header="Too many targets to load."
                  content={
                    <p>
                      The complete list can be requested via{' '}
                      <a href="https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=notifications-service">
                        Snow Ticket.
                      </a>
                    </p>
                  }
                />
              )}
              <Segment basic>
                {loadingTargetUsers ? (
                  <div className="targets-loader-div">
                    <Loader inverted active>
                      Loading target users
                    </Loader>
                  </div>
                ) : (
                  <>
                    <Divider horizontal style={{marginTop: 0}}>
                      Users
                    </Divider>
                    {targetUsers?.map(user => {
                      return (
                        <Label style={{margin: 2}} key={user.id}>
                          {' '}
                          {user.username || user.email}
                        </Label>
                      );
                    })}
                  </>
                )}
                {targetUsersCount > targetUsers.length && (
                  <Label basic color="orange">
                    ... {targetUsersCount - targetUsers.length} more
                  </Label>
                )}
              </Segment>
              <Segment basic>
                {loadingTargetGroups ? (
                  <Loader inverted active>
                    Loading target groups
                  </Loader>
                ) : (
                  <>
                    <Divider horizontal style={{marginTop: '-1rem'}}>
                      Groups
                    </Divider>
                    {targetGroups?.map(group => {
                      return (
                        <Label style={{margin: 2}} key={group.groupIdentifier}>
                          {group.groupIdentifier}
                        </Label>
                      );
                    })}
                  </>
                )}
                {targetGroupsCount > targetGroups.length && (
                  <Label basic color="orange">
                    ... {targetGroupsCount - targetGroups.length} more
                  </Label>
                )}
              </Segment>
            </Accordion.Content>
          </Accordion>
        </Modal.Actions>
      )}
      <Modal.Actions>
        {process.env.REACT_APP_EXTERNAL_AUDIT_ENABLED === 'true' &&
          manage &&
          notification?.sentAt &&
          isTimeAfter(
            notification.sentAt,
            process.env.REACT_APP_EXTERNAL_AUDIT_AVAILABLE_FROM_DATE
          ) && (
            <Button
              loading={loadingAudit}
              icon="download"
              onClick={() => downloadAuditReport()}
              content="Download Audit"
            />
          )}
        {!!notification?.sendAt && (
          <>
            <Button
              type="button"
              color="red"
              onClick={() => setOpenRemoveConfirmation(true)}
              icon="trash"
              content="Delete"
            />
            <Confirm
              open={openRemoveConfirmation}
              onCancel={() => setOpenRemoveConfirmation(false)}
              onConfirm={() => deleteNotification(notification?.id)}
              confirmButton="Confirm"
              content={
                <div className="content">
                  Are you sure you want to cancel and delete the notification? It won't be sent to
                  any users.
                </div>
              }
            />
          </>
        )}
        <Button
          type="button"
          onClick={copyNotificationLinkToClipboard}
          icon="share"
          content="Share"
        />
        <Button onClick={onCloseModal}>Close</Button>
      </Modal.Actions>
    </Modal>
  );
};

const mapStateToProps = state => {
  return {
    manage: state.channels.channel.channel.manage,
    loadingAudit: state.notifications.notificationsListAdmin.loadingAudit,
    targetUsers: state.notifications.notification.targetUsers,
    targetGroups: state.notifications.notification.targetGroups,
    targetUsersCount: state.notifications.notification.targetUsersCount,
    targetGroupsCount: state.notifications.notification.targetGroupsCount,
    loadingTargetUsers: state.notifications.notification.loadingTargetUsers,
    loadingTargetGroups: state.notifications.notification.loadingTargetGroups,
    roles: state.auth.roles,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(showSnackBarActionCreator, dispatch),
    ...bindActionCreators(getAuditByIdActionCreator, dispatch),
    ...bindActionCreators(deleteNotificationByIdCreator, dispatch),
    ...bindActionCreators(setNotificationActionCreator, dispatch),
    ...bindActionCreators(getNotificationTargetUsersActionCreator, dispatch),
    ...bindActionCreators(getNotificationTargetGroupsActionCreator, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NotificationDisplay);
