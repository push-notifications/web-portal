import React from 'react';
import {Modal, Button, Message, Segment, Icon} from 'semantic-ui-react';
import {getDisplayDatetime, isTimeInFuture} from '../../../utils/datetime';

import NotificationComponent from './NotificationComponent';

const NotificationPreview = props => {
  const {notification} = props;

  return (
    <Modal
      open={props.modalOpen}
      onClose={() => {
        if (props.onCloseModal) props.onCloseModal(false);
      }}
    >
      {notification.sendAt && isTimeInFuture(notification.sendAt) && (
        <Segment basic style={{margin: 0, paddingBottom: 0}}>
          <Message
            info
            icon={
              <Icon.Group size="big">
                <Icon name="envelope" />
                <Icon corner name="clock" />
              </Icon.Group>
            }
            size="small"
            header={`Send scheduled for ${getDisplayDatetime(notification.sendAt)}`}
          />
        </Segment>
      )}

      <Modal.Header>Notification Preview</Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <NotificationComponent notification={notification} />
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button
          onClick={() => {
            if (props.onCloseModal) props.onCloseModal(false);
          }}
        >
          Cancel
        </Button>
        <Button
          primary
          onClick={() => {
            if (props.onCloseModal) props.onCloseModal(true);
          }}
        >
          Send Notification
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

export default NotificationPreview;
