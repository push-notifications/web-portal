import React from 'react';
import {Item, Icon, Popup} from 'semantic-ui-react';
import NotificationIcon from 'utils/notification-type-icon';
import {getDisplayDatetime, isTimeInFuture} from '../../../utils/datetime';
import IndicoEventButton from '../IndicoEventButton/IndicoEventButton';

import './NotificationComponent.scss';

const NotificationComponent = props => {
  const {notification} = props;

  return (
    <Item.Group>
      <Item className="notification-component">
        {notification?.imgUrl && <Item.Image size="tiny" src={notification.imgUrl} />}
        <Item.Content className="scrolling">
          <Item.Header>
            <div>
              {notification.private && (
                <Popup
                  style={{marginLeft: 5}}
                  trigger={<Icon name="paper plane outline" style={{color: '#ccc'}} />}
                  content="Direct notification"
                />
              )}
              <NotificationIcon priority={notification.priority} /> {notification.summary}
            </div>
            <IndicoEventButton notification={notification} />
          </Item.Header>
          <Item.Meta>
            {notification.sentAt ? getDisplayDatetime(notification.sentAt) : ''}
            {notification.sendAt &&
              isTimeInFuture(notification.sendAt) &&
              getDisplayDatetime(notification.sendAt)}
          </Item.Meta>
          {notification && notification.link && (
            <Item.Extra>
              Link:{' '}
              <a href={notification.link} target="_blank" rel="noopener noreferrer">
                {notification.link}
              </a>
            </Item.Extra>
          )}

          <Item.Description>
            <div dangerouslySetInnerHTML={{__html: notification.body}} />
          </Item.Description>
        </Item.Content>
      </Item>
    </Item.Group>
  );
};

export default NotificationComponent;
