import React, {useEffect, useState} from 'react';
import {Button, Icon, Popup, Label, Image} from 'semantic-ui-react';

import indicoLogoBw from './logo_indico_bw.png';

import './IndicoEventButton.scss';
const POPUP_CLASS = 'indico-event-popup';
const POPUP_ZINDEX = 2000;

const IndicoEventButton = ({notification}) => {
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    if (isOpen) {
      const popup = document.getElementsByClassName(POPUP_CLASS)[0];
      if (popup && popup.parentElement) popup.parentElement.style.zIndex = POPUP_ZINDEX;
    }
  }, [isOpen]);

  function linkedToIndicoEvent(link) {
    // i.e: https://indico.cern.ch/event/1258032/
    return link && link.match(/^(https?:\/\/)?indico.cern.ch\/event\/[0-9]+[/]?$/);
  }

  function eventCalendarLink(link) {
    return link.replace(/\/$/, '').endsWith('/event.ics') ? link : `${link}/event.ics`;
  }

  if (linkedToIndicoEvent(notification.link)) {
    return (
      <Popup
        content="Open Indico page and download Calendar event."
        className={POPUP_CLASS}
        onOpen={() => {
          setIsOpen(true);
        }}
        onClose={() => {
          setIsOpen(false);
        }}
        open={isOpen}
        trigger={
          <div className="indicobutton">
            <Button
              compact
              basic
              size="mini"
              as="div"
              color="blue"
              labelPosition="right"
              onClick={() =>
                window.open(`${eventCalendarLink(notification.link)}`, '_blank', 'noreferrer')
              }
            >
              <Button color="blue" basic compact>
                <Icon name="download" />
              </Button>
              <Label color="blue" pointing="left">
                <Image src={indicoLogoBw} centered />
              </Label>
            </Button>
          </div>
        }
      />
    );
  }
  return null;
};

export default IndicoEventButton;
