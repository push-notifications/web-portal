import {
  CREATE_NOTIFICATION,
  CREATE_NOTIFICATION_FAILURE,
  CREATE_NOTIFICATION_SUCCESS,
} from 'notifications/actions/CreateNotification';

const INITIAL_STATE = {
  notification: null,
  error: null,
  loading: false,
};

function processCreateNotification(state) {
  return {
    ...state,
    loading: true,
  };
}

function processCreateNotificationSuccess(state, notification) {
  return {
    ...state,
    notification,
    error: null,
    loading: false,
  };
}

function processCreateNotificationFailure(state, error) {
  return {
    ...state,
    notification: null,
    error,
    loading: false,
  };
}

export default function (state = INITIAL_STATE, action) {
  let error;

  switch (action.type) {
    case CREATE_NOTIFICATION:
      return processCreateNotification(state);
    case CREATE_NOTIFICATION_SUCCESS:
      return processCreateNotificationSuccess(state, action.payload);
    case CREATE_NOTIFICATION_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processCreateNotificationFailure(state, error);

    default:
      return state;
  }
}
