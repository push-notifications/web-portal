import {
  GET_DRAFT_SUCCESS,
  DELETE_DRAFT_SUCCESS,
  GET_DRAFT_FAILURE,
  GET_DRAFT,
  DELETE_DRAFT,
  DELETE_DRAFT_FAILURE,
  SAVE_DRAFT,
  SAVE_DRAFT_SUCCESS,
  SAVE_DRAFT_FAILURE,
} from 'notifications/actions/Draft';

const INITIAL_STATE = {
  draft: undefined,
  error: null,
  loadingGet: false,
  loadingDelete: false,
  loadingSave: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_DRAFT:
      return {...state, loadingGet: true, error: null};
    case GET_DRAFT_SUCCESS:
      return {
        ...state,
        draft: action.payload ? JSON.parse(action.payload) : undefined,
        loadingGet: false,
        error: null,
      };
    case GET_DRAFT_FAILURE:
      return {...state, error: action.payload, loadingGet: false};
    case DELETE_DRAFT:
      return {...state, loadingDelete: true, error: null};
    case DELETE_DRAFT_SUCCESS:
      return {...state, draft: undefined, error: null, loadingDelete: false};
    case DELETE_DRAFT_FAILURE:
      return {...state, error: action.payload, loadingDelete: false};
    case SAVE_DRAFT:
      return {...state, loadingSave: true, error: null};
    case SAVE_DRAFT_SUCCESS:
      return {...state, error: null, loadingSave: false};
    case SAVE_DRAFT_FAILURE:
      return {...state, error: action.payload, loadingSave: false};
    default:
      return state;
  }
};
