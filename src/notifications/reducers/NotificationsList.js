import {
  GET_NOTIFICATIONS,
  GET_NOTIFICATIONS_SUCCESS,
  GET_NOTIFICATIONS_FAILURE,
  SET_GET_NOTIFICATIONS_QUERY,
} from 'notifications/actions/GetNotifications';

import {CREATE_NOTIFICATION_SUCCESS} from 'notifications/actions/CreateNotification';
import {RESET_GET_NOTIFICATIONS_QUERY} from '../actions/GetPublicNotifications';

const DEFAULT_NOTIFICATIONS_QUERY = {
  searchText: '',
  skip: 0,
  take: 10,
};

const INITIAL_STATE = {
  notifications: [],
  count: 0,
  getNotificationsQuery: DEFAULT_NOTIFICATIONS_QUERY,
  channelId: '',
  error: null,
  loading: false,
};

function processGetNotifications(state, channelId) {
  const {getNotificationsQuery} = state;

  return {
    ...state,
    loading: true,
    notifications: [],
    count: 0,
    channelId,
    getNotificationsQuery,
  };
}

function processGetNotificationsSuccess(state, response) {
  return {
    ...state,
    notifications: [...response.items],
    count: response.count,
    manage: response.manage,
    error: null,
    loading: false,
  };
}

function processGetNotificationsFailure(state, error) {
  return {
    ...state,
    notifications: [],
    error,
    loading: false,
  };
}

function processCreateNotificationSuccess(state, notification) {
  return {
    ...state,
    notifications: [notification, ...state.notifications],
  };
}

function processSetGetNotificationsQuery(state, query) {
  const {notifications} = state;
  const getNotificationsQuery = query;
  if (getNotificationsQuery.searchText !== state.getNotificationsQuery.searchText) {
    getNotificationsQuery.skip = INITIAL_STATE.getNotificationsQuery.skip;
    getNotificationsQuery.take = INITIAL_STATE.getNotificationsQuery.take;
  }

  return {
    ...state,
    notifications,
    getNotificationsQuery,
  };
}

function processResetGetNotificationsQuery(state) {
  return {
    ...state,
    getNotificationsQuery: DEFAULT_NOTIFICATIONS_QUERY,
  };
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_NOTIFICATIONS:
      return processGetNotifications(state, action.payload);
    case GET_NOTIFICATIONS_SUCCESS:
      return processGetNotificationsSuccess(state, action.payload);
    case GET_NOTIFICATIONS_FAILURE: {
      const error = action.payload || {message: action.payload.message};
      return processGetNotificationsFailure(state, error);
    }

    case CREATE_NOTIFICATION_SUCCESS:
      return processCreateNotificationSuccess(state, action.payload);

    case SET_GET_NOTIFICATIONS_QUERY:
      return processSetGetNotificationsQuery(state, action.payload);
    case RESET_GET_NOTIFICATIONS_QUERY:
      return processResetGetNotificationsQuery(state);

    default:
      return state;
  }
}
