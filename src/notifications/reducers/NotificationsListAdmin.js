import {
  GET_NOTIFICATIONS_ADMIN,
  GET_NOTIFICATIONS_ADMIN_SUCCESS,
  GET_NOTIFICATIONS_ADMIN_FAILURE,
  SET_GET_NOTIFICATIONS_QUERY_ADMIN,
} from 'notifications/actions/GetNotificationsAdmin';

import {
  GET_AUDIT_BY_ID,
  GET_AUDIT_BY_ID_SUCCESS,
  GET_AUDIT_BY_ID_FAILURE,
} from 'notifications/actions/GetAuditById';

const INITIAL_STATE = {
  notifications: [],
  count: 0,
  getNotificationsQuery: {
    searchText: '',
    skip: 0,
    take: 10,
  },
  channelId: '',
  error: null,
  loading: false,
  auditAdminData: {},
};

function processGetNotifications(state, channelId) {
  let {getNotificationsQuery} = state;

  return {
    ...state,
    loading: true,
    notifications: [],
    count: 0,
    channelId,
    getNotificationsQuery,
  };
}

function processGetNotificationsSuccess(state, response) {
  return {
    ...state,
    notifications: [...response.items],
    count: response.count,
    error: null,
    loading: false,
  };
}

function processGetNotificationsFailure(state, error) {
  return {
    ...state,
    error,
    loading: false,
  };
}

function processGetAuditById(state) {
  return {
    ...state,
    loadingAudit: true,
    error: null,
  };
}

function processGetAuditByIdSuccess(state, response) {
  return {
    ...state,
    auditAdminData: response,
    error: null,
    loadingAudit: false,
  };
}

function processGetAuditByIdFailure(state, error) {
  return {
    ...state,
    error: error,
    loadingAudit: false,
  };
}

function processSetGetNotificationsQuery(state, query) {
  let {notifications} = state;
  const getNotificationsQuery = query;
  if (getNotificationsQuery.searchText !== state.getNotificationsQuery.searchText) {
    getNotificationsQuery.skip = INITIAL_STATE.getNotificationsQuery.skip;
    getNotificationsQuery.take = INITIAL_STATE.getNotificationsQuery.take;
  }

  return {
    ...state,
    notifications,
    getNotificationsQuery,
  };
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_NOTIFICATIONS_ADMIN:
      return processGetNotifications(state, action.payload);
    case GET_NOTIFICATIONS_ADMIN_SUCCESS:
      return processGetNotificationsSuccess(state, action.payload);
    case GET_NOTIFICATIONS_ADMIN_FAILURE: {
      const error = action.payload || {message: action.payload.message};
      return processGetNotificationsFailure(state, error);
    }

    case GET_AUDIT_BY_ID:
      return processGetAuditById(state);
    case GET_AUDIT_BY_ID_SUCCESS:
      return processGetAuditByIdSuccess(state, action.payload);
    case GET_AUDIT_BY_ID_FAILURE: {
      const error = action.payload || {message: action.payload.message};
      return processGetAuditByIdFailure(state, error);
    }

    case SET_GET_NOTIFICATIONS_QUERY_ADMIN:
      return processSetGetNotificationsQuery(state, action.payload);

    default:
      return state;
  }
}
