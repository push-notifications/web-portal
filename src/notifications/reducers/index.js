import {combineReducers} from 'redux';
import newNotification from './NewNotification';
import notificationsList from './NotificationsList';
import notificationsListAdmin from './NotificationsListAdmin';
import NotificationFormReducer from './NotificationFormReducer';
import notification from './Notification';
import draft from './Draft';

const notificationsReducer = combineReducers({
  newNotification,
  notificationsList,
  notificationsListAdmin,
  notification,
  newNotificationForm: NotificationFormReducer,
  draft,
});

export default notificationsReducer;
