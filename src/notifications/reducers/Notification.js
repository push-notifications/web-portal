import {
  GET_NOTIFICATION_BY_ID,
  GET_NOTIFICATION_BY_ID_SUCCESS,
  GET_NOTIFICATION_BY_ID_FAILURE,
  SET_NOTIFICATION,
} from 'notifications/actions/GetNotificationById';

import {
  GET_TARGET_USERS,
  GET_TARGET_USERS_SUCCESS,
  GET_TARGET_USERS_FAILURE,
} from 'notifications/actions/GetNotificationTargetUsers';

import {
  GET_TARGET_GROUPS,
  GET_TARGET_GROUPS_SUCCESS,
  GET_TARGET_GROUPS_FAILURE,
} from 'notifications/actions/GetNotificationTargetGroups';

const INITIAL_STATE = {
  notification: null,
  channelId: '',
  error: null,
  loading: false,
  targetGroups: [],
  targetUsers: [],
  targetUsersCount: 0,
  targetGroupsCount: 0,
  loadingTargetUsers: false,
  loadingTargetGroups: false,
};

function processGetNotificationById(state, channelId) {
  let {notification} = state;

  return {
    ...state,
    loading: true,
    notification,
    channelId,
  };
}

function processGetNotificationByIdSuccess(state, notification) {
  return {
    ...state,
    notification: notification,
    error: null,
    loading: false,
  };
}

function processGetNotificationByIdFailure(state, error) {
  return {
    ...state,
    notification: null,
    error,
    loading: false,
  };
}

function processSetNotification(state) {
  return {
    ...state,
    notification: state.payload,
    error: null,
    loading: false,
  };
}

function processGetNotificationTargetUsers(state) {
  return {
    ...state,
    loadingTargetUsers: true,
    targetUsers: [],
    targetUsersCount: 0,
  };
}

function processGetNotificationTargetUsersSuccess(state, users) {
  return {
    ...state,
    targetUsers: users.items,
    targetUsersCount: users.total,
    loadingTargetUsers: false,
  };
}

function processGetNotificationTargetUsersFailure(state, error) {
  return {
    ...state,
    error,
    loadingTargetUsers: false,
  };
}

function processGetNotificationTargetGroups(state) {
  return {
    ...state,
    loadingTargetGroups: true,
    targetGroups: [],
    targetGroupsCount: 0,
  };
}

function processGetNotificationTargetGroupsSuccess(state, groups) {
  return {
    ...state,
    targetGroups: groups.items,
    targetGroupsCount: groups.total,
    loadingTargetGroups: false,
  };
}

function processGetNotificationTargetGroupsFailure(state, error) {
  return {
    ...state,
    error,
    loadingTargetGroups: false,
  };
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_NOTIFICATION_BY_ID:
      return processGetNotificationById(state, action.payload);

    case GET_NOTIFICATION_BY_ID_SUCCESS:
      return processGetNotificationByIdSuccess(state, action.payload);

    case GET_NOTIFICATION_BY_ID_FAILURE: {
      const error = action.payload || {message: action.payload.message};
      return processGetNotificationByIdFailure(state, error);
    }
    case SET_NOTIFICATION: {
      return processSetNotification(state, action.payload);
    }

    case GET_TARGET_USERS:
      return processGetNotificationTargetUsers(state, action.payload);

    case GET_TARGET_USERS_SUCCESS:
      return processGetNotificationTargetUsersSuccess(state, action.payload);

    case GET_TARGET_USERS_FAILURE: {
      const error = action.payload || {message: action.payload.message};
      return processGetNotificationTargetUsersFailure(state, error);
    }

    case GET_TARGET_GROUPS:
      return processGetNotificationTargetGroups(state, action.payload);

    case GET_TARGET_GROUPS_SUCCESS:
      return processGetNotificationTargetGroupsSuccess(state, action.payload);

    case GET_TARGET_GROUPS_FAILURE: {
      const error = action.payload || {message: action.payload.message};
      return processGetNotificationTargetGroupsFailure(state, error);
    }

    default:
      return state;
  }
}
