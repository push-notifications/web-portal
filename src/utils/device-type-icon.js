import React from 'react';
import {Icon, Popup} from 'semantic-ui-react';
import './device-type-icon.scss';

const iconFromType = (type, subType) => {
  if (type === 'MAIL') return <Icon size="large" color="grey" name="mail outline" />;
  // if (type === 'BROWSER' && subType === 'SAFARI') return <Icon size="large" color="grey" name="safari" />;
  if (type === 'BROWSER') return <Icon className="browser-icon" size="large" color="grey" />;
  if (type === 'APP') return <Icon className="chat" size="large" color="grey" />;
  if (type === 'PHONE') return <Icon className="mobile alternate" size="large" color="grey" />;
  if (type === 'NO DEVICE') return <Icon className="warning sign" size="large" color="red" />;

  return <Icon size="large" color="grey" name="question circle outline" />;
};

const DeviceTypeIcon = props => {
  const expandedStr = (() => {
    switch (props.type) {
      case '_other_':
        return 'Apps';
      case 'PHONE':
        return 'SMS';
      default:
        return props.type.charAt(0) + props.type.substring(1).toLowerCase();
    }
  })();

  return (
    <>
      <Popup trigger={iconFromType(props.type, props.subType)} position="top center">
        {expandedStr}
      </Popup>
      {props.expanded && ` ${expandedStr}`}
    </>
  );
};

export default DeviceTypeIcon;
