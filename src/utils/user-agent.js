const os = ['Win', 'Linux', 'Mac', 'Android', 'iPhone']; // add OS values
const br = ['Chrome', 'Firefox', 'MSIE', 'Edge', 'Safari', 'Opera']; // add browser values

export function getClientInformation() {
  return (
    (os.find(v => navigator.platform.indexOf(v) >= 0) || 'Other') +
    ' ' +
    (br.find(v => navigator.userAgent.indexOf(v) >= 0) || 'Other')
  );
}

export function isSafari() {
  return (
    navigator.vendor &&
    navigator.vendor.indexOf('Apple') > -1 &&
    navigator.userAgent &&
    navigator.userAgent.indexOf('CriOS') === -1 &&
    navigator.userAgent.indexOf('FxiOS') === -1
  );
}

//export default getClientInformation;
