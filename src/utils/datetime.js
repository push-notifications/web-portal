import moment from 'moment-timezone';

const TIMEZONE_CH = 'Europe/Zurich';
const TIME_FORMAT = 'HH:mm:ss';
const DATE_FORMAT = 'DD-MM-YYYY';
const DATETIME_FORMAT = `${DATE_FORMAT} ${TIME_FORMAT}`;

const DISPLAY_DATETIME_FORMAT = `D MMM YYYY, H:mm (z)`;
const CALENDAR_FORMAT = {
  sameDay: '[Today], H:mm (z)',
  nextDay: '[Tomorrow], H:mm (z)',
  lastDay: '[Yesterday], H:mm (z)',
  nextWeek: `dddd ${DISPLAY_DATETIME_FORMAT}`,
  lastWeek: `dddd ${DISPLAY_DATETIME_FORMAT}`,
  sameElse: DISPLAY_DATETIME_FORMAT,
};

const getDisplayDatetime = (date, tz = TIMEZONE_CH, f = CALENDAR_FORMAT) => {
  return moment.tz(date, moment.ISO_8601, tz).calendar(f);
};

const isTimeInFuture = time => {
  return moment.tz(time, moment.ISO_8601, TIMEZONE_CH).isAfter(moment.tz(TIMEZONE_CH));
};

const isTimeAfter = (time, after) => {
  return moment
    .tz(time, moment.ISO_8601, TIMEZONE_CH)
    .isAfter(moment.tz(after, moment.ISO_8601, TIMEZONE_CH));
};

const loadDate = date => {
  return moment.tz(date, moment.ISO_8601, TIMEZONE_CH);
};

const datetimeToDisplay = (date, f = CALENDAR_FORMAT) => {
  return date.calendar(f);
};

const isDatetimeInPast = date => {
  return date.isBefore(moment.tz(TIMEZONE_CH));
};

const isTimeInPast = time => {
  return moment.tz(time, TIME_FORMAT, TIMEZONE_CH).isBefore(moment.tz(TIMEZONE_CH));
};

export {
  getDisplayDatetime,
  isTimeInFuture,
  isTimeInPast,
  isTimeAfter,
  isDatetimeInPast,
  loadDate,
  datetimeToDisplay,
};
export {TIMEZONE_CH, TIME_FORMAT, DATE_FORMAT, DATETIME_FORMAT};
