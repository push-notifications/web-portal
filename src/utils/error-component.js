import React from 'react';
import {Message, Icon} from 'semantic-ui-react';
import {isAuthenticated, buildAuthorizeUrl} from 'auth/utils/authUtils';

const ErrorComponent = props => {
  let icon = 'info circle';
  let header = 'Error';
  let helper;

  if (props.error.status === 404) {
    icon = 'question circle';
    header = 'Not Found';
  } else if (props.error.status === 403 || props.error.status === 401) {
    icon = 'lock';
    header = 'Access denied';

    if (isAuthenticated()) {
      helper = (
        <div>
          <Icon name="info circle" />
          You are not authorized to access this page.
          <br />
          <br />
          You can try signing in with another account or open a support &nbsp;
          <a href="https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=notifications-service">
            ticket
          </a>
          &nbsp; if you believe you should have access to this page.
        </div>
      );
    } else {
      helper = (
        <div>
          <Icon name="info circle" />
          You are not authenticated.&nbsp;
          <a title="Sign in to your CERN account" href={buildAuthorizeUrl()}>
            Please login and try again
          </a>
          .
        </div>
      );
    }
  }

  return (
    <Message negative icon>
      <Icon name={icon} />
      <Message.Content>
        <Message.Header>{header}</Message.Header>

        {props.error.status !== 403 && props.error.status !== 401
          ? props.error.response.message || props.error
          : ''}
        {helper}
      </Message.Content>
    </Message>
  );
};

export default ErrorComponent;
