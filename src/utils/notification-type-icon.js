import React from 'react';
import {Icon, Popup} from 'semantic-ui-react';

const NotificationIcon = props => {
  if (!props.priority) return null;
  const priority = props.priority.toUpperCase();
  if (priority === 'LOW' || priority === 'NORMAL') return null;

  let icon = <Icon name="info circle" color="green" size="small" />;
  if (priority === 'CRITICAL') icon = <Icon name="exclamation triangle" color="red" size="small" />;
  else if (priority === 'IMPORTANT') icon = <Icon name="exclamation" color="orange" size="small" />;

  return (
    <Popup trigger={icon} position="top center">
      Priority: {priority ? priority.charAt(0) + priority.substring(1).toLowerCase() : ''}
    </Popup>
  );
};

export default NotificationIcon;
