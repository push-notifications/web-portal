import React from 'react';
import {Icon, Image, Popup} from 'semantic-ui-react';

const iconFromVisibility = value => {
  if (value === 'PUBLIC') return 'world';
  // RESTRICTED
  return 'lock';
};

const VisibilityIcon = props => {
  const expandedStr = props.value.charAt(0) + props.value.substring(1).toLowerCase();

  return (
    <>
      <Popup
        trigger={
          props.value === 'INTERNAL' ? (
            <i className="icon" style={{margin: '0 4px 0 2px'}}>
              <Image
                centered
                src="/images/cern-logo-grey.svg"
                style={{width: 17, maxWidth: 'unset'}}
              />
            </i>
          ) : (
            <Icon color="grey" style={{fontSize: '1.1em'}} name={iconFromVisibility(props.value)} />
          )
        }
        position="top center"
      >
        {expandedStr}
      </Popup>
      {props.expanded && ` ${expandedStr}`}
    </>
  );
};

export default VisibilityIcon;
