import {isRSAA, apiMiddleware} from 'redux-api-middleware';

import isEqual from 'lodash/isEqual';
import {
  REFRESH_TOKEN_SUCCESS,
  REFRESH_TOKEN_FAILURE,
  refreshAccessToken,
} from './auth/actions/RefreshToken';
import {
  getRefreshToken,
  isAccessTokenExpired,
  buildAuthorizeUrl,
  deleteSavedTokens,
} from './auth/utils/authUtils';

const redirectToSSO = () => {
  window.location = buildAuthorizeUrl();
};

const listIncludesObject = (list, obj) => {
  console.log(
    list,
    obj,
    list.some(elem => JSON.stringify(elem) === JSON.stringify(obj))
  );
  return list.some(elem => isEqual(elem, obj));
};

export function createApiMiddleware() {
  const postponedRSAAs = [];

  return ({dispatch, getState}) => {
    const rsaaMiddleware = apiMiddleware({dispatch, getState});
    return next => action => {
      const nextCheckPostponed = nextAction => {
        // Run postponed actions after token refresh
        if (nextAction.type === REFRESH_TOKEN_SUCCESS) {
          console.debug('nextCheckPostponed');
          console.debug(nextAction.type);
          next(nextAction);
          postponedRSAAs.forEach(postponed => {
            postponedRSAAs.splice(postponedRSAAs.indexOf(postponed), 1);
            rsaaMiddleware(next)(postponed);
          });
        } else if (nextAction.type === REFRESH_TOKEN_FAILURE) {
          console.debug('REFRESH_TOKEN_FAILURE, redirecting to SSO');
          // Clear access and refresh tokens, otherwise we'll have an endless loop
          deleteSavedTokens();
          next(redirectToSSO());
          // eslint-disable-next-line no-empty
        } else {
          next(nextAction);
        }
      };

      if (isRSAA(action)) {
        const refreshToken = getRefreshToken();
        if (refreshToken && isAccessTokenExpired()) {
          console.debug('Access token is expired but we have refresh token');
          postponedRSAAs.push(action);
          console.debug('postponed RSAAs: ', postponedRSAAs, action);

          const {
            auth: {loginInProgress},
          } = getState();
          if (loginInProgress) return;

          if (postponedRSAAs.length > 0) {
            // eslint-disable-next-line consistent-return
            return rsaaMiddleware(nextCheckPostponed)(refreshAccessToken());
          }
          return;
        }

        // avoid duplicate calls
        if (postponedRSAAs.length > 0 && listIncludesObject(postponedRSAAs, action)) {
          return;
        }

        // eslint-disable-next-line consistent-return
        return rsaaMiddleware(next)(action);
      }
      // eslint-disable-next-line consistent-return
      return next(action);
    };
  };
}

export default createApiMiddleware();
