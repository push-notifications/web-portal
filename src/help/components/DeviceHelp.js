import AboutDevices from 'about/components/AboutDevices';
import React, {useState} from 'react';
import {Modal, Button, Popup} from 'semantic-ui-react';

const DeviceHelp = props => {
  const [open, setOpen] = useState(false);

  return (
    <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={
        <Popup
          trigger={
            <Button
              type="button"
              onClick={() => setOpen(true)}
              icon="help circle"
              className={props.className}
            />
          }
          content="Need some help?"
        />
      }
    >
      <Modal.Header>Need some help?</Modal.Header>
      <Modal.Content>
        <AboutDevices />
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={() => setOpen(false)}>Close</Button>
      </Modal.Actions>
    </Modal>
  );
};

export default DeviceHelp;
