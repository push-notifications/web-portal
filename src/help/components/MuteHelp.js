import AboutMutes from 'about/components/AboutMutes';
import React, {useState} from 'react';
import {Modal, Button, Popup} from 'semantic-ui-react';

const MuteHelp = props => {
  const [open, setOpen] = useState(false);

  return (
    <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={
        <Popup
          trigger={<Button type="button" onClick={() => setOpen(true)} icon="help circle" />}
          content="Need some help?"
        />
      }
    >
      <Modal.Header>Need some help?</Modal.Header>
      <Modal.Content>
        <AboutMutes />
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={() => setOpen(false)}>Close</Button>
      </Modal.Actions>
    </Modal>
  );
};

export default MuteHelp;
