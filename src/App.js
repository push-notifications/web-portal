import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import PropTypes from 'prop-types';

import LoginPageContainer from 'auth/containers/pages/LoginPageContainer';
import RedirectPageContainer from 'auth/containers/pages/RedirectPageContainer';
import * as authRoutes from 'auth/routes';
import LogoutPage from './auth/pages/logoutPage/LogoutPage';
import MainPage from './common/pages/MainPage/MainPage';

const NoMatch = ({location}) => (
  <div>
    <h3>
      404 - No match for <code>{location.pathname}</code>
    </h3>
  </div>
);

NoMatch.propTypes = {location: PropTypes.isRequired};

class App extends Component {
  render() {
    return (
      <Switch>
        <Route path={authRoutes.redirectRoute.path} component={RedirectPageContainer} />
        <Route path={authRoutes.loginRoute.path} component={LoginPageContainer} />
        <Route path={authRoutes.logoutRoute.path} component={LogoutPage} />
        <Route path="/" component={MainPage} />
      </Switch>
    );
  }
}

export default App;
