import {connect} from 'react-redux';
import Snackbar from 'common/components/snackbar/Snackbar.js';
import {hideSnackbar} from 'common/actions/Snackbar';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => {
  return {
    handleClose: index => {
      dispatch(hideSnackbar(index));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Snackbar);
