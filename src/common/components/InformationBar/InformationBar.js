import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Message} from 'semantic-ui-react';

import NoPreferencesWarningBanner from 'preferences/components/NoPreferencesWarningBanner/NoPreferencesWarningBanner';
import * as getServiceStatusCreator from 'common/actions/GetServiceStatus';
import * as getPreferencesActionsCreator from 'preferences/actions/preferences';

import './InformationBar.scss';

function InformationBar({
  message,
  isAuthenticated,
  getServiceStatus,
  getPreferences,
  preferences,
  loadingPreferences,
}) {
  useEffect(() => {
    getServiceStatus();
    // Avoid race problem with channel getPreference and avoid duplicate global getPrefereces call
    if (
      isAuthenticated &&
      preferences.global.length === 0 &&
      !window.location.pathname.startsWith('/channels')
    ) {
      getPreferences(null, true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getServiceStatus, getPreferences]);

  const [messageDismissed, setMessageDismissed] = useState(
    sessionStorage.getItem('informationBarDismissed')
  );

  const handleDismiss = () => {
    sessionStorage.setItem('informationBarDismissed', 'true');
    setMessageDismissed(true);
  };
  return (
    <>
      {message && !messageDismissed && (
        <Message
          attached
          icon="info circle"
          color="blue"
          header="Service Information"
          content={<div dangerouslySetInnerHTML={{__html: message}} />}
          className="information-bar"
          onDismiss={handleDismiss}
        />
      )}

      {isAuthenticated && !loadingPreferences && preferences.global.length === 0 && (
        <NoPreferencesWarningBanner root="preferences" />
      )}
    </>
  );
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.loggedIn,
    message: state.common?.servicestatus?.message,
    preferences: state.preferences,
    loadingPreferences: state.preferences.loading,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(getServiceStatusCreator, dispatch),
    ...bindActionCreators(getPreferencesActionsCreator, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InformationBar);
