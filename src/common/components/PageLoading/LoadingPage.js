import React from 'react';
import {Segment} from 'semantic-ui-react';

const LoadingPage = () => {
  return <Segment basic loading style={{height: '100vh'}} />;
};

export default LoadingPage;
