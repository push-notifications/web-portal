import React from 'react';
import {Input, Label, Popup, Icon, Button} from 'semantic-ui-react';
import {connect} from 'react-redux';

const SearchComponent = ({value, setValue, onClick}) => {
  const onKeyPress = event => {
    if (event.key === 'Enter') {
      onClick();
    }
  };

  const onChange = event => setValue(event.target.value);

  return (
    <Input
      fluid
      action
      placeholder="Search name or description..."
      labelPosition="left corner"
      onChange={onChange}
      value={value}
      onKeyPress={onKeyPress}
    >
      <Popup
        trigger={
          <Label corner="left" style={{zIndex: 0}}>
            <Icon name="info" color="blue" />
          </Label>
        }
        content="The search is not case sensitive"
      />
      <input />
      <Button type="submit" icon="search" onClick={onClick} />
    </Input>
  );
};

export default connect(null, null)(SearchComponent);
