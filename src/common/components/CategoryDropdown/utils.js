// The logic behind the type is:
// - filter: used for the user filter. All (sub)categories have to be selectable and no "General" category
// - channel: used for create/edit channel. Only leaf categories have to be selectable and "General" category available
// - admin: used for the admin page: None (sub)categories have to be selectable and no "General" category

import {CategoryDropdownModes} from './types';

function processCategory(category, mode = CategoryDropdownModes.DEFAULT, parentCat = null) {
  const generalCat = {
    disabled: false,
    label: 'General',
    parentName: category.name,
  };
  const processedCat = {
    ...category,
    disabled: false,
    label: category.name === category.info ? category.name : `${category.info} (${category.name})`,
  };
  if (parentCat?.path) {
    processedCat.path = `${parentCat.path} - ${processedCat.label}`;
  }
  if (processedCat?.children?.length) {
    processedCat.disabled = mode !== CategoryDropdownModes.DEFAULT;
    processedCat.children =
      mode === CategoryDropdownModes.FORCE_GENERAL
        ? [
            generalCat,
            ...processedCat.children.map(childCat => processCategory(childCat, mode, processedCat)),
          ]
        : processedCat.children.map(childCat => processCategory(childCat, mode, processedCat));
  }
  return processedCat;
}

export default function prepareCategoriesForDropdown(categories, type) {
  return categories.map(category => processCategory(category, type));
}
