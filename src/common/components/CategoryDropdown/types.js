export const CategoryDropdownModes = Object.freeze({
  FORCE_GENERAL: 'FORCE_GENERAL',
  DEFAULT: 'DEFAULT',
});

export const CategoryDropdownSelectorMode = Object.freeze({
  RADIO: 'radioSelect',
  DEFAULT: 'hierarchical',
});
