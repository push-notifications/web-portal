import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import DropdownTreeSelect from 'react-dropdown-tree-select';
import * as getCategoriesActionCreators from 'channels/actions/GetCategories';
import {Dimmer, Icon, Label, Loader} from 'semantic-ui-react';
import prepareCategoriesForDropdown from './utils';
import {CategoryDropdownSelectorMode} from './types';

function searchPredicate(node, searchTerm) {
  // keep previous implementantion
  if (node.label.toLowerCase().indexOf(searchTerm) >= 0) return true;

  // fallback to search in category info
  return node.info && node.info.toLowerCase().indexOf(searchTerm) >= 0;
}

const CategoryDropdown = ({
  placeholder,
  setSelectedCategory,
  categories,
  loadingCategories,
  getCategories,
  specificClassName,
  mode,
  selectedCategory,
  onCategoryChange,
  selectMode,
  refresh,
}) => {
  const [categoryList, setCategoryList] = useState();

  useEffect(() => {
    getCategories();
  }, [refresh, getCategories]);

  useEffect(() => {
    if (!loadingCategories) {
      const data = prepareCategoriesForDropdown(categories, mode);
      setCategoryList(data);
    }
  }, [categories, loadingCategories, mode]);

  function setCategory(currentNode) {
    const isGeneral = currentNode?.label === 'General';

    const category = isGeneral
      ? {id: currentNode._parent, name: currentNode.parentName}
      : currentNode;

    setSelectedCategory(category);

    if (onCategoryChange) {
      onCategoryChange(category);
    }
  }

  return (
    <div className="CategorySegment">
      {selectedCategory && (
        <Label className="CategoryLabel">
          <div>{selectedCategory.name}</div>
          <Icon
            name="remove"
            onClick={() => {
              if (onCategoryChange) {
                onCategoryChange(null);
              }
              setSelectedCategory(undefined);
            }}
            style={{cursor: 'pointer'}}
          />
        </Label>
      )}
      {!selectedCategory &&
        (loadingCategories || categoryList === undefined ? (
          <Dimmer active inverted>
            <Loader />
          </Dimmer>
        ) : (
          <DropdownTreeSelect
            data={categoryList}
            onChange={currentNode => setCategory(currentNode)}
            keepTreeOnSearch
            keepChildrenOnSearch
            mode={selectMode || CategoryDropdownSelectorMode.DEFAULT}
            texts={{placeholder}}
            className={specificClassName}
            searchPredicate={searchPredicate}
          />
        ))}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    categories: state.channels.categories.categories,
    loadingCategories: state.channels.categories.loadingCategories,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(getCategoriesActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryDropdown);
