import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as logoutActionCreators from 'auth/actions/Logout';
import {buildAuthorizeUrl, getDecodedToken, isAuthenticated} from 'auth/utils/authUtils';
import './toolbar-ie.css';
import './toolbar.css';

const CERNToolBarAnonymous = () => {
  return (
    <div id="cern-toolbar">
      <h1>
        <a href="http://home.cern" title="CERN">
          CERN <span>Accelerating science</span>
        </a>
      </h1>

      <ul>
        <li className="cern-accountlinks">
          <a
            className="cern-account"
            href={buildAuthorizeUrl()}
            title="Sign in to your CERN account"
          >
            Sign in
          </a>
        </li>
        <li>
          <a
            className="cern-directory"
            href="http://cern.ch/directory"
            title="Search CERN resources and browse the directory"
          >
            Directory
          </a>
        </li>
      </ul>
    </div>
  );
};

const CERNToolBarAuthenticated = ({logout}) => {
  return (
    <div id="cern-toolbar">
      <h1>
        <a href="http://home.cern" title="CERN">
          CERN <span>Accelerating science</span>
        </a>
      </h1>

      <ul className="cern-signedin">
        <li className="cern-accountlinks">
          <span>
            Signed in as:{' '}
            <a
              className="account"
              href="http://cern.ch/account"
              title={`Signed in as ${getDecodedToken().name} (${getDecodedToken().cern_upn})`}
            >
              {getDecodedToken().cern_upn} (CERN)
            </a>{' '}
          </span>
          <a
            className="cern-signout"
            title="Sign out of your account"
            href={`${process.env.REACT_APP_OAUTH_LOGOUT_URI}?client_id=${process.env.REACT_APP_OAUTH_CLIENT_ID}&post_logout_redirect_uri=${process.env.REACT_APP_OAUTH_LOGOUT_URL}`}
          >
            Sign out
          </a>
        </li>
        <li>
          <a
            className="cern-directory"
            href="http://cern.ch/directory"
            title="Search CERN resources and browse the directory"
          >
            Directory
          </a>
        </li>
      </ul>
    </div>
  );
};

const CERNToolBar = ({isAuthenticated, logout}) => {
  if (isAuthenticated) return <CERNToolBarAuthenticated {...logout} />;
  return <CERNToolBarAnonymous />;
};

const mapStateToProps = state => {
  return {
    isAuthenticated: isAuthenticated(),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: bindActionCreators(logoutActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CERNToolBar);
