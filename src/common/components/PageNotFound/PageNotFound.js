import React from 'react';
import {Grid, Header, Image} from 'semantic-ui-react';

import NotFoundImage from './not_found.png';
import './PageNotFound.scss';

const PageNotFound = () => {
  return (
    <Grid container verticalAlign="middle" centered style={{height: '100%'}}>
      <Grid.Column>
        <Image src={NotFoundImage} size="small centered" />
        <Header textAlign="center" size="large">
          Sorry, the URL requested was not found
          <Header.Subheader>
            Please, check if the URL entered is correct and try it again
          </Header.Subheader>
        </Header>
      </Grid.Column>
    </Grid>
  );
};

export default PageNotFound;
