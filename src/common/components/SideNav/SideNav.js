import {withRR4, Nav, NavIcon, NavText} from 'react-sidenav';
import React from 'react';
import './SideNav.scss';

const SideNav = withRR4();

const Sidenav = props => {
  const {routes} = props;
  return (
    <SideNav highlightColor="#fff" highlightBgColor="#08c" defaultSelected="notifications">
      {routes.map(route => {
        return (
          <Nav id={route.id} key={route.id}>
            <NavIcon>
              <i className="material-icons" style={{color: 'white'}}>
                {route.icon}
              </i>
            </NavIcon>
            <NavText> {route.text} </NavText>
          </Nav>
        );
      })}
    </SideNav>
  );
};

export default Sidenav;
