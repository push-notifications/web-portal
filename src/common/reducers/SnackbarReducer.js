import {
  CREATE_NOTIFICATION_SUCCESS,
  CREATE_NOTIFICATION_FAILURE,
} from 'notifications/actions/CreateNotification';
import {HIDE_SNACKBAR, SHOW_SNACKBAR} from 'common/actions/Snackbar';

const INITIAL_STATE = [];

const processShowSnackbar = (state, message, variant) => {
  return [...state, {message, variant}];
};

const processHideSnackbar = (snackbars, index) => {
  return snackbars.filter((snackbar, i) => index !== i);
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SHOW_SNACKBAR:
      return processShowSnackbar(state, action.message, action.variant);

    case HIDE_SNACKBAR:
      return processHideSnackbar(state, action.index);

    case CREATE_NOTIFICATION_SUCCESS:
      return processShowSnackbar(state, action.message, 'success');

    case CREATE_NOTIFICATION_FAILURE:
      return processShowSnackbar(state, action.message, 'error');

    default:
      return state;
  }
};
