import {SHOW_MESSAGE} from 'common/actions/Messages';

const INITIAL_STATE = {
  message: '',
  color: '',
  timeout: 0,
};

const processShowMessage = (message, color) => {
  return {message, color};
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SHOW_MESSAGE:
      return processShowMessage(state, action.message, action.color);
    default:
      return state;
  }
};
