import {
  GET_SERVICE_STATUS,
  GET_SERVICE_STATUS_SUCCESS,
  GET_SERVICE_STATUS_FAILURE,
} from 'common/actions/GetServiceStatus';

const INITIAL_STATE = {
  message: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_SERVICE_STATUS_SUCCESS:
      return {...state, message: action.payload?.message};
    case GET_SERVICE_STATUS:
    case GET_SERVICE_STATUS_FAILURE:
      return state;

    default:
      return state;
  }
};
