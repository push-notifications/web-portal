import {combineReducers} from 'redux';
import SnackbarReducer from './SnackbarReducer';
import MessagesReducer from './MessagesReducer';
import ServiceStatusReducer from './ServiceStatusReducer';

const commonReducer = combineReducers({
  snackbars: SnackbarReducer,
  messages: MessagesReducer,
  servicestatus: ServiceStatusReducer,
});

export default commonReducer;
