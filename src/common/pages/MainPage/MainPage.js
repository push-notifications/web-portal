import React, {Fragment} from 'react';
import {Link, Switch} from 'react-router-dom';
import {Route} from 'react-router';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {Menu, Icon} from 'semantic-ui-react';

import NotificationsPage from 'notifications/pages/NotificationsPage/NotificationsPage';
import Snackbar from 'common/containers/components/SnackbarContainer';
import AllChannelsPage from 'channels/pages/AllChannelsPage/AllChannelsPage';
import CERNToolBar from 'common/components/CERNToolBar/CERNToolBar';
import InformationBar from 'common/components/InformationBar/InformationBar';
import PageNotFound from 'common/components/PageNotFound/PageNotFound';
import EditChannelPage from 'channels/pages/EditChannelPage/EditChannelPage';
import GlobalPreferences from 'preferences/pages/GlobalPreferences/GlobalPreferences';
import DevicesGlobal from 'devices/pages/DevicesGlobal';
import AboutPage from 'about/pages/AboutPage';
import MutesPage from 'mutes/pages/MutesPage';
import UnSubscribePage from 'mutes/pages/UnSubscribePage';
import AdminPage from 'admin/pages/AdminPage';

import './MainPage.scss';
import {connect} from 'react-redux';
import {isAdmin} from '../../../auth/utils/authUtils';
import LoadingPage from '../../components/PageLoading/LoadingPage';

const MainPage = props => {
  const {snackbars, isAuthenticated, loginInProgress, roles} = props;

  return (
    // eslint-disable-next-line react/jsx-fragments
    <Fragment>
      <CERNToolBar />
      <InformationBar />
      {loginInProgress ? (
        <Switch>
          <Route component={LoadingPage} />
        </Switch>
      ) : (
        <div className={classNames('flex-container', 'flex-column', 'full-height')}>
          <Menu pointing secondary>
            <Menu.Item as={Link} name="Channels" to="/">
              <Icon name="home" /> Channels
            </Menu.Item>
            {isAuthenticated && (
              <>
                <Menu.Item as={Link} name="Preferences" to="/preferences" />
                <Menu.Item as={Link} name="Mutes" to="/mutes" />
                <Menu.Item as={Link} name="Devices" to="/devices" />
              </>
            )}
            {isAdmin(roles) && <Menu.Item as={Link} name="Admin" to="/admin" />}
            <Menu.Item as={Link} name="Help" to="/help" />
          </Menu>

          <div className={classNames('flex-container', 'flex-item')}>
            <div className={classNames('flex-container', 'flex-item', 'flex-column', 'content')}>
              {snackbars.map((snackbar, index) => (
                <Snackbar key={index} index={index} {...snackbar} />
              ))}
              <Switch>
                <Route
                  exact
                  path="/channels/:channelId/notifications/:notificationId?"
                  component={NotificationsPage}
                  key={1}
                />
                <Route exact path="/channels/:channelId" component={EditChannelPage} key={2} />
                <Route exact path="/preferences" component={GlobalPreferences} key={3} />
                <Route exact path="/mutes" component={MutesPage} key={4} />
                <Route exact path="/devices" component={DevicesGlobal} key={5} />
                <Route exact path="/help" component={AboutPage} key={6} />
                <Route exact path="/admin" component={AdminPage} key={7} />
                <Route exact path="/" component={AllChannelsPage} key={8} />
                <Route
                  exact
                  path="/unsubscribe/:encryptedEmail/:email"
                  component={UnSubscribePage}
                  key={9}
                />
                <Route component={PageNotFound} />
              </Switch>
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
};

MainPage.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  loginInProgress: PropTypes.bool.isRequired,
  roles: PropTypes.array.isRequired,
};

const mapStateToProps = state => {
  return {
    errors: state.errors,
    isAuthenticated: state.auth.loggedIn,
    loginInProgress: state.auth.loginInProgress,
    roles: state.auth.roles,
    snackbars: state.common.snackbars,
  };
};

export default connect(mapStateToProps)(MainPage);
