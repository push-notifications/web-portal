export const SHOW_SNACKBAR = 'SHOW_SNACKBAR';
export const HIDE_SNACKBAR = 'HIDE_SNACKBAR';

/**
 *
 * @param {*} message
 * @param {*} variant ["error", "warning", "info", "success"]
 */
export const showSnackbar = (message, variant) => ({
  type: SHOW_SNACKBAR,
  message,
  variant,
});

export const hideSnackbar = index => ({
  type: HIDE_SNACKBAR,
  index,
});
