import {RSAA} from 'redux-api-middleware';

// GET service status Message for information bar
export const GET_SERVICE_STATUS = 'GET_SERVICE_STATUS';
export const GET_SERVICE_STATUS_SUCCESS = 'GET_SERVICE_STATUS_SUCCESS';
export const GET_SERVICE_STATUS_FAILURE = 'GET_SERVICE_STATUS_FAILURE';

export const getServiceStatus = () => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/servicestatus`,
    method: 'GET',
    credentials: 'include',
    headers: {'Content-Type': 'application/json'},
    types: [GET_SERVICE_STATUS, GET_SERVICE_STATUS_SUCCESS, GET_SERVICE_STATUS_FAILURE],
  },
});
