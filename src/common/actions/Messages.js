export const SHOW_MESSAGE = 'SHOW_MESSAGE';

/**
 *
 * @param {*} message
 * @param {*} color
 */
export const showMessage = (message, color) => ({
  type: SHOW_MESSAGE,
  message,
  color,
});
