export const submissionByEmailTypes = Object.freeze({
  EMAIL: 'EMAIL',
  MEMBERS: 'MEMBERS',
  ADMINISTRATORS: 'ADMINISTRATORS',
  EGROUP: 'EGROUP',
});
