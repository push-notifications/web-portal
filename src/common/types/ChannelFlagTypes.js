export const channelFlagTypes = Object.freeze({
  MANDATORY: 'MANDATORY',
  CRITICAL: 'CRITICAL',
});
