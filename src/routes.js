import notificationsRoute from 'notifications/routes/index';

const routes = [
  notificationsRoute,
  {
    id: 'rules',
    icon: 'notifications',
    text: 'Rules',
  },
];

export default routes;
