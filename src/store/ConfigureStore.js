import {createStore, applyMiddleware, compose} from 'redux';
import apiMiddleware from 'middleware';
import createRootReducer from 'reducers';
import {routerMiddleware} from 'connected-react-router';
import thunkMiddleware from 'redux-thunk';

export default function configureSore(history) {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  return createStore(
    createRootReducer(history), // root reducer with router state
    composeEnhancers(applyMiddleware(thunkMiddleware, apiMiddleware, routerMiddleware(history)))
  );
}
