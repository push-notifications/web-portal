import React, {useEffect, useState} from 'react';
import {Divider, Grid, Header, Menu} from 'semantic-ui-react';
import {Helmet} from 'react-helmet';
import Categories from 'admin/components/Categories';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import AuditDisplay from 'admin/components/AuditDisplay';
import EditChannelFlags from 'admin/components/EditChannelFlags';
import {Link, useLocation} from 'react-router-dom';
import {isAdmin} from '../../auth/utils/authUtils';
import ErrorComponent from '../../utils/error-component';
import ServiceMessageSegment from '../components/ServiceMessage/ServiceMessageSegment';

const TITLE = `${process.env.REACT_APP_NAME} | Admin`;
const SECTIONS = Object.freeze({
  AUDIT: '#audit',
  CATEGORIES: '#category',
  CHANNELS: '#channel',
  SERVICE_STATUS: '#servicemessage',
});

const AdminPage = ({roles}) => {
  const location = useLocation();
  const [hasAdminAccess, setHasAdminAccess] = useState(false);
  const [activeSection, setActiveSection] = useState(location.hash || SECTIONS.AUDIT);

  useEffect(() => {
    if (isAdmin(roles)) {
      setHasAdminAccess(true);
    }
  }, [roles]);

  useEffect(() => {
    setActiveSection(location.hash || SECTIONS.AUDIT);
  }, [location]);

  const handleSectionClick = (e, {to}) => {
    setActiveSection(to);
  };

  return !hasAdminAccess ? (
    <ErrorComponent error={{status: 403}} />
  ) : (
    <Grid centered columns={1} padded verticalAlign="middle">
      <Grid.Column width={10} stretched>
        <Helmet>
          <title>{TITLE}</title>
        </Helmet>
        <Header as="h3">Admin Tools</Header>
        <Divider />
        <Grid columns={2} centered stackable>
          <Grid.Column width={4}>
            <Menu vertical>
              <Menu.Item>
                <Menu.Header>Admin Tools</Menu.Header>
                <Menu.Menu>
                  <Menu.Item
                    as={Link}
                    name="Audit"
                    to={SECTIONS.AUDIT}
                    active={activeSection === SECTIONS.AUDIT}
                    onClick={handleSectionClick}
                  />
                  <Menu.Item
                    as={Link}
                    name="Channels"
                    to={SECTIONS.CHANNELS}
                    active={activeSection === SECTIONS.CHANNELS}
                    onClick={handleSectionClick}
                  />
                  <Menu.Item
                    as={Link}
                    to={SECTIONS.CATEGORIES}
                    name="Categories"
                    active={activeSection === SECTIONS.CATEGORIES}
                    onClick={handleSectionClick}
                  />
                  <Menu.Item
                    as={Link}
                    to={SECTIONS.SERVICE_STATUS}
                    name="Service Status"
                    active={activeSection === SECTIONS.SERVICE_STATUS}
                    onClick={handleSectionClick}
                  />
                </Menu.Menu>
              </Menu.Item>
            </Menu>
          </Grid.Column>

          <Grid.Column stretched width={12}>
            {activeSection === SECTIONS.CHANNELS && <EditChannelFlags />}
            {activeSection === SECTIONS.CATEGORIES && <Categories />}
            {activeSection === SECTIONS.AUDIT && <AuditDisplay />}
            {activeSection === SECTIONS.SERVICE_STATUS && <ServiceMessageSegment />}
          </Grid.Column>
        </Grid>
      </Grid.Column>
    </Grid>
  );
};

AdminPage.propTypes = {
  roles: PropTypes.array.isRequired,
};

const mapStateToProps = state => {
  return {
    roles: state.auth.roles,
  };
};

export default connect(mapStateToProps, null)(AdminPage);
