import {combineReducers} from 'redux';
import audit from './audit';
import serviceStatus from './servicestatus';

const adminReducer = combineReducers({
  audit,
  serviceStatus,
});

export default adminReducer;
