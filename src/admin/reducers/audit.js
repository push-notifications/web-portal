import {GET_AUDIT, GET_AUDIT_SUCCESS, GET_AUDIT_FAILURE} from 'admin/actions/GetAudit';

const INITIAL_STATE = {
  auditData: {},
  error: null,
  loadingAudit: false,
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_AUDIT:
      return {...state, loadingAudit: true, error: null};
    case GET_AUDIT_SUCCESS:
      return {
        ...state,
        auditData: action.payload,
        loadingAudit: false,
      };
    case GET_AUDIT_FAILURE:
      return {...state, error: action.payload, loadingAudit: false};

    default:
      return state;
  }
}
