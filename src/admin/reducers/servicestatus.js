import {
  SET_SERVICE_STATUS_MESSAGE,
  SET_SERVICE_STATUS_MESSAGE_FAILURE,
  SET_SERVICE_STATUS_MESSAGE_SUCCESS,
} from 'admin/actions/SetServiceStatusMessage';
import {
  UPDATE_SERVICE_STATUS_MESSAGE,
  UPDATE_SERVICE_STATUS_MESSAGE_FAILURE,
  UPDATE_SERVICE_STATUS_MESSAGE_SUCCESS,
} from 'admin/actions/UpdateServiceStatusMessage';
import {
  GET_ALL_SERVICE_STATUS_MESSAGE,
  GET_ALL_SERVICE_STATUS_MESSAGE_FAILURE,
  GET_ALL_SERVICE_STATUS_MESSAGE_SUCCESS,
  SET_PAGINATION,
} from 'admin/actions/GetAllServiceStatusMessage';
import {
  DELETE_SERVICE_STATUS_MESSAGE,
  DELETE_SERVICE_STATUS_MESSAGE_FAILURE,
  DELETE_SERVICE_STATUS_MESSAGE_SUCCESS,
} from 'admin/actions/DeleteServiceStatusMessage';

const INITIAL_STATE = {
  items: [],
  count: 0,
  error: null,
  loadingGet: false,
  loadingSet: false,
  loadingDel: false,
  pagination: {
    skip: 0,
    take: 10,
  },
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_SERVICE_STATUS_MESSAGE:
      return {...state, loadingSet: true, error: null};
    case SET_SERVICE_STATUS_MESSAGE_SUCCESS:
      return {
        ...state,
        success: action.payload,
        loadingSet: false,
      };
    case SET_SERVICE_STATUS_MESSAGE_FAILURE:
      return {...state, error: action.payload, loadingSet: false};

    case UPDATE_SERVICE_STATUS_MESSAGE:
      return {...state, loadingSet: true, error: null};
    case UPDATE_SERVICE_STATUS_MESSAGE_SUCCESS:
      return {
        ...state,
        error: null,
        loadingSet: false,
      };
    case UPDATE_SERVICE_STATUS_MESSAGE_FAILURE:
      return {...state, error: action.payload, loadingSet: false};

    case GET_ALL_SERVICE_STATUS_MESSAGE:
      return {...state, loadingGet: true, error: null};
    case GET_ALL_SERVICE_STATUS_MESSAGE_SUCCESS:
      return {
        ...state,
        count: action.payload.count,
        items: action.payload.items,
        loadingGet: false,
      };
    case GET_ALL_SERVICE_STATUS_MESSAGE_FAILURE:
      return {...state, error: action.payload, loadingGet: false};

    case DELETE_SERVICE_STATUS_MESSAGE:
      return {...state, loadingDel: true, error: null};
    case DELETE_SERVICE_STATUS_MESSAGE_SUCCESS:
      return {
        ...state,
        error: null,
        loadingDel: false,
      };
    case DELETE_SERVICE_STATUS_MESSAGE_FAILURE:
      return {...state, error: action.payload, loadingDel: false};

    case SET_PAGINATION:
      return {...state, pagination: {...state.pagination, ...action.pagination}};

    default:
      return state;
  }
}
