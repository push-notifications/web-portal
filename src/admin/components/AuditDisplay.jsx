import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Button, Form, Header, Modal} from 'semantic-ui-react';
import isEmpty from 'lodash/isEmpty';

import * as getAuditActionCreator from 'admin/actions/GetAudit';
import * as showSnackBarActionCreator from 'common/actions/Snackbar';
import JSONTree from 'react-json-tree';
import {GET_CHANNEL_PATH, GET_NOTIFICATION_PATH, GET_IP_ADDRESS_PATH} from 'admin/actions/GetAudit';

const AuditDisplay = ({showSnackbar, loadingAudit, auditData, getAudit}) => {
  const [channelId, setChannelId] = useState('');
  const [notificationId, setNotificationId] = useState('');
  const [IPAddress, setIPAddress] = useState('');

  const [open, setOpen] = useState(false);
  const [downloadId, setDownloadId] = useState('');

  const theme = {
    scheme: 'monokai',
    author: 'wimer hazenberg (http://www.monokai.nl)',
    base00: '#272822',
    base01: '#383830',
    base02: '#49483e',
    base03: '#75715e',
    base04: '#a59f85',
    base05: '#f8f8f2',
    base06: '#f5f4f1',
    base07: '#f9f8f5',
    base08: '#f92672',
    base09: '#fd971f',
    base0A: '#f4bf75',
    base0B: '#a6e22e',
    base0C: '#a1efe4',
    base0D: '#66d9ef',
    base0E: '#ae81ff',
    base0F: '#cc6633',
  };

  async function loadAudit(prefix, searchId) {
    if (!searchId) return;

    const response = await getAudit(prefix, searchId);
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while getting audit information',
        'error'
      );
    } else {
      setOpen(true);
      setDownloadId(`${prefix}-${searchId}`);
      showSnackbar('The audit has been loaded successfully', 'success');
    }
  }

  return (
    <>
      <Header as="h2">Audit</Header>
      <Form loading={loadingAudit}>
        <Header as="h3">Notification audit</Header>
        <Form.Group inline>
          <label>Notification ID</label>
          <Form.Input
            width={6}
            name="notificationId"
            placeholder="Notification ID"
            // value={searchId}
            onChange={(e, d) => setNotificationId(d.value)}
          />
          <Button type="button" onClick={() => loadAudit(GET_NOTIFICATION_PATH, notificationId)}>
            Show Notification Audit
          </Button>
        </Form.Group>
        <Header as="h3">Channel audit</Header>
        <Form.Group inline>
          <label>Channel ID</label>
          <Form.Input
            width={6}
            name="channelId"
            placeholder="Channel ID"
            // value={searchId}
            onChange={(e, d) => setChannelId(d.value)}
          />
          <Button type="button" onClick={() => loadAudit(GET_CHANNEL_PATH, channelId)}>
            Show Channel Audit
          </Button>
        </Form.Group>
        <Header as="h3">Security audit</Header>
        <Form.Group inline>
          <label>IP address</label>
          <Form.Input
            width={6}
            name="ipaddress"
            placeholder="IP address"
            onChange={(e, d) => setIPAddress(d.value)}
          />
          <Button type="button" onClick={() => loadAudit(GET_IP_ADDRESS_PATH, IPAddress)}>
            IP address audit
          </Button>
        </Form.Group>
        <Modal scrolling open={open} onClose={() => setOpen(false)} size="large">
          <Modal.Header>Audit</Modal.Header>
          <Modal.Content scrolling>
            {isEmpty(auditData) ? (
              'There is no content for this audit or the audit target does not exist'
            ) : (
              <JSONTree
                data={auditData}
                hideRoot
                theme={theme}
                invertTheme
                shouldExpandNode={(keyPath, data, level) => {
                  return !keyPath.includes('target_users');
                }}
              />
            )}
          </Modal.Content>
          <Modal.Actions>
            <Button
              as="a"
              href={URL.createObjectURL(
                new Blob([JSON.stringify(auditData)], {type: 'application/json'})
              )}
              download={`${downloadId}.json`}
              icon="download"
            />
            <Button onClick={() => setOpen(false)}>Close</Button>
          </Modal.Actions>
        </Modal>
      </Form>
    </>
  );
};

AuditDisplay.propTypes = {
  showSnackbar: PropTypes.func.isRequired,
  loadingAudit: PropTypes.bool,
  auditData: PropTypes.oneOfType([PropTypes.object]),
  getAudit: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return {
    loadingAudit: state.admin.audit.loadingAudit,
    auditData: state.admin.audit.auditData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(showSnackBarActionCreator, dispatch),
    ...bindActionCreators(getAuditActionCreator, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AuditDisplay);
