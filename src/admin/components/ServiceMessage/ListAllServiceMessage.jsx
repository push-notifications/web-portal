import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';

import {Dimmer, Divider, Header, Loader} from 'semantic-ui-react';

import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import ServiceMessageItems from './ServiceMessageItems';
import * as showSnackBarActionCreator from '../../../common/actions/Snackbar';
import * as updateServiceStatusActionCreators from '../../actions/UpdateServiceStatusMessage';
import * as deleteServiceStatusActionCreators from '../../actions/DeleteServiceStatusMessage';

const ListAllServiceMessage = ({
  items,
  setRefreshList,
  onPageChange,
  count,
  skip,
  take,
  deleteServiceStatus,
  updateServiceStatus,
  showSnackbar,
}) => {
  const [totalPages, setTotalPages] = useState(1);
  const [activePage, setActivePage] = useState(1);

  useEffect(() => {
    setTotalPages(Math.ceil(count / take));
  }, [count, take]);

  useEffect(() => {
    setActivePage(skip / take + 1);
  }, [skip, take]);

  async function handleSubmit(id, message, validity) {
    const response = await updateServiceStatus({id, message, validity});
    if (response?.error) {
      showSnackbar(
        response?.payload?.response?.message ||
          'An error occurred while setting the service status message',
        'error'
      );
      return;
    }
    setRefreshList(message);
    showSnackbar('The service status message has been set successfully', 'success');
  }

  async function deleteServiceStatusMessage(message) {
    const response = await deleteServiceStatus(message.id);
    if (response?.error) {
      showSnackbar(
        response?.payload?.response?.message ||
          'An error occurred while removing the service status message',
        'error'
      );
      return;
    }
    showSnackbar('The service status message has been deleted successfully', 'success');
    setRefreshList(message);
  }

  return (
    <>
      <Divider section hidden />
      <Header as="h4">Manage service status messages</Header>
      {items ? (
        <ServiceMessageItems
          count={count}
          activePage={activePage}
          onPageChange={onPageChange}
          items={items}
          setRefreshList={setRefreshList}
          deleteServiceStatusMessage={deleteServiceStatusMessage}
          handleSubmit={handleSubmit}
          totalPages={totalPages}
        />
      ) : (
        <Dimmer active inverted>
          <Loader />
        </Dimmer>
      )}
    </>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(showSnackBarActionCreator, dispatch),
    ...bindActionCreators(updateServiceStatusActionCreators, dispatch),
    ...bindActionCreators(deleteServiceStatusActionCreators, dispatch),
  };
};

ListAllServiceMessage.propTypes = {
  setRefreshList: PropTypes.func.isRequired,
  onPageChange: PropTypes.func.isRequired,
  take: PropTypes.number.isRequired,
  skip: PropTypes.number.isRequired,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  count: PropTypes.number.isRequired,
  updateServiceStatus: PropTypes.func.isRequired,
  deleteServiceStatus: PropTypes.func.isRequired,
  showSnackbar: PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(ListAllServiceMessage);
