import React from 'react';
import CKEditor from 'notifications/components/SendNotificationComponent/CKEditor';

export const ServiceStatusCKEditor = ({message, dEventFun, dEventData}) => {
  return (
    <CKEditor
      config={{
        removePlugins: 'image,blockquote',
      }}
      data={message}
      dispatchEvent={d =>
        dEventFun({
          ...dEventData,
          message: d.payload.editor.getData(),
        })
      }
      uniqueName="service-message-editor"
    />
  );
};
