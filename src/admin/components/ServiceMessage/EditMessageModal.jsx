import React, {useState} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';

import {Button, Form, Message, Modal} from 'semantic-ui-react';
import * as showSnackBarActionCreator from 'common/actions/Snackbar';
import * as setServiceStatusActionCreators from '../../actions/SetServiceStatusMessage';
import * as updateServiceStatusActionCreators from '../../actions/UpdateServiceStatusMessage';
import {ServiceStatusCKEditor} from './ServiceStatusCKEditor';

const EditMessageModal = ({
  showSnackbar,
  updateServiceStatus,
  editMessage,
  setEditMessage,
  editMessageOpenModal,
  setEditMessageOpenModal,
  setRefreshList,
}) => {
  const [serviceMessage, setServiceMessage] = useState(editMessage);

  async function handleSubmit() {
    const response = await updateServiceStatus(serviceMessage);
    if (response?.error) {
      showSnackbar(
        response?.payload?.response?.message ||
          'An error occurred while setting the service status message',
        'error'
      );
      return;
    }
    setEditMessageOpenModal(false);
    setRefreshList(serviceMessage);
    showSnackbar('The service status message has been set successfully', 'success');
  }

  const onClose = () => {
    setEditMessage(undefined);
    setEditMessageOpenModal(false);
  };

  return (
    <>
      <Modal onClose={onClose} open={editMessageOpenModal}>
        <Modal.Header>Set Message</Modal.Header>
        <Modal.Content>
          <b>Message preview</b>
          <Message
            attached
            icon="info circle"
            color="blue"
            header="Service Information"
            content={<div dangerouslySetInnerHTML={{__html: serviceMessage.message}} />}
            className="information-bar"
            style={{marginBottom: '2rem'}}
          />
          <Form>
            <Form.Field>
              <label>Status Message</label>
              <ServiceStatusCKEditor
                message={editMessage?.message}
                dEventFun={setServiceMessage}
                dEventData={serviceMessage}
              />
            </Form.Field>
            <Form.Group inline>
              <label>Validity (in days)</label>
              <Form.Input
                type="number"
                min="1"
                width={3}
                name="validity"
                value={serviceMessage.validity}
                onChange={(_e, d) =>
                  setServiceMessage({...serviceMessage, validity: Number(d.value)})
                }
              />
            </Form.Group>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={onClose}>Close</Button>
          <Button primary content="Confirm" type="submit" onClick={() => handleSubmit()} />
        </Modal.Actions>
      </Modal>
    </>
  );
};

EditMessageModal.propTypes = {
  roles: PropTypes.array.isRequired,
  setServiceStatus: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return {
    roles: state.auth.roles,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(showSnackBarActionCreator, dispatch),
    ...bindActionCreators(setServiceStatusActionCreators, dispatch),
    ...bindActionCreators(updateServiceStatusActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessageModal);
