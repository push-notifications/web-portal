import React, {useState} from 'react';
import {connect} from 'react-redux';

import {Pagination, Table} from 'semantic-ui-react';
import moment from 'moment-timezone';
import PropTypes from 'prop-types';
import EditMessageModal from './EditMessageModal';

import ServiceMessageItem from './ServiceMessageItem';
import {loadDate, TIMEZONE_CH} from '../../../utils/datetime';

const ServiceMessageItems = ({
  deleteServiceStatusMessage,
  items,
  setRefreshList,
  onPageChange,
  count,
  totalPages,
  activePage,
  handleSubmit,
}) => {
  const [editMessageInline, setEditMessageInline] = useState({id: undefined, message: undefined});
  const [editMessage, setEditMessage] = useState();
  const [editMessageOpenModal, setEditMessageOpenModal] = useState();

  const openEditModal = message => {
    setEditMessageOpenModal(true);
    setEditMessage(message);
  };

  let foundFirstActive = false;
  return (
    <>
      {editMessageOpenModal && (
        <EditMessageModal
          setEditMessage={setEditMessage}
          editMessage={editMessage}
          setEditMessageOpenModal={setEditMessageOpenModal}
          editMessageOpenModal={editMessageOpenModal}
          setRefreshList={setRefreshList}
        />
      )}

      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Preview</Table.HeaderCell>
            <Table.HeaderCell>Created</Table.HeaderCell>
            <Table.HeaderCell>Validity</Table.HeaderCell>
            <Table.HeaderCell>Actions</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {items.map(message => {
            const validUntil = loadDate(message.created).add(message.validity, 'days');
            const isExpired = validUntil.isBefore(moment.tz(TIMEZONE_CH));
            const isActive = !isExpired && !foundFirstActive;
            if (isActive) {
              foundFirstActive = true;
            }
            return (
              <ServiceMessageItem
                key={message.id}
                message={message}
                validUntil={validUntil}
                isExpired={isExpired}
                isActive={isActive}
                active={isActive}
                handleSubmit={handleSubmit}
                deleteServiceStatusMessage={deleteServiceStatusMessage}
                editMessageInline={editMessageInline}
                setEditMessageInline={setEditMessageInline}
                openEditModal={openEditModal}
              />
            );
          })}
        </Table.Body>
        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="4" textAlign="center">
              {!!count && count > 0 && (
                <Pagination
                  activePage={activePage}
                  onPageChange={(e, {activePage}) => {
                    onPageChange(activePage);
                  }}
                  totalPages={totalPages}
                />
              )}
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
    </>
  );
};

ServiceMessageItems.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  count: PropTypes.number.isRequired,
  activePage: PropTypes.number.isRequired,
  totalPages: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  setRefreshList: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  deleteServiceStatusMessage: PropTypes.func.isRequired,
};

export default connect(null, null)(ServiceMessageItems);
