import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {Segment} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import * as getAllServiceStatusActionCreators from '../../actions/GetAllServiceStatusMessage';
import CreateMessageModal from './CreateMessageModal';
import ListAllServiceMessage from './ListAllServiceMessage';
import ErrorComponent from '../../../utils/error-component';

const ServiceMessageSegment = ({
  getAllServiceStatus,
  setPagination,
  items,
  count,
  error,
  loadingGet,
  take,
  skip,
}) => {
  const [refreshList, setRefreshList] = useState();

  useEffect(() => {
    getAllServiceStatus({skip, take});
    setRefreshList(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [refreshList, skip, take]);

  const onPageChange = activePage => {
    setPagination({skip: (activePage - 1) * take});
  };

  return error ? (
    <ErrorComponent error={error} />
  ) : (
    <Segment basic loading={loadingGet}>
      <CreateMessageModal setRefreshList={setRefreshList} />

      <ListAllServiceMessage
        items={items}
        setRefreshList={setRefreshList}
        onPageChange={onPageChange}
        skip={skip}
        count={count}
        take={take}
      />
    </Segment>
  );
};

ServiceMessageSegment.propTypes = {
  getAllServiceStatus: PropTypes.func.isRequired,
  setPagination: PropTypes.func.isRequired,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  count: PropTypes.number.isRequired,
  error: PropTypes.oneOf([PropTypes.object]),
  loadingGet: PropTypes.bool.isRequired,
  take: PropTypes.number.isRequired,
  skip: PropTypes.number.isRequired,
};

ServiceMessageSegment.defaultProps = {
  error: null,
};

const mapStateToProps = state => {
  return {
    roles: state.auth.roles,
    items: state.admin.serviceStatus.items,
    count: state.admin.serviceStatus.count,
    error: state.admin.serviceStatus.error,
    loadingGet: state.admin.serviceStatus.loadingGet,
    take: state.admin.serviceStatus.pagination.take,
    skip: state.admin.serviceStatus.pagination.skip,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(getAllServiceStatusActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ServiceMessageSegment);
