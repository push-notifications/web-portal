import React from 'react';
import {connect} from 'react-redux';

import {Button, Label, Message, Popup, Table} from 'semantic-ui-react';
import {datetimeToDisplay, getDisplayDatetime} from 'utils/datetime';

import PropTypes from 'prop-types';

const ServiceMessageItem = ({
  message,
  deleteServiceStatusMessage,
  openEditModal,
  validUntil,
  isExpired,
  isActive,
}) => {
  return (
    <Table.Row positive={isActive} key={message.id}>
      <Table.Cell>
        {isActive && (
          <Label color="green" ribbon>
            Active
          </Label>
        )}
        <Message
          key={message.id}
          icon="info circle"
          color="blue"
          header="Service Information"
          content={<div dangerouslySetInnerHTML={{__html: message.message}} />}
          className="information-bar"
        />
      </Table.Cell>
      <Table.Cell>{getDisplayDatetime(message.created)}</Table.Cell>
      <Table.Cell>{isExpired ? `Expired` : datetimeToDisplay(validUntil)}</Table.Cell>
      <Table.Cell>
        <Popup
          trigger={
            <Button
              inverted
              color="blue"
              icon="pencil"
              onClick={() => {
                openEditModal(message);
              }}
              size="mini"
            />
          }
          position="top center"
          content="Edit"
        />
        <Popup
          trigger={
            <Button
              inverted
              color="red"
              icon="trash"
              onClick={() => deleteServiceStatusMessage(message)}
              size="mini"
            />
          }
          position="top center"
          content="Delete"
        />
      </Table.Cell>
    </Table.Row>
  );
};

ServiceMessageItem.propTypes = {
  deleteServiceStatusMessage: PropTypes.func.isRequired,
  openEditModal: PropTypes.func.isRequired,
  message: PropTypes.oneOf([PropTypes.object]).isRequired,
  validUntil: PropTypes.instanceOf(Date).isRequired,
  isExpired: PropTypes.bool.isRequired,
  isActive: PropTypes.bool.isRequired,
};

export default connect(null, null)(ServiceMessageItem);
