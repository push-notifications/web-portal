import React, {useState} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {Form, Message, Modal, Button} from 'semantic-ui-react';
import * as showSnackBarActionCreator from 'common/actions/Snackbar';
import PropTypes from 'prop-types';
import * as setServiceStatusActionCreators from '../../actions/SetServiceStatusMessage';
import {ServiceStatusCKEditor} from './ServiceStatusCKEditor';

const CreateMessageModal = ({showSnackbar, setServiceStatus, setRefreshList}) => {
  const [serviceMessage, setServiceMessage] = useState({message: '', validity: 3});
  const [openModal, setOpenModal] = useState();

  async function handleSubmit() {
    const response = await setServiceStatus(serviceMessage);
    if (response?.error) {
      showSnackbar(
        response?.payload?.response?.message ||
          'An error occurred while setting the service status message',
        'error'
      );
      return;
    }
    setServiceMessage({message: '', validity: 3});
    setOpenModal(false);
    setRefreshList(serviceMessage.message);
    showSnackbar('The service status message has been set successfully', 'success');
  }

  return (
    <>
      <Modal
        onClose={() => setOpenModal(false)}
        onOpen={() => setOpenModal(true)}
        open={openModal}
        trigger={<Button>Create message</Button>}
      >
        <Modal.Header>Set Message</Modal.Header>
        <Modal.Content>
          <b>Message preview</b>
          <Message
            attached
            icon="info circle"
            color="blue"
            header="Service Information"
            content={<div dangerouslySetInnerHTML={{__html: serviceMessage.message}} />}
            className="information-bar"
            style={{marginBottom: '2rem'}}
          />
          <Form>
            <Form.Field>
              <label>Status Message</label>
              <ServiceStatusCKEditor dEventFun={setServiceMessage} dEventData={serviceMessage} />
            </Form.Field>
            <Form.Group inline>
              <label>Validity (in days)</label>
              <Form.Input
                type="number"
                min="1"
                width={3}
                name="validity"
                value={serviceMessage.validity}
                onChange={(_e, d) => setServiceMessage({...serviceMessage, validity: d.value})}
              />
            </Form.Group>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={() => setOpenModal(false)}>Close</Button>
          <Button primary content="Confirm" type="submit" onClick={() => handleSubmit()} />
        </Modal.Actions>
      </Modal>
    </>
  );
};

CreateMessageModal.propTypes = {
  showSnackbar: PropTypes.func.isRequired,
  setRefreshList: PropTypes.func.isRequired,
  setServiceStatus: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(showSnackBarActionCreator, dispatch),
    ...bindActionCreators(setServiceStatusActionCreators, dispatch),
  };
};

export default connect(null, mapDispatchToProps)(CreateMessageModal);
