import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Form, Button, Header} from 'semantic-ui-react';

import * as getCategoriesActionCreators from 'channels/actions/GetCategories';
import * as createCategoryActionCreators from 'channels/actions/CreateCategory';
import * as showSnackBarActionCreator from 'common/actions/Snackbar';
import CategoryDropdown from '../../common/components/CategoryDropdown/CategoryDropdown';

const Categories = ({createCategory, showSnackbar, loadingCategories}) => {
  const [newCategory, setNewCategory] = useState({name: '', info: '', parent: ''});
  const [parentCategory, setParentCategory] = useState();
  const [refreshCategoriesToggle, setRefreshCategoriesToggle] = useState(false);

  async function handleCreateCategory() {
    const response = await createCategory(newCategory);
    if (response?.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while creating the category',
        'error'
      );
      return;
    }

    showSnackbar('The category has been created successfully', 'success');
    setNewCategory({name: '', info: '', parent: ''});
    setParentCategory();
    setRefreshCategoriesToggle(!refreshCategoriesToggle);
  }

  return (
    <>
      <Header as="h4">Add Category</Header>
      <Form loading={loadingCategories}>
        <Form.Group inline>
          <Form.Input
            width={4}
            name="name"
            placeholder="Name"
            value={newCategory.name}
            onChange={(e, d) => setNewCategory({...newCategory, name: d.value})}
          />
          <Form.Input
            width={6}
            name="info"
            placeholder="Info"
            value={newCategory.info}
            onChange={(e, d) => setNewCategory({...newCategory, info: d.value})}
          />
          <Form.Field width={6}>
            <CategoryDropdown
              refresh={refreshCategoriesToggle}
              placeholder="Parent"
              setSelectedCategory={setParentCategory}
              selectedCategory={parentCategory}
              specificClassName="CategorySelector"
              onCategoryChange={parent =>
                setNewCategory({...newCategory, parent: parent?.name || ''})
              }
            />
          </Form.Field>
          <Button type="button" onClick={handleCreateCategory}>
            Add Category
          </Button>
        </Form.Group>
      </Form>
    </>
  );
};

Categories.propTypes = {
  showSnackbar: PropTypes.func.isRequired,
  createCategory: PropTypes.func.isRequired,
  loadingCategories: PropTypes.bool.isRequired,
};

const mapStateToProps = state => {
  return {
    loadingCategories: state.channels.categories.loadingCategories,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(showSnackBarActionCreator, dispatch),
    ...bindActionCreators(getCategoriesActionCreators, dispatch),
    ...bindActionCreators(createCategoryActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories);
