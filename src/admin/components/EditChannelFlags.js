import React, {useEffect, useState} from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Button, Checkbox, Form, Label, Modal, Segment} from 'semantic-ui-react';

import * as updateChannelActionCreators from 'channels/actions/UpdateChannel';
import * as getChannelEditInfoByIdActionCreators from 'channels/actions/GetChannelEditInfoById';
import * as showSnackBarActionCreators from 'common/actions/Snackbar';
import {channelFlagTypes} from 'common/types/ChannelFlagTypes';

const EditChannelFlags = ({
  channel,
  getChannelEditInfoById,
  updateChannel,
  showSnackbar,
  loading,
  loadingUpdate,
}) => {
  const [updatedChannel, setChannel] = useState(channel);
  const [searchId, setSearchId] = useState('');
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setChannel(channel);
  }, [setChannel, channel]);

  const updateField = (event, data) => {
    setChannel({
      ...updatedChannel,
      [data.name]: data.value || data.checked,
    });
  };

  const updateFlags = value => {
    if (updatedChannel.channelFlags?.includes(value))
      setChannel({
        ...updatedChannel,
        channelFlags: updatedChannel.channelFlags.filter(s => s !== value),
      });
    else
      setChannel({
        ...updatedChannel,
        channelFlags: [...updatedChannel.channelFlags, value],
      });
  };

  function handleValidation() {
    return !/[^0-9a-zA-Z.,:;\-_!?()[\]\\#@"'=#@/& ]/.test(updatedChannel.name);
  }

  async function handleSubmit() {
    if (!handleValidation()) {
      showSnackbar(
        `Channel name ${updatedChannel.name} contains invalid characters, only [a-z][A-Z][0-9][.,:;-_!?()[]#@"\\/'=#@& ] are allowed`,
        'error'
      );
      return;
    }
    const response = await updateChannel(updatedChannel);
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while updating the channel',
        'error'
      );
    } else {
      showSnackbar('The channel has been updated successfully', 'success');
    }
  }

  async function loadChannel() {
    if (!searchId) return;

    const response = await getChannelEditInfoById(searchId, false);
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message ||
          'An error occurred while getting channel information',
        'error'
      );
    } else {
      setOpen(true);
      showSnackbar('The channel info has been loaded successfully', 'success');
    }
  }

  return (
    <>
      <Form loading={loading}>
        <Form.Group inline>
          <label>Channel ID</label>
          <Form.Input
            width={6}
            name="channelId"
            placeholder="Channel ID"
            onChange={(e, d) => setSearchId(d.value)}
          />
          <Button type="button" onClick={() => loadChannel()}>
            Load Channel to Edit
          </Button>
        </Form.Group>
      </Form>

      <Modal scrolling open={open} onClose={() => setOpen(false)} size="large">
        <Modal.Header>Edit channel flags</Modal.Header>

        <Segment basic>
          <Form onSubmit={handleSubmit} loading={loading}>
            <Form.Group inline>
              <label>Channel ID</label>
              <Form.Input
                disabled
                width={6}
                name="channelId"
                placeholder="Channel ID"
                value={searchId}
                autoFocus
              />
            </Form.Group>

            <Label color="blue" ribbon className="form-ribbon">
              Name *
            </Label>
            <Form.Input
              name="name"
              placeholder="Name"
              value={updatedChannel.name || ''}
              onChange={updateField}
              maxLength="128"
              minLength="4"
            />
            <Label color="blue" ribbon className="form-ribbon">
              Slug *
            </Label>
            <Form.Input
              disabled
              label="Identifier for mail to channel"
              name="slug"
              placeholder="Slug"
              value={updatedChannel.slug || ''}
              onChange={updateField}
            />

            <Label color="blue" ribbon className="form-ribbon">
              Channel Flags
            </Label>
            <Form.Group grouped>
              <Form.Field
                control={Checkbox}
                label="Mandatory - means members cannot unsubscribe."
                checked={updatedChannel.channelFlags?.includes(channelFlagTypes.MANDATORY)}
                value={channelFlagTypes.MANDATORY}
                onChange={() => updateFlags(channelFlagTypes.MANDATORY)}
              />
              <Form.Field
                control={Checkbox}
                label="Critical - allow sending critical level notifications on all user devices,
                bypassing all preferences."
                checked={updatedChannel.channelFlags?.includes(channelFlagTypes.CRITICAL)}
                value={channelFlagTypes.CRITICAL}
                onChange={() => updateFlags(channelFlagTypes.CRITICAL)}
              />
            </Form.Group>

            <Form.Group inline>
              <Form.Button type="submit" primary disabled={loadingUpdate} loading={loadingUpdate}>
                Save
              </Form.Button>
            </Form.Group>
          </Form>
        </Segment>
      </Modal>
    </>
  );
};

EditChannelFlags.propTypes = {
  channel: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
  }).isRequired,
};

const mapStateToProps = state => {
  return {
    channel: state.channels.channel.channel,
    loading: state.channels.channel.loading,
    loadingUpdate: state.channels.channel.loadingUpdate,
    loadingDelete: state.channels.channelsList.loadingDelete,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(showSnackBarActionCreators, dispatch),
    ...bindActionCreators(updateChannelActionCreators, dispatch),
    ...bindActionCreators(getChannelEditInfoByIdActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditChannelFlags);
