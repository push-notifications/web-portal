import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

// Get audit
export const GET_AUDIT = 'GET_AUDIT';
export const GET_AUDIT_SUCCESS = 'GET_AUDIT_SUCCESS';
export const GET_AUDIT_FAILURE = 'GET_AUDIT_FAILURE';

export const GET_CHANNEL_PATH = 'channels';
export const GET_NOTIFICATION_PATH = 'notifications';
export const GET_IP_ADDRESS_PATH = 'security';

export const getAudit = (target, id) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/audit/${target}/${id}`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [GET_AUDIT, GET_AUDIT_SUCCESS, GET_AUDIT_FAILURE],
  },
});
