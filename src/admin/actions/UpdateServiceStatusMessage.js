import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

// Update Service Status Message
export const UPDATE_SERVICE_STATUS_MESSAGE = 'UPDATE_SERVICE_STATUS_MESSAGE';
export const UPDATE_SERVICE_STATUS_MESSAGE_SUCCESS = 'UPDATE_SERVICE_STATUS_MESSAGE_SUCCESS';
export const UPDATE_SERVICE_STATUS_MESSAGE_FAILURE = 'UPDATE_SERVICE_STATUS_MESSAGE_FAILURE';

export const updateServiceStatus = (id, message, validity) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/servicestatus`,
    method: 'PUT',
    body: JSON.stringify({serviceStatusMessage: id, message, validity}),
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [
      UPDATE_SERVICE_STATUS_MESSAGE,
      UPDATE_SERVICE_STATUS_MESSAGE_SUCCESS,
      UPDATE_SERVICE_STATUS_MESSAGE_FAILURE,
    ],
  },
});
