import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

// Set Service Status Message
export const SET_SERVICE_STATUS_MESSAGE = 'SET_SERVICE_STATUS_MESSAGE';
export const SET_SERVICE_STATUS_MESSAGE_SUCCESS = 'SET_SERVICE_STATUS_MESSAGE_SUCCESS';
export const SET_SERVICE_STATUS_MESSAGE_FAILURE = 'SET_SERVICE_STATUS_MESSAGE_FAILURE';

export const setServiceStatus = (message, validity) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/servicestatus`,
    method: 'POST',
    body: JSON.stringify(message, validity),
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [
      SET_SERVICE_STATUS_MESSAGE,
      SET_SERVICE_STATUS_MESSAGE_SUCCESS,
      SET_SERVICE_STATUS_MESSAGE_FAILURE,
    ],
  },
});
