import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';
import qs from 'qs';

// Get all Service Status Message
export const GET_ALL_SERVICE_STATUS_MESSAGE = 'GET_ALL_SERVICE_STATUS_MESSAGE';
export const GET_ALL_SERVICE_STATUS_MESSAGE_SUCCESS = 'GET_ALL_SERVICE_STATUS_MESSAGE_SUCCESS';
export const GET_ALL_SERVICE_STATUS_MESSAGE_FAILURE = 'GET_ALL_SERVICE_STATUS_MESSAGE_FAILURE';

export const SET_PAGINATION = 'SET_PAGINATION';

export const getAllServiceStatus = pagination => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/servicestatus/all?${qs.stringify(pagination)}`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [
      GET_ALL_SERVICE_STATUS_MESSAGE,
      GET_ALL_SERVICE_STATUS_MESSAGE_SUCCESS,
      GET_ALL_SERVICE_STATUS_MESSAGE_FAILURE,
    ],
  },
});

export const setPagination = pagination => {
  return {
    type: SET_PAGINATION,
    pagination: pagination,
  };
};
