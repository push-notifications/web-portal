import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

// Delete a Service Status Message
export const DELETE_SERVICE_STATUS_MESSAGE = 'DELETE_SERVICE_STATUS_MESSAGE';
export const DELETE_SERVICE_STATUS_MESSAGE_SUCCESS = 'DELETE_SERVICE_STATUS_MESSAGE_SUCCESS';
export const DELETE_SERVICE_STATUS_MESSAGE_FAILURE = 'DELETE_SERVICE_STATUS_MESSAGE_FAILURE';

export const deleteServiceStatus = messageId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/servicestatus/${messageId}`,
    method: 'DELETE',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [
      DELETE_SERVICE_STATUS_MESSAGE,
      DELETE_SERVICE_STATUS_MESSAGE_SUCCESS,
      DELETE_SERVICE_STATUS_MESSAGE_FAILURE,
    ],
  },
});
