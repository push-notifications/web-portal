import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import {connectRouter} from 'connected-react-router';

import NotificationsReducer from 'notifications/reducers/index';
import AuthReducer from 'auth/reducers/index';
import commonReducer from 'common/reducers/index';
import channelsReducer from 'channels/reducers';
import preferencesReducer from 'preferences/reducers/preferences';
import mutesReducer from 'mutes/reducers/mutes';
import devicesReducer from 'devices/reducers/devices';
import adminReducer from 'admin/reducers/index';

export default history =>
  combineReducers({
    notifications: NotificationsReducer,
    channels: channelsReducer,
    preferences: preferencesReducer,
    mutes: mutesReducer,
    devices: devicesReducer,
    auth: AuthReducer,
    common: commonReducer,
    admin: adminReducer,

    // Reducers needed by  other dependencies
    router: connectRouter(history),
    form: formReducer,
  });
