import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Header, Divider, Segment} from 'semantic-ui-react';
import {Helmet} from 'react-helmet';

import PreferencesList from 'preferences/components/PreferencesList';
import * as preferencesActions from 'preferences/actions/preferences';
import NoPreferencesWarningBanner from 'preferences/components/NoPreferencesWarningBanner/NoPreferencesWarningBanner';
import ErrorComponent from 'utils/error-component';

import * as getChannelsActionCreators from 'channels/actions/GetChannels';
import ChannelPreferencesList from 'preferences/components/ChannelPreferencesList';
import * as paginationActionCreators from 'channels/actions/PaginationActions';

const TITLE = `${process.env.REACT_APP_NAME} | Preferences`;
function GlobalPreferences({
  preferences,
  getPreferences,
  loading,
  channelsLoading,
  channels,
  getChannels,
  history,
  error,
  totalNumberOfChannels,
}) {
  const [preferencesWithoutDevices, setPreferencesWithoutDevices] = useState();
  const [localChannelQuery, setLocalChannelQuery] = useState({
    take: 10,
    skip: 0,
    searchText: '',
    ownerFilter: false,
    subscribedFilter: true,
    favoritesFilter: false,
  });

  useEffect(() => {
    setPreferencesWithoutDevices(preferences.filter(preference => preference.devices.length !== 0));
  }, [preferences]);

  useEffect(() => {
    getPreferences(null, true);
  }, [getPreferences]);

  useEffect(() => {
    getChannels(localChannelQuery);
  }, [getChannels, localChannelQuery]);

  return error ? (
    <ErrorComponent error={error} />
  ) : (
    <div
      style={{
        width: 933,
        marginTop: 100,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginBottom: 50,
      }}
    >
      <Helmet>
        <title>{TITLE}</title>
      </Helmet>
      {!loading && preferences.length !== 0 && preferencesWithoutDevices?.length === 0 && (
        <NoPreferencesWarningBanner root="devices" />
      )}
      <Header as="h3">
        Preferences
        <Header.Subheader>
          Register your global and specific channel preferences for receiving notifications. <br />
          Customize when, where and how to receive notifications. Creating multiple preferences by
          many filters such as channel or notification level is possible.
        </Header.Subheader>
      </Header>
      <Divider />

      <Segment basic loading={loading}>
        <Header as="h4">Global Preferences</Header>
        <PreferencesList preferences={preferences} global allowDelete allowEdit />
      </Segment>
      <Divider hidden section />

      <Segment basic loading={loading || channelsLoading}>
        <Header as="h4">Channel preferences</Header>
        <ChannelPreferencesList
          channelList={channels}
          history={history}
          localChannelQuery={localChannelQuery}
          setLocalChannelQuery={setLocalChannelQuery}
          totalNumberOfChannels={totalNumberOfChannels}
        />
      </Segment>
    </div>
  );
}

GlobalPreferences.propTypes = {
  preferences: PropTypes.arrayOf(PropTypes.object).isRequired,
  getPreferences: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  channelsLoading: PropTypes.bool.isRequired,
  channels: PropTypes.arrayOf(PropTypes.object).isRequired,
};

const mapStateToProps = state => {
  return {
    preferences: state.preferences.global || [],
    loading: state.preferences.loading,
    channelsLoading: state.channels.channelsList.loading,
    error: state.preferences.pageError,
    channels: state.channels.channelsList.channels,
    totalNumberOfChannels: state.channels.channelsList.totalNumberOfChannels,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getPreferences: preferencesActions.getPreferences,
      ...getChannelsActionCreators,
      ...preferencesActions,
      ...paginationActionCreators,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(GlobalPreferences);
