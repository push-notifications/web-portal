import React, {useState, useEffect} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {useParams, useHistory} from 'react-router-dom';
import {Form, Radio, Modal, Button, Checkbox, Label, Segment, Dropdown} from 'semantic-ui-react';
import TimeField from 'react-simple-timefield';

import * as preferenceActions from 'preferences/actions/preferences';
import * as devicesActions from 'devices/actions/devices';
import DeviceTypeIcon from 'utils/device-type-icon';
import * as showSnackBarActionCreators from 'common/actions/Snackbar';
import {notificationPriorityTypes} from 'common/types/NotificationPriorityTypes';
import './AddPreference.scss';

const NotificationPriorityTimes = {
  startDefaultTime: '10:00',
  endDefaultTime: '11:00',
};

const FeedTimeOptions = [
  {
    key: '1',
    text: 'Morning (9am)',
    value: '09:00:00',
  },
  {
    key: '2',
    text: 'Lunch time (1pm)',
    value: '13:00:00',
  },
  {
    key: '3',
    text: 'Afternoon (5pm)',
    value: '17:00:00',
  },
  {
    key: '4',
    text: 'Night time (9pm)',
    value: '21:00:00',
  },
];

const WeeklyDayOptions = [
  {
    key: '1',
    text: 'Monday',
    value: 0,
  },
  {
    key: '2',
    text: 'Tuesday',
    value: 1,
  },
  {
    key: '3',
    text: 'Wednesday',
    value: 2,
  },
  {
    key: '4',
    text: 'Thursday',
    value: 3,
  },
  {
    key: '5',
    text: 'Friday',
    value: 4,
  },
  {
    key: '6',
    text: 'Saturday',
    value: 5,
  },
  {
    key: '7',
    text: 'Sunday',
    value: 6,
  },
];

const AddPreference = ({
  createPreference,
  updatePreference,
  showSnackbar,
  global,
  allDevices,
  getAllDevices,
  loading,
  editPreference,
}) => {
  const {channelId} = useParams();

  const initialChannelIdInput = () => {
    if (global) return null;
    if (editPreference) return editPreference.target?.id || null;

    return channelId || null;
  };

  // Initialize the form as Empty for Add, or with values for Edit
  const [channelIdInput, setChannelIdInput] = useState(initialChannelIdInput());

  const [preferenceName, setPreferenceName] = useState(editPreference ? editPreference.name : '');
  const [notificationPriority, setNotificationPriority] = useState({
    low: editPreference
      ? editPreference.notificationPriority.includes(notificationPriorityTypes.LOW)
      : true,
    normal: editPreference
      ? editPreference.notificationPriority.includes(notificationPriorityTypes.NORMAL)
      : true,
    important: editPreference
      ? editPreference.notificationPriority.includes(notificationPriorityTypes.IMPORTANT)
      : true,
  });
  const [startTime, setStartTime] = useState(editPreference ? editPreference.rangeStart : null);
  const [endTime, setEndTime] = useState(editPreference ? editPreference.rangeEnd : null);
  const [frequency, setFrequency] = useState(editPreference ? editPreference.type : 'LIVE');
  const [devices, setDevices] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const [picker, setPicker] = useState(
    editPreference && editPreference.rangeStart ? 'TimeRange' : 'AllDay'
  );
  const [scheduledTime, setScheduledTime] = useState(
    editPreference ? editPreference.scheduledTime : null
  );
  const [scheduledDay, setScheduledDay] = useState(
    editPreference ? editPreference.scheduledDay : null
  );
  const [privateFlag, setPrivateFlag] = useState(editPreference ? editPreference.private : 'ALL');

  const history = useHistory();

  useEffect(() => {
    getAllDevices();
  }, [getAllDevices]);

  // Set default target device to MAIL, or use editPreference current values if any
  useEffect(() => {
    if (editPreference && editPreference.devices) {
      setDevices(editPreference.devices);
      return;
    }
    if (allDevices.length > 0) {
      setDevices(allDevices.filter(device => device.type === 'MAIL'));
    }
  }, [allDevices, setDevices, editPreference]);

  const resetFormValues = () => {
    setChannelIdInput(initialChannelIdInput());
    setPreferenceName('');
    setNotificationPriority({
      low: true,
      normal: true,
      important: true,
    });
    setPicker('AllDay');
    setStartTime(null);
    setEndTime(null);
    setFrequency('LIVE');
    setDevices(devices.filter(device => device.type === 'MAIL'));
    setScheduledTime(null);
    setScheduledDay(null);
    setPrivateFlag('ALL');
  };

  const resetEditFormValues = () => {
    // Initialize the form as Empty for Add, or with values for Edit
    setChannelIdInput(initialChannelIdInput());
    setPreferenceName(editPreference.name);
    setNotificationPriority({
      low: editPreference.notificationPriority.includes(notificationPriorityTypes.LOW),
      normal: editPreference.notificationPriority.includes(notificationPriorityTypes.NORMAL),
      important: editPreference.notificationPriority.includes(notificationPriorityTypes.IMPORTANT),
    });
    setStartTime(editPreference.rangeStart);
    setEndTime(editPreference.rangeEnd);
    setFrequency(editPreference.type);
    setPicker(editPreference.rangeStart ? 'TimeRange' : 'AllDay');
    setDevices(editPreference.devices);
    setScheduledTime(editPreference.scheduledTime);
    setScheduledDay(editPreference.scheduledDay);
    setPrivateFlag('ALL');
  };

  async function handleSubmitUpdatePreference() {
    const response = await updatePreference(editPreference.id, {
      id: editPreference.id,
      name: preferenceName,
      target: channelIdInput,
      type: frequency,
      notificationPriority: Object.keys(notificationPriority).filter(
        key => notificationPriority[key]
      ),
      rangeStart: startTime,
      rangeEnd: endTime,
      scheduledTime,
      scheduledDay,
      devices,
      private: privateFlag,
    });
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while updating your preference',
        'error'
      );
    } else {
      showSnackbar('The preference has been updated successfully', 'success');
      setModalOpen(false);
    }
  }

  async function handleSubmitCreatePreference() {
    const response = await createPreference({
      name: preferenceName,
      target: channelIdInput,
      type: frequency,
      notificationPriority: Object.keys(notificationPriority).filter(
        key => notificationPriority[key]
      ),
      rangeStart: startTime,
      rangeEnd: endTime,
      scheduledTime,
      scheduledDay,
      devices,
      private: privateFlag,
    });
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while adding your preference',
        'error'
      );
    } else {
      showSnackbar('The preference has been created successfully', 'success');
      resetFormValues();
      setModalOpen(false);
    }
  }

  async function handleSubmit() {
    if (
      !notificationPriority.low &&
      !notificationPriority.normal &&
      !notificationPriority.important
    ) {
      showSnackbar('You have to select at least one Priority', 'error');
    } else if (!(startTime !== endTime || (startTime === null && endTime === null))) {
      showSnackbar('Start Time and End Time can´t be the same', 'error');
    } else if (devices.length === 0) {
      showSnackbar('You have to select at least one Target Device', 'error');
    } else if (editPreference) {
      await handleSubmitUpdatePreference();
    } else {
      await handleSubmitCreatePreference();
    }
  }

  const renderTargetDevices = (devicetype, targettype) => {
    let filter;
    if (devicetype === '_other_') {
      filter = allDevices.filter(
        device =>
          device.type !== 'MAIL' &&
          device.type !== 'BROWSER' &&
          device.type !== 'PHONE' &&
          device.type !== 'APP'
      );
    } else {
      filter = allDevices && allDevices.filter(device => device.type === devicetype);
    }

    return (
      filter &&
      filter.length > 0 && (
        <Form.Group inline>
          <Form.Field width="1" />
          <Form.Field width="2">
            <label>
              <DeviceTypeIcon type={devicetype} expanded />
            </label>
          </Form.Field>
          {filter.map(oneDevice => {
            return (
              <Form.Field
                key={oneDevice.id}
                control={Checkbox}
                label={oneDevice.name}
                name="LiveTargetsList"
                value={oneDevice.id}
                checked={devices && devices.some(item => item.id === oneDevice.id)}
                onClick={() => {
                  // Remove if exists, or Add
                  if (devices && devices.some(d => d.id === oneDevice.id))
                    setDevices(devices.filter(item => item.id !== oneDevice.id));
                  else setDevices([...devices, oneDevice]);
                }}
              />
            );
          })}
        </Form.Group>
      )
    );
  };

  function onClose() {
    setModalOpen(false);
    if (editPreference) resetEditFormValues();
    else resetFormValues();
  }

  return (
    <Modal
      as={Form}
      onSubmit={handleSubmit}
      className="add-preferences"
      trigger={
        editPreference ? (
          <Button icon="pencil" onClick={() => setModalOpen(true)} size="mini" />
        ) : (
          <Button primary onClick={() => setModalOpen(true)}>
            Add preference
          </Button>
        )
      }
      open={modalOpen}
      onClose={onClose}
    >
      <Modal.Header>{editPreference ? 'Edit' : 'Add'} preference</Modal.Header>
      <Segment basic>
        <Form.Field>
          {!global && channelIdInput && (
            <>
              <Label color="blue" ribbon className="form-ribbon">
                Channel id
              </Label>
              <Form.Input
                readOnly
                placeholder="Channel id"
                value={channelIdInput}
                onChange={(e, d) => setChannelIdInput(d.value)}
                disabled
              />
            </>
          )}
        </Form.Field>
        <Label color="blue" ribbon className="form-ribbon">
          Preference Name *
        </Label>
        <Form.Field>
          <Form.Input
            required
            placeholder="Preference name"
            value={preferenceName}
            onChange={(e, d) => setPreferenceName(d.value)}
            autoFocus
          />
        </Form.Field>

        <Label color="blue" ribbon className="form-ribbon">
          Notification Type
        </Label>
        {/* <Form.Field
          control={Checkbox}
          label="Apply preference to direct notifications only (targeted to user)"
          name="privateCheckbox"
          value={privateFlag}
          checked={privateFlag}
          onChange={() => setPrivateFlag(!privateFlag)}
        /> */}
        <Form.Group grouped>
          <Form.Field
            control={Radio}
            label="All"
            name="NotificationType"
            value="ALL"
            checked={privateFlag === 'ALL'}
            onChange={(object, d) => {
              setPrivateFlag(d.value);
            }}
          />
          <Form.Field
            control={Radio}
            label="General only"
            name="NotificationType"
            value="GENERAL"
            checked={privateFlag === 'GENERAL'}
            onChange={(object, d) => {
              setPrivateFlag(d.value);
            }}
          />
          <Form.Field
            control={Radio}
            label="Direct only"
            name="NotificationType"
            value="DIRECT"
            checked={privateFlag === 'DIRECT'}
            onChange={(object, d) => {
              setPrivateFlag(d.value);
            }}
          />
        </Form.Group>

        <Label color="blue" ribbon className="form-ribbon">
          Priority
        </Label>
        <Form.Group grouped>
          <Form.Field
            control={Checkbox}
            label="Important"
            name="notificationPriorityGroup"
            value={notificationPriorityTypes.IMPORTANT}
            checked={notificationPriority.important}
            onClick={() => {
              setNotificationPriority({
                ...notificationPriority,
                important: !notificationPriority.important,
              });
            }}
          />
          <Form.Field
            control={Checkbox}
            label="Normal"
            name="notificationPriorityGroup"
            value={notificationPriorityTypes.NORMAL}
            checked={notificationPriority.normal}
            onClick={() => {
              setNotificationPriority({
                ...notificationPriority,
                normal: !notificationPriority.normal,
              });
            }}
          />
          <Form.Field
            control={Checkbox}
            label="Low"
            name="notificationPriorityGroup"
            value={notificationPriorityTypes.LOW}
            checked={notificationPriority.low}
            onClick={() => {
              setNotificationPriority({
                ...notificationPriority,
                low: !notificationPriority.low,
              });
            }}
          />
        </Form.Group>

        <Label color="blue" ribbon className="form-ribbon">
          Preference Time
        </Label>
        <Form.Group grouped>
          <label>
            Select a time in which this preference applies. Ranges that finish the next day are
            accepted, e.g. 18:00-09:00
          </label>
          <Form.Field
            control={Radio}
            label="All day"
            name="TimeGroup"
            value="AllDay"
            checked={picker === 'AllDay'}
            onChange={(object, time) => {
              setPicker(time.value);
              setStartTime(null);
              setEndTime(null);
            }}
          />
          <Form.Field
            control={Radio}
            label="Specific time range"
            name="TimeGroup"
            value="TimeRange"
            checked={picker === 'TimeRange'}
            onChange={(object, time) => {
              setStartTime(NotificationPriorityTimes.startDefaultTime);
              setEndTime(NotificationPriorityTimes.endDefaultTime);
              setPicker(time.value);
            }}
          />
          {picker === 'TimeRange' ? (
            <Form.Group>
              <Form.Field
                width={2}
                className="add-preferences-time"
                label="Start hour"
                control={TimeField}
                value={NotificationPriorityTimes.startDefaultTime}
                onChange={(object, time) => {
                  setStartTime(time);
                }}
              />
              <Form.Field
                width={2}
                className="add-preferences-time"
                label="End hour"
                control={TimeField}
                value={NotificationPriorityTimes.endDefaultTime}
                onChange={(object, time) => {
                  setEndTime(time);
                }}
              />
            </Form.Group>
          ) : null}
        </Form.Group>

        <Label color="blue" ribbon className="form-ribbon">
          Frequency
        </Label>
        <Form.Group grouped>
          <Form.Field
            control={Radio}
            label="Live"
            name="frequencyGroup"
            value="LIVE"
            checked={frequency === 'LIVE'}
            onChange={(e, d) => {
              setFrequency(d.value);
              if (allDevices) setDevices(allDevices.filter(device => device.type === 'MAIL'));
              setScheduledTime(null);
              setScheduledDay(null);
            }}
          />
          {frequency === 'LIVE' ? (
            <>
              {renderTargetDevices('MAIL', 'LIVE')}
              {renderTargetDevices('BROWSER', 'LIVE')}
              {renderTargetDevices('APP', 'LIVE')}
              {renderTargetDevices('PHONE', 'LIVE')}
              {renderTargetDevices('_other_', 'LIVE')}
              <Form.Group inline>
                <Form.Field width="1" />
                <Button
                  type="reset"
                  content="Manage Devices"
                  size="mini"
                  onClick={() => {
                    history.push(`/devices`);
                  }}
                />
              </Form.Group>
            </>
          ) : null}
          <Form.Field inline className="add-preferences-feed-frequency">
            <Radio
              label="Daily"
              name="frequencyGroup"
              value="DAILY"
              checked={frequency === 'DAILY'}
              onChange={(e, d) => {
                setFrequency(d.value);
                if (allDevices) setDevices(allDevices.filter(device => device.type === 'MAIL'));
                setScheduledTime(FeedTimeOptions[0].value);
                setScheduledDay(null);
              }}
            />
            {frequency === 'DAILY' && (
              <>
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <label className="daily-frequency-time-label">at</label>
                <Dropdown
                  inline
                  onChange={(e, {value}) => setScheduledTime(value)}
                  header="Select time to receive"
                  options={FeedTimeOptions}
                  value={scheduledTime}
                />
              </>
            )}
          </Form.Field>

          {frequency === 'DAILY' && renderTargetDevices('MAIL', 'DAILY')}

          <Form.Field inline className="add-preferences-feed-frequency">
            <Radio
              label="Weekly"
              name="frequencyGroup"
              value="WEEKLY"
              checked={frequency === 'WEEKLY'}
              onChange={(e, d) => {
                setFrequency(d.value);
                if (allDevices) setDevices(allDevices.filter(device => device.type === 'MAIL'));
                setScheduledDay(WeeklyDayOptions[0].value);
                setScheduledTime(FeedTimeOptions[0].value);
              }}
            />
            {frequency === 'WEEKLY' && (
              <>
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <label className="weekly-frequency-day-label">on</label>
                <Dropdown
                  inline
                  onChange={(e, {value}) => setScheduledDay(value)}
                  header="Select day to receive"
                  options={WeeklyDayOptions}
                  value={scheduledDay}
                />
                <label className="weekly-frequency-time-label" style={{marginLeft: 12}}>
                  at
                </label>
                <Dropdown
                  inline
                  onChange={(e, {value}) => setScheduledTime(value)}
                  header="Select time to receive"
                  options={FeedTimeOptions}
                  value={scheduledTime}
                />
              </>
            )}
          </Form.Field>

          {frequency === 'WEEKLY' && renderTargetDevices('MAIL', 'WEEKLY')}

          <Form.Field inline className="add-preferences-feed-frequency">
            <Radio
              label="Monthly"
              name="frequencyGroup"
              value="MONTHLY"
              checked={frequency === 'MONTHLY'}
              onChange={(e, d) => {
                setFrequency(d.value);
                if (allDevices) setDevices(allDevices.filter(device => device.type === 'MAIL'));
                setScheduledTime(FeedTimeOptions[0].value);
                setScheduledDay(WeeklyDayOptions[0].value);
              }}
            />
            {frequency === 'MONTHLY' && (
              <>
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <label className="monthly-frequency-day-label">on every first</label>
                <Dropdown
                  inline
                  onChange={(e, {value}) => setScheduledDay(value)}
                  header="Select day to receive"
                  options={WeeklyDayOptions}
                  value={scheduledDay}
                />
                <label className="monthly-frequency-time-label" style={{marginLeft: 12}}>
                  at
                </label>
                <Dropdown
                  inline
                  onChange={(e, {value}) => setScheduledTime(value)}
                  header="Select time to receive"
                  options={FeedTimeOptions}
                  value={scheduledTime}
                />
              </>
            )}
          </Form.Field>

          {frequency === 'MONTHLY' && renderTargetDevices('MAIL', 'MONTHLY')}
        </Form.Group>
      </Segment>
      <Modal.Actions>
        <Button type="reset" onClick={onClose}>
          Cancel
        </Button>
        <Button type="submit" primary disabled={loading} loading={loading}>
          Submit
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

export default connect(
  state => ({
    allDevices: state.devices.userDevices || [],
    loading: state.preferences.loadingCreate || state.preferences.loadingUpdate,
  }),
  dispatch =>
    bindActionCreators(
      {
        createPreference: preferenceActions.createPreference,
        updatePreference: preferenceActions.updatePreference,
        ...showSnackBarActionCreators,
        getAllDevices: devicesActions.getDevices,
      },
      dispatch
    )
)(AddPreference);
