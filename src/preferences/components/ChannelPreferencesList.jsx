import React from 'react';
import {connect} from 'react-redux';
import {Button, Table, Pagination, Dropdown} from 'semantic-ui-react';
import {bindActionCreators} from 'redux';

import SearchPreferenceChannelComponent from './SearchPreferenceChannelComponent/SearchPreferenceChannelComponent';
import * as getChannelsActionCreators from '../../channels/actions/GetChannels';
import * as paginationActionCreators from '../../channels/actions/PaginationActions';

function ChannelPreferencesList({
  channelList,
  history,
  localChannelQuery,
  setLocalChannelQuery,
  totalNumberOfChannels,
}) {
  const subsChannelList = channelList.filter(channel => channel.subscribed);

  return (
    <>
      <SearchPreferenceChannelComponent
        channelQuery={localChannelQuery}
        updateChannelQuery={setLocalChannelQuery}
      />
      <Table striped>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {subsChannelList.map(channel => {
            return (
              <Table.Row key={channel.id}>
                <Table.Cell>{channel.name}</Table.Cell>
                <Table.Cell>
                  <Button
                    icon="setting"
                    label="Manage preferences"
                    floated="right"
                    onClick={() => {
                      history.push({
                        pathname: `/channels/${channel.id}/notifications`,
                        section: 'Preferences',
                      });
                    }}
                  />
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="2">
              <Dropdown
                value={localChannelQuery.take}
                onChange={(e, {value}) =>
                  setLocalChannelQuery({
                    ...localChannelQuery,
                    take: value,
                    skip: 0,
                  })
                }
                selection
                options={[
                  {key: 5, value: 5, text: 5},
                  {key: 10, value: 10, text: 10},
                  {key: 20, value: 20, text: 20},
                ]}
              />
              <Pagination
                style={{float: 'right'}}
                activePage={localChannelQuery.skip / localChannelQuery.take + 1}
                // eslint-disable-next-line no-shadow
                onPageChange={(e, {activePage}) =>
                  setLocalChannelQuery({
                    ...localChannelQuery,
                    skip: (activePage - 1) * localChannelQuery.take,
                  })
                }
                totalPages={Math.ceil(totalNumberOfChannels / localChannelQuery.take)}
              />
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
    </>
  );
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      ...getChannelsActionCreators,
      ...paginationActionCreators,
    },
    dispatch
  );
};

export default connect(null, mapDispatchToProps)(ChannelPreferencesList);
