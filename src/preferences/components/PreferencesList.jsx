import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Button, Checkbox, Popup, Table} from 'semantic-ui-react';

import * as preferenceActions from 'preferences/actions/preferences';
import AddPreference from 'preferences/components/AddPreference';
import DeviceTypeIcon from 'utils/device-type-icon';
import * as snackBarActions from 'common/actions/Snackbar';
import PreferenceHelp from 'help/components/PreferenceHelp';

const baseColumns = [
  {id: 'name', label: 'Name'},
  {id: 'targetId', label: 'Channel'},
  {id: 'notificationPriority', label: 'Priority'},
  {id: 'startTime', label: 'Start Time'},
  {id: 'endTime', label: 'End Time'},
  {id: 'type', label: 'Frequency'},
  {id: 'device', label: 'Targets'},
];

const renderDevices = preference => {
  let devices;
  if (preference.devices) devices = preference.devices.map(d => d.type);
  if (devices && devices.length > 0) {
    const types = [...new Set(devices)];
    return types.map(t => <DeviceTypeIcon key={t} type={t} />);
  }

  return <DeviceTypeIcon key="NO DEVICE" type="NO DEVICE" />;
};

function PreferencesList({
  preferences,
  global,
  channelId,
  allowAdd,
  allowDelete,
  allowEdit,
  allowToggleEnable,
  deletePreferenceById,
  toggleGlobalPreferenceForChannel,
  showSnackbar,
  loadingDelete,
}) {
  const [deleteId, setDeleteId] = useState(null);

  async function handleDelete(preference) {
    setDeleteId(preference.id);
    const response = await deletePreferenceById(preference.id);
    if (response.error) {
      setDeleteId(null);
      showSnackbar('An error occurred while deleting the preference', 'error');
    } else {
      setDeleteId(null);
      showSnackbar('Preference removed successfully', 'success');
    }
  }

  const shouldShowColumn = column =>
    column.label !== 'Channel' || (!global && column.label === 'Channel');

  return (
    <>
      <Table striped>
        <Table.Header>
          <Table.Row>
            {baseColumns.map(
              column =>
                shouldShowColumn(column) && (
                  <Table.HeaderCell key={column.id}>{column.label}</Table.HeaderCell>
                )
            )}
            <Table.HeaderCell />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {preferences.map(preference => {
            const isEnabled =
              global && channelId
                ? !preference.disabledChannels.some(c => c.id === channelId)
                : false;
            return (
              <Table.Row key={preference.id}>
                <Table.Cell>{preference.name}</Table.Cell>
                {/* eslint-disable-next-line react/no-unescaped-entities */}
                {!global && (
                  <Table.Cell>{preference.target ? preference.target.name : ''}</Table.Cell>
                )}
                <Table.Cell style={{textTransform: 'capitalize'}}>
                  {preference.notificationPriority
                    ? preference.notificationPriority.join(', ').toLowerCase()
                    : ''}
                </Table.Cell>
                <Table.Cell>{preference.rangeStart}</Table.Cell>
                <Table.Cell>{preference.rangeEnd}</Table.Cell>
                <Table.Cell>{preference.type}</Table.Cell>
                <Table.Cell>{renderDevices(preference)}</Table.Cell>
                <Table.Cell singleLine={true}>
                  {global && allowToggleEnable && (
                    <Popup
                      trigger={
                        <Checkbox
                          checked={isEnabled}
                          onChange={(_, {checked}) =>
                            toggleGlobalPreferenceForChannel(preference.id, channelId, checked)
                          }
                          toggle
                        />
                      }
                      content="Toggle the global preference for this channel"
                      position="right center"
                    />
                  )}
                  {allowEdit && <AddPreference key={preference.id} editPreference={preference} />}
                  {allowDelete && (
                    <Button
                      disabled={preference.id === deleteId && loadingDelete}
                      loading={preference.id === deleteId && loadingDelete}
                      inverted
                      color="red"
                      icon="trash"
                      onClick={() => handleDelete(preference)}
                      size="mini"
                    />
                  )}
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
      {allowAdd && <AddPreference global={global} />}
      <PreferenceHelp />
    </>
  );
}

PreferencesList.propTypes = {
  preferences: PropTypes.arrayOf(PropTypes.object).isRequired,
  global: PropTypes.bool,
  channelId: PropTypes.string,
  allowAdd: PropTypes.bool,
  allowDelete: PropTypes.bool,
  allowEdit: PropTypes.bool,
  allowToggleEnable: PropTypes.bool,
  deletePreferenceById: PropTypes.func.isRequired,
  toggleGlobalPreferenceForChannel: PropTypes.func.isRequired,
  showSnackbar: PropTypes.func.isRequired,
  loadingDelete: PropTypes.bool,
  loadingUpdate: PropTypes.bool,
};

PreferencesList.defaultProps = {
  global: false,
  channelId: null,
  allowAdd: true,
  allowDelete: false,
  allowEdit: false,
  allowToggleEnable: false,
  loadingDelete: false,
  loadingUpdate: false,
};

const mapStateToProps = state => {
  return {
    loadingDelete: state.preferences.loadingDelete,
    loadingUpdate: state.preferences.loadingUpdate,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      deletePreferenceById: preferenceActions.deletePreferenceById,
      toggleGlobalPreferenceForChannel: preferenceActions.toggleGlobalPreferenceForChannel,
      showSnackbar: snackBarActions.showSnackbar,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PreferencesList);
