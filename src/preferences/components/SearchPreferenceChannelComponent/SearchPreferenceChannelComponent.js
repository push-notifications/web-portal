import React, {useState} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as paginationActionCreators from 'channels/actions/PaginationActions';
import SearchComponent from '../../../common/components/search/SearchComponent';

const SearchPreferenceChannelComponent = ({channelQuery, updateChannelQuery}) => {
  const [text, setText] = useState(channelQuery.searchText);

  function handleClick() {
    updateChannelQuery({...channelQuery, searchText: text});
  }

  return <SearchComponent value={text} setValue={setText} onClick={handleClick} />;
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({...paginationActionCreators}, dispatch);
};

export default connect(null, mapDispatchToProps)(SearchPreferenceChannelComponent);
