import React from 'react';
import {Message, Icon} from 'semantic-ui-react';

import './NoPreferencesWarningBanner.scss';

const MessageTemplate = message => {
  return (
    <Message warning className="no-preference-warning-banner">
      <Message.Header>
        Warning <Icon name="warning sign" />
      </Message.Header>
      {message}
    </Message>
  );
};

const NoPreferencesWarningBanner = ({root}) => {
  if (root === 'preferences') {
    return MessageTemplate(
      "You disabled all global preferences. You will not receive notifications unless you define global and/or channel preferences. Please review and configure at least one global preference to make sure you don't miss anything."
    );
  }
  if (root === 'devices') {
    return MessageTemplate(
      'One or more of your global preferences is not associated with any device! Please review your  preferences and devices. You will not receive notifications if your preferences are not associated with devices (mattermost, email, etc).'
    );
  } else {
    return MessageTemplate(
      'You disabled all the preferences! You will not receive notifications unless you define global or channel preferences.'
    );
  }
};

export default NoPreferencesWarningBanner;
