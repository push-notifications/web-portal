import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const GET_PREFERENCES = 'GET_PREFERENCES';
export const GET_PREFERENCES_SUCCESS = 'GET_PREFERENCES_SUCCESS';
export const GET_PREFERENCES_FAILURE = 'GET_PREFERENCES_FAILURE';
export const GET_PREFERENCES_PAGE_ERROR = 'GET_PREFERENCES_PAGE_ERROR';

export const CREATE_PREFERENCE = 'CREATE_PREFERENCE';
export const CREATE_PREFERENCE_SUCCESS = 'CREATE_PREFERENCE_SUCCESS';
export const CREATE_PREFERENCE_FAILURE = 'CREATE_PREFERENCE_FAILURE';

export const UPDATE_PREFERENCE = 'UPDATE_PREFERENCE';
export const UPDATE_PREFERENCE_SUCCESS = 'UPDATE_PREFERENCE_SUCCESS';
export const UPDATE_PREFERENCE_FAILURE = 'UPDATE_PREFERENCE_FAILURE';

export const DELETE_PREFERENCE = 'DELETE_PREFERENCE';
export const DELETE_PREFERENCE_SUCCESS = 'DELETE_PREFERENCE_SUCCESS';
export const DELETE_PREFERENCE_FAILURE = 'DELETE_PREFERENCE_FAILURE';

export const TOGGLE_GLOBAL_PREFERENCE = 'TOGGLE_GLOBAL_PREFERENCE';
export const TOGGLE_GLOBAL_PREFERENCE_SUCCESS = 'TOGGLE_GLOBAL_PREFERENCE_SUCCESS';
export const TOGGLE_GLOBAL_PREFERENCE_FAILURE = 'TOGGLE_GLOBAL_PREFERENCE_FAILURE';

export const getPreferences = (channelId, pageError) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/preferences/${channelId || ''}`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [
      GET_PREFERENCES,
      GET_PREFERENCES_SUCCESS,
      pageError ? GET_PREFERENCES_PAGE_ERROR : GET_PREFERENCES_FAILURE,
    ],
  },
});

export const createPreference = preference => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/preferences`,
    method: 'POST',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({preference}),
    types: [CREATE_PREFERENCE, CREATE_PREFERENCE_SUCCESS, CREATE_PREFERENCE_FAILURE],
  },
});

export const deletePreferenceById = preferenceId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/preferences/${preferenceId}`,
    method: 'DELETE',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [DELETE_PREFERENCE, DELETE_PREFERENCE_SUCCESS, DELETE_PREFERENCE_FAILURE],
  },
});

export const toggleGlobalPreferenceForChannel = (preferenceId, channelId, isEnabled) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/preferences/${preferenceId}/disabled-channels/${channelId}?isEnabled=${isEnabled}`,
    method: 'POST',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [
      TOGGLE_GLOBAL_PREFERENCE,
      TOGGLE_GLOBAL_PREFERENCE_SUCCESS,
      TOGGLE_GLOBAL_PREFERENCE_FAILURE,
    ],
  },
});

export const updatePreference = (preferenceId, preference) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/preferences/${preferenceId}`,
    method: 'PUT',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({preference}),
    types: [UPDATE_PREFERENCE, UPDATE_PREFERENCE_SUCCESS, UPDATE_PREFERENCE_FAILURE],
  },
});
