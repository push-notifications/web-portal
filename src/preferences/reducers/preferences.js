import {
  GET_PREFERENCES,
  GET_PREFERENCES_SUCCESS,
  GET_PREFERENCES_FAILURE,
  GET_PREFERENCES_PAGE_ERROR,
  CREATE_PREFERENCE,
  CREATE_PREFERENCE_SUCCESS,
  CREATE_PREFERENCE_FAILURE,
  DELETE_PREFERENCE,
  DELETE_PREFERENCE_SUCCESS,
  DELETE_PREFERENCE_FAILURE,
  UPDATE_PREFERENCE,
  UPDATE_PREFERENCE_SUCCESS,
  UPDATE_PREFERENCE_FAILURE,
  TOGGLE_GLOBAL_PREFERENCE_SUCCESS,
} from 'preferences/actions/preferences';

const INITIAL_STATE = {
  global: [],
  channel: [],
  error: null,
  pageError: null,
  loading: true,
  loadingCreate: false,
  loadingUpdate: false,
  loadingDelete: false,
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_PREFERENCES:
      return {...state, loading: true, error: null, pageError: null};
    case GET_PREFERENCES_SUCCESS:
      return {
        ...state,
        global: action.payload.globalPreferences || [],
        channel: action.payload.channelPreferences || [],
        loading: false,
      };
    case GET_PREFERENCES_FAILURE:
      return {...state, error: action.payload, loading: false};
    case GET_PREFERENCES_PAGE_ERROR:
      return {...state, pageError: action.payload, loading: false};

    case CREATE_PREFERENCE:
      return {...state, loadingCreate: true, error: null, pageError: null};
    case CREATE_PREFERENCE_SUCCESS: {
      const isChannelPreference = !!action.payload.target;
      if (!isChannelPreference) {
        return {...state, loadingCreate: false, global: [...state.global, action.payload]};
      }
      return {...state, loadingCreate: false, channel: [...state.channel, action.payload]};
    }
    case CREATE_PREFERENCE_FAILURE:
      return {...state, error: action.payload, loadingCreate: false};

    case DELETE_PREFERENCE:
      return {...state, loadingDelete: true, error: null, pageError: null};
    case DELETE_PREFERENCE_SUCCESS:
      return {
        ...state,
        global: state.global.filter(p => p.id !== action.payload),
        channel: state.channel.filter(p => p.id !== action.payload),
        loadingDelete: false,
      };
    case DELETE_PREFERENCE_FAILURE:
      return {...state, error: action.payload, loadingDelete: false};

    case TOGGLE_GLOBAL_PREFERENCE_SUCCESS: {
      const globalPreference = action.payload;
      return {
        ...state,
        global: [
          ...state.global.map(pref => (pref.id !== globalPreference.id ? pref : globalPreference)),
        ],
      };
    }

    case UPDATE_PREFERENCE:
      return {...state, loadingUpdate: true, error: null, pageError: null};
    case UPDATE_PREFERENCE_SUCCESS: {
      const isChannelPreference = !!action.payload.target;
      if (!isChannelPreference) {
        // object replace avoiding mutate
        return {
          ...state,
          loadingUpdate: false,
          global: state.global.map(item => {
            if (item.id === action.payload.id) {
              // item to replace - return an updated value
              return {
                ...action.payload,
              };
            }
            // the rest keep it as-is
            return item;
          }),
        };
      }
      // object replace avoiding mutate
      return {
        ...state,
        loadingUpdate: false,
        channel: state.channel.map(item => {
          if (item.id === action.payload.id) {
            // item to replace - return an updated value
            return {
              ...action.payload,
            };
          }
          // the rest keep it as-is
          return item;
        }),
      };
    }

    case UPDATE_PREFERENCE_FAILURE:
      return {...state, error: action.payload, loadingUpdate: false};

    default:
      return state;
  }
}
