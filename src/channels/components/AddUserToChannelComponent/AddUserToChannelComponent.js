import React, {useState} from 'react';
import {Button, Modal, Form} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {useParams} from 'react-router-dom';

import * as AddUserToChannelActionCreators from 'channels/actions/AddUserToChannel';
import * as showSnackBarActionCreators from 'common/actions/Snackbar';
import * as getChannelMembersActionCreators from 'channels/actions/GetChannelMembers';

const AddGroupToChannelComponent = ({
  addUserToChannel,
  showSnackbar,
  loading,
  getChannelMembers,
  totalNumberOfMembers,
  getChannelMembersQuery,
}) => {
  const [username, updateUsername] = useState(null);
  const [modalOpen, setModelOpen] = useState(false);

  const {channelId} = useParams();

  function handleClose() {
    setModelOpen(false);
    updateUsername(null);
  }

  async function handleSubmit() {
    const response = await addUserToChannel(username, channelId);
    if (response.error) {
      handleClose();
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while adding the user',
        'error'
      );
      return;
    }
    const updateFetchResponse = await getChannelMembers(channelId, getChannelMembersQuery);
    if (updateFetchResponse.error) {
      handleClose();
      showSnackbar(
        'Member added with success, but an error ocurred while fetching the updated member list.' +
          'Please refresh the page.',
        'error'
      );
      return;
    }
    handleClose();
    showSnackbar('The user has been added successfully', 'success');
  }

  return (
    <Modal
      as={Form}
      onSubmit={handleSubmit}
      trigger={
        <Button primary onClick={() => setModelOpen(true)}>
          Add user
        </Button>
      }
      open={modalOpen}
      onClose={handleClose}
    >
      <Modal.Header>Add user</Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <Form.Input
            required
            label="User name"
            name="group-name"
            placeholder="User name or email"
            value={username}
            onChange={(e, d) => updateUsername(d.value)}
            autoFocus
          />
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button type="reset" onClick={handleClose}>
          Cancel
        </Button>
        <Button type="submit" primary disabled={loading} loading={loading}>
          Submit
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

const mapStateToProps = state => {
  return {
    getChannelMembersQuery: state.channels.channel.getChannelMembersQuery,
    totalNumberOfMembers: state.channels.channel.totalNumberOfMembers,
    loading: state.channels.channel.loadingAddUser,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(AddUserToChannelActionCreators, dispatch),
    ...bindActionCreators(showSnackBarActionCreators, dispatch),
    ...bindActionCreators(getChannelMembersActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddGroupToChannelComponent);
