import React, {useState, useEffect} from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Form, Radio, Label, Checkbox, Segment, Button} from 'semantic-ui-react';
import {withRouter} from 'react-router-dom';

import * as updateChannelActionCreators from 'channels/actions/UpdateChannel';
import * as showSnackBarActionCreators from 'common/actions/Snackbar';
import * as deleteChannelActionCreators from 'channels/actions/DeleteChannel';
import {submissionByEmailTypes} from 'common/types/ChannelSubmissionEmailTypes';
import {submissionByFormTypes} from 'common/types/ChannelSubmissionFormTypes';
import ChannelHelp from 'help/components/ChannelHelp';
import './EditChannelComponent.scss';
import CategoryDropdown from 'common/components/CategoryDropdown/CategoryDropdown';
import {CategoryDropdownModes} from 'common/components/CategoryDropdown/types';

const EditChannelComponent = ({channel, updateChannel, showSnackbar, loading, loadingUpdate}) => {
  const [updatedChannel, setUpdatedChannel] = useState(channel);
  const [selectedCategory, setSelectedCategory] = useState();

  useEffect(() => {
    setUpdatedChannel(channel);
    if (channel.category) {
      setSelectedCategory(channel.category);
    } else {
      setSelectedCategory(undefined);
    }
  }, [setUpdatedChannel, channel]);

  const updateField = (e, d) => {
    setUpdatedChannel({
      ...updatedChannel,
      [d.name]: d.value || d.checked,
    });
  };

  const updateSubmissionByEmail = value => {
    if (updatedChannel.submissionByEmail?.includes(value))
      setUpdatedChannel({
        ...updatedChannel,
        submissionByEmail: updatedChannel.submissionByEmail.filter(s => s !== value),
      });
    else
      setUpdatedChannel({
        ...updatedChannel,
        submissionByEmail: [...updatedChannel.submissionByEmail, value],
      });
  };
  const updateSubmissionByForm = value => {
    if (updatedChannel.submissionByForm?.includes(value))
      setUpdatedChannel({
        ...updatedChannel,
        submissionByForm: updatedChannel.submissionByForm.filter(s => s !== value),
      });
    else
      setUpdatedChannel({
        ...updatedChannel,
        submissionByForm: [...updatedChannel.submissionByForm, value],
      });
  };

  function handleValidation() {
    return !/[^0-9a-zA-Z.,:;\-_!?()[\]\\#@"'=#@/& ]/.test(updatedChannel.name);
  }

  async function handleSubmit() {
    if (!handleValidation()) {
      showSnackbar(
        `Channel name ${updatedChannel.name} contains invalid characters, only [a-z][A-Z][0-9][.,:;-_!?()[]#@"\\/'=#@& ] are allowed`,
        'error'
      );
      return;
    }
    const FilteredUpdatedChannel = {
      id: updatedChannel.id,
      name: updatedChannel.name,
      description: updatedChannel.description,
      archive: updatedChannel.archive,
      visibility: updatedChannel.visibility,
      sendPrivate: updatedChannel.sendPrivate,
      submissionByEmail: updatedChannel.submissionByEmail,
      submissionByForm: updatedChannel.submissionByForm,
      incomingEgroup: updatedChannel.incomingEgroup,
      incomingEmail: updatedChannel.incomingEmail,
      subscriptionPolicy: updatedChannel.subscriptionPolicy,
      category: selectedCategory,
      channelType: updatedChannel.channelType,
    };

    if (process.env.REACT_APP_FLAG_CATEGORY_ENABLED === 'true' && selectedCategory === undefined) {
      showSnackbar('Channel category is mandatory', 'error');
      return;
    }

    const response = await updateChannel(FilteredUpdatedChannel);
    if (response?.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while updating the channel',
        'error'
      );
    } else {
      showSnackbar('The channel has been updated successfully', 'success');
    }
  }

  return (
    <Segment>
      <Form onSubmit={handleSubmit} loading={loading}>
        <Label color="blue" ribbon className="form-ribbon">
          Name *
        </Label>
        <Form.Input
          name="name"
          placeholder="Name"
          value={updatedChannel.name || ''}
          onChange={updateField}
          maxLength="128"
          minLength="4"
          autoFocus
        />
        <Label color="blue" ribbon className="form-ribbon">
          Slug *
        </Label>
        <Form.Input
          disabled
          label="Identifier for mail to channel"
          name="slug"
          placeholder="Slug"
          value={updatedChannel.slug || ''}
          onChange={updateField}
        />
        <Label color="blue" ribbon className="form-ribbon">
          Short URL
        </Label>

        <Form.Group inline>
          <label>{updatedChannel.shortUrl || <i>There is no Short URL yet</i>}</label>
          <Button
            disabled={!updatedChannel.shortUrl}
            type="button"
            icon="copy"
            content="Copy"
            onClick={() => navigator.clipboard.writeText(updatedChannel.shortUrl)}
          />
        </Form.Group>

        <Label color="blue" ribbon className="form-ribbon">
          Description
        </Label>
        <Form.TextArea
          rows={2}
          placeholder="Description"
          name="description"
          value={updatedChannel.description || ''}
          onChange={updateField}
          maxLength="10000"
        />
        <Label color="blue" ribbon className="form-ribbon">
          Channel Type
        </Label>
        <Form.Group grouped>
          <label>
            Official channels will be kept after their owner&apos;s departure, the other types will
            be deleted
          </label>
          <Form.Field
            control={Radio}
            label="Official: Departments, Groups, Sections, Projects, HEP, Experiments, Conferences, Events, Topics, Clubs, ..."
            value="OFFICIAL"
            checked={updatedChannel.channelType === 'OFFICIAL'}
            onChange={() => setUpdatedChannel({...channel, channelType: 'OFFICIAL'})}
          />
          <Form.Field
            control={Radio}
            label="Personal: Personal usage, hobbies, ..."
            value="PERSONAL"
            checked={updatedChannel.channelType === 'PERSONAL'}
            onChange={() => setUpdatedChannel({...channel, channelType: 'PERSONAL'})}
          />
        </Form.Group>
        {process.env.REACT_APP_FLAG_CATEGORY_ENABLED === 'true' && (
          <Form.Field>
            <Label color="blue" ribbon className="form-ribbon">
              Setting a Category allows customized digests, filtering, grouping and targeting
              preferences *
            </Label>
            <CategoryDropdown
              placeholder="Search for a category"
              setSelectedCategory={setSelectedCategory}
              selectedCategory={selectedCategory}
              specificClassName="CategorySelector"
              mode={CategoryDropdownModes.FORCE_GENERAL}
            />
          </Form.Field>
        )}
        <Label color="blue" ribbon className="form-ribbon">
          Sending Notifications to this channel
        </Label>
        <Form.Group grouped>
          <label>Via Web or API</label>
          <Form.Field
            control={Checkbox}
            label="Allow channel Members to send notifications"
            checked={updatedChannel.submissionByForm?.includes(submissionByFormTypes.MEMBERS)}
            value={submissionByFormTypes.MEMBERS}
            onChange={() => updateSubmissionByForm(submissionByFormTypes.MEMBERS)}
          />
          <Form.Field
            disabled={!updatedChannel.APIKey}
            control={Checkbox}
            label={`Allow scripts with the API Key to send notifications.${
              updatedChannel.APIKey
                ? ''
                : ' Must first generate the key in the Advanced settings tab.'
            }`}
            checked={updatedChannel.submissionByForm?.includes(submissionByFormTypes.APIKEY)}
            value={submissionByFormTypes.APIKEY}
            onChange={() => updateSubmissionByForm(submissionByFormTypes.APIKEY)}
          />
        </Form.Group>
        <Form.Group grouped>
          <label>
            Via E-mail (
            <code style={{fontWeight: 'normal'}}>
              to: {process.env.REACT_APP_EMAIL_GATEWAY_USER}+
              {updatedChannel.slug || '<channel-slug>'}
              +&lt;level&gt;@dovecotmta.cern.ch
            </code>
            )
          </label>
          <Form.Field
            control={Checkbox}
            label="From channel Administrators"
            checked={updatedChannel.submissionByEmail?.includes(
              submissionByEmailTypes.ADMINISTRATORS
            )}
            value={submissionByEmailTypes.ADMINISTRATORS}
            onChange={() => updateSubmissionByEmail(submissionByEmailTypes.ADMINISTRATORS)}
          />
          <Form.Field
            control={Checkbox}
            label="From channel Members"
            checked={updatedChannel.submissionByEmail?.includes(submissionByEmailTypes.MEMBERS)}
            value={submissionByEmailTypes.MEMBERS}
            onChange={() => updateSubmissionByEmail(submissionByEmailTypes.MEMBERS)}
          />
          <Form.Group inline style={{margin: 0}}>
            <Form.Field
              control={Checkbox}
              label="From e-mail address"
              checked={updatedChannel.submissionByEmail?.includes(submissionByEmailTypes.EMAIL)}
              value={submissionByEmailTypes.EMAIL}
              width={4}
              onChange={() => updateSubmissionByEmail(submissionByEmailTypes.EMAIL)}
            />
            <Form.Input
              disabled={!updatedChannel.submissionByEmail?.includes(submissionByEmailTypes.EMAIL)}
              type="email"
              name="incomingEmail"
              width={6}
              placeholder="e-mail address"
              value={updatedChannel.incomingEmail || ''}
              onChange={updateField}
            />
          </Form.Group>
          {/* <Form.Group inline style={{margin: 0}} hi>
            <Form.Field
              control={Checkbox}
              label="From e-group"
              checked={updatedChannel.submissionByEmail?.includes(submissionByEmailTypes.EGROUP)}
              value={submissionByEmailTypes.EGROUP}
              width={4}
              onChange={() => updateSubmissionByEmail(submissionByEmailTypes.EGROUP)}
            />
            <Form.Input
              disabled={!updatedChannel.submissionByEmail?.includes(submissionByEmailTypes.EGROUP)}
              type="egroup"
              name="incomingEgroup"
              width={6}
              placeholder="<e-group name>@cern.ch"
              value={updatedChannel.incomingEgroup || ''}
              onChange={updateField}
            />
          </Form.Group> */}
        </Form.Group>

        <Label color="blue" ribbon className="form-ribbon">
          Allow direct notifications to members
        </Label>
        <Form.Group grouped>
          <label>Sending member direct notification can only be done by administrators.</label>
          <Form.Field
            control={Radio}
            label="Enabled"
            value="true"
            checked={updatedChannel.sendPrivate}
            onChange={() => setUpdatedChannel({...channel, sendPrivate: true})}
          />
          <Form.Field
            control={Radio}
            label="Disabled"
            value="false"
            checked={updatedChannel.sendPrivate === false}
            onChange={() => setUpdatedChannel({...channel, sendPrivate: false})}
          />
        </Form.Group>

        <Label color="blue" ribbon className="form-ribbon">
          Visibility
        </Label>
        <Form.Group grouped>
          <label>Who can view channel content</label>
          <Form.Field
            control={Radio}
            label="Public: channel content is accessible anonymously"
            value="PUBLIC"
            checked={updatedChannel.visibility === 'PUBLIC'}
            onChange={() => setUpdatedChannel({...channel, visibility: 'PUBLIC'})}
          />
          <Form.Field
            control={Radio}
            label="Internal: accessible only to CERN authenticated users"
            value="INTERNAL"
            checked={updatedChannel.visibility === 'INTERNAL'}
            onChange={() => setUpdatedChannel({...channel, visibility: 'INTERNAL'})}
          />
          <Form.Field
            control={Radio}
            label="Restricted: accessible only to channel members"
            value="RESTRICTED"
            checked={updatedChannel.visibility === 'RESTRICTED'}
            onChange={() => setUpdatedChannel({...channel, visibility: 'RESTRICTED'})}
          />
        </Form.Group>
        {false && updatedChannel.visibility !== 'RESTRICTED' && (
          <>
            <Label color="blue" ribbon className="form-ribbon">
              Subscription Policy
            </Label>
            <Form.Group inline>
              <Form.Field
                control={Radio}
                label="Self subscription"
                value="SELF_SUBSCRIPTION"
                checked={updatedChannel.subscriptionPolicy === 'SELF_SUBSCRIPTION'}
                onChange={() =>
                  setUpdatedChannel({
                    ...channel,
                    subscriptionPolicy: 'SELF_SUBSCRIPTION',
                  })
                }
              />
              <Form.Field
                control={Radio}
                label="Self subscription with approval"
                value="SELF_SUBSCRIPTION_APPROVAL"
                checked={updatedChannel.subscriptionPolicy === 'SELF_SUBSCRIPTION_APPROVAL'}
                onChange={() =>
                  setUpdatedChannel({
                    ...channel,
                    subscriptionPolicy: 'SELF_SUBSCRIPTION_APPROVAL',
                  })
                }
              />
            </Form.Group>
          </>
        )}

        <Label color="blue" ribbon className="form-ribbon">
          Archive
        </Label>
        <Form.Group grouped>
          <label>
            Notifications are deleted from the channel after a while, if you wish to keep them for a
            longer duration, please enable Archive.
          </label>
          <Form.Field
            control={Radio}
            label={
              <label>
                Enabled (archive content to{' '}
                <a
                  href={`${process.env.REACT_APP_ARCHIVE_URL}/data/${channel.id}`}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  channel's archive
                </a>
                )
              </label>
            }
            value="true"
            checked={updatedChannel.archive}
            onChange={() => setUpdatedChannel({...channel, archive: true})}
          />
          <Form.Field
            control={Radio}
            label="Disabled"
            value="false"
            checked={updatedChannel.archive === false}
            onChange={() => setUpdatedChannel({...channel, archive: false})}
          />
        </Form.Group>
        <Form.Group inline>
          <Form.Button type="submit" primary disabled={loadingUpdate} loading={loadingUpdate}>
            Save
          </Form.Button>
          <ChannelHelp />
        </Form.Group>
      </Form>
    </Segment>
  );
};

EditChannelComponent.propTypes = {
  channel: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
    visibility: PropTypes.string,
    sendPrivate: PropTypes.bool,
    subscriptionPolicy: PropTypes.string,
    archive: PropTypes.bool,
  }).isRequired,
};

const mapStateToProps = state => {
  return {
    channel: state.channels.channel.channel,
    loading: state.channels.channel.loading,
    loadingUpdate: state.channels.channel.loadingUpdate,
    loadingDelete: state.channels.channelsList.loadingDelete,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(showSnackBarActionCreators, dispatch),
    ...bindActionCreators(updateChannelActionCreators, dispatch),
    ...bindActionCreators(deleteChannelActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EditChannelComponent));
