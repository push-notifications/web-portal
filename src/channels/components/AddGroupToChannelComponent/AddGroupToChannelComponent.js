import React, {useState} from 'react';
import {Button, Modal, Form} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {useParams} from 'react-router-dom';

import * as AddGroupToChannelActionCreators from 'channels/actions/AddGroupToChannel';
import * as showSnackBarActionCreators from 'common/actions/Snackbar';
import * as getChannelGroupsActionCreators from 'channels/actions/GetChannelGroups';

const AddGroupToChannelComponent = ({
  addGroupToChannel,
  showSnackbar,
  getChannelGroups,
  getChannelGroupsQuery,
  loading,
}) => {
  const [groupName, updateGroupName] = useState(null);
  const [modalOpen, setModelOpen] = useState(false);

  const {channelId} = useParams();

  function handleClose() {
    setModelOpen(false);
    updateGroupName(null);
  }

  async function handleSubmit() {
    const response = await addGroupToChannel(groupName, channelId);
    if (response.error) {
      handleClose();
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while adding the group',
        'error'
      );
      return;
    }
    const updateFetchResponse = await getChannelGroups(channelId, getChannelGroupsQuery);
    if (updateFetchResponse.error) {
      handleClose();
      showSnackbar(
        'Group added with success, but an error ocurred while fetching the updated group member list.' +
          'Please refresh the page.',
        'error'
      );
      return;
    }
    handleClose();
    showSnackbar('The group has been added successfully', 'success');
  }

  return (
    <Modal
      as={Form}
      onSubmit={handleSubmit}
      trigger={
        <Button primary onClick={() => setModelOpen(true)}>
          Add group
        </Button>
      }
      open={modalOpen}
      onClose={handleClose}
    >
      <Modal.Header>Add Group</Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <Form.Input
            required
            label="Group name"
            name="group-name"
            placeholder="Group name"
            value={groupName}
            onChange={(e, d) => updateGroupName(d.value)}
            autoFocus
          />
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button type="reset" onClick={handleClose}>
          Cancel
        </Button>
        <Button type="submit" primary disabled={loading} loading={loading}>
          Submit
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

const mapStateToProps = state => {
  return {
    getChannelGroupsQuery: state.channels.channel.getChannelGroupsQuery,
    totalNumberOfGroups: state.channels.channel.totalNumberOfGroups,
    loading: state.channels.channel.loadingAddGroup,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(AddGroupToChannelActionCreators, dispatch),
    ...bindActionCreators(showSnackBarActionCreators, dispatch),
    ...bindActionCreators(getChannelGroupsActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddGroupToChannelComponent);
