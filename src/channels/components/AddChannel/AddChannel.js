import React, {useState} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Form, Radio, Label, Modal, Button, Segment} from 'semantic-ui-react';

import * as showSnackBarActionCreator from 'common/actions/Snackbar';
import * as createChannelActionCreator from 'channels/actions/CreateChannel';
import * as paginationActionCreators from 'channels/actions/PaginationActions';
import './AddChannel.scss';
import ChannelHelp from 'help/components/ChannelHelp';
import CategoryDropdown from 'common/components/CategoryDropdown/CategoryDropdown';
import {CategoryDropdownModes} from 'common/components/CategoryDropdown/types';

const AddChannel = ({
  createChannel,
  showSnackbar,
  getChannelsQuery,
  setGetChannelsQuery,
  loading,
  history,
}) => {
  const [channel, setChannel] = useState({
    slug: '',
    name: '',
    description: '',
    adminGroup: {
      groupIdentifier: '',
    },
    visibility: 'RESTRICTED',
    // subscriptionPolicy: 'SELF_SUBSCRIPTION',
    archive: false,
    submissionByForm: ['ADMINISTRATORS'],
    submissionByEmail: [],
    category: null,
    tags: [],
    channelType: 'PERSONAL',
  });
  const [modalOpen, setModalOpen] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState();

  const resetFormValues = () => {
    setChannel({
      slug: '',
      name: '',
      description: '',
      adminGroup: {
        groupIdentifier: '',
      },
      visibility: 'RESTRICTED',
      // subscriptionPolicy: 'SELF_SUBSCRIPTION',
      archive: false,
      submissionByForm: ['ADMINISTRATORS'],
      submissionByEmail: [],
      category: null,
      tags: [],
      channelType: 'PERSONAL',
    });
  };

  const updateField = (e, d) => {
    // Auto gen the Slug if name changed
    if (d.name === 'name') {
      setChannel({
        ...channel,
        [d.name]: d.value || d.checked,
        slug: d.value.toLowerCase().replace(/[^0-9a-z-_]/g, '-'),
      });
    } else {
      setChannel({
        ...channel,
        [d.name]: d.value || d.checked,
      });
    }
  };

  function handleClose() {
    resetFormValues();
    setModalOpen(false);
  }

  function handleValidation() {
    return !/[^0-9a-zA-Z.,:;\-_!?()[\]\\#@"'=#@/& ]/.test(channel.name);
  }

  async function handleSubmit() {
    if (!handleValidation()) {
      showSnackbar(
        `Channel name ${channel.name} contains invalid characters, only [a-z][A-Z][0-9][.,:;-_!?()[]#@"\\/'=#@& ] are allowed`,
        'error'
      );
      return;
    }
    if (channel.adminGroup?.groupIdentifier === '') delete channel.adminGroup;

    if (process.env.REACT_APP_FLAG_CATEGORY_ENABLED === 'true' && selectedCategory === undefined) {
      showSnackbar('Channel category is mandatory', 'error');
      return;
    }
    const response = await createChannel({...channel, category: selectedCategory});
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while creating the channel',
        'error'
      );
    } else {
      showSnackbar('The channel has been created successfully', 'success');
      handleClose();
      if (response.payload?.id) history.push(`/channels/${response.payload.id}`);
      else
        setGetChannelsQuery({
          ...getChannelsQuery,
          ownerFilter: true,
          subscribedFilter: false,
          skip: 0,
        });
    }
  }

  return (
    <Modal
      as={Form}
      onSubmit={handleSubmit}
      trigger={
        <Button
          basic
          fluid
          onClick={() => setModalOpen(true)}
          icon="plus"
          content="Add channel"
          labelPosition="left"
        />
      }
      open={modalOpen}
      onClose={handleClose}
      className="add-channel-modal"
    >
      <Modal.Header>Add channel</Modal.Header>
      <Segment basic>
        <Label color="blue" ribbon className="form-ribbon">
          Name *
        </Label>
        <Form.Input
          required
          name="name"
          placeholder="Name"
          value={channel.name}
          onChange={updateField}
          maxLength="128"
          minLength="4"
          autoFocus
        />
        <Label color="blue" ribbon className="form-ribbon">
          Slug *
        </Label>
        <Form.Input
          required
          label="Identifier for mail to channel"
          name="slug"
          placeholder="Slug"
          value={channel.slug}
          onChange={updateField}
        />
        <Label color="blue" ribbon className="form-ribbon">
          Description
        </Label>
        <Form.TextArea
          rows={2}
          placeholder="Description"
          name="description"
          value={channel.description}
          onChange={updateField}
          maxLength="10000"
        />
        <Label color="blue" ribbon className="form-ribbon">
          Channel Type
        </Label>
        <Form.Group grouped>
          <label>
            Official channels will be kept after their owner's departure, the other types will be
            deleted
          </label>
          <Form.Field
            control={Radio}
            label="Official: Departments, Groups, Sections, Projects, HEP, Experiments, Conferences, Events, Topics, Clubs, ..."
            value="OFFICIAL"
            checked={channel.channelType === 'OFFICIAL'}
            onChange={() => setChannel({...channel, channelType: 'OFFICIAL'})}
          />
          <Form.Field
            control={Radio}
            label="Personal: Personal usage, hobbies, ..."
            value="PERSONAL"
            checked={channel.channelType === 'PERSONAL'}
            onChange={() => setChannel({...channel, channelType: 'PERSONAL'})}
          />
        </Form.Group>
        {process.env.REACT_APP_FLAG_CATEGORY_ENABLED === 'true' && (
          <Form.Field>
            <Label color="blue" ribbon className="form-ribbon">
              Setting a Category allows customized digests, filtering, grouping and targeting
              preferences *
            </Label>
            <CategoryDropdown
              placeholder="Search for a category"
              setSelectedCategory={setSelectedCategory}
              selectedCategory={selectedCategory}
              specificClassName="CategorySelector"
              mode={CategoryDropdownModes.FORCE_GENERAL}
            />
          </Form.Field>
        )}
        <Label color="blue" ribbon className="form-ribbon">
          Visibility
        </Label>
        <Form.Group grouped>
          <label>Who can view channel content</label>
          <Form.Field
            control={Radio}
            label="Public: channel content is accessible anonymously"
            value="PUBLIC"
            checked={channel.visibility === 'PUBLIC'}
            onChange={() => setChannel({...channel, visibility: 'PUBLIC'})}
          />
          <Form.Field
            control={Radio}
            label="Internal: accessible only to CERN authenticated users"
            value="INTERNAL"
            checked={channel.visibility === 'INTERNAL'}
            onChange={() => setChannel({...channel, visibility: 'INTERNAL'})}
          />
          <Form.Field
            control={Radio}
            label="Restricted: accessible only to channel members"
            value="RESTRICTED"
            checked={channel.visibility === 'RESTRICTED'}
            onChange={() => setChannel({...channel, visibility: 'RESTRICTED'})}
          />
        </Form.Group>
      </Segment>
      <Modal.Actions>
        <ChannelHelp />
        <Button type="reset" onClick={handleClose}>
          Cancel
        </Button>
        <Button type="submit" primary disabled={loading} loading={loading}>
          Submit
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

const mapStateToProps = state => {
  return {
    getChannelsQuery: state.channels.channelsList.getChannelsQuery,
    loading: state.channels.newChannel.loading,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(createChannelActionCreator, dispatch),
    ...bindActionCreators(showSnackBarActionCreator, dispatch),
    ...bindActionCreators(paginationActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddChannel);
