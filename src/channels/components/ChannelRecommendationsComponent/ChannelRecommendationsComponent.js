import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {
  Modal,
  Button,
  Grid,
  Accordion,
  Icon,
  Message,
  Popup,
  Loader,
  Dimmer,
} from 'semantic-ui-react';
import * as showSnackBarActionCreator from 'common/actions/Snackbar';
import * as getChannelRecommendationsActionCreators from 'channels/actions/GetChannelRecommendations';
import * as subscribeToChannelRecommendationActionCreators from 'channels/actions/SubscribeToChannelRecommendation';
import * as ignoreChannelRecommendationActionCreators from 'channels/actions/IgnoreChannelRecommendation';
import * as getChannelsActionCreators from 'channels/actions/GetChannels';

const ChannelRecommendationsComponent = ({
  showSnackbar,
  getChannelRecommendations,
  subscribeToChannelRecommendation,
  ignoreChannelRecommendation,
  channelRecommendations,
  getChannels,
  getChannelsQuery,
  loading,
  loadingSubscribe,
  loadingIgnore,
  isAuthenticated,
}) => {
  const [openModal, setOpenModal] = useState(false);
  const [activeIndex, setActiveIndex] = useState(-1);
  const [subscribeId, setSubscribeId] = useState(null);
  const [ignoreId, setIgnoreId] = useState(null);
  const [needsRefreshChannels, setNeedsRefreshChannels] = useState(false);

  useEffect(() => {
    if (isAuthenticated && !loadingIgnore && !loadingSubscribe) {
      getChannelRecommendations();
    }
  }, [isAuthenticated, getChannelRecommendations, loadingSubscribe, loadingIgnore]);

  async function handleSubscribe(channelRecommendation) {
    setSubscribeId(channelRecommendation.channelId);
    const response = await subscribeToChannelRecommendation(channelRecommendation.id);
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while subscribing to channel',
        'error'
      );
    } else {
      setNeedsRefreshChannels(true);
      showSnackbar(`Subscribed to channel successfully`, 'success');
    }
    setSubscribeId(null);
  }

  async function handleIgnore(channelRecommendation) {
    setIgnoreId(channelRecommendation.channelId);
    const response = await ignoreChannelRecommendation(channelRecommendation.id);
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message ||
          'An error occurred while ignoring channel recommendation',
        'error'
      );
    } else {
      showSnackbar(`Ignored channel recommendation`, 'success');
    }
    setIgnoreId(null);
  }

  function handleClose() {
    if (needsRefreshChannels) {
      getChannels(getChannelsQuery);
    }
    setOpenModal(false);
  }

  function handleAccordionToggle(index) {
    setActiveIndex(activeIndex === index ? -1 : index);
  }

  useEffect(() => {
    if (channelRecommendations?.length) {
      setOpenModal(true);
    }
  }, [channelRecommendations]);

  return (
    <Modal onClose={() => handleClose()} onOpen={() => setOpenModal(true)} open={openModal}>
      <Modal.Header>Channel Recommendations</Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <Grid divided="vertically" verticalAlign="middle">
            {loading && (
              <Dimmer active inverted>
                <Loader content="Loading your channels" />
              </Dimmer>
            )}
            {!channelRecommendations?.length ? (
              <Grid.Row columns={1}>
                <Grid.Column textAlign="center">
                  <Message positive compact>
                    <p>
                      You have subscribed or ignored all suggested channels. Currently, there are no
                      additional suggestions for you.
                    </p>
                  </Message>
                </Grid.Column>
              </Grid.Row>
            ) : (
              channelRecommendations?.map((channelRecommendation, ix) => {
                return (
                  <Grid.Row columns={2} key={channelRecommendation.id}>
                    <Grid.Column width={11}>
                      <Accordion style={{display: 'inline'}}>
                        <Accordion.Title
                          active={activeIndex === ix}
                          index={ix}
                          onClick={() => handleAccordionToggle(ix)}
                        >
                          <Icon name="dropdown" />
                          <Popup
                            content="Open Channel Information in new Tab"
                            trigger={
                              <a
                                target="_blank"
                                href={`/channels/${channelRecommendation.channelId}/notifications`}
                                rel="noopener noreferrer"
                              >
                                {channelRecommendation.channelName}
                              </a>
                            }
                          />
                        </Accordion.Title>
                        <Accordion.Content active={activeIndex === ix}>
                          {channelRecommendation.channelDescription}
                        </Accordion.Content>
                      </Accordion>
                    </Grid.Column>
                    <Grid.Column textAlign="right" width={5}>
                      <Button
                        icon="cancel"
                        size="mini"
                        content="Ignore"
                        labelPosition="left"
                        loading={channelRecommendation.channelId === ignoreId && loadingIgnore}
                        disabled={
                          (channelRecommendation.id === subscribeId && loadingSubscribe) ||
                          (channelRecommendation.id === ignoreId && loadingIgnore)
                        }
                        onClick={() => handleIgnore(channelRecommendation)}
                      />
                      <Button
                        icon="alarm"
                        size="mini"
                        color="blue"
                        content="Subscribe"
                        labelPosition="left"
                        loading={
                          channelRecommendation.channelId === subscribeId && loadingSubscribe
                        }
                        disabled={
                          (channelRecommendation.id === subscribeId && loadingSubscribe) ||
                          (channelRecommendation.id === ignoreId && loadingIgnore)
                        }
                        onClick={() => handleSubscribe(channelRecommendation)}
                      />
                    </Grid.Column>
                  </Grid.Row>
                );
              })
            )}
          </Grid>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={() => handleClose()}>Close</Button>
      </Modal.Actions>
    </Modal>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.loggedIn,
    channelRecommendations: state.channels.channelRecommendations.recommendations,
    getChannelsQuery: state.channels.channelsList.getChannelsQuery,
    loading: state.channels.channelRecommendations.loading,
    loadingSubscribe: state.channels.channelRecommendations.loadingSubscribe,
    loadingIgnore: state.channels.channelRecommendations.loadingIgnore,
  };
};

ChannelRecommendationsComponent.propTypes = {
  showSnackbar: PropTypes.func.isRequired,
  getChannelRecommendations: PropTypes.func.isRequired,
  subscribeToChannelRecommendation: PropTypes.func.isRequired,
  ignoreChannelRecommendation: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  channelRecommendations: PropTypes.arrayOf(PropTypes.object).isRequired,
  getChannels: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  getChannelsQuery: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  loadingSubscribe: PropTypes.bool.isRequired,
  loadingIgnore: PropTypes.bool.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(showSnackBarActionCreator, dispatch),
    ...bindActionCreators(getChannelRecommendationsActionCreators, dispatch),
    ...bindActionCreators(subscribeToChannelRecommendationActionCreators, dispatch),
    ...bindActionCreators(ignoreChannelRecommendationActionCreators, dispatch),
    ...bindActionCreators(getChannelsActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChannelRecommendationsComponent);
