import React, {useState} from 'react';
import {Button, Modal, Form, Popup} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {useParams} from 'react-router-dom';

import * as updateChannelAdminGroup from 'channels/actions/UpdateChannelAdminGroup';
import * as showSnackBarActionCreators from 'common/actions/Snackbar';
import * as getChannelGroupsActionCreators from 'channels/actions/GetChannelGroups';

const UpdateChannelAdminGroupComponent = ({
  updateChannelAdminGroup,
  showSnackbar,
  getChannelGroups,
  getChannelGroupsQuery,
  loading,
  currentGroupName,
}) => {
  const [groupName, setGroupName] = useState(currentGroupName);
  const [modalOpen, setModelOpen] = useState(false);

  const {channelId} = useParams();

  function handleClose() {
    setModelOpen(false);
  }

  async function handleSubmit() {
    if (!groupName) {
      showSnackbar('The admin group is not correct', 'error');
      return;
    }
    const response = await updateChannelAdminGroup(groupName ? groupName : null, channelId);
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while updating the admin group',
        'error'
      );
      return;
    }

    handleClose();
    getChannelGroups(channelId, getChannelGroupsQuery);
    showSnackbar('The admin group has been updated successfully', 'success');
  }

  return (
    <Modal
      as={Form}
      onSubmit={handleSubmit}
      trigger={
        <Popup
          trigger={
            <Button type="button" size="mini" onClick={() => setModelOpen(true)} icon="edit" />
          }
          content={
            currentGroupName ? "Update channel's admin group" : 'Add an admin group to the channel'
          }
        />
      }
      open={modalOpen}
      onClose={handleClose}
    >
      <Modal.Header>Add Group</Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <Form.Input
            label={
              <div>
                <span style={{fontSize: 13, fontWeight: 'bold'}}>Group name </span>
                <a
                  style={{fontSize: 11}}
                  href="https://groups-portal.web.cern.ch/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  (search groups at GRAPPA's portal)
                </a>
              </div>
            }
            name="group-name"
            placeholder="Group name"
            defaultValue={currentGroupName}
            onChange={(e, d) => setGroupName(d.value)}
            autoFocus
            required
          />
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button type="reset" onClick={handleClose}>
          Cancel
        </Button>
        <Button type="submit" primary disabled={loading} loading={loading}>
          Submit
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

const mapStateToProps = state => {
  return {
    getChannelGroupsQuery: state.channels.channel.getChannelGroupsQuery,
    loading: state.channels.channel.loadingUpdateAdmin,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(updateChannelAdminGroup, dispatch),
    ...bindActionCreators(showSnackBarActionCreators, dispatch),
    ...bindActionCreators(getChannelGroupsActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateChannelAdminGroupComponent);
