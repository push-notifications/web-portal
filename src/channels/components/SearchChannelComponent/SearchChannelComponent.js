import React, {useState} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as paginationActionCreators from 'channels/actions/PaginationActions';
import SearchComponent from '../../../common/components/search/SearchComponent';

const SearchChannelComponent = ({getChannelsQuery, setGetChannelsQuery}) => {
  const [text, setText] = useState(getChannelsQuery.searchText);

  const onSearch = () => {
    setGetChannelsQuery({...getChannelsQuery, searchText: text, skip: 0});
  };

  return <SearchComponent value={text} setValue={setText} onClick={onSearch} />;
};

const mapStateToProps = state => {
  return {
    getChannelsQuery: state.channels.channelsList.getChannelsQuery,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({...paginationActionCreators}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchChannelComponent);
