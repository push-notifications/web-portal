import React, {useEffect, useState} from 'react';
import {
  Button,
  Dropdown,
  Icon,
  Image,
  Item,
  Label,
  List,
  Pagination,
  Popup,
  Segment,
  Table,
  Loader,
  Dimmer,
} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as showSnackBarActionCreator from 'common/actions/Snackbar';
import * as updateChannelActionCreators from 'channels/actions/UpdateChannel';
import * as subscribeToChannelActionCreators from 'channels/actions/SubscribeToChannel';
import * as noContentActionCreators from 'channels/actions/NoContentActionsUpdate';
import * as unsubscribeFromChannelActionCreators from 'channels/actions/UnsubscribeFromChannel';
import * as paginationActioncreators from 'channels/actions/PaginationActions';
import * as getChannelsActionCreators from 'channels/actions/GetChannels';
import * as toggleFavoriteChannelActionCreators from 'channels/actions/ToggleFavoriteChannel';
import * as getPublicChannelsActionCreators from 'channels/actions/GetPublicChannels';
import * as getUserSettingsActionCreator from 'channels/actions/GetUserSettings';
import VisibilityIcon from 'utils/visibility-icon';
import AddMute from 'mutes/components/AddMute';
import './ChannelsList.scss';
import {channelFlagTypes} from 'common/types/ChannelFlagTypes';

const ChannelsList = ({
  channels,
  totalNumberOfChannels,
  userSettings,
  getUserSettings,
  history,
  expanded,
  subscribeToChannel,
  updateSubscribeToChannel,
  unsubscribeFromChannel,
  toggleFavoriteChannel,
  getChannelsQuery,
  setGetChannelsQuery,
  getChannels,
  getPublicChannels,
  isAuthenticated,
  loading,
  showSnackbar,
  channelMutes,
  loadingSubscribe,
  loadingFavorite,
  loadingUnsubscribe,
  globalMuteEnabled,
}) => {
  const [subscribeId, setSubscribeId] = useState(null);
  const [unsubscribeId, setUnsubscribeId] = useState(null);
  const [expandedIdList, setExpandedIdList] = useState([]);
  const [toggledId, setToggledId] = useState(null);

  useEffect(() => {
    if (isAuthenticated) {
      getChannels(getChannelsQuery);
      getUserSettings();
    } else getPublicChannels(getChannelsQuery);
  }, [isAuthenticated, getChannels, getPublicChannels, getChannelsQuery, getUserSettings]);

  useEffect(() => {
    setExpandedIdList(expanded ? channels.map(c => c.id) : []);
  }, [expanded, channels]);

  async function handleToggleChannelInCollection(channel) {
    setToggledId(channel.id);
    const response = await toggleFavoriteChannel(channel.id);
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message ||
          `An error occurred while moving channel to/from Favorites.`,
        'error'
      );
    }
    setToggledId(null);
  }

  async function handleUnsubscribe(channel) {
    setUnsubscribeId(channel.id);
    const response = await unsubscribeFromChannel(channel.id);
    if (response.error) {
      setUnsubscribeId(null);
      showSnackbar(
        response?.payload?.response?.message ||
          'An error occurred while unsubscribing from channel',
        'error'
      );
    } else {
      updateSubscribeToChannel(channel.id, false);
      setUnsubscribeId(null);
      showSnackbar(`Unsubscribed from channel ${channel.name} successfully`, 'success');
    }
  }

  async function handleSubscribe(channel) {
    setSubscribeId(channel.id);
    const response = await subscribeToChannel(channel.id);
    if (response.error) {
      setSubscribeId(null);
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while subscribing to channel',
        'error'
      );
    } else {
      updateSubscribeToChannel(channel.id, true);
      setSubscribeId(null);
      showSnackbar(`Subscribed channel  ${channel.name} successfully`, 'success');
    }
  }

  function findActiveChannelMute(channelId) {
    const now = new Date();
    return channelMutes.find(
      m =>
        m.target.id === channelId &&
        (m.type === 'PERMANENT' ||
          (m.type === 'RANGE' &&
            (m.start === null || new Date(m.start) <= now) &&
            new Date(m.end) > now))
    );
  }

  function expandCollapse(id) {
    if (expandedIdList.includes(id)) setExpandedIdList(expandedIdList.filter(c => c !== id));
    else setExpandedIdList([...expandedIdList, id]);
  }

  const renderChannel = channel => {
    const subscribe = () => {
      if (!isAuthenticated) return;

      if (channel.channelFlags && channel.channelFlags.includes(channelFlagTypes.MANDATORY)) return;

      if (channel.subscribed) {
        return (
          <Popup
            trigger={
              <Button
                disabled={channel.id === unsubscribeId && loadingUnsubscribe}
                loading={channel.id === unsubscribeId && loadingUnsubscribe}
                icon="alarm"
                size="mini"
                basic
                color="blue"
                onClick={() => handleUnsubscribe(channel)}
                content="Unsubscribe"
                labelPosition="left"
              />
            }
            position="top center"
            content="Unsubscribe"
          />
        );
      }

      return (
        <Popup
          trigger={
            <Button
              disabled={channel.id === subscribeId && loadingSubscribe}
              loading={channel.id === subscribeId && loadingSubscribe}
              icon="alarm mute"
              size="mini"
              basic
              onClick={() => handleSubscribe(channel)}
              content="Subscribe"
              labelPosition="left"
            />
          }
          position="top center"
          content="Subscribe"
        />
      );
    };

    const channelName = () => {
      return (
        <a href={`/channels/${channel.id}/notifications`} style={{marginRight: 5}}>
          {channel.name}
        </a>
      );
    };

    const channelCategory = () => {
      if (channel.category) {
        return (
          <Label
            color="grey"
            size="small"
            content={channel.category.name}
            style={{marginRight: 5}}
          />
        );
      }
    };

    const channelTags = () => {
      if (channel.tags && channel.tags.length > 0) {
        return (
          <>
            <Popup style={{marginLeft: 5}} trigger={<Icon name="tags" style={{color: '#ccc'}} />}>
              <Popup.Header>Tags</Popup.Header>
              <Popup.Content>
                <List className="list--flex">
                  {channel.tags.map(data => {
                    return <List.Item key={data.id} icon="tag" content={data.name} />;
                  })}
                </List>
              </Popup.Content>
            </Popup>
          </>
        );
      }
    };

    const channelExtras = () => {
      if (!isAuthenticated) return;

      return (
        <>
          {channel.subscribed && (
            <AddMute
              isChannelList
              disabled={globalMuteEnabled}
              channelId={channel.id}
              channelName={channel.name}
              editMute={findActiveChannelMute(channel.id)}
            />
          )}

          {channel.manage && (
            <Popup
              trigger={
                <Button
                  basic
                  size="mini"
                  icon="settings"
                  content="Manage"
                  labelPosition="left"
                  onClick={() => {
                    history.push(`/channels/${channel.id}`);
                  }}
                />
              }
              position="top center"
              content="Edit Channel"
            />
          )}

          {channel.send && (
            <Popup
              trigger={
                <Button
                  basic
                  size="mini"
                  icon={{name: 'send', color: 'green'}}
                  content="Send"
                  labelPosition="left"
                  onClick={() => {
                    history.push({
                      pathname: `/channels/${channel.id}/notifications`,
                      section: 'Send notification',
                    });
                  }}
                />
              }
              position="top center"
              content="Send Notification"
            />
          )}
        </>
      );
    };

    const channelCanSendPrivate = () => {
      if (!isAuthenticated) return;
      return (
        channel.sendPrivate && (
          <Popup
            style={{marginLeft: 5}}
            trigger={<Icon name="inbox" style={{color: '#ccc'}} />}
            content="This channel can send you direct notifications."
          />
        )
      );
    };

    const channelIsMandatory = () => {
      if (!isAuthenticated) return;
      return (
        channel.channelFlags &&
        channel.channelFlags.includes(channelFlagTypes.MANDATORY) && (
          <Popup
            style={{marginLeft: 5}}
            trigger={<Icon name="shield" style={{color: '#ccc'}} />}
            content="This channel is mandatory. Unsubscribe is not possible, it can however be muted if needed."
          />
        )
      );
    };

    const channelIsCritical = () => {
      if (!isAuthenticated) return;
      return (
        channel.channelFlags &&
        channel.channelFlags.includes(channelFlagTypes.CRITICAL) && (
          <Popup
            style={{marginLeft: 5}}
            trigger={<Icon name="exclamation" style={{color: '#ccc'}} />}
            content="This channel can send critical (emergency) notifications on all your devices."
          />
        )
      );
    };

    const favorite = () => {
      if (!isAuthenticated) return;
      return (
        <span style={{float: 'right', backgroundColor: 'white'}}>
          <Popup
            trigger={
              <Icon
                disabled={channel.id === toggledId && loadingFavorite}
                loading={channel.id === toggledId && loadingFavorite}
                name={userSettings?.favoriteList?.includes(channel.id) ? 'star' : 'star outline'}
                color={userSettings?.favoriteList?.includes(channel.id) ? 'yellow' : 'grey'}
                onClick={() => handleToggleChannelInCollection(channel)}
              />
            }
            position="top center"
            content={
              userSettings?.favoriteList?.includes(channel.id)
                ? 'Remove from Favorites'
                : 'Add to Favorites'
            }
          />
        </span>
      );
    };

    const displayCount = count => {
      return Intl.NumberFormat('en-GB', {
        notation: 'compact',
        maximumFractionDigits: 0,
      }).format(count);
    };

    const itemCount = () => {
      if (!isAuthenticated) return;

      return (
        <>
          <Image
            centered
            src="/favicon/favicon-16x16.png"
            style={{opacity: '0.5', display: 'inline-block', filter: 'grayscale(1.0)'}}
          />
          <span>{displayCount(channel.relationships?.notifications?.count)}</span>
        </>
      );
    };

    const itemExpander = () => {
      return (
        <Popup
          trigger={
            <Icon
              name={expandedIdList.includes(channel.id) ? 'triangle down' : 'triangle right'}
              onClick={() => expandCollapse(channel.id)}
              style={{float: 'right'}}
            />
          }
          content={expandedIdList.includes(channel.id) ? 'Collapse' : 'Expand'}
        />
      );
    };

    const descriptionPreview = body => {
      if (body.length > 100) return `${body.substring(0, 100)}...`;
      return body;
    };

    return (
      <Item key={channel.id}>
        <Item.Content
          style={{
            borderLeft: '6px solid',
            borderLeftColor: channel.subscribed ? '#2185D0' : 'white',
            paddingLeft: 5,
          }}
        >
          <Item.Header
            style={{
              width: 'calc(100% - 45px)',
              fontSize: 'unset',
              marginTop: 2,
            }}
          >
            <VisibilityIcon value={channel.visibility} />
            {channelName()}
            {channelCategory()}
            {favorite()}
          </Item.Header>
          {itemExpander()}
          {expandedIdList.includes(channel.id) && (
            <>
              <Item.Meta>
                <span className="cinema">{descriptionPreview(channel.description)}</span>
              </Item.Meta>
              {isAuthenticated && (
                <Item.Extra>
                  <div style={{width: '36px', borderBottom: '1px solid rgb(221, 221, 221)'}} />
                  {itemCount()}
                  {channel.sendPrivate && <span> · </span>}
                  {channelCanSendPrivate()}
                  {channel?.channelFlags.includes(channelFlagTypes.MANDATORY) && <span> · </span>}
                  {channelIsMandatory()}
                  {channel?.channelFlags.includes(channelFlagTypes.CRITICAL) && <span> · </span>}
                  {channelIsCritical()}
                  {channel.tags && channel.tags.length > 0 && <span> · </span>}
                  {channelTags()}
                </Item.Extra>
              )}
              <Item.Extra>
                {subscribe()}
                {channelExtras()}
              </Item.Extra>
            </>
          )}
        </Item.Content>
      </Item>
    );
  };

  if (loading) {
    return (
      <Dimmer active inverted>
        <Loader content="Loading your channels" />
      </Dimmer>
    );
  } else {
    return (
      <Segment className="channel-list" basic>
        <Item.Group divided>
          {channels.map(channel => {
            return renderChannel(channel);
          })}
        </Item.Group>

        <Table striped fixed>
          <Table.Footer>
            <Table.Row>
              <Table.HeaderCell>
                <Dropdown
                  value={getChannelsQuery.take}
                  onChange={(e, {value}) =>
                    setGetChannelsQuery({
                      ...getChannelsQuery,
                      take: value,
                      skip: 0,
                    })
                  }
                  selection
                  options={[
                    {key: 10, value: 10, text: 10},
                    {key: 25, value: 25, text: 25},
                    {key: 50, value: 50, text: 50},
                  ]}
                />
                <Pagination
                  style={{float: 'right'}}
                  activePage={getChannelsQuery.skip / getChannelsQuery.take + 1}
                  // eslint-disable-next-line no-shadow
                  onPageChange={(e, {activePage}) =>
                    setGetChannelsQuery({
                      ...getChannelsQuery,
                      skip: (activePage - 1) * getChannelsQuery.take,
                    })
                  }
                  totalPages={Math.ceil(totalNumberOfChannels / getChannelsQuery.take)}
                />
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>
      </Segment>
    );
  }
};

const mapStateToProps = state => {
  return {
    channels: state.channels.channelsList.channels,
    totalNumberOfChannels: state.channels.channelsList.totalNumberOfChannels,
    elementsPerPage: state.channels.channelsList.elementsPerPage,
    activePage: state.channels.channelsList.activePage,
    getChannelsQuery: state.channels.channelsList.getChannelsQuery,
    isAuthenticated: state.auth.loggedIn,
    loading: state.channels.channelsList.loading,
    loadingUnsubscribe: state.channels.channelsList.loadingUnsubscribe,
    loadingSubscribe: state.channels.channelsList.loadingSubscribe,
    loadingFavorite: state.channels.channelsList.loadingFavorite,
    // globalMutes: state.mutes.global,
    channelMutes: state.mutes.channel,
    userSettings: state.channels.channelsList.userSettings,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(getChannelsActionCreators, dispatch),
    ...bindActionCreators(updateChannelActionCreators, dispatch),
    ...bindActionCreators(subscribeToChannelActionCreators, dispatch),
    ...bindActionCreators(noContentActionCreators, dispatch),
    ...bindActionCreators(unsubscribeFromChannelActionCreators, dispatch),
    ...bindActionCreators(paginationActioncreators, dispatch),
    ...bindActionCreators(getPublicChannelsActionCreators, dispatch),
    ...bindActionCreators(showSnackBarActionCreator, dispatch),
    ...bindActionCreators(toggleFavoriteChannelActionCreators, dispatch),
    ...bindActionCreators(getUserSettingsActionCreator, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChannelsList);
