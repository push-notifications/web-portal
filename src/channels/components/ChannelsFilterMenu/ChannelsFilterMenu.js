import React from 'react';
import {Menu, Icon} from 'semantic-ui-react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import * as paginationActionCreators from 'channels/actions/PaginationActions';

const ALL_CHANNELS = 'ALL_CHANNELS';
export const ALL_CHANNELS_I_OWN = 'ALL_CHANNELS_I_OWN';
export const MY_SUBSCRIPTIONS = 'MY_SUBSCRIPTIONS';
export const FAVORITE_CHANNELS = 'FAVORITE_CHANNELS';

const ChannelsFilterMenu = ({
  setGetChannelsQuery,
  getChannelsQuery,
  subscribedFilter,
  manageFilter,
  favoritesFilter,
}) => {
  return (
    <>
      <Menu fluid vertical color="blue" size="small">
        <Menu.Item
          as={Link}
          name={ALL_CHANNELS}
          id="all"
          to="#all"
          active={!subscribedFilter && !manageFilter && !favoritesFilter}
          onClick={() => {
            setGetChannelsQuery({
              ...getChannelsQuery,
              ownerFilter: false,
              subscribedFilter: false,
              favoritesFilter: false,
              skip: 0,
            });
            localStorage.setItem('ChannelListMenu', ALL_CHANNELS);
          }}
        >
          All channels
        </Menu.Item>
        <Menu.Item
          as={Link}
          name={ALL_CHANNELS_I_OWN}
          id="manage"
          to="#manage"
          active={manageFilter}
          onClick={() => {
            setGetChannelsQuery({
              ...getChannelsQuery,
              ownerFilter: true,
              subscribedFilter: false,
              favoritesFilter: false,
              skip: 0,
            });
            localStorage.setItem('ChannelListMenu', ALL_CHANNELS_I_OWN);
          }}
        >
          Channels I own or manage
        </Menu.Item>
        <Menu.Item
          as={Link}
          name={MY_SUBSCRIPTIONS}
          id="subscriptions"
          to="#subscriptions"
          active={subscribedFilter}
          onClick={() => {
            setGetChannelsQuery({
              ...getChannelsQuery,
              ownerFilter: false,
              subscribedFilter: true,
              favoritesFilter: false,
              skip: 0,
            });
            localStorage.setItem('ChannelListMenu', MY_SUBSCRIPTIONS);
          }}
        >
          My subscriptions
        </Menu.Item>
      </Menu>
      <Menu fluid vertical color="blue" size="small">
        <Menu.Item
          as={Link}
          name={FAVORITE_CHANNELS}
          id="favorites"
          to="#favorites"
          active={favoritesFilter}
          onClick={() => {
            setGetChannelsQuery({
              ...getChannelsQuery,
              ownerFilter: false,
              subscribedFilter: false,
              favoritesFilter: true,
              skip: 0,
            });
            localStorage.setItem('ChannelListMenu', FAVORITE_CHANNELS);
          }}
        >
          Favorites
          <Icon name="star" color={favoritesFilter ? 'yellow' : 'grey'} />
        </Menu.Item>
      </Menu>
    </>
  );
};

const mapStateToProps = state => {
  return {
    elementsPerPage: state.channels.channelsList.elementsPerPage,
    activePage: state.channels.channelsList.activePage,
    getChannelsQuery: state.channels.channelsList.getChannelsQuery,
    subscribedFilter: state.channels.channelsList.getChannelsQuery.subscribedFilter,
    manageFilter: state.channels.channelsList.getChannelsQuery.ownerFilter,
    favoritesFilter: state.channels.channelsList.getChannelsQuery.favoritesFilter,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(paginationActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChannelsFilterMenu);
