/* eslint-disable react/jsx-fragments */
import React from 'react';
import {Segment} from 'semantic-ui-react';

import ChannelUsersComponent from 'channels/components/ChannelMembers/ChannelUsersComponent';
import ChannelGroupsComponent from 'channels/components/ChannelMembers/ChannelGroupsComponent';

const ChannelMembersComponent = () => {
  return (
    <Segment>
      <ChannelUsersComponent />
      <ChannelGroupsComponent />
    </Segment>
  );
};

export default ChannelMembersComponent;
