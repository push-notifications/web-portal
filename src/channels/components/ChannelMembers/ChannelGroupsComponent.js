/* eslint-disable react/jsx-fragments */
import React, {useEffect, useState} from 'react';
import {
  Input,
  Table,
  Button,
  Pagination,
  Dropdown,
  Segment,
  Confirm,
  Popup,
} from 'semantic-ui-react';
import {bindActionCreators} from 'redux';
import {useParams} from 'react-router-dom';
import {connect} from 'react-redux';

import * as showSnackBarActionCreator from 'common/actions/Snackbar';
import * as removeGroupFromChannelActionCreators from 'channels/actions/RemoveGroupFromChannel';
import * as getChannelGroupsActionCreators from 'channels/actions/GetChannelGroups';
import AddGroupToChannelComponent from 'channels/components/AddGroupToChannelComponent/AddGroupToChannelComponent';
import * as noContentActionCreators from 'channels/actions/NoContentActionsUpdate';

import './ChannelMembers.scss';

let timer = null;
const WAIT_INTERVAL = 500;

const groupsColumns = [
  {
    id: 'groupIdentifier',
    numeric: false,
    disablePadding: true,
    label: 'Group name',
  },
];

const ChannelGroupsComponent = ({
  groups,
  getChannelGroups,
  removeGroupFromChannel,
  updateRemoveGroup,
  setGetChannelGroupsQuery,
  resetChannelGroupsQuery,
  getChannelGroupsQuery,
  totalNumberOfGroups,
  loadingGroups,
  loadingRemoveGroup,
  showSnackbar,
}) => {
  const {channelId} = useParams();
  const [searchGroupsValue, setSearchGroupsValue] = useState('');
  const [deleteId, setDeleteId] = useState(null);
  const [deleteName, setDeleteName] = useState('');
  const [channelGroupDeletionModalVisible, setChannelGroupDeletionModalVisible] = useState(false);

  useEffect(() => {
    return () => {
      resetChannelGroupsQuery();
    };
  }, [resetChannelGroupsQuery]);

  async function handleConfirm() {
    setChannelGroupDeletionModalVisible(false);
    const response = await removeGroupFromChannel(deleteId, channelId);
    if (response.error) {
      setDeleteId(null);
      showSnackbar(
        `An error occurred while removing the group ${deleteName} from the channel`,
        'error'
      );
      return;
    }
    updateRemoveGroup(deleteId);
    const updateFetchResponse = await getChannelGroups(channelId, getChannelGroupsQuery);
    if (updateFetchResponse.error) {
      setDeleteId(null);
      showSnackbar(
        'Group deleted with success, but an error ocurred while fetching the updated group member list.' +
          'Please refresh the page.',
        'error'
      );
      return;
    }
    setDeleteId(null);
    showSnackbar(`Group ${deleteName || ''} removed successfully`, 'success');
  }

  useEffect(() => {
    getChannelGroups(channelId, getChannelGroupsQuery);
  }, [getChannelGroups, channelId, getChannelGroupsQuery]);

  return (
    <Segment className="channel-members" basic loading={loadingGroups}>
      <Input
        onChange={(e, {value}) => {
          clearTimeout(timer);
          setSearchGroupsValue(value);
          timer = setTimeout(
            () =>
              setGetChannelGroupsQuery({
                ...getChannelGroupsQuery,
                searchText: value,
              }),
            WAIT_INTERVAL
          );
        }}
        value={searchGroupsValue}
        style={{width: '100%', marginTop: 40}}
        focus
        placeholder="Search..."
      />
      <Table striped>
        <Table.Header>
          <Table.Row>
            {groupsColumns.map(c => (
              <Table.HeaderCell key={c.label}>{c.label}</Table.HeaderCell>
            ))}
            <Table.HeaderCell collapsing />
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {groups.map(group => {
            return (
              <Table.Row key={group.id}>
                {groupsColumns.map(column => {
                  return (
                    <Table.Cell key={column.id}>
                      {group[column.id] ? group[column.id].toString() : ''}
                    </Table.Cell>
                  );
                })}
                <Table.Cell>
                  <Popup
                    trigger={
                      <Button
                        color="red"
                        inverted
                        size="mini"
                        icon="trash"
                        disabled={deleteId === group.groupIdentifier && loadingRemoveGroup}
                        loading={deleteId === group.groupIdentifier && loadingRemoveGroup}
                        onClick={(e, d) => {
                          setDeleteId(group.id);
                          setDeleteName(group.groupIdentifier);
                          setChannelGroupDeletionModalVisible(true);
                        }}
                      />
                    }
                    content={`Remove ${group.groupIdentifier}`}
                  />
                  <Confirm
                    open={channelGroupDeletionModalVisible}
                    header={`Remove group ${deleteName}`}
                    content={`Are you sure that you want to remove the group ${deleteName}?`}
                    onCancel={(e, d) => {
                      setChannelGroupDeletionModalVisible(false);
                    }}
                    onConfirm={handleConfirm}
                  />
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="5">
              <Dropdown
                value={getChannelGroupsQuery.take}
                onChange={(e, {value}) =>
                  setGetChannelGroupsQuery({
                    ...getChannelGroupsQuery,
                    take: value,
                    skip: 0,
                  })
                }
                selection
                options={[
                  {key: 10, value: 10, text: 10},
                  {key: 25, value: 25, text: 25},
                  {key: 50, value: 50, text: 50},
                ]}
              />
              <Pagination
                style={{float: 'right'}}
                activePage={getChannelGroupsQuery.skip / getChannelGroupsQuery.take + 1}
                // eslint-disable-next-line no-shadow
                onPageChange={(e, {activePage}) =>
                  setGetChannelGroupsQuery({
                    ...getChannelGroupsQuery,
                    skip: (activePage - 1) * getChannelGroupsQuery.take,
                  })
                }
                totalPages={Math.ceil(totalNumberOfGroups / getChannelGroupsQuery.take)}
              />
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <AddGroupToChannelComponent />
    </Segment>
  );
};

const mapStateToProps = state => {
  return {
    groups: state.channels.channel.groups,
    getChannelGroupsQuery: state.channels.channel.getChannelGroupsQuery,
    totalNumberOfGroups: state.channels.channel.totalNumberOfGroups,
    loadingGroups: state.channels.channel.loadingGroups,
    loadingRemoveGroup: state.channels.channel.loadingRemoveGroup,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(getChannelGroupsActionCreators, dispatch),
    ...bindActionCreators(removeGroupFromChannelActionCreators, dispatch),
    ...bindActionCreators(noContentActionCreators, dispatch),
    ...bindActionCreators(showSnackBarActionCreator, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChannelGroupsComponent);
