import React, {useState, useEffect} from 'react';
import {
  Table,
  Button,
  Input,
  Dropdown,
  Pagination,
  Segment,
  Confirm,
  Popup,
  Label,
} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {useParams} from 'react-router-dom';

import * as showSnackBarActionCreator from 'common/actions/Snackbar';
import * as getChannelMembersActionCreators from 'channels/actions/GetChannelMembers';
import AddUserToChannelComponent from 'channels/components/AddUserToChannelComponent/AddUserToChannelComponent';
import * as removeUserFromChannelActionCreators from 'channels/actions/RemoveUserFromChannel';
import * as noContentActionCreators from 'channels/actions/NoContentActionsUpdate';

import './ChannelMembers.scss';

let timer = null;
const WAIT_INTERVAL = 500;

const membersColumns = [
  {id: 'username', numeric: false, disablePadding: true, label: 'User name'},
  {
    id: 'email',
    numeric: false,
    disablePadding: false,
    label: 'Email',
  },
];

const ChannelUsersComponent = ({
  members,
  getChannelMembers,
  removeUserFromChannel,
  setGetChannelMembersQuery,
  resetChannelMembersQuery,
  getChannelMembersQuery,
  totalNumberOfMembers,
  loadingMembers,
  loadingRemoveUser,
  showSnackbar,
}) => {
  const {channelId} = useParams();
  const [searchMembersValue, setSearchMembersValue] = useState('');
  const [deleteId, setDeleteId] = useState(null);
  const [deleteName, setDeleteName] = useState('');
  const [channelUserDeletionModalVisible, setChannelUserDeletionModalVisible] = useState(false);

  useEffect(() => {
    return () => {
      resetChannelMembersQuery();
    };
  }, [resetChannelMembersQuery]);

  async function handleConfirm() {
    setChannelUserDeletionModalVisible(false);
    const response = await removeUserFromChannel(deleteId, channelId);
    if (response.error) {
      setDeleteId(null);
      showSnackbar(
        `An error occurred while removing the user ${deleteName} from the channel`,
        'error'
      );
      return;
    }
    const updateFetchResponse = await getChannelMembers(channelId, getChannelMembersQuery);
    if (updateFetchResponse.error) {
      setDeleteId(null);
      showSnackbar(
        'Member deleted with success, but an error ocurred while fetching the updated member list.' +
          'Please refresh the page.',
        'error'
      );
      return;
    }
    setDeleteId(null);
    showSnackbar(`User ${deleteName || ''} removed successfully`, 'success');
  }

  useEffect(() => {
    getChannelMembers(channelId, getChannelMembersQuery);
  }, [getChannelMembers, channelId, getChannelMembersQuery]);

  const renderColumn = (member, column) => {
    if (column.id === 'username' && !member[column.id])
      return (
        <Table.Cell key={column.id}>
          <Label basic color="orange">
            Unconfirmed Email
          </Label>
        </Table.Cell>
      );

    return (
      <Table.Cell key={column.id}>
        {member[column.id] ? member[column.id].toString() : ''}
      </Table.Cell>
    );
  };

  return (
    // eslint-disable-next-line react/jsx-fragments
    <Segment className="channel-members" basic loading={loadingMembers}>
      <Input
        onChange={(e, {value}) => {
          clearTimeout(timer);
          setSearchMembersValue(value);
          timer = setTimeout(
            () =>
              setGetChannelMembersQuery({
                ...getChannelMembersQuery,
                searchText: value,
              }),
            WAIT_INTERVAL
          );
        }}
        value={searchMembersValue}
        style={{width: '100%'}}
        focus
        placeholder="Search..."
      />
      <Table striped>
        <Table.Header>
          <Table.Row>
            {membersColumns.map(c => (
              <Table.HeaderCell key={c.label}>{c.label}</Table.HeaderCell>
            ))}
            <Table.HeaderCell collapsing />
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {members.map(member => {
            return (
              <Table.Row key={member.id}>
                {membersColumns.map(column => {
                  return renderColumn(member, column);
                })}
                <Table.Cell collapsing>
                  <Popup
                    trigger={
                      <Button
                        color="red"
                        inverted
                        size="mini"
                        icon="trash"
                        disabled={deleteId === member.id && loadingRemoveUser}
                        loading={deleteId === member.id && loadingRemoveUser}
                        onClick={(e, d) => {
                          setDeleteId(member.id);
                          setDeleteName(member.username);
                          setChannelUserDeletionModalVisible(true);
                        }}
                      />
                    }
                    content={`Remove ${member.username}`}
                  />
                  <Confirm
                    open={channelUserDeletionModalVisible}
                    header={`Remove user ${deleteName}`}
                    content={`Are you sure that you want to remove the user ${deleteName}?`}
                    onCancel={(e, d) => {
                      setChannelUserDeletionModalVisible(false);
                    }}
                    onConfirm={handleConfirm}
                  />
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="5">
              <Dropdown
                value={getChannelMembersQuery.take}
                onChange={(e, {value}) =>
                  setGetChannelMembersQuery({
                    ...getChannelMembersQuery,
                    take: value,
                    skip: 0,
                  })
                }
                selection
                options={[
                  {key: 10, value: 10, text: 10},
                  {key: 25, value: 25, text: 25},
                  {key: 50, value: 50, text: 50},
                ]}
              />
              <Pagination
                style={{float: 'right'}}
                activePage={getChannelMembersQuery.skip / getChannelMembersQuery.take + 1}
                // eslint-disable-next-line no-shadow
                onPageChange={(e, {activePage}) =>
                  setGetChannelMembersQuery({
                    ...getChannelMembersQuery,
                    skip: (activePage - 1) * getChannelMembersQuery.take,
                  })
                }
                totalPages={Math.ceil(totalNumberOfMembers / getChannelMembersQuery.take)}
              />
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <AddUserToChannelComponent />
    </Segment>
  );
};

const mapStateToProps = state => {
  return {
    members: state.channels.channel.members,
    getChannelMembersQuery: state.channels.channel.getChannelMembersQuery,
    totalNumberOfMembers: state.channels.channel.totalNumberOfMembers,
    loadingMembers: state.channels.channel.loadingMembers,
    loadingRemoveUser: state.channels.channel.loadingRemoveUser,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(getChannelMembersActionCreators, dispatch),
    ...bindActionCreators(removeUserFromChannelActionCreators, dispatch),
    ...bindActionCreators(noContentActionCreators, dispatch),
    ...bindActionCreators(showSnackBarActionCreator, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChannelUsersComponent);
