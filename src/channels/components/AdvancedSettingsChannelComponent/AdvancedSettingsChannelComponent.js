import React, {useEffect, useState} from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  Button,
  Form,
  Header,
  Input,
  Label,
  Modal,
  Segment,
  Grid,
  Popup,
  Confirm,
} from 'semantic-ui-react';
import {useParams, withRouter} from 'react-router-dom';

import * as removeChannelAdminGroup from 'channels/actions/RemoveChannelAdminGroup';
import * as updateChannelActionCreators from 'channels/actions/UpdateChannel';
import * as showSnackBarActionCreators from 'common/actions/Snackbar';
import * as deleteChannelActionCreators from 'channels/actions/DeleteChannel';
import * as generateAPIKeyActionCreators from 'channels/actions/GenerateAPIKey';
import * as getTagsActionCreators from 'channels/actions/GetTags';
import * as createTagActionCreators from 'channels/actions/CreateTag';
import * as setChannelTagsActionCreators from 'channels/actions/SetChannelTags';
import * as noContentActionCreators from 'channels/actions/NoContentActionsUpdate';

import UpdateChannelAdminGroupComponent from 'channels/components/UpdateChannelAdminGroupComponent/UpdateChannelAdminGroupComponent';
import APIHelp from 'help/components/APIHelp';
import './AdvancedSettingsChannelComponent.scss';

const AdvancedSettingsChannelComponent = ({
  channel,
  tags,
  showSnackbar,
  deleteChannel,
  updateDeletedChannel,
  history,
  loading,
  loadingDelete,
  loadingGenerate,
  generateAPIKey,
  loadingTags,
  getTags,
  createTag,
  setChannelTags,
  loadingUpdateTags,
  loadingCreateTags,
  tagsIds,
  removeChannelAdminGroup,
}) => {
  const {channelId} = useParams();
  const [confirmation, setConfirmation] = useState('');
  const [newKey, setNewKey] = useState('');
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [generateModalOpen, setGenerateModalOpen] = useState(false);
  const [loadingPage, setLoadingPage] = useState(loadingTags || loading);
  const [tagsValue, setTagsValue] = useState([]);
  // local copy to allow adding tags to the dropdown while we wait for their creation
  const [tagsOptions, setTagsOptions] = useState([]);
  const [openRemoveConfirmation, setOpenRemoveConfirmation] = useState(false);

  useEffect(() => {
    setLoadingPage(loadingTags || loading);
  }, [loadingTags, loading, setLoadingPage]);

  useEffect(() => {
    setTagsValue(channel.tags?.map(data => data.id || data) || []);
  }, [channel, setTagsValue]);

  const formatForDropdown = dataArray =>
    dataArray.map(data => {
      return {
        key: data.id,
        value: data.id,
        text: data.name,
      };
    });

  useEffect(() => {
    setTagsOptions(formatForDropdown(tags));
  }, [tags, setTagsOptions]);

  useEffect(() => {
    getTags();
  }, [getTags]);

  function handleDeleteModalClose() {
    setDeleteModalOpen(false);
    setConfirmation('');
  }

  function handleGenerateModalClose() {
    setGenerateModalOpen(false);
    setNewKey('');
  }

  async function handleDeleteChannel() {
    const response = await deleteChannel(channelId);
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while deleting the channel',
        'error'
      );
    } else {
      updateDeletedChannel(channelId);
      showSnackbar('The channel has been deleted successfully', 'success');
      history.push('/');
    }
  }

  async function handleGenerateNewKey() {
    const response = await generateAPIKey(channelId);
    setNewKey(response.payload);
  }

  async function copyApiKeyToClipboard() {
    navigator.clipboard.writeText(newKey);
  }

  // eslint-disable-next-line no-underscore-dangle
  async function _setChannelTags(value) {
    setTagsValue(value);
    const response = await setChannelTags(value, channel.id);
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while saving the tags',
        'error'
      );
    } else {
      showSnackbar('The tags were saved successfully', 'success');
    }
  }
  async function handleSetChannelTags(e, {value}) {
    const tagsExist = tagsIds && value.reduce((acc, cur) => acc && tagsIds.includes(cur), true);
    // if a tag does not exist means it's an addition
    if (!tagsExist) return;

    await _setChannelTags(value);
  }

  async function handleCreateTag(e, {value}) {
    // added manually to give the users instant feedback
    // will be replaced after, either on the response error handling, or by useEffect on success
    setTagsOptions([...tagsOptions, {key: value, value, text: value}]);
    setTagsValue([...tagsValue, value]);

    const response = await createTag({name: value});
    if (response.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while creating the tag',
        'error'
      );
      // remove not created tag
      setTagsOptions(tagsOptions);
      setTagsValue(channel.tags?.map(data => data.id || data) || []);
    } else {
      const {id} = response.payload;
      await _setChannelTags([...tagsValue, id]);
    }
  }

  async function handleRemoveAdminGroup() {
    const response = await removeChannelAdminGroup(channelId);
    if (response?.error) {
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while updating the admin group',
        'error'
      );
      return;
    }

    setOpenRemoveConfirmation(false);
    showSnackbar('The admin group has been updated successfully', 'success');
  }

  return (
    <>
      <Segment loading={loadingPage}>
        <Form>
          <Label color="blue" ribbon className="form-ribbon">
            Tags
          </Label>
          <Form.Group grouped>
            <label>Setting Tags facilitates searching, filtering and grouping channels</label>
            <Form.Dropdown
              name="tags"
              onChange={handleSetChannelTags}
              clearable
              search
              fluid
              multiple
              selection
              allowAdditions
              closeOnChange
              selectOnNavigation={false}
              onAddItem={handleCreateTag}
              // additionLabel="Create and Add "
              additionLabel="Create "
              options={tagsOptions}
              loading={loadingTags || loadingUpdateTags || loadingCreateTags}
              placeholder="[Optional] Select or create Tags"
              value={tagsValue}
            />
          </Form.Group>

          <Label color="blue" ribbon className="form-ribbon">
            API Key for programmatic send
          </Label>
          <Form.Group inline>
            <Button type="button" onClick={() => setGenerateModalOpen(true)}>
              Generate
            </Button>
            <APIHelp />

            <Modal
              as={Form}
              open={generateModalOpen}
              onClose={handleGenerateModalClose}
              onOpen={() => setGenerateModalOpen(true)}
            >
              <Modal.Content>
                <Modal.Description>
                  {channel.APIKey ? (
                    <>
                      <Header>Regenerate API Key</Header>
                      <p>
                        Are you sure you want to generate a new API Key? The previous key will no
                        longer be valid.
                      </p>
                      <p>Make sure you save it - you will not be able to access it again.</p>
                    </>
                  ) : (
                    <>
                      <Header>Generate API Key</Header>
                      <p>Make sure you save it - you will not be able to access it again.</p>
                    </>
                  )}
                </Modal.Description>
              </Modal.Content>
              <Modal.Actions>
                <Grid columns={2}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Input
                        readOnly
                        placeholder="Click button to generate a new API Key"
                        value={newKey}
                      />
                    </Grid.Column>
                    <Grid.Column textAlign="left">
                      <Button
                        labelPosition="left"
                        onClick={copyApiKeyToClipboard}
                        icon="copy"
                        content="Copy to Clipboad"
                      />
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column textAlign="left">
                      <Button
                        type="submit"
                        loading={loadingGenerate}
                        disabled={loadingGenerate}
                        inverted
                        color={channel.APIKey ? 'red' : 'green'}
                        onClick={handleGenerateNewKey}
                      >
                        {channel.APIKey
                          ? 'I understand the consequences, generate new API Key.'
                          : 'Generate'}
                      </Button>
                    </Grid.Column>
                    <Grid.Column>
                      <Button type="reset" onClick={handleGenerateModalClose}>
                        {channel.APIKey ? 'Close' : 'Cancel'}
                      </Button>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Modal.Actions>
            </Modal>
          </Form.Group>

          <Label color="blue" ribbon className="form-ribbon">
            Managers
          </Label>

          <Form.Field inline>
            <label>Owner:</label>
            <Input readOnly value={channel.owner?.username || ''} />
          </Form.Field>
          <Form.Field inline>
            <label>Admin group:</label>
            <label style={{paddingRight: '10px'}}>
              {channel.adminGroup ? channel.adminGroup.groupIdentifier : ''}
            </label>
            <UpdateChannelAdminGroupComponent
              currentGroupName={channel.adminGroup?.groupIdentifier}
            />
            {channel.adminGroup && (
              <>
                <Popup
                  trigger={
                    <Button
                      style={{marginLeft: '0.5rem'}}
                      icon="trash"
                      size="mini"
                      onClick={() => setOpenRemoveConfirmation(true)}
                    />
                  }
                  content="Remove channel's admin group"
                />
                <Confirm
                  open={openRemoveConfirmation}
                  onCancel={() => setOpenRemoveConfirmation(false)}
                  onConfirm={() => handleRemoveAdminGroup()}
                  confirmButton="Proceed"
                  content={
                    <div className="content">
                      You are going to remove <b>{channel.adminGroup.groupIdentifier}</b> group as
                      the admin group of the channel. Do you want to continue with this action?
                    </div>
                  }
                />
              </>
            )}
          </Form.Field>
        </Form>
      </Segment>

      <Segment loading={loadingPage}>
        <Header>Remove Channel</Header>
        <p>Removed channels can not be restored!</p>
        <Modal
          as={Form}
          onSubmit={handleDeleteChannel}
          trigger={<Button color="red">Delete</Button>}
          open={deleteModalOpen}
          onClose={handleDeleteModalClose}
          onOpen={() => setDeleteModalOpen(true)}
        >
          <Modal.Content>
            <Modal.Description>
              <Header>Confirmation required</Header>
              <p>
                You are going to remove <strong>{channel.name}</strong> channel. Are you sure?
              </p>
              <p>
                Please type <strong>{channel.name}</strong> to proceed or close this modal to
                cancel.
              </p>
              <Input
                name="confirmation"
                value={confirmation}
                onChange={(e, d) => setConfirmation(d.value)}
              />
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button type="reset" onClick={handleDeleteModalClose}>
              Cancel
            </Button>
            <Button
              type="submit"
              loading={loadingDelete}
              disabled={channel.name !== confirmation || loadingDelete}
              basic
              negative
              onClick={handleDeleteChannel}
            >
              I understand the consequences, delete this channel
            </Button>
          </Modal.Actions>
        </Modal>
      </Segment>
    </>
  );
};

AdvancedSettingsChannelComponent.propTypes = {
  channel: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
    visibility: PropTypes.string,
    subscriptionPolicy: PropTypes.string,
    archive: PropTypes.bool,
  }).isRequired,
};

const mapStateToProps = state => {
  return {
    channel: state.channels.channel.channel,
    tags: state.channels.tags.tags,
    tagsIds: state.channels.tags.tagsIds,
    loading: state.channels.channel.loading,
    loadingDelete: state.channels.channelsList.loadingDelete,
    loadingGenerate: state.loadingGenerate,
    loadingTags: state.channels.tags.loading,
    loadingCreateTags: state.channels.tags.loadingCreate,
    loadingUpdateTags: state.channels.channel.loadingUpdateTags,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(showSnackBarActionCreators, dispatch),
    ...bindActionCreators(updateChannelActionCreators, dispatch),
    ...bindActionCreators(deleteChannelActionCreators, dispatch),
    ...bindActionCreators(noContentActionCreators, dispatch),
    ...bindActionCreators(generateAPIKeyActionCreators, dispatch),
    ...bindActionCreators(getTagsActionCreators, dispatch),
    ...bindActionCreators(createTagActionCreators, dispatch),
    ...bindActionCreators(setChannelTagsActionCreators, dispatch),
    ...bindActionCreators(removeChannelAdminGroup, dispatch),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AdvancedSettingsChannelComponent));
