import React, {useState, useEffect} from 'react';
import {Menu, Header, Icon, Container} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Helmet} from 'react-helmet';
import {useParams, useLocation, useHistory} from 'react-router-dom';

import SendNotificationComponent from 'notifications/components/SendNotificationComponent/SendNotificationComponent';
import EditChannelComponent from 'channels/components/EditChannelComponent/EditChannelComponent';
import AdvancedSettingsChannelComponent from 'channels/components/AdvancedSettingsChannelComponent/AdvancedSettingsChannelComponent';
import ChannelMembers from 'channels/components/ChannelMembers/ChannelMembers';
import * as getChannelMembersActionCreators from 'channels/actions/GetChannelMembers';
import {getChannelEditInfoById} from 'channels/actions/GetChannelEditInfoById';
import ErrorComponent from 'utils/error-component';
import './EditChannelPage.css';

const EditChannelPage = ({channel, getChannelEditInfoById, error}) => {
  const [activeItem, setActiveItem] = useState('Edit channel');
  const handleItemClick = (e, {name}) => setActiveItem(name);
  const {channelId} = useParams();
  const TITLE = `${process.env.REACT_APP_NAME} | Manage ${channel.name}`;
  const location = useLocation();
  const history = useHistory();

  if (location.section) {
    setActiveItem(location.section);
    location.section = null;
  }

  useEffect(() => {
    getChannelEditInfoById(channelId, true);
  }, [channelId, getChannelEditInfoById]);

  return error ? (
    <ErrorComponent error={error} />
  ) : (
    <div className="edit-channel-page-container">
      <Helmet>
        <title>{TITLE}</title>
      </Helmet>
      <Header as="h2">
        <Icon name="settings" />
        <Header.Content
          style={{cursor: 'pointer'}}
          onClick={() => {
            history.push(`/channels/${channel.id}/notifications`);
          }}
        >
          {channel.name}
          <Header.Subheader>Manage channel</Header.Subheader>
        </Header.Content>
      </Header>

      <Menu style={{width: '100%'}} pointing secondary color="blue">
        <Menu.Item
          name="Edit channel"
          active={activeItem === 'Edit channel'}
          onClick={handleItemClick}
        />
        <Menu.Item
          name="Advanced Settings"
          active={activeItem === 'Advanced Settings'}
          onClick={handleItemClick}
        />
        <Menu.Item name="Members" active={activeItem === 'Members'} onClick={handleItemClick} />
        <Menu.Item
          name="Send notification"
          active={activeItem === 'Send notification'}
          onClick={handleItemClick}
        />
      </Menu>
      <Container style={{paddingBottom: 16}}>
        {activeItem === 'Edit channel' && <EditChannelComponent />}
        {activeItem === 'Advanced Settings' && <AdvancedSettingsChannelComponent />}
        {activeItem === 'Members' && <ChannelMembers />}
        {activeItem === 'Send notification' && <SendNotificationComponent channel={channel} />}
      </Container>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    channels: state.channels.channel.members,
    channel: state.channels.channel.channel,
    error: state.channels.channel.pageError,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(getChannelMembersActionCreators, dispatch),
    getChannelEditInfoById: bindActionCreators(getChannelEditInfoById, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditChannelPage);
