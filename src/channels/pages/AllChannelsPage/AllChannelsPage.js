import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Container, Grid, Segment, Header, Checkbox, Dropdown, Form} from 'semantic-ui-react';
import {Helmet} from 'react-helmet';
import {isInternal} from 'auth/utils/authUtils';
import ChannelsList from 'channels/components/ChannelsList/ChannelsList';
import ChannelsFilterMenu from 'channels/components/ChannelsFilterMenu/ChannelsFilterMenu';
import SearchChannelComponent from 'channels/components/SearchChannelComponent/SearchChannelComponent';
import AddChannel from 'channels/components/AddChannel/AddChannel';
import AddMute from 'mutes/components/AddMute';
import * as getMutesActionCreator from 'mutes/actions/mutes';
import * as getTagsActionCreator from 'channels/actions/GetTags';
import * as paginationActionCreators from 'channels/actions/PaginationActions';
import CategoryDropdown from 'common/components/CategoryDropdown/CategoryDropdown';
import ChannelRecommendationsComponent from 'channels/components/ChannelRecommendationsComponent/ChannelRecommendationsComponent';
import './AllChannelsPage.scss';

const TITLE = `${process.env.REACT_APP_NAME} | Channels`;

const AllChannelsPage = ({
  history,
  isAuthenticated,
  roles,
  getMutes,
  globalMutes,
  tags,
  loadingTags,
  getTags,
  getChannelsQuery,
  setGetChannelsQuery,
}) => {
  const [expanded, setExpanded] = useState(localStorage.getItem('ChannelListExpanded') !== 'false');
  const [selectedCategory, setSelectedCategory] = useState();

  useEffect(() => {
    if (isAuthenticated) {
      getMutes(null, true);
      getTags();
    }
  }, [isAuthenticated, getMutes, getTags]);

  function findActiveGlobalMute() {
    const now = new Date();
    return globalMutes.find(
      m =>
        m.type === 'PERMANENT' ||
        (m.type === 'RANGE' &&
          (m.start === null || new Date(m.start) <= now) &&
          new Date(m.end) > now)
    );
  }

  const formatForDropdown = dataArray =>
    dataArray.map(data => {
      return {
        key: data.id,
        value: data.id,
        text: data.name,
      };
    });

  return (
    <Container className="all-channels-page">
      <Segment basic>
        <Helmet>
          <title>{TITLE}</title>
        </Helmet>
        <Grid columns="equal" stackable>
          <Grid.Row>
            <Grid.Column>
              <h2>{process.env.REACT_APP_NAME}</h2>
              <SearchChannelComponent />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            {isAuthenticated && (
              <Grid.Column width={4} style={{minWidth: 300}}>
                <ChannelsFilterMenu />
                <Header as="h4">Filter</Header>
                <Form className="channel-filters">
                  <Form.Field>
                    <CategoryDropdown
                      placeholder="By category"
                      setSelectedCategory={setSelectedCategory}
                      selectedCategory={selectedCategory}
                      specificClassName="CategorySelector"
                      onCategoryChange={cat =>
                        setGetChannelsQuery({
                          ...getChannelsQuery,
                          category: cat?.id || null,
                        })
                      }
                    />
                  </Form.Field>
                  <Form.Field>
                    <Dropdown
                      placeholder="By tag"
                      search
                      clearable
                      multiple
                      selection
                      options={formatForDropdown(tags)}
                      loading={loadingTags}
                      onChange={(e, d) => setGetChannelsQuery({...getChannelsQuery, tags: d.value})}
                    />
                  </Form.Field>
                </Form>
                <Header as="h4">View</Header>
                <Checkbox
                  checked={expanded}
                  onChange={() => {
                    setExpanded(!expanded);
                    localStorage.setItem('ChannelListExpanded', (!expanded).toString());
                  }}
                  toggle
                  label="Expanded"
                  style={{marginBottom: '10px'}}
                />
                <Header as="h4">Actions</Header>
                <AddMute isChannelList global editMute={findActiveGlobalMute()} />
                <div style={{marginBottom: '5px'}} />
                {isInternal(roles) && <AddChannel history={history} />}
              </Grid.Column>
            )}
            <Grid.Column>
              <ChannelsList
                history={history}
                expanded={expanded}
                globalMuteEnabled={findActiveGlobalMute()}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
      <ChannelRecommendationsComponent />
    </Container>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.loggedIn,
    roles: state.auth.roles,
    globalMutes: state.mutes.global,
    channelMutes: state.mutes.channel,
    categories: state.channels.categories.categories,
    tags: state.channels.tags.tags,
    loadingCategories: state.channels.categories.loadingCategories,
    loadingTags: state.channels.tags.loadingTags,
    getChannelsQuery: state.channels.channelsList.getChannelsQuery,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(getMutesActionCreator, dispatch),
    ...bindActionCreators(getTagsActionCreator, dispatch),
    ...bindActionCreators(paginationActionCreators, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AllChannelsPage);
