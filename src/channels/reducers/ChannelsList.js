import {LOCATION_CHANGE} from 'connected-react-router';

import {
  GET_CHANNELS,
  GET_CHANNELS_SUCCESS,
  GET_CHANNELS_FAILURE,
} from 'channels/actions/GetChannels';
import {
  SUBSCRIBE_TO_CHANNEL,
  SUBSCRIBE_TO_CHANNEL_FAILURE,
  SUBSCRIBE_TO_CHANNEL_SUCCESS,
} from 'channels/actions/SubscribeToChannel';
import {
  SUBSCRIBE_TO_CHANNEL_UPDATE_SUCCESS,
  DELETE_CHANNEL_UPDATE_SUCCESS,
} from 'channels/actions/NoContentActionsUpdate';
import {
  UNSUBSCRIBE_FROM_CHANNEL,
  UNSUBSCRIBE_FROM_CHANNEL_FAILURE,
  UNSUBSCRIBE_FROM_CHANNEL_SUCCESS,
} from 'channels/actions/UnsubscribeFromChannel';

import {
  SET_ELEMENTS_PER_PAGE,
  SET_ACTIVE_PAGE,
  SET_GET_CHANNELS_QUERY,
} from 'channels/actions/PaginationActions';
import {
  DELETE_CHANNEL,
  DELETE_CHANNEL_SUCCESS,
  DELETE_CHANNEL_FAILURE,
} from 'channels/actions/DeleteChannel';
import {
  GET_USER_SETTINGS,
  GET_USER_SETTINGS_SUCCESS,
  GET_USER_SETTINGS_FAILURE,
} from 'channels/actions/GetUserSettings';
import {
  TOGGLE_FAVORITE_CHANNEL,
  TOGGLE_FAVORITE_CHANNEL_SUCCESS,
  TOGGLE_FAVORITE_CHANNEL_FAILURE,
} from 'channels/actions/ToggleFavoriteChannel';
import {
  ALL_CHANNELS_I_OWN,
  FAVORITE_CHANNELS,
  MY_SUBSCRIPTIONS,
} from '../components/ChannelsFilterMenu/ChannelsFilterMenu';

const INITIAL_STATE = {
  channels: [],
  totalNumberOfChannels: 0,
  activePage: 1,
  error: null,
  loading: false,
  loadingDelete: false,
  getChannelsQuery: {
    take: Number(localStorage.getItem('take')) || 10,
    skip: 0,
    searchText: '',
    ownerFilter: false,
    subscribedFilter: false,
    favoritesFilter: false,
  },
  loadingSubscribe: false,
  loadingUnsubscribe: false,
  loadingFavorite: false,
  userSettings: {},
};

function processGetChannels(state) {
  return {
    ...state,
    loading: true,
    channels: [],
  };
}

function processGetChannelsSuccess(state, response) {
  return {
    ...state,
    channels: response.channels,
    totalNumberOfChannels: response.count,
    error: null,
    loading: false,
  };
}

function processGetChannelsFailure(state, error) {
  return {
    ...state,
    error,
    loading: false,
  };
}

function processSubscribeToChannel(state) {
  return {
    ...state,
    error: null,
    loadingSubscribe: true,
  };
}

function processSubscribeToChannelSuccess(state) {
  return {
    ...state,
    loadingSubscribe: false,
  };
}

function processUpdateSubscribeToChannel(state, data) {
  return {
    ...state,
    loadingSubscribe: false,
    channels: state.channels.map(c =>
      c.id === data.channelId ? {...c, subscribed: data.newValue} : c
    ),
  };
}

function processSubscribeToChannelFailure(state, error) {
  return {
    ...state,
    error,
    loadingSubscribe: false,
  };
}

function processUnsubscribeFromChannel(state) {
  return {
    ...state,
    error: null,
    loadingUnsubscribe: true,
  };
}
function processUnsubscribeFromChannelSuccess(state) {
  return {
    ...state,
    loadingUnsubscribe: false,
  };
}

function processUnsubscribeFromChannelFailure(state, error) {
  return {
    ...state,
    error,
    loadingUnsubscribe: false,
  };
}

function processSetElementsPerPage(state, elementsPerPage) {
  return {
    ...state,
    elementsPerPage,
  };
}

function processSetActivePage(state, activePage) {
  return {
    ...state,
    activePage,
  };
}

function processSetGetChannelsQuery(state, getChannelsQuery) {
  localStorage.setItem('take', getChannelsQuery.take);
  return {
    ...state,
    getChannelsQuery,
  };
}

function processDeleteChannel(state) {
  return {
    ...state,
    loadingDelete: true,
  };
}

function processUpdateDeleteChannel(state, channelId) {
  return {
    ...state,
    channels: state.channels.filter(c => c.id !== channelId),
    loadingDelete: false,
  };
}

function processDeleteChannelFailure(state, error) {
  return {
    ...state,
    loadingDelete: false,
    error,
  };
}

function processGetUserSettings(state) {
  return {
    ...state,
    loading: true,
  };
}

function processGetUserSettingsSuccess(state, response) {
  return {
    ...state,
    userSettings: response,
    error: null,
    loading: false,
  };
}

function processGetUserSettingsFailure(state, error) {
  return {
    ...state,
    userSettings: {},
    error,
    loading: false,
  };
}

function toggleFavoriteChannel(state) {
  return {
    ...state,
    loadingFavorite: true,
  };
}

function toggleFavoriteChannelSuccess(state, response) {
  const updatedFavorites = state.userSettings.favoriteList;
  const updatedSettings = {
    ...state.userSettings,
    favoriteList: updatedFavorites.includes(response)
      ? updatedFavorites.filter(f => f !== response)
      : [...updatedFavorites, response],
  };
  return {
    ...state,
    userSettings: updatedSettings,
    error: null,
  };
}

function toggleFavoriteChannelFailure(state, error) {
  return {
    ...state,
    error,
    loadingFavorite: false,
  };
}

export default function (state = INITIAL_STATE, action) {
  let error;
  switch (action.type) {
    case GET_CHANNELS:
      return processGetChannels(state);
    case GET_CHANNELS_SUCCESS:
      return processGetChannelsSuccess(state, action.payload);
    case GET_CHANNELS_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processGetChannelsFailure(state, error);

    case SUBSCRIBE_TO_CHANNEL:
      return processSubscribeToChannel(state);
    case SUBSCRIBE_TO_CHANNEL_SUCCESS:
      return processSubscribeToChannelSuccess(state, action.payload);
    case SUBSCRIBE_TO_CHANNEL_UPDATE_SUCCESS:
      return processUpdateSubscribeToChannel(state, action.data);
    case SUBSCRIBE_TO_CHANNEL_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processSubscribeToChannelFailure(state, error);

    case UNSUBSCRIBE_FROM_CHANNEL:
      return processUnsubscribeFromChannel(state);
    case UNSUBSCRIBE_FROM_CHANNEL_SUCCESS:
      return processUnsubscribeFromChannelSuccess(state, action.payload);
    case UNSUBSCRIBE_FROM_CHANNEL_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processUnsubscribeFromChannelFailure(state, error);

    case SET_ELEMENTS_PER_PAGE:
      return processSetElementsPerPage(state, action.payload);
    case SET_ACTIVE_PAGE:
      return processSetActivePage(state, action.payload);
    case SET_GET_CHANNELS_QUERY:
      return processSetGetChannelsQuery(state, action.payload);

    case DELETE_CHANNEL:
      return processDeleteChannel(state);
    case DELETE_CHANNEL_SUCCESS:
      return state;
    case DELETE_CHANNEL_UPDATE_SUCCESS:
      return processUpdateDeleteChannel(state, action.channelId);
    case DELETE_CHANNEL_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processDeleteChannelFailure(state, error);

    case GET_USER_SETTINGS:
      return processGetUserSettings(state);
    case GET_USER_SETTINGS_SUCCESS:
      return processGetUserSettingsSuccess(state, action.payload);
    case GET_USER_SETTINGS_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processGetUserSettingsFailure(state, error);

    case TOGGLE_FAVORITE_CHANNEL:
      return toggleFavoriteChannel(state);
    case TOGGLE_FAVORITE_CHANNEL_SUCCESS:
      return toggleFavoriteChannelSuccess(state, action.payload);
    case TOGGLE_FAVORITE_CHANNEL_FAILURE:
      error = action.payload || {message: action.payload.message};
      return toggleFavoriteChannelFailure(state, error);

    case LOCATION_CHANGE: {
      // this action is dispatched by connected-react-router, thanks to that we can update the filters
      // based on the hash in the URL
      // NOTE: having this here has a small drawback though. If an unathenticated user manually changes the URL
      // to something consisting of #manage or #subscriptions the redux state will be updated and the filters
      // will be sent to the backend (even though they are not taken into account by the backend). Having this
      // at the component level would incur even more problems thus I decided to leave it here until we fix other
      // components.
      const {
        location: {hash, pathname},
      } = action.payload;

      // TODO: use an object with all route mappings in order to replace hardcoding URLs like below
      // https://gitlab.cern.ch/push-notifications/web-portal/-/issues/55
      if (pathname !== '/') {
        return state;
      }

      if (hash === '#subscriptions') {
        return {
          ...state,
          getChannelsQuery: {
            ...state.getChannelsQuery,
            subscribedFilter: true,
            ownerFilter: false,
            favoritesFilter: false,
          },
        };
      }
      if (hash === '#manage') {
        return {
          ...state,
          getChannelsQuery: {
            ...state.getChannelsQuery,
            subscribedFilter: false,
            ownerFilter: true,
            favoritesFilter: false,
          },
        };
      }
      if (hash === '#favorites') {
        return {
          ...state,
          getChannelsQuery: {
            ...state.getChannelsQuery,
            subscribedFilter: false,
            ownerFilter: false,
            favoritesFilter: true,
          },
        };
      }
      // consecutive redirections to '/' might lead to an undesired state in which `channels === []`
      // and `getChannels` is not invoked because `getChannelsQuery` is the same object (the same references)
      // in this case `useEffect` declared in `ChannelsList` will not be used to fetch new channels. That is why
      // we need to create a copy of the `getChannelsQuery` each time user is redirected to '/'
      if (hash === '#all' || hash === '') {
        return {
          ...INITIAL_STATE,
          getChannelsQuery: {
            ...INITIAL_STATE.getChannelsQuery,
            take: Number(localStorage.getItem('take')) || 10,
            ownerFilter: localStorage.getItem('ChannelListMenu') === ALL_CHANNELS_I_OWN,
            subscribedFilter: localStorage.getItem('ChannelListMenu') === MY_SUBSCRIPTIONS,
            favoritesFilter: localStorage.getItem('ChannelListMenu') === FAVORITE_CHANNELS,
          },
        };
      }
      return state;
    }
    default:
      return state;
  }
}
