import {
  CREATE_CHANNEL,
  CREATE_CHANNEL_SUCCESS,
  CREATE_CHANNEL_FAILURE,
} from 'channels/actions/CreateChannel';

const INITIAL_STATE = {
  channel: null,
  error: null,
  loading: false,
};

function processCreateChannel(state) {
  return {
    ...state,
    loading: true,
    error: null,
  };
}

function processCreateChannelSuccess(state, channel) {
  return {
    ...state,
    channel,
    loading: false,
  };
}

function processCreateChannelFailure(state, error) {
  return {
    ...state,
    channel: null,
    error,
    loading: false,
  };
}

export default function (state = INITIAL_STATE, action) {
  let error;

  switch (action.type) {
    case CREATE_CHANNEL:
      return processCreateChannel(state);
    case CREATE_CHANNEL_SUCCESS:
      return processCreateChannelSuccess(state, action.payload);
    case CREATE_CHANNEL_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processCreateChannelFailure(state, error);

    default:
      return state;
  }
}
