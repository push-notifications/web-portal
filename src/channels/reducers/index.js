import {combineReducers} from 'redux';
import channelsList from './ChannelsList';
import newChannel from './NewChannel';
import channel from './Channel';
import categories from './Categories';
import tags from './Tags';
import channelRecommendations from './ChannelRecommendations';

const channelsReducer = combineReducers({
  channelsList,
  newChannel,
  channel,
  categories,
  tags,
  channelRecommendations,
});

export default channelsReducer;
