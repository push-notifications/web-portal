import {
  SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS,
  SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS_FAILURE,
  SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS_SUCCESS,
} from '../actions/SubscribeToChannelRecommendation';
import {
  GET_CHANNEL_RECOMMENDATIONS,
  GET_CHANNEL_RECOMMENDATIONS_FAILURE,
  GET_CHANNEL_RECOMMENDATIONS_SUCCESS,
} from '../actions/GetChannelRecommendations';
import {
  IGNORE_CHANNEL_RECOMMENDATIONS,
  IGNORE_CHANNEL_RECOMMENDATIONS_FAILURE,
  IGNORE_CHANNEL_RECOMMENDATIONS_SUCCESS,
} from '../actions/IgnoreChannelRecommendation';

const INITIAL_STATE = {
  recommendations: [],
  error: null,
  loading: false,
  loadingSubscribe: false,
  loadingIgnore: false,
};

function processGetList(state) {
  return {
    ...state,
    loading: true,
  };
}

function processGetListSuccess(state, response) {
  return {
    ...state,
    recommendations: response.channelRecommendations,
    error: null,
    loading: false,
  };
}

function processGetListFailure(state, error) {
  return {
    ...state,
    recommendations: [],
    error,
    loading: false,
  };
}

function processSubscribe(state) {
  return {
    ...state,
    loadingSubscribe: true,
  };
}

function processSubscribeSuccess(state) {
  return {
    ...state,
    error: null,
    loadingSubscribe: false,
  };
}

function processSubscribeFailure(state, error) {
  return {
    ...state,
    error,
    loadingCreate: false,
  };
}

function processIgnore(state) {
  return {
    ...state,
    loadingIgnore: true,
  };
}

function processIgnoreSuccess(state) {
  return {
    ...state,
    error: null,
    loadingIgnore: false,
  };
}

function processIgnoreFailure(state, error) {
  return {
    ...state,
    error,
    loadingCreate: false,
  };
}

export default function (state = INITIAL_STATE, action) {
  let error;
  switch (action.type) {
    case GET_CHANNEL_RECOMMENDATIONS:
      return processGetList(state);
    case GET_CHANNEL_RECOMMENDATIONS_SUCCESS:
      return processGetListSuccess(state, action.payload);
    case GET_CHANNEL_RECOMMENDATIONS_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processGetListFailure(state, error);

    case SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS:
      return processSubscribe(state);
    case SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS_SUCCESS:
      return processSubscribeSuccess(state);
    case SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processSubscribeFailure(state, error);

    case IGNORE_CHANNEL_RECOMMENDATIONS:
      return processIgnore(state);
    case IGNORE_CHANNEL_RECOMMENDATIONS_SUCCESS:
      return processIgnoreSuccess(state);
    case IGNORE_CHANNEL_RECOMMENDATIONS_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processIgnoreFailure(state, error);

    default:
      return state;
  }
}
