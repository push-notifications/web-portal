import {
  UPDATE_CHANNEL,
  UPDATE_CHANNEL_SUCCESS,
  UPDATE_CHANNEL_FAILURE,
} from 'channels/actions/UpdateChannel';
import {
  GET_CHANNELS_MEMBERS,
  GET_CHANNELS_MEMBERS_SUCCESS,
  GET_CHANNELS_MEMBERS_FAILURE,
  SET_GET_CHANNEL_MEMBERS_QUERY,
  RESET_GET_CHANNEL_MEMBERS_QUERY,
} from 'channels/actions/GetChannelMembers';
import {
  GET_CHANNEL_BY_ID_SUCCESS,
  GET_CHANNEL_BY_ID_FAILURE,
  GET_CHANNEL_BY_ID,
} from 'channels/actions/GetChannelById';
import {
  GET_CHANNEL_EDIT_INFO_BY_ID_SUCCESS,
  GET_CHANNEL_EDIT_INFO_BY_ID_FAILURE,
  GET_CHANNEL_EDIT_INFO_BY_ID_PAGE_ERROR,
  GET_CHANNEL_EDIT_INFO_BY_ID,
} from 'channels/actions/GetChannelEditInfoById';
import {
  GET_CHANNELS_GROUPS,
  GET_CHANNELS_GROUPS_SUCCESS,
  GET_CHANNELS_GROUPS_FAILURE,
  SET_GET_CHANNEL_GROUPS_QUERY,
  RESET_GET_CHANNEL_GROUPS_QUERY,
} from 'channels/actions/GetChannelGroups';
import {
  REMOVE_USER_FROM_CHANNEL,
  REMOVE_USER_FROM_CHANNEL_FAILURE,
  REMOVE_USER_FROM_CHANNEL_SUCCESS,
} from 'channels/actions/RemoveUserFromChannel';
import {
  REMOVE_GROUP_FROM_CHANNEL,
  REMOVE_GROUP_FROM_CHANNEL_FAILURE,
  REMOVE_GROUP_FROM_CHANNEL_SUCCESS,
} from 'channels/actions/RemoveGroupFromChannel';
import {
  ADD_USER_TO_CHANNEL,
  ADD_USER_TO_CHANNEL_FAILURE,
  ADD_USER_TO_CHANNEL_SUCCESS,
} from 'channels/actions/AddUserToChannel';
import {
  ADD_GROUP_TO_CHANNEL,
  ADD_GROUP_TO_CHANNEL_FAILURE,
  ADD_GROUP_TO_CHANNEL_SUCCESS,
} from 'channels/actions/AddGroupToChannel';
import {
  UPDATE_CHANNEL_ADMIN,
  UPDATE_CHANNEL_ADMIN_FAILURE,
  UPDATE_CHANNEL_ADMIN_SUCCESS,
} from 'channels/actions/UpdateChannelAdminGroup';
import {
  REMOVE_CHANNEL_ADMIN,
  REMOVE_CHANNEL_ADMIN_FAILURE,
  REMOVE_CHANNEL_ADMIN_SUCCESS,
} from 'channels/actions/RemoveChannelAdminGroup';
import {
  GENERATE_APIKEY,
  GENERATE_APIKEY_FAILURE,
  GENERATE_APIKEY_SUCCESS,
} from 'channels/actions/GenerateAPIKey';
import {
  SET_CHANNEL_CATEGORY,
  SET_CHANNEL_CATEGORY_FAILURE,
  SET_CHANNEL_CATEGORY_SUCCESS,
} from 'channels/actions/SetChannelCategory';
import {
  SET_CHANNEL_TAGS,
  SET_CHANNEL_TAGS_FAILURE,
  SET_CHANNEL_TAGS_SUCCESS,
} from 'channels/actions/SetChannelTags';
import {
  UPDATE_REMOVE_USER_FROM_CHANNEL,
  UPDATE_REMOVE_GROUP_FROM_CHANNEL,
  SUBSCRIBE_TO_CHANNEL_UPDATE_SUCCESS,
} from 'channels/actions/NoContentActionsUpdate';

const DEFAULT_GET_QUERY_PARAMS = {
  searchText: '',
  skip: 0,
  take: 10,
};

const INITIAL_STATE = {
  channel: {},
  error: null,
  pageError: null,
  loading: false,
  loadingMembers: false,
  loadingGroups: false,
  loadingUpdate: false,
  loadingRemoveUser: false,
  loadingRemoveGroup: false,
  loadingAddGroup: false,
  loadingAddUser: false,
  loadingUpdateAdmin: false,
  loadingUpdateCategory: false,
  loadingUpdateTags: false,
  loadingGenerate: false,
  members: [],
  getChannelMembersQuery: DEFAULT_GET_QUERY_PARAMS,
  totalNumberOfMembers: 0,
  groups: [],
  getChannelGroupsQuery: DEFAULT_GET_QUERY_PARAMS,
  totalNumberOfGroups: 0,
};

function processGetChannelById(state) {
  return {
    ...state,
    channel: {},
    error: null,
    pageError: null,
    loading: true,
  };
}

function processUpdateSubscribeToChannel(state, data) {
  return {
    ...state,
    channel: {...state.channel, subscribed: data.newValue},
  };
}

function processGetChannelByIdSuccess(state, channel) {
  return {
    ...state,
    loading: false,
    error: null,
    channel,
  };
}

function processGetChannelByIdFailure(state, error) {
  return {
    ...state,
    channel: {},
    error,
    loading: false,
  };
}

function processGetChannelEditInfoById(state) {
  return {
    ...state,
    channel: {},
    error: null,
    pageError: null,
    loading: true,
  };
}

function processGetChannelEditInfoByIdSuccess(state, channel) {
  return {
    ...state,
    loading: false,
    error: null,
    pageError: null,
    channel,
  };
}

function processGetChannelEditInfoByIdFailure(state, error) {
  return {
    ...state,
    channel: {},
    error,
    loading: false,
  };
}

function processGetChannelEditInfoByIdPageError(state, error) {
  return {
    ...state,
    channel: {},
    pageError: error,
    loading: false,
  };
}

function processUpdateChannel(state) {
  return {
    ...state,
    error: null,
    pageError: null,
    loadingUpdate: true,
  };
}

function processUpdateChannelSuccess(state, channel) {
  return {
    ...state,
    channel,
    loadingUpdate: false,
  };
}

function processUpdateChannelFailure(state, error) {
  return {
    ...state,
    error,
    loadingUpdate: false,
  };
}

function processUpdateAPIKey(state) {
  return {
    ...state,
    error: null,
    pageError: null,
    loadingGenerate: true,
  };
}
function processUpdateAPIKeySuccess(state, newAPIkey) {
  const updatedChannel = state.channel;
  updatedChannel.APIKey = newAPIkey;
  return {
    ...state,
    channel: updatedChannel,
    loadingGenerate: false,
  };
}

function processUpdateAPIKeyFailure(state, error) {
  return {
    ...state,
    error,
    loadingGenerate: false,
  };
}

function processUpdateChannelAdmin(state) {
  return {
    ...state,
    error: null,
    pageError: null,
    loadingUpdateAdmin: true,
  };
}

function processUpdateChannelAdminSuccess(state, channel) {
  return {
    ...state,
    channel,
    loadingUpdateAdmin: false,
  };
}

function processUpdateChannelAdminFailure(state, error) {
  return {
    ...state,
    error,
    loadingUpdateAdmin: false,
  };
}

function processRemoveChannelAdmin(state) {
  return {
    ...state,
    error: null,
    loadingUpdateAdmin: true,
  };
}

function processRemoveChannelAdminSuccess(state) {
  // Since this is a NoContent return call but we need the channel information to build the
  // EditChannel page, we need to use the current state, but removingfrom it the adminGroup
  // (in this funtion we are sure that the deletion was successful)
  delete state.channel.adminGroup;
  return {
    ...state,
    channel: state.channel,
    loadingUpdateAdmin: false,
  };
}

function processRemoveChannelAdminFailure(state, error) {
  return {
    ...state,
    error,
    loadingUpdateAdmin: false,
  };
}

function processGetChannelMembers(state) {
  return {
    ...state,
    error: null,
    pageError: null,
    loadingMembers: true,
  };
}

function processGetChannelMembersSuccess(state, {members, totalNumberOfMembers}) {
  return {
    ...state,
    members,
    totalNumberOfMembers,
    loadingMembers: false,
  };
}

function processGetChannelMembersFailure(state, error) {
  return {
    ...state,
    channel: {},
    members: [],
    error,
    loadingMembers: false,
  };
}

function processGetChannelGroups(state) {
  return {
    ...state,
    groups: [],
    error: null,
    pageError: null,
    loadingGroups: true,
  };
}

function processGetChannelGroupsSuccess(state, {groups, totalNumberOfGroups}) {
  return {
    ...state,
    groups,
    totalNumberOfGroups,
    loadingGroups: false,
  };
}

function processGetChannelGroupsFailure(state, error) {
  return {
    ...state,
    groups: [],
    error,
    loadingGroups: false,
  };
}

function processRemoveUserFromChannel(state) {
  return {
    ...state,
    error: null,
    pageError: null,
    loadingRemoveUser: true,
  };
}

function processUpdateRemoveUserFromChannel(state, userId) {
  const updatedMemberList = state.members.filter(m => m.id !== userId);
  return {
    ...state,
    loadingRemoveUser: false,
    members: updatedMemberList,
  };
}

function processRemoveUserFromChannelFailure(state, error) {
  return {
    ...state,
    error,
    loadingRemoveUser: false,
  };
}

function processRemoveGroupFromChannel(state) {
  return {
    ...state,
    error: null,
    pageError: null,
    loadingRemoveGroup: true,
  };
}

function processUpdateRemoveGroupFromChannel(state, groupId) {
  const updatedGroups = state.groups.filter(g => g.id !== groupId);
  return {
    ...state,
    loadingRemoveGroup: false,
    groups: updatedGroups,
  };
}

function processRemoveGroupFromChannelFailure(state, error) {
  return {
    ...state,
    error,
    loadingRemoveGroup: false,
  };
}

function processSetGetChannelMembersQuery(state, getChannelMembersQuery) {
  return {
    ...state,
    getChannelMembersQuery: {...getChannelMembersQuery},
  };
}

function processResetGetChannelMembersQuery(state) {
  return {
    ...state,
    getChannelMembersQuery: {...DEFAULT_GET_QUERY_PARAMS},
  };
}

function processSetGetChannelGroupsQuery(state, getChannelGroupsQuery) {
  return {
    ...state,
    getChannelGroupsQuery: {...getChannelGroupsQuery},
  };
}

function processResetGetChannelGroupsQuery(state) {
  return {
    ...state,
    getChannelGroupsQuery: {...DEFAULT_GET_QUERY_PARAMS},
  };
}

function processAddUserToChannel(state) {
  return {
    ...state,
    error: null,
    pageError: null,
    loadingAddUser: true,
  };
}

function processAddUserToChannelSuccess(state, member) {
  return {
    ...state,
    loadingAddUser: false,
    members: [...state.members, member],
  };
}

function processAddUserToChannelFailure(state, error) {
  return {
    ...state,
    error,
    loadingAddUser: false,
  };
}

function processAddGroupToChannel(state) {
  return {
    ...state,
    error: null,
    pageError: null,
    loadingAddGroup: true,
  };
}

function processAddGroupToChannelSuccess(state, group) {
  return {
    ...state,
    loadingAddGroup: false,
    groups: [...state.groups, group],
  };
}

function processAddGroupToChannelFailure(state, error) {
  return {
    ...state,
    error,
    loadingAddGroup: false,
  };
}

function processSetChannelCategory(state) {
  return {
    ...state,
    error: null,
    pageError: null,
    loadingUpdateCategory: true,
  };
}

function processSetChannelCategorySuccess(state, {category}) {
  return {
    ...state,
    channel: {...state.channel, category},
    loadingUpdateCategory: false,
  };
}

function processSetChannelCategoryFailure(state, error) {
  return {
    ...state,
    error,
    loadingUpdateCategory: false,
  };
}

function processSetChannelTags(state) {
  return {
    ...state,
    error: null,
    pageError: null,
    loadingUpdateTags: true,
  };
}

function processSetChannelTagsSuccess(state, {tags}) {
  return {
    ...state,
    channel: {...state.channel, tags},
    loadingUpdateTags: false,
  };
}

function processSetChannelTagsFailure(state, error) {
  return {
    ...state,
    error,
    loadingUpdateTags: false,
  };
}

export default function (state = INITIAL_STATE, action) {
  let error;

  switch (action.type) {
    case GET_CHANNEL_BY_ID:
      return processGetChannelById(state);
    case GET_CHANNEL_BY_ID_SUCCESS:
      return processGetChannelByIdSuccess(state, action.payload);
    case GET_CHANNEL_BY_ID_FAILURE:
      error = action.payload || {message: action.payload.response.message};
      return processGetChannelByIdFailure(state, error);

    case GET_CHANNEL_EDIT_INFO_BY_ID:
      return processGetChannelEditInfoById(state);
    case GET_CHANNEL_EDIT_INFO_BY_ID_SUCCESS:
      return processGetChannelEditInfoByIdSuccess(state, action.payload);
    case GET_CHANNEL_EDIT_INFO_BY_ID_FAILURE:
      error = action.payload || {message: action.payload.response.message};
      return processGetChannelEditInfoByIdFailure(state, error);
    case GET_CHANNEL_EDIT_INFO_BY_ID_PAGE_ERROR:
      error = action.payload || {message: action.payload.response.message};
      return processGetChannelEditInfoByIdPageError(state, error);

    case UPDATE_CHANNEL:
      return processUpdateChannel(state);
    case UPDATE_CHANNEL_SUCCESS:
      return processUpdateChannelSuccess(state, action.payload);
    case UPDATE_CHANNEL_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processUpdateChannelFailure(state, error);

    case UPDATE_CHANNEL_ADMIN:
      return processUpdateChannelAdmin(state);
    case UPDATE_CHANNEL_ADMIN_SUCCESS:
      return processUpdateChannelAdminSuccess(state, action.payload);
    case UPDATE_CHANNEL_ADMIN_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processUpdateChannelAdminFailure(state, error);

    case REMOVE_CHANNEL_ADMIN:
      return processRemoveChannelAdmin(state);
    case REMOVE_CHANNEL_ADMIN_SUCCESS:
      return processRemoveChannelAdminSuccess(state, action.payload);
    case REMOVE_CHANNEL_ADMIN_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processRemoveChannelAdminFailure(state, error);

    case GET_CHANNELS_MEMBERS:
      return processGetChannelMembers(state);
    case GET_CHANNELS_MEMBERS_SUCCESS:
      return processGetChannelMembersSuccess(state, action.payload);
    case GET_CHANNELS_MEMBERS_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processGetChannelMembersFailure(state, error);

    case GET_CHANNELS_GROUPS:
      return processGetChannelGroups(state);
    case GET_CHANNELS_GROUPS_SUCCESS:
      return processGetChannelGroupsSuccess(state, action.payload);
    case GET_CHANNELS_GROUPS_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processGetChannelGroupsFailure(state, error);

    case REMOVE_USER_FROM_CHANNEL:
      return processRemoveUserFromChannel(state);
    case REMOVE_USER_FROM_CHANNEL_SUCCESS:
      return state;
    case UPDATE_REMOVE_USER_FROM_CHANNEL:
      return processUpdateRemoveUserFromChannel(state, action.userId);
    case REMOVE_USER_FROM_CHANNEL_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processRemoveUserFromChannelFailure(state, error);

    case REMOVE_GROUP_FROM_CHANNEL:
      return processRemoveGroupFromChannel(state);
    case REMOVE_GROUP_FROM_CHANNEL_SUCCESS:
      return state;
    case UPDATE_REMOVE_GROUP_FROM_CHANNEL:
      return processUpdateRemoveGroupFromChannel(state, action.groupId);
    case REMOVE_GROUP_FROM_CHANNEL_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processRemoveGroupFromChannelFailure(state, error);

    case SET_GET_CHANNEL_MEMBERS_QUERY:
      return processSetGetChannelMembersQuery(state, action.payload);
    case RESET_GET_CHANNEL_MEMBERS_QUERY:
      return processResetGetChannelMembersQuery(state);
    case SET_GET_CHANNEL_GROUPS_QUERY:
      return processSetGetChannelGroupsQuery(state, action.payload);
    case RESET_GET_CHANNEL_GROUPS_QUERY:
      return processResetGetChannelGroupsQuery(state);

    case ADD_USER_TO_CHANNEL:
      return processAddUserToChannel(state);
    case ADD_USER_TO_CHANNEL_SUCCESS:
      return processAddUserToChannelSuccess(state, action.payload);
    case ADD_USER_TO_CHANNEL_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processAddUserToChannelFailure(state, error);

    case ADD_GROUP_TO_CHANNEL:
      return processAddGroupToChannel(state);
    case ADD_GROUP_TO_CHANNEL_SUCCESS:
      return processAddGroupToChannelSuccess(state, action.payload);
    case ADD_GROUP_TO_CHANNEL_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processAddGroupToChannelFailure(state, error);

    // Same processing as channel update
    case GENERATE_APIKEY:
      return processUpdateAPIKey(state);
    case GENERATE_APIKEY_SUCCESS:
      return processUpdateAPIKeySuccess(state, action.payload);
    case GENERATE_APIKEY_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processUpdateAPIKeyFailure(state, error);

    case SET_CHANNEL_CATEGORY:
      return processSetChannelCategory(state);
    case SET_CHANNEL_CATEGORY_SUCCESS:
      return processSetChannelCategorySuccess(state, action.payload);
    case SET_CHANNEL_CATEGORY_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processSetChannelCategoryFailure(state, error);

    case SET_CHANNEL_TAGS:
      return processSetChannelTags(state);
    case SET_CHANNEL_TAGS_SUCCESS:
      return processSetChannelTagsSuccess(state, action.payload);
    case SET_CHANNEL_TAGS_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processSetChannelTagsFailure(state, error);

    case SUBSCRIBE_TO_CHANNEL_UPDATE_SUCCESS:
      return processUpdateSubscribeToChannel(state, action.data);

    default:
      return state;
  }
}
