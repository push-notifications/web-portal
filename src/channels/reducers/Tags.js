import {GET_TAGS, GET_TAGS_SUCCESS, GET_TAGS_FAILURE} from 'channels/actions/GetTags';
import {CREATE_TAG, CREATE_TAG_SUCCESS, CREATE_TAG_FAILURE} from 'channels/actions/CreateTag';

const INITIAL_STATE = {
  tags: [],
  tagsIds: [],
  error: null,
  loading: false,
  loadingCreate: false,
};

function processGetTags(state) {
  return {
    ...state,
    loading: true,
  };
}

function processGetTagsSuccess(state, response) {
  return {
    ...state,
    tags: response.tags,
    tagsIds: response.tags.map(tag => tag.id),
    error: null,
    loading: false,
  };
}

function processGetTagsFailure(state, error) {
  return {
    ...state,
    tags: [],
    tagsIds: [],
    error,
    loading: false,
  };
}

function processCreateTag(state) {
  return {
    ...state,
    loadingCreate: true,
  };
}

function processCreateTagSuccess(state, response) {
  return {
    ...state,
    tags: [...state.tags, response],
    tagsIds: [...state.tagsIds, response.id],
    error: null,
    loadingCreate: false,
  };
}

function processCreateTagFailure(state, error) {
  return {
    ...state,
    error,
    loadingCreate: false,
  };
}

export default function (state = INITIAL_STATE, action) {
  let error;
  switch (action.type) {
    case GET_TAGS:
      return processGetTags(state);
    case GET_TAGS_SUCCESS:
      return processGetTagsSuccess(state, action.payload);
    case GET_TAGS_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processGetTagsFailure(state, error);

    case CREATE_TAG:
      return processCreateTag(state);
    case CREATE_TAG_SUCCESS:
      return processCreateTagSuccess(state, action.payload);
    case CREATE_TAG_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processCreateTagFailure(state, error);

    default:
      return state;
  }
}
