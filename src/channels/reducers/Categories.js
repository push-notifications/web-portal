import {
  GET_CATEGORIES,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAILURE,
} from 'channels/actions/GetCategories';
import {
  CREATE_CATEGORY,
  CREATE_CATEGORY_SUCCESS,
  CREATE_CATEGORY_FAILURE,
} from 'channels/actions/CreateCategory';

const INITIAL_STATE = {
  categories: [],
  error: null,
  loadingCategories: false,
};

function processGetCategories(state) {
  return {
    ...state,
    loadingCategories: true,
  };
}

function processGetCategoriesSuccess(state, response) {
  return {
    ...state,
    categories: response,
    error: null,
    loadingCategories: false,
  };
}

function processGetCategoriesFailure(state, error) {
  return {
    ...state,
    categories: [],
    error,
    loadingCategories: false,
  };
}

function processCreateCategory(state) {
  return {
    ...state,
    loadingCategories: true,
  };
}

function processCreateCategorySuccess(state, response) {
  return {
    ...state,
    categories: [...state.categories, response],
    error: null,
    loadingCategories: false,
  };
}

function processCreateCategoryFailure(state, error) {
  return {
    ...state,
    error,
    loadingCategories: false,
  };
}

export default function (state = INITIAL_STATE, action) {
  let error;
  switch (action.type) {
    case GET_CATEGORIES:
      return processGetCategories(state);
    case GET_CATEGORIES_SUCCESS:
      return processGetCategoriesSuccess(state, action.payload);
    case GET_CATEGORIES_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processGetCategoriesFailure(state, error);

    case CREATE_CATEGORY:
      return processCreateCategory(state);
    case CREATE_CATEGORY_SUCCESS:
      return processCreateCategorySuccess(state, action.payload);
    case CREATE_CATEGORY_FAILURE:
      error = action.payload || {message: action.payload.message};
      return processCreateCategoryFailure(state, error);

    default:
      return state;
  }
}
