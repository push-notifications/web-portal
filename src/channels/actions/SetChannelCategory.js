import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const SET_CHANNEL_CATEGORY = 'SET_CHANNEL_CATEGORY';
export const SET_CHANNEL_CATEGORY_SUCCESS = 'SET_CHANNEL_CATEGORY_SUCCESS';
export const SET_CHANNEL_CATEGORY_FAILURE = 'SET_CHANNEL_CATEGORY_FAILURE';
// eslint-disable-next-line import/prefer-default-export
export const setChannelCategory = (category, channelId) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}/category`,
    method: 'PUT',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({category}),
    types: [SET_CHANNEL_CATEGORY, SET_CHANNEL_CATEGORY_SUCCESS, SET_CHANNEL_CATEGORY_FAILURE],
  },
});
