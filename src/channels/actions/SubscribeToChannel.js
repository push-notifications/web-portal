import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const SUBSCRIBE_TO_CHANNEL = 'SUBSCRIBE_TO_CHANNEL';
export const SUBSCRIBE_TO_CHANNEL_SUCCESS = 'SUBSCRIBE_TO_CHANNEL_SUCCESS';
export const SUBSCRIBE_TO_CHANNEL_FAILURE = 'SUBSCRIBE_TO_CHANNEL_FAILURE';

export const subscribeToChannel = channelId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}/subscribe`,
    method: 'PUT',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [SUBSCRIBE_TO_CHANNEL, SUBSCRIBE_TO_CHANNEL_SUCCESS, SUBSCRIBE_TO_CHANNEL_FAILURE],
  },
});
