import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS = 'SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS';
export const SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS_SUCCESS =
  'SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS_SUCCESS';
export const SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS_FAILURE =
  'SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS_FAILURE';

export const subscribeToChannelRecommendation = channelRecommendationId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channel-recommendations/${channelRecommendationId}`,
    method: 'PATCH',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({subscribedAt: new Date()}),
    types: [
      SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS,
      SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS_SUCCESS,
      SUBSCRIBE_TO_CHANNEL_RECOMMENDATIONS_FAILURE,
    ],
  },
});
