// Triggered after any 204 no Content response (usually Deletes), to update the state
export const SUBSCRIBE_TO_CHANNEL_UPDATE_SUCCESS = 'SUBSCRIBE_TO_CHANNEL_UPDATE_SUCCESS';
export const DELETE_CHANNEL_UPDATE_SUCCESS = 'DELETE_CHANNEL_UPDATE_SUCCESS';
export const UPDATE_REMOVE_USER_FROM_CHANNEL = 'UPDATE_REMOVE_USER_FROM_CHANNEL';
export const UPDATE_REMOVE_GROUP_FROM_CHANNEL = 'UPDATE_REMOVE_GROUP_FROM_CHANNEL';

export const updateSubscribeToChannel = (channelId, newValue) => {
  return {
    data: {
      channelId,
      newValue,
    },
    type: SUBSCRIBE_TO_CHANNEL_UPDATE_SUCCESS,
  };
};

export const updateDeletedChannel = channelId => {
  return {
    channelId,
    type: DELETE_CHANNEL_UPDATE_SUCCESS,
  };
};

export const updateRemoveUser = userId => {
  return {
    userId,
    type: UPDATE_REMOVE_USER_FROM_CHANNEL,
  };
};

export const updateRemoveGroup = groupId => {
  return {
    groupId,
    type: UPDATE_REMOVE_GROUP_FROM_CHANNEL,
  };
};
