import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils.js';

// CREATE notifications
export const DELETE_CHANNEL = 'DELETE_CHANNEL';
export const DELETE_CHANNEL_SUCCESS = 'DELETE_CHANNEL_SUCCESS';
export const DELETE_CHANNEL_FAILURE = 'DELETE_CHANNEL_FAILURE';

export const deleteChannel = channelId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}`,
    method: 'DELETE',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [DELETE_CHANNEL, DELETE_CHANNEL_SUCCESS, DELETE_CHANNEL_FAILURE],
  },
});
