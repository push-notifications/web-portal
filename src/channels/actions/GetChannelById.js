import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

// Get channel
export const GET_CHANNEL_BY_ID = 'GET_CHANNEL_BY_ID';
export const GET_CHANNEL_BY_ID_SUCCESS = 'GET_CHANNEL_BY_ID_SUCCESS';
export const GET_CHANNEL_BY_ID_FAILURE = 'GET_CHANNEL_BY_ID_FAILURE';

export const getChannelById = channelId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [GET_CHANNEL_BY_ID, GET_CHANNEL_BY_ID_SUCCESS, GET_CHANNEL_BY_ID_FAILURE],
  },
});
