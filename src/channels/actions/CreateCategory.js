import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils.js';

// CREATE CATEGORY
export const CREATE_CATEGORY = 'CREATE_CATEGORY';
export const CREATE_CATEGORY_SUCCESS = 'CREATE_CATEGORY_SUCCESS';
export const CREATE_CATEGORY_FAILURE = 'CREATE_CATEGORY_FAILURE';

export const createCategory = category => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/categories`,
    method: 'POST',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({category}),
    types: [CREATE_CATEGORY, CREATE_CATEGORY_SUCCESS, CREATE_CATEGORY_FAILURE],
  },
});
