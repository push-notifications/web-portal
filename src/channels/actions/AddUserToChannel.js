import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

// CREATE notifications
export const ADD_USER_TO_CHANNEL = 'ADD_USER_TO_CHANNEL';
export const ADD_USER_TO_CHANNEL_SUCCESS = 'ADD_USER_TO_CHANNEL_SUCCESS';
export const ADD_USER_TO_CHANNEL_FAILURE = 'ADD_USER_TO_CHANNEL_FAILURE';

export const addUserToChannel = (username, channelId) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}/members/${username}`,
    method: 'POST',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [ADD_USER_TO_CHANNEL, ADD_USER_TO_CHANNEL_SUCCESS, ADD_USER_TO_CHANNEL_FAILURE],
  },
});
