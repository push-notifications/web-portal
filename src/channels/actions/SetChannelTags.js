import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const SET_CHANNEL_TAGS = 'SET_CHANNEL_TAGS';
export const SET_CHANNEL_TAGS_SUCCESS = 'SET_CHANNEL_TAGS_SUCCESS';
export const SET_CHANNEL_TAGS_FAILURE = 'SET_CHANNEL_TAGS_FAILURE';
// eslint-disable-next-line import/prefer-default-export
export const setChannelTags = (tags, channelId) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}/tags`,
    method: 'PUT',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({tagIds: tags}),
    types: [SET_CHANNEL_TAGS, SET_CHANNEL_TAGS_SUCCESS, SET_CHANNEL_TAGS_FAILURE],
  },
});
