import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

// Get tags
export const GET_TAGS = 'GET_TAGS';
export const GET_TAGS_SUCCESS = 'GET_TAGS_SUCCESS';
export const GET_TAGS_FAILURE = 'GET_TAGS_FAILURE';

export const getTags = () => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/tags`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [GET_TAGS, GET_TAGS_SUCCESS, GET_TAGS_FAILURE],
  },
});
