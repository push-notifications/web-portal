import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const IGNORE_CHANNEL_RECOMMENDATIONS = 'IGNORE_CHANNEL_RECOMMENDATIONS';
export const IGNORE_CHANNEL_RECOMMENDATIONS_SUCCESS = 'IGNORE_CHANNEL_RECOMMENDATIONS_SUCCESS';
export const IGNORE_CHANNEL_RECOMMENDATIONS_FAILURE = 'IGNORE_CHANNEL_RECOMMENDATIONS_FAILURE';

export const ignoreChannelRecommendation = channelRecommendationId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channel-recommendations/${channelRecommendationId}`,
    method: 'PATCH',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({ignoredAt: new Date()}),
    types: [
      IGNORE_CHANNEL_RECOMMENDATIONS,
      IGNORE_CHANNEL_RECOMMENDATIONS_SUCCESS,
      IGNORE_CHANNEL_RECOMMENDATIONS_FAILURE,
    ],
  },
});
