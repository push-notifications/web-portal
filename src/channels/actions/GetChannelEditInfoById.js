import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

// Edit Channel by ID
export const GET_CHANNEL_EDIT_INFO_BY_ID = 'GET_CHANNEL_EDIT_INFO_BY_ID';
export const GET_CHANNEL_EDIT_INFO_BY_ID_SUCCESS = 'GET_CHANNEL_EDIT_INFO_BY_ID_SUCCESS';
export const GET_CHANNEL_EDIT_INFO_BY_ID_FAILURE = 'GET_CHANNEL_EDIT_INFO_BY_ID_FAILURE';
export const GET_CHANNEL_EDIT_INFO_BY_ID_PAGE_ERROR = 'GET_CHANNEL_EDIT_INFO_BY_ID_PAGE_ERROR';

export const getChannelEditInfoById = (channelId, pageError) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}/edit`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [
      GET_CHANNEL_EDIT_INFO_BY_ID,
      GET_CHANNEL_EDIT_INFO_BY_ID_SUCCESS,
      pageError ? GET_CHANNEL_EDIT_INFO_BY_ID_PAGE_ERROR : GET_CHANNEL_EDIT_INFO_BY_ID_FAILURE,
    ],
  },
});
