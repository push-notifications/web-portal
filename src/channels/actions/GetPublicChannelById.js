import {RSAA} from 'redux-api-middleware';

// Get (PUBLIC) channel (User is not logged in)
export const GET_CHANNEL_BY_ID = 'GET_CHANNEL_BY_ID';
export const GET_CHANNEL_BY_ID_SUCCESS = 'GET_CHANNEL_BY_ID_SUCCESS';
export const GET_CHANNEL_BY_ID_FAILURE = 'GET_CHANNEL_BY_ID_FAILURE';

export const getPublicChannelById = channelId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/public/channels/${channelId}`,
    method: 'GET',
    credentials: 'include',
    headers: {'Content-Type': 'application/json'},
    types: [GET_CHANNEL_BY_ID, GET_CHANNEL_BY_ID_SUCCESS, GET_CHANNEL_BY_ID_FAILURE],
  },
});
