import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const ADD_GROUP_TO_CHANNEL = 'ADD_GROUP_TO_CHANNEL';
export const ADD_GROUP_TO_CHANNEL_SUCCESS = 'ADD_GROUP_TO_CHANNEL_SUCCESS';
export const ADD_GROUP_TO_CHANNEL_FAILURE = 'ADD_GROUP_TO_CHANNEL_FAILURE';

export const addGroupToChannel = (groupName, channelId) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}/groups/${groupName}`,
    method: 'PUT',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    //body: JSON.stringify({group}),
    types: [ADD_GROUP_TO_CHANNEL, ADD_GROUP_TO_CHANNEL_SUCCESS, ADD_GROUP_TO_CHANNEL_FAILURE],
  },
});
