import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils.js';

// CREATE TAG
export const CREATE_TAG = 'CREATE_TAG';
export const CREATE_TAG_SUCCESS = 'CREATE_TAG_SUCCESS';
export const CREATE_TAG_FAILURE = 'CREATE_TAG_FAILURE';

export const createTag = tag => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/tags`,
    method: 'POST',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({tag}),
    types: [CREATE_TAG, CREATE_TAG_SUCCESS, CREATE_TAG_FAILURE],
  },
});
