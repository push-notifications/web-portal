import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const UNSUBSCRIBE_FROM_CHANNEL = 'UNSUBSCRIBE_FROM_CHANNEL';
export const UNSUBSCRIBE_FROM_CHANNEL_SUCCESS = 'UNSUBSCRIBE_FROM_CHANNEL_SUCCESS';
export const UNSUBSCRIBE_FROM_CHANNEL_FAILURE = 'UNSUBSCRIBE_FROM_CHANNEL_FAILURE';

export const unsubscribeFromChannel = channelId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}/unsubscribe`,
    method: 'PUT',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [
      UNSUBSCRIBE_FROM_CHANNEL,
      UNSUBSCRIBE_FROM_CHANNEL_SUCCESS,
      UNSUBSCRIBE_FROM_CHANNEL_FAILURE,
    ],
  },
});
