import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

// CREATE notifications
export const REMOVE_USER_FROM_CHANNEL = 'REMOVE_USER_FROM_CHANNEL';
export const REMOVE_USER_FROM_CHANNEL_SUCCESS = 'REMOVE_USER_FROM_CHANNEL_SUCCESS';
export const REMOVE_USER_FROM_CHANNEL_FAILURE = 'REMOVE_USER_FROM_CHANNEL_FAILURE';

export const removeUserFromChannel = (userId, channelId) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}/members/${userId}`,
    method: 'DELETE',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [
      REMOVE_USER_FROM_CHANNEL,
      REMOVE_USER_FROM_CHANNEL_SUCCESS,
      REMOVE_USER_FROM_CHANNEL_FAILURE,
    ],
  },
});
