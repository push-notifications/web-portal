import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

// CREATE notifications
export const UPDATE_CHANNEL = 'UPDATE_CHANNEL';
export const UPDATE_CHANNEL_SUCCESS = 'UPDATE_CHANNEL_SUCCESS';
export const UPDATE_CHANNEL_FAILURE = 'UPDATE_CHANNEL_FAILURE';

export const updateChannel = channel => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels`,
    method: 'PUT',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({channel}),
    types: [UPDATE_CHANNEL, UPDATE_CHANNEL_SUCCESS, UPDATE_CHANNEL_FAILURE],
  },
});
