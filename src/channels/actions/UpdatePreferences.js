import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const UPDATE_CHANNEL_GLOBAL_PREFERENCES = 'UPDATE_CHANNEL_GLOBAL_PREFERENCES';
export const UPDATE_CHANNEL_GLOBAL_PREFERENCES_SUCCESS =
  'UPDATE_CHANNEL_GLOBAL_PREFERENCES_SUCCESS';
export const UPDATE_CHANNEL_GLOBAL_PREFERENCES_FAILURE =
  'UPDATE_CHANNEL_GLOBAL_PREFERENCES_FAILURE';

// eslint-disable-next-line import/prefer-default-export
export const updatePreferences = preference => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/preferences`,
    method: 'PUT',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({preference}),
    types: [
      UPDATE_CHANNEL_GLOBAL_PREFERENCES,
      UPDATE_CHANNEL_GLOBAL_PREFERENCES_SUCCESS,
      UPDATE_CHANNEL_GLOBAL_PREFERENCES_FAILURE,
    ],
  },
});
