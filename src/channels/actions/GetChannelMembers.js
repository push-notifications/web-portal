import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';
import qs from 'qs';

// Get notifications
export const GET_CHANNELS_MEMBERS = 'GET_CHANNELS_MEMBERS';
export const GET_CHANNELS_MEMBERS_SUCCESS = 'GET_CHANNELS_MEMBERS_SUCCESS';
export const GET_CHANNELS_MEMBERS_FAILURE = 'GET_CHANNELS_MEMBERS_FAILURE';

export const SET_GET_CHANNEL_MEMBERS_QUERY = 'SET_GET_CHANNEL_MEMBERS_QUERY';
export const RESET_GET_CHANNEL_MEMBERS_QUERY = 'RESET_GET_CHANNEL_MEMBERS_QUERY';

export const getChannelMembers = (channelId, query) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}/members?${qs.stringify(
      query
    )}`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [GET_CHANNELS_MEMBERS, GET_CHANNELS_MEMBERS_SUCCESS, GET_CHANNELS_MEMBERS_FAILURE],
  },
});

export const setGetChannelMembersQuery = query => {
  return {
    type: SET_GET_CHANNEL_MEMBERS_QUERY,
    payload: query,
  };
};

export const resetChannelMembersQuery = () => {
  return {
    type: RESET_GET_CHANNEL_MEMBERS_QUERY,
  };
};
