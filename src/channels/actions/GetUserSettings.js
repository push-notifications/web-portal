import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

// Get user settings
export const GET_USER_SETTINGS = 'GET_FILTERS';
export const GET_USER_SETTINGS_SUCCESS = 'GET_FILTERS_SUCCESS';
export const GET_USER_SETTINGS_FAILURE = 'GET_FILTERS_FAILURE';

export const getUserSettings = () => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/usersettings/`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [GET_USER_SETTINGS, GET_USER_SETTINGS_SUCCESS, GET_USER_SETTINGS_FAILURE],
  },
});
