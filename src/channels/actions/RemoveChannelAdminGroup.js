import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const REMOVE_CHANNEL_ADMIN = 'REMOVE_CHANNEL_ADMIN';
export const REMOVE_CHANNEL_ADMIN_SUCCESS = 'REMOVE_CHANNEL_ADMIN_SUCCESS';
export const REMOVE_CHANNEL_ADMIN_FAILURE = 'REMOVE_CHANNEL_ADMIN_FAILURE';

export const removeChannelAdminGroup = channelId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}/admingroup/`,
    method: 'DELETE',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [REMOVE_CHANNEL_ADMIN, REMOVE_CHANNEL_ADMIN_SUCCESS, REMOVE_CHANNEL_ADMIN_FAILURE],
  },
});
