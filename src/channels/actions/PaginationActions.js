export const SET_ELEMENTS_PER_PAGE = 'SET_ELEMENTS_PER_PAGE';
export const SET_ACTIVE_PAGE = 'SET_ACTIVE_PAGE';
export const SET_GET_CHANNELS_QUERY = 'SET_GET_CHANNELS_QUERY';

export const setElementsPerPage = elementsPerPage => {
  return {
    type: SET_ELEMENTS_PER_PAGE,
    payload: elementsPerPage,
  };
};

export const setActivePage = pageNumber => {
  return {
    type: SET_ACTIVE_PAGE,
    payload: pageNumber,
  };
};

export const setGetChannelsQuery = query => {
  return {
    type: SET_GET_CHANNELS_QUERY,
    payload: query,
  };
};
