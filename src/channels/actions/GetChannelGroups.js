import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';
import qs from 'qs';

// Get notifications
export const GET_CHANNELS_GROUPS = 'GET_CHANNELS_GROUPS';
export const GET_CHANNELS_GROUPS_SUCCESS = 'GET_CHANNELS_GROUPS_SUCCESS';
export const GET_CHANNELS_GROUPS_FAILURE = 'GET_CHANNELS_GROUPS_FAILURE';

export const SET_GET_CHANNEL_GROUPS_QUERY = 'SET_GET_CHANNEL_GROUPS_QUERY';
export const RESET_GET_CHANNEL_GROUPS_QUERY = 'RESET_GET_CHANNEL_GROUPS_QUERY';

export const getChannelGroups = (channelId, query) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}/groups?${qs.stringify(
      query
    )}`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [GET_CHANNELS_GROUPS, GET_CHANNELS_GROUPS_SUCCESS, GET_CHANNELS_GROUPS_FAILURE],
  },
});

export const setGetChannelGroupsQuery = query => {
  return {
    type: SET_GET_CHANNEL_GROUPS_QUERY,
    payload: query,
  };
};

export const resetChannelGroupsQuery = () => {
  return {
    type: RESET_GET_CHANNEL_GROUPS_QUERY,
  };
};
