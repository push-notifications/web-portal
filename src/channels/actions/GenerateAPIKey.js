import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const GENERATE_APIKEY = 'GENERATE_APIKEY';
export const GENERATE_APIKEY_SUCCESS = 'GENERATE_APIKEY_SUCCESS';
export const GENERATE_APIKEY_FAILURE = 'GENERATE_APIKEY_FAILURE';

export const generateAPIKey = channelId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}/apikey`,
    method: 'PUT',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [GENERATE_APIKEY, GENERATE_APIKEY_SUCCESS, GENERATE_APIKEY_FAILURE],
  },
});
