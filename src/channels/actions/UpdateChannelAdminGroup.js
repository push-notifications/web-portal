import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const UPDATE_CHANNEL_ADMIN = 'UPDATE_CHANNEL_ADMIN';
export const UPDATE_CHANNEL_ADMIN_SUCCESS = 'UPDATE_CHANNEL_SUCCESS_ADMIN';
export const UPDATE_CHANNEL_ADMIN_FAILURE = 'UPDATE_CHANNEL_FAILURE_ADMIN';
// eslint-disable-next-line import/prefer-default-export
export const updateChannelAdminGroup = (newAdminGroup, channelId) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels/${channelId}/admingroup`,
    method: 'PUT',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({newAdminGroup}),
    types: [UPDATE_CHANNEL_ADMIN, UPDATE_CHANNEL_ADMIN_SUCCESS, UPDATE_CHANNEL_ADMIN_FAILURE],
  },
});
