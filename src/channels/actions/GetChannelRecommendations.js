import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const GET_CHANNEL_RECOMMENDATIONS = 'GET_CHANNEL_RECOMMENDATIONS';
export const GET_CHANNEL_RECOMMENDATIONS_SUCCESS = 'GET_CHANNEL_RECOMMENDATIONS_SUCCESS';
export const GET_CHANNEL_RECOMMENDATIONS_FAILURE = 'GET_CHANNEL_RECOMMENDATIONS_FAILURE';

export const getChannelRecommendations = () => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channel-recommendations/`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [
      GET_CHANNEL_RECOMMENDATIONS,
      GET_CHANNEL_RECOMMENDATIONS_SUCCESS,
      GET_CHANNEL_RECOMMENDATIONS_FAILURE,
    ],
  },
});
