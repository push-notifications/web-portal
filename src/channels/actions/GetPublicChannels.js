import {RSAA} from 'redux-api-middleware';
import qs from 'qs';
import {GET_CHANNELS, GET_CHANNELS_SUCCESS, GET_CHANNELS_FAILURE} from './GetChannels';

export const getPublicChannels = query => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/public/channels?${qs.stringify(query)}`,
    method: 'GET',
    credentials: 'include',
    headers: {'Content-Type': 'application/json'},
    types: [GET_CHANNELS, GET_CHANNELS_SUCCESS, GET_CHANNELS_FAILURE],
  },
});
