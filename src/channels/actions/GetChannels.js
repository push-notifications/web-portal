import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';
import qs from 'qs';

// Get notifications
export const GET_CHANNELS = 'GET_CHANNELS';
export const GET_CHANNELS_SUCCESS = 'GET_CHANNELS_SUCCESS';
export const GET_CHANNELS_FAILURE = 'GET_CHANNELS_FAILURE';

export const getChannels = filter => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/channels?${qs.stringify({
      ...filter,
    })}`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [GET_CHANNELS, GET_CHANNELS_SUCCESS, GET_CHANNELS_FAILURE],
  },
});
