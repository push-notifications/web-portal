import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const TOGGLE_FAVORITE_CHANNEL = 'TOGGLE_FAVORITE_CHANNEL';
export const TOGGLE_FAVORITE_CHANNEL_SUCCESS = 'TOGGLE_FAVORITE_CHANNEL_SUCCESS';
export const TOGGLE_FAVORITE_CHANNEL_FAILURE = 'TOGGLE_FAVORITE_CHANNEL_FAILURE';

//  @Post('/favorites/toggle/:channelid')
export const toggleFavoriteChannel = channelid => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/usersettings/favorites/toggle/${channelid}`,
    method: 'POST',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [
      TOGGLE_FAVORITE_CHANNEL,
      TOGGLE_FAVORITE_CHANNEL_SUCCESS,
      TOGGLE_FAVORITE_CHANNEL_FAILURE,
    ],
  },
});
