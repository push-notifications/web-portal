import {
  GET_DEVICES,
  GET_DEVICES_SUCCESS,
  GET_DEVICES_FAILURE,
  GET_DEVICES_PAGE_ERROR,
  CREATE_DEVICE_SUCCESS,
  DELETE_DEVICE_SUCCESS,
  TEST_DEVICE_SUCCESS,
  UPDATE_DEVICE_SUCCESS,
  CREATE_DEVICE,
  CREATE_DEVICE_FAILURE,
  DELETE_DEVICE,
  DELETE_DEVICE_FAILURE,
  UPDATE_DEVICE,
  UPDATE_DEVICE_FAILURE,
} from 'devices/actions/devices';
import {UPDATE_REMOVE_DEVICE} from 'devices/actions/NoContentActionsUpdate';

const INITIAL_STATE = {
  userDevices: [],
  error: null,
  pageError: null,
  loading: false,
  loadingDelete: false,
  loadingUpdate: false,
  loadingCreate: false,
};

function processDeleteDeviceSuccess(state) {
  return state;
}

function processUpdateDeleteDevice(state, deviceId) {
  return {
    ...state,
    loadingDelete: false,
    userDevices: state.userDevices.filter(p => p.id !== deviceId),
  };
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_DEVICES:
      return {...state, loading: true, error: null, pageError: null};
    case GET_DEVICES_SUCCESS:
      return {
        ...state,
        userDevices: action.payload.userDevices || [],
        loading: false,
      };
    case GET_DEVICES_FAILURE:
      return {...state, error: action.payload, loading: false};
    case GET_DEVICES_PAGE_ERROR:
      return {...state, pageError: action.payload, loading: false};
    case CREATE_DEVICE:
      return {...state, loadingCreate: true, error: null, pageError: null};
    case CREATE_DEVICE_SUCCESS: {
      return {...state, loadingCreate: false, userDevices: [...state.userDevices, action.payload]};
    }
    case CREATE_DEVICE_FAILURE:
      return {
        ...state,
        error: action.payload,
        loadingCreate: false,
      };
    case DELETE_DEVICE:
      return {...state, loadingDelete: true, error: null, pageError: null};
    case DELETE_DEVICE_SUCCESS:
      return processDeleteDeviceSuccess(state, action.payload);
    case UPDATE_REMOVE_DEVICE:
      return processUpdateDeleteDevice(state, action.deviceId);
    case DELETE_DEVICE_FAILURE:
      return {
        ...state,
        error: action.payload,
        loadingDelete: false,
      };
    case TEST_DEVICE_SUCCESS:
      return state;
    case UPDATE_DEVICE:
      return {...state, loadingUpdate: true, error: null, pageError: null};

    case UPDATE_DEVICE_SUCCESS:
      // object replace avoiding mutate
      return {
        ...state,
        loadingUpdate: false,
        userDevices: state.userDevices.map(item => {
          if (item.id === action.payload.id) {
            // item to replace - return an updated value
            return {
              ...action.payload,
            };
          }
          // the rest keep it as-is
          return item;
        }),
      };

    case UPDATE_DEVICE_FAILURE:
      return {
        ...state,
        error: action.payload,
        loadingUpdate: false,
      };

    default:
      return state;
  }
}
