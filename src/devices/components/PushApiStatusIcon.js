import React, {useEffect, useState} from 'react';
import {Icon, Popup} from 'semantic-ui-react';

const PushApiStatusIcon = props => {
  const [currentEndpoint, setCurrentEndpoint] = useState('');

  useEffect(() => {
    navigator.serviceWorker.getRegistrations('/').then(registrations => {
      if (registrations && registrations.length > 0) {
        registrations[0].pushManager
          .getSubscription()
          .then(subscription => {
            if (subscription) {
              //console.debug('Push subscription found.', subscription.endpoint);
              setCurrentEndpoint(subscription.endpoint);
            } else {
              console.debug('No Push subscription.');
            }
          })
          .catch(error => {
            console.error(
              'Error on load for getCurrentToken, when checking Push subscription',
              error
            );
          });
      }
    });
  }, []);

  if (!props.token) return null;
  let endpoint = null;
  try {
    endpoint = JSON.parse(props.token)?.endpoint;
  } catch {
    console.debug('Cannot JSON decode token.', props.token);
    return null;
  }
  if (!endpoint) {
    console.debug('No endpoint found in token.', props.token);
    return null;
  }

  const valid = <Icon className="check circle" size="large" color="green" />;
  const unknown = <Icon className="question circle" size="large" color="grey" />;
  const warn = <Icon className="warning sign" size="large" color="red" />;
  const blacklist = <Icon className="ban" size="large" color="red" />;

  if (props?.status === 'OK' && currentEndpoint === endpoint) {
    return (
      <Popup trigger={valid} position="top center">
        This is the current device, and it is active.
      </Popup>
    );
  }

  if (props?.status === 'EXPIRED') {
    return (
      <Popup trigger={warn} position="top center">
        This device is invalid or expired, and should be removed.
      </Popup>
    );
  }

  if (props?.status === 'BLACKLISTED') {
    return (
      <Popup trigger={blacklist} position="top center">
        This device is blacklisted, for security reasons.
      </Popup>
    );
  }

  return (
    <Popup trigger={unknown} position="top center">
      This is another device, status is unknown.
    </Popup>
  );
};

export default PushApiStatusIcon;
