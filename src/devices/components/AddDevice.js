import React, {useState} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Form, Modal, Button, Label, Radio, Message, Segment, Divider} from 'semantic-ui-react';
import {v4 as uuidv4} from 'uuid';

import * as deviceActions from 'devices/actions/devices';
import * as showSnackBarActionCreators from 'common/actions/Snackbar';
import {getClientInformation, isSafari} from 'utils/user-agent';
import PushAPIRegistration from './PushAPIRegistration';
import SafariRegistration from './SafariRegistration';
import './AddDevice.scss';

const clientInformation = getClientInformation();

// Current supported list
const deviceTypesEnum = ['BROWSER', 'APP', 'MAIL'];
const deviceSubTypesEnum = [
  'SAFARI',
  'OTHER',
  'IOS',
  'ANDROID',
  'WINDOWS',
  'LINUX',
  'MAC',
  'MATTERMOST',
  'PRIMARY',
];

const AddDevice = ({createDevice, showSnackbar, loading}) => {
  const [deviceName, setDeviceName] = useState(clientInformation);
  const [deviceInfo, setDeviceInfo] = useState(navigator.userAgent);
  const [deviceType, setDeviceType] = useState('BROWSER');
  const [deviceSubType, setDeviceSubType] = useState(isSafari() ? 'SAFARI' : 'OTHER');
  const [deviceUuid, setdeviceUuid] = useState(uuidv4());
  const [deviceToken, setDeviceToken] = useState('');

  const [modalOpen, setModalOpen] = useState(false);
  // For now: show advanced options only if query string contains ?advanced
  // Will change if we allow adding alternate EMAIL
  const advancedAdd = new URLSearchParams(window.location.search).has('advanced');

  const [workerAlreadyRegistered, setWorkerAlreadyRegistered] = useState(false);

  const resetFormValues = () => {
    setDeviceName(clientInformation); // initialize with platform name, let user edit for easy remembering
    setDeviceInfo(navigator.userAgent);
    setDeviceType('BROWSER');
    setDeviceSubType(isSafari() ? 'SAFARI' : 'OTHER');
    setdeviceUuid(uuidv4());
    setDeviceToken('');
    setModalOpen(false);
  };

  const saveSubscriptionBlob = subscriptionBlob => {
    setDeviceToken(JSON.stringify(subscriptionBlob));
  };

  function handleClose() {
    resetFormValues();
  }

  async function handleSubmit() {
    if (!deviceToken) {
      showSnackbar(
        'Notification subscription data missing, please try to toggle subscription again.',
        'error'
      );
      return;
    }

    const response = await createDevice({
      name: deviceName,
      info: deviceInfo,
      type: deviceType,
      subType: deviceSubType,
      uuid: deviceUuid,
      token: deviceToken,
    });

    if (response.error) {
      showSnackbar('An error occurred while adding your device', 'error');
    } else {
      handleClose();
      showSnackbar('The device has been added successfully', 'success');
    }
  }

  const handleTypeChange = (e, d) => setDeviceType(d.value);
  const handleSubTypeChange = (e, d) => setDeviceSubType(d.value);

  return (
    <Modal
      as={Form}
      onSubmit={handleSubmit}
      trigger={
        <Button primary onClick={() => setModalOpen(true)}>
          Add device
        </Button>
      }
      open={modalOpen}
      onClose={handleClose}
    >
      <Modal.Header>Add device</Modal.Header>
      <Segment basic>
        <Label color="blue" ribbon className="form-ribbon">
          Device Name *
        </Label>
        <Form.Input
          label="Specify a device or computer name for easy identification in preferences targets"
          placeholder="Device name"
          value={deviceName}
          onChange={(e, d) => setDeviceName(d.value)}
        />

        {deviceType === 'BROWSER' && (
          <>
            <Label color="blue" ribbon className="form-ribbon">
              Activate Push
            </Label>
            <Form.Group grouped className="add-device">
              <label>
                Toggle to activate push notifications, click authorize if browser asks for
                permission
              </label>
              {deviceSubType !== 'SAFARI' && (
                <PushAPIRegistration
                  onSubscription={saveSubscriptionBlob}
                  onLoadStatus={setWorkerAlreadyRegistered}
                />
              )}
              {deviceSubType === 'SAFARI' && (
                <SafariRegistration
                  onSubscription={saveSubscriptionBlob}
                  onLoadStatus={setWorkerAlreadyRegistered}
                  deviceUuid={deviceUuid}
                />
              )}
            </Form.Group>
          </>
        )}

        {deviceType === 'BROWSER' && deviceSubType !== 'SAFARI' && workerAlreadyRegistered && (
          <Message negative>
            This browser seems to be already registered. If you want to proceed anyway, make sure
            you delete FIRST the other device entry if it still exists, and toggle the above
            subscription box off and on.
          </Message>
        )}

        {advancedAdd && (
          <>
            <Divider horizontal>Advanced options, use at your own risk</Divider>
            <Label color="blue" ribbon className="form-ribbon">
              Device Type
            </Label>
            <Form.Group inline>
              {deviceTypesEnum.map(oneType => (
                <Form.Field key={oneType}>
                  <Radio
                    label={oneType.charAt(0) + oneType.substring(1).toLowerCase()}
                    control="input"
                    name="radioDeviceTypes"
                    value={oneType}
                    checked={deviceType === oneType}
                    onChange={handleTypeChange}
                  />
                </Form.Field>
              ))}
            </Form.Group>

            <Label color="blue" ribbon className="form-ribbon">
              Device Subtype
            </Label>
            <Form.Group inline>
              {deviceSubTypesEnum.map(oneSubType => (
                <Form.Field key={oneSubType}>
                  <Radio
                    label={oneSubType.charAt(0) + oneSubType.substring(1).toLowerCase()}
                    control="input"
                    name="radioDeviceSubTypes"
                    value={oneSubType}
                    checked={deviceSubType === oneSubType}
                    onChange={handleSubTypeChange}
                  />
                </Form.Field>
              ))}
            </Form.Group>

            <Label color="blue" ribbon className="form-ribbon">
              Information
            </Label>
            <Form.Field>
              <Form.Input
                placeholder="Information"
                value={deviceInfo}
                onChange={(e, d) => setDeviceInfo(d.value)}
              />
            </Form.Field>

            <Label color="blue" ribbon className="form-ribbon">
              Device Token
            </Label>
            <Form.Field>
              <Form.Input
                placeholder="Device Token"
                value={deviceToken}
                onChange={(e, d) => setDeviceToken(d.value)}
              />
            </Form.Field>
          </>
        )}
      </Segment>
      <Modal.Actions>
        <Button type="reset" onClick={handleClose}>
          Cancel
        </Button>
        <Button type="submit" disabled={loading} loading={loading} primary>
          Submit
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

const mapStateToProps = state => {
  return {
    loading: state.devices.loadingCreate,
  };
};

export default connect(mapStateToProps, dispatch =>
  bindActionCreators(
    {
      createDevice: deviceActions.createDevice,
      ...showSnackBarActionCreators,
    },
    dispatch
  )
)(AddDevice);
