import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Button, Table, Form, Popup, Container} from 'semantic-ui-react';

import * as snackBarActions from 'common/actions/Snackbar';
import * as deviceActions from 'devices/actions/devices';
import AddDevice from 'devices/components/AddDevice';
import DeviceTypeIcon from 'utils/device-type-icon';
import DeviceHelp from 'help/components/DeviceHelp';
import {updateRemoveDevice as updateRemoveDeviceActionCreator} from 'devices/actions/NoContentActionsUpdate';
import PushApiStatusIcon from './PushApiStatusIcon';
import PushAPIRepair from './PushAPIRepair';
import {isSafari} from 'utils/user-agent';

const baseColumns = [
  {id: 'name', label: 'Name'},
  {id: 'info', label: 'Information'},
  {id: 'type', label: 'Device type'},
];

function DevicesList({
  devices,
  allowAdd,
  allowDelete,
  deleteDeviceById,
  tryBrowserPushNotification,
  updateDeviceById,
  updateRemoveDevice,
  showSnackbar,
  loadingDelete,
  loadingUpdate,
}) {
  const [editDeviceId, setEditDeviceId] = useState('');
  const [editDeviceName, setEditDeviceName] = useState('');
  const [editDeviceInfo, setEditDeviceInfo] = useState('');
  const [deleteId, setDeleteId] = useState(null);

  function resetUpdateValues() {
    setEditDeviceId('');
    setEditDeviceName('');
    setEditDeviceInfo('');
  }
  async function handleDelete(device) {
    setDeleteId(device.id);
    const response = await deleteDeviceById(device.id);
    if (response.error) {
      setDeleteId(null);
      showSnackbar(
        response?.payload?.response?.message || 'An error occurred while deleting the device',
        'error'
      );
    } else {
      updateRemoveDevice(device.id);
      setDeleteId(null);
      showSnackbar('Device removed successfully', 'success');
    }
  }

  async function handleUpdate(device) {
    const response = await updateDeviceById(device.id, editDeviceName, editDeviceInfo);
    if (response.error) {
      showSnackbar('An error occurred while updating the device', 'error');
      resetUpdateValues();
    } else {
      showSnackbar('Device updated successfully', 'success');
      resetUpdateValues();
    }
  }

  return (
    <Container>
      <Form>
        <Table striped>
          <Table.Header>
            <Table.Row>
              {baseColumns.map(column => (
                <Table.HeaderCell key={column.id}>{column.label}</Table.HeaderCell>
              ))}
              <Table.HeaderCell />
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {devices.map(device => {
              return (
                <Table.Row key={device.id}>
                  <Table.Cell>
                    {device.id === editDeviceId ? (
                      <Form.Input
                        placeholder="device name"
                        value={editDeviceName}
                        onChange={(e, d) => setEditDeviceName(d.value)}
                      />
                    ) : (
                      <>{device.name}</>
                    )}
                  </Table.Cell>
                  <Table.Cell>
                    {device.id === editDeviceId ? (
                      <Form.TextArea
                        placeholder="device info"
                        value={editDeviceInfo}
                        onChange={(e, d) => setEditDeviceInfo(d.value)}
                      />
                    ) : (
                      <>
                        {device.info == null ? (
                          <a href="/help#devices" target="_blank">
                            About phone numbers and this device.
                          </a>
                        ) : (
                          device.info
                        )}
                      </>
                    )}
                  </Table.Cell>
                  <Table.Cell singleLine>
                    <DeviceTypeIcon type={device.type} subType={device.subType} />
                    {device.type === 'BROWSER' && (
                      <PushApiStatusIcon token={device.token} status={device.status} />
                    )}
                  </Table.Cell>
                  <Table.Cell singleLine textAlign="right">
                    {device.id === editDeviceId && (
                      <>
                        <Popup
                          trigger={
                            <Button
                              disabled={device.id === editDeviceId && loadingUpdate}
                              loading={device.id === editDeviceId && loadingUpdate}
                              size="mini"
                              color="green"
                              icon="check"
                              onClick={() => handleUpdate(device)}
                            />
                          }
                          position="top center"
                          content="Save"
                        />
                        <Popup
                          trigger={
                            <Button
                              size="mini"
                              icon="cancel"
                              onClick={() => {
                                setEditDeviceId('');
                                setEditDeviceName('');
                                setEditDeviceInfo('');
                              }}
                            />
                          }
                          position="top center"
                          content="Cancel"
                        />
                      </>
                    )}

                    {device.id !== editDeviceId &&
                      device.type !== 'MAIL' &&
                      device.type !== 'PHONE' &&
                      device.subType !== 'MATTERMOST' && (
                        <Popup
                          trigger={
                            <Button
                              icon="pencil"
                              onClick={() => {
                                setEditDeviceId(device.id);
                                setEditDeviceName(device.name);
                                setEditDeviceInfo(device.info);
                              }}
                              size="mini"
                            />
                          }
                          position="top center"
                          content="Edit"
                        />
                      )}

                    {device.id !== editDeviceId &&
                      allowDelete &&
                      !(device.type === 'MAIL' && device.info === 'Default') &&
                      !(device.type === 'PHONE') &&
                      device.subType !== 'MATTERMOST' && (
                        <Popup
                          trigger={
                            <Button
                              inverted
                              color="red"
                              icon="trash"
                              disabled={deleteId === device.id && loadingDelete}
                              loading={deleteId === device.id && loadingDelete}
                              onClick={() => handleDelete(device)}
                              size="mini"
                            />
                          }
                          position="top center"
                          content="Delete"
                        />
                      )}

                    {device.id !== editDeviceId &&
                      device.status === 'EXPIRED' &&
                      !isSafari() &&
                      device.type === 'BROWSER' &&
                      device.subType === 'OTHER' && (
                        <PushAPIRepair deviceId={device.id} deviceName={device.name} />
                      )}

                    {device.id !== editDeviceId &&
                      (device.type === 'BROWSER' ||
                        device.type === 'WINDOWS' ||
                        device.subType === 'MATTERMOST') && (
                        <Popup
                          trigger={
                            <Button
                              inverted
                              content="Try me"
                              onClick={() => tryBrowserPushNotification(device.id)}
                              color="green"
                              size="mini"
                            />
                          }
                          position="top center"
                          content="Send a test notification"
                        />
                      )}
                  </Table.Cell>
                </Table.Row>
              );
            })}
          </Table.Body>
        </Table>
        {allowAdd && <AddDevice />}
        <DeviceHelp />
        <Popup
          trigger={<Button type="button" onClick={() => window.location.reload()} icon="refresh" />}
          content="Refresh list and statuses"
        />
      </Form>
    </Container>
  );
}

DevicesList.propTypes = {
  devices: PropTypes.arrayOf(PropTypes.object).isRequired,
  allowAdd: PropTypes.bool,
  allowDelete: PropTypes.bool,
  deleteDeviceById: PropTypes.func.isRequired,
  tryBrowserPushNotification: PropTypes.func.isRequired,
  updateDeviceById: PropTypes.func.isRequired,
  updateRemoveDevice: PropTypes.func.isRequired,
  showSnackbar: PropTypes.func.isRequired,
  loadingDelete: PropTypes.bool,
  loadingUpdate: PropTypes.bool,
};

DevicesList.defaultProps = {
  allowAdd: true,
  allowDelete: false,
  loadingDelete: false,
  loadingUpdate: false,
};

const mapStateToProps = state => {
  return {
    loadingDelete: state.devices.loadingDelete,
    loadingUpdate: state.devices.loadingUpdate,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      deleteDeviceById: deviceActions.deleteDeviceById,
      tryBrowserPushNotification: deviceActions.tryBrowserPushNotification,
      updateDeviceById: deviceActions.updateDeviceById,
      showSnackbar: snackBarActions.showSnackbar,
      updateRemoveDevice: updateRemoveDeviceActionCreator,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(DevicesList);
