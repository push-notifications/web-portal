import React, {useEffect, useState} from 'react';
import {Checkbox, Dimmer, Form, Loader, Message, Segment} from 'semantic-ui-react';

function SafariRegistration({onSubscription, onLoadStatus, deviceUuid}) {
  const [status, setStatus] = useState([]);
  const [errormsg, setErrormsg] = useState([]);
  const [isEnabled, setIsEnabled] = useState(false);
  const [activating, setActivating] = useState(false);

  useEffect(() => {
    // Ensure that the user can receive Safari Push Notifications.
    if (!('safari' in window && 'pushNotification' in window.safari)) {
      setErrormsg(prevState => [...prevState, 'Not Safari or no push support in this version !']);
      onLoadStatus(false);
      return;
    }
    const permissionData = window.safari.pushNotification.permission(
      process.env.REACT_APP_APPLE_WEBSITEPUSHID
    );
    if (!permissionData) {
      setErrormsg(prevState => [...prevState, 'Not Safari or no push support in this version !']);
      onLoadStatus(false);
      return;
    }
    if (permissionData.permission === 'granted') {
      setIsEnabled(true);
      onLoadStatus(true);
      onSubscription(permissionData.deviceToken);
      setStatus(prevState => [...prevState, 'Registration is already active.']);
      return;
    }
    if (permissionData.permission === 'denied') {
      onLoadStatus(false);
      setErrormsg(prevState => [
        ...prevState,
        'Permission for notifications on this site is denied. Check your preferences.',
      ]);
      return;
    }
    onLoadStatus(false);
  }, [onLoadStatus, onSubscription]);

  function checkRemotePermission(permissionData) {
    if (permissionData.permission === 'default') {
      window.safari.pushNotification.requestPermission(
        process.env.REACT_APP_APPLE_WEBSERVICEURL, // The web service URL.
        process.env.REACT_APP_APPLE_WEBSITEPUSHID, // The Website Push ID.
        // TODO: Encrypt uuid
        {anonid: deviceUuid}, // Data that you choose to send to your server to help you identify the user.
        checkRemotePermission // The callback function.
      );
    } else if (permissionData.permission === 'denied') {
      // The user said no.
      setErrormsg(prevState => [
        ...prevState,
        'Permission for notifications on this site is denied. Check your preferences.',
      ]);
    } else if (permissionData.permission === 'granted') {
      // The web service URL is a valid push provider, and the user said yes.
      // permissionData.deviceToken is available to use.
      setIsEnabled(true);
      setStatus(prevState => [...prevState, 'Registration completed.']);
      onSubscription(permissionData.deviceToken);
    }
    setActivating(false);
  }

  function registerSafari() {
    // console.log('Service worker is active.');
    // setStatus(prevState => [...prevState, 'Service worker is active.']);
    // setErrormsg(prevState => [...prevState, 'No notification permission granted!']);
    checkRemotePermission(
      window.safari.pushNotification.permission(process.env.REACT_APP_APPLE_WEBSITEPUSHID)
    );
  }

  function unRegisterSafari() {
    setErrormsg(prevState => [
      ...prevState,
      'To remove registration, open Safari Preferences / Notifications, and Remove CERN Notification.',
    ]);
    setActivating(false);
  }

  function toggleRegistration() {
    setActivating(true);
    if (isEnabled) unRegisterSafari();
    else registerSafari();
  }

  return (
    <Form.Field>
      <Segment basic>
        <Checkbox
          checked={isEnabled}
          onChange={toggleRegistration}
          toggle
          label="Safari Push Notifications Registration"
        />
        <Dimmer active={activating} inverted>
          <Loader size="mini" content="Processing" />
        </Dimmer>
      </Segment>

      {status && status.length > 0 && (
        <Message size="tiny">
          <Message.Header>Status</Message.Header>
          {status.map((str, i) => (
            <div key={i}>{str} </div>
          ))}
        </Message>
      )}
      {errormsg && errormsg.length > 0 && (
        <Message negative size="tiny">
          <Message.Header>Error</Message.Header>
          {errormsg.map((str, i) => (
            <div key={i}>{str} </div>
          ))}
        </Message>
      )}
    </Form.Field>
  );
}

export default SafariRegistration;
