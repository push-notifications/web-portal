import React, {useState} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Form, Modal, Button, Popup} from 'semantic-ui-react';
import * as showSnackBarActionCreators from 'common/actions/Snackbar';
import {urlB64ToUint8Array} from 'utils/push-utils';
import * as deviceActions from 'devices/actions/devices';

const PushAPIRepair = ({updateDeviceById, showSnackbar, deviceId, deviceName}) => {
  const [modalOpen, setModalOpen] = useState(false);

  function handleClose() {
    setModalOpen(false);
  }

  // Removes existing subscription in browser and registers a new one, then written in the device.token in db
  async function handleRepair() {
    Notification.requestPermission(function (result) {
      if (result !== 'granted') {
        showSnackbar('Notifications permissions are NOT granted in browser!', 'error');
      } else {
        try {
          navigator.serviceWorker.getRegistrations('/').then(registrations => {
            if (registrations && registrations.length > 0) {
              registrations[0].pushManager
                .getSubscription()
                .then(subscription => {
                  if (subscription) subscription.unsubscribe();
                })
                .catch(error => {
                  showSnackbar(
                    'An error occurred while removing previous subscription ' + error,
                    'error'
                  );
                })
                .finally(() => {
                  // No need to check existing Push subscriptions, subscribe method handles it all
                  // Application Key used for registration to push services
                  const applicationServerKey = urlB64ToUint8Array(
                    process.env.REACT_APP_VAPID_PUBLICKEY
                  );
                  const subscriptionOptions = {applicationServerKey, userVisibleOnly: true};
                  registrations[0].pushManager
                    .subscribe(subscriptionOptions)
                    .then(async subscription => {
                      const response = await updateDeviceById(
                        deviceId,
                        undefined,
                        undefined,
                        JSON.stringify(subscription)
                      );
                      if (response.error) {
                        showSnackbar('An error occurred while repairing your device', 'error');
                      } else {
                        showSnackbar('The device has been repaired successfully', 'success');
                        handleClose();
                      }
                    })
                    .catch(err => {
                      console.log('Error', err);
                      showSnackbar('Error ' + err, 'error');
                    });
                });
            }
          });
        } catch (regErr) {
          showSnackbar('Error ' + regErr.message, 'error');
        }
      }
    });
  }

  return (
    <Modal
      as={Form}
      onSubmit={handleRepair}
      trigger={
        <Popup
          trigger={
            <Button
              inverted
              content="Repair"
              onClick={() => setModalOpen(true)}
              color="orange"
              size="mini"
            />
          }
          position="top center"
          content="Fix this device registration"
        />
      }
      open={modalOpen}
      onClose={handleClose}
    >
      <Modal.Header>Repair device {deviceName}</Modal.Header>
      <Modal.Content>
        <p>
          Click Repair to fix the Push registration of this device with the current browser push
          system.
          <br />
          If this browser is already linked to another registered device, this other device will be
          invalidated.
        </p>
        <p>
          When done, please click the Refresh button in the device list page to update list and
          device statuses.
        </p>
      </Modal.Content>
      <Modal.Actions>
        <Button type="reset" onClick={handleClose}>
          Cancel
        </Button>
        <Button type="submit" primary>
          Repair
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

export default connect(null, dispatch =>
  bindActionCreators(
    {
      ...showSnackBarActionCreators,
      updateDeviceById: deviceActions.updateDeviceById,
    },
    dispatch
  )
)(PushAPIRepair);
