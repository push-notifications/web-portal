import React, {useEffect, useState} from 'react';
import {register, unregister} from 'register-service-worker';
import {Checkbox, Dimmer, Form, Loader, Message, Segment} from 'semantic-ui-react';

function ServiceWorkerRegistration({onSubscription, onLoadStatus}) {
  const [status, setStatus] = useState([]);
  const [errorMsg, setErrorMsg] = useState([]);
  const [isEnabled, setIsEnabled] = useState(false);
  const [activating, setActivating] = useState(false);

  useEffect(() => {
    // Anything in here is fired on component mount.
    navigator.serviceWorker.getRegistrations('/').then(function (registrations) {
      if (registrations && registrations.length > 0) {
        setIsEnabled(true);
        onLoadStatus(true);
      } else onLoadStatus(false);
    });
  }, [onLoadStatus]);

  function registerWorker() {
    Notification.requestPermission(function (result) {
      if (result !== 'granted') {
        setErrorMsg(prevState => [...prevState, 'No notification permission granted!']);
      } else {
        try {
          register('/serviceWorker.js', {
            registrationOptions: {scope: '/'},
            ready(registration) {
              setStatus(prevState => [...prevState, 'Service worker is active.']);

              // This will be called only once when the service worker is installed for first time.
              const applicationServerKey = urlB64ToUint8Array(
                process.env.REACT_APP_VAPID_PUBLICKEY
              );
              const subscriptionOptions = {applicationServerKey, userVisibleOnly: true};
              registration.pushManager
                .subscribe(subscriptionOptions)
                .then(subscription => {
                  console.log('subscribed to pushManager');
                  onSubscription(subscription);
                  setActivating(false);
                })
                .catch(err => {
                  console.log('Error', err);
                  if (err.toString().includes('storage error'))
                    setErrorMsg(prevState => [
                      ...prevState,
                      err +
                        ' Please unregister with toggle above, refresh the page (F5) and try again',
                    ]);
                  else setErrorMsg(prevState => [...prevState, 'Error: ' + err]);
                  setActivating(false);
                });
            },
            registered(registration) {
              setStatus(prevState => [...prevState, 'Service worker has been registered.']);
              setIsEnabled(true);
            },
            cached(registration) {
              setStatus(prevState => [...prevState, 'Content has been cached for offline use.']);
            },
            updatefound(registration) {
              setStatus(prevState => [...prevState, 'New content is downloading.']);
            },
            updated(registration) {
              setStatus(prevState => [...prevState, 'New content is available; please refresh.']);
            },
            offline() {
              setStatus(prevState => [
                ...prevState,
                'No internet connection found. App is running in offline mode.',
              ]);
            },
            error(error) {
              setErrorMsg(prevState => [
                ...prevState,
                'Error during service worker registration:' + error.message,
              ]);
              setActivating(false);
            },
          });
        } catch (regErr) {
          setErrorMsg(prevState => [
            ...prevState,
            'Error during service worker registration:' + regErr.message,
          ]);
          setActivating(false);
        }
      }
    });
  }

  function unRegisterWorker() {
    unregister();
    setIsEnabled(false);
    setStatus(prevState => [...prevState, 'service worker unregistered']);
    setActivating(false);
  }

  // urlB64ToUint8Array is a magic function that will encode the base64 public key
  // to Array buffer which is needed by the subscription option
  function urlB64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - (base64String.length % 4)) % 4);
    const base64 = (base64String + padding).replace(/-/g, '+').replace(/_/g, '/');
    const rawData = atob(base64);
    const outputArray = new Uint8Array(rawData.length);
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  }

  function toggleRegistration() {
    setActivating(true);
    if (isEnabled) unRegisterWorker();
    else registerWorker();
  }

  return (
    <Form.Field>
      <Segment basic>
        <Checkbox
          checked={isEnabled}
          onChange={toggleRegistration}
          toggle
          label="Browser Push Notifications Registration"
        />
        <Dimmer active={activating} inverted>
          <Loader size="mini" content="Processing" />
        </Dimmer>
      </Segment>

      {status && status.length > 0 && (
        <Message size="tiny">
          <Message.Header>Status</Message.Header>
          {status.map((str, i) => (
            <div key={i}>{str} </div>
          ))}
        </Message>
      )}
      {errorMsg && errorMsg.length > 0 && (
        <Message negative size="tiny">
          <Message.Header>Error</Message.Header>
          {errorMsg.map((str, i) => (
            <div key={i}>{str} </div>
          ))}
        </Message>
      )}
    </Form.Field>
  );
}

export default ServiceWorkerRegistration;
