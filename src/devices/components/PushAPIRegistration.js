import React, {useEffect, useState} from 'react';
import {Checkbox, Dimmer, Form, Loader, Message, Segment} from 'semantic-ui-react';
import {urlB64ToUint8Array} from 'utils/push-utils';

function PushAPIRegistration({onSubscription, onLoadStatus}) {
  const [status, setStatus] = useState([]);
  const [errorMsg, setErrorMsg] = useState([]);
  const [isEnabled, setIsEnabled] = useState(false);
  const [activating, setActivating] = useState(false);

  useEffect(() => {
    // Anything in here is fired on component mount.
    navigator.serviceWorker.getRegistrations('/').then(registrations => {
      if (registrations && registrations.length > 0) {
        registrations[0].pushManager
          .getSubscription()
          .then(subscription => {
            if (subscription) {
              console.debug('Push subscription found', subscription);
              setIsEnabled(true);
              onLoadStatus(true);
            } else {
              console.debug('Push subscription not found');
              onLoadStatus(false);
            }
          })
          .catch(error => {
            console.error('Error on load, checking Push subscription', error);
            onLoadStatus(false);
          });
      }
    });
  }, [onLoadStatus]);

  function registerPushAPI() {
    Notification.requestPermission(function (result) {
      if (result !== 'granted') {
        setErrorMsg(prevState => [
          ...prevState,
          'Notifications permissions are NOT granted in browser!',
        ]);
      } else {
        try {
          navigator.serviceWorker.getRegistrations('/').then(registrations => {
            if (registrations && registrations.length > 0) {
              // No need to check existing Push subscriptions, subscribe method handles it all
              // Application Key used for registration to push services
              const applicationServerKey = urlB64ToUint8Array(
                process.env.REACT_APP_VAPID_PUBLICKEY
              );
              const subscriptionOptions = {applicationServerKey, userVisibleOnly: true};

              registrations[0].pushManager
                .subscribe(subscriptionOptions)
                .then(subscription => {
                  console.log('subscribed to Push Notifications');
                  setStatus(prevState => [...prevState, 'Push Notifications subscribed.']);
                  onSubscription(subscription);
                  setIsEnabled(true);
                })
                .catch(err => {
                  console.log('Error', err);
                  if (err.toString().includes('storage error'))
                    setErrorMsg(prevState => [
                      ...prevState,
                      err +
                        ' Please unsubscribe with toggle above, refresh the page (F5) and try again',
                    ]);
                  else setErrorMsg(prevState => [...prevState, 'Error: ' + err]);
                });

              setActivating(false);
            }
          });
        } catch (regErr) {
          setErrorMsg(prevState => [
            ...prevState,
            'Error during service worker registration:' + regErr.message,
          ]);
          setActivating(false);
        }
      }
    });
  }

  function unRegisterPushAPI() {
    console.debug('unRegisterPushAPI starting.');

    navigator.serviceWorker.getRegistrations('/').then(registrations => {
      if (registrations && registrations.length > 0) {
        registrations[0].pushManager
          .getSubscription()
          .then(subscription => {
            if (subscription) {
              subscription.unsubscribe();
              console.debug('Push subscription removed.');
            } else {
              console.debug('No Push subscription to remove.');
            }
            setIsEnabled(false);
            setStatus(prevState => [...prevState, 'Push Notifications unsubscribed']);
            setActivating(false);
          })
          .catch(error => {
            console.error(
              'Error on load for unRegisterPushAPI, when checking Push subscription',
              error
            );
          });
      }
    });
  }

  function toggleRegistration() {
    setActivating(true);
    if (isEnabled) unRegisterPushAPI();
    else registerPushAPI();
  }

  return (
    <Form.Field>
      <Segment basic>
        <Checkbox
          checked={isEnabled}
          onChange={toggleRegistration}
          toggle
          label="Browser Push Notifications Registration"
        />
        <Dimmer active={activating} inverted>
          <Loader size="mini" content="Processing" />
        </Dimmer>
      </Segment>

      {status && status.length > 0 && (
        <Message size="tiny">
          <Message.Header>Status</Message.Header>
          {status.map((str, i) => (
            <div key={i}>{str} </div>
          ))}
        </Message>
      )}
      {errorMsg && errorMsg.length > 0 && (
        <Message negative size="tiny">
          <Message.Header>Error</Message.Header>
          {errorMsg.map((str, i) => (
            <div key={i}>{str} </div>
          ))}
        </Message>
      )}
    </Form.Field>
  );
}

export default PushAPIRegistration;
