// Triggered after any 204 no Content response (usually Deletes), to update the state
export const UPDATE_REMOVE_DEVICE = 'UPDATE_REMOVE_DEVICE';

export const updateRemoveDevice = deviceId => {
  return {
    deviceId,
    type: UPDATE_REMOVE_DEVICE,
  };
};
