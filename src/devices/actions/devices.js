import {RSAA} from 'redux-api-middleware';
import {withAuth} from 'auth/utils/authUtils';

export const GET_DEVICES = 'GET_DEVICES';
export const GET_DEVICES_SUCCESS = 'GET_DEVICES_SUCCESS';
export const GET_DEVICES_FAILURE = 'GET_DEVICES_FAILURE';
export const GET_DEVICES_PAGE_ERROR = 'GET_DEVICES_PAGE_ERROR';

export const CREATE_DEVICE = 'CREATE_DEVICE';
export const CREATE_DEVICE_SUCCESS = 'CREATE_DEVICE_SUCCESS';
export const CREATE_DEVICE_FAILURE = 'CREATE_DEVICE_FAILURE';

export const DELETE_DEVICE = 'DELETE_DEVICE';
export const DELETE_DEVICE_SUCCESS = 'DELETE_DEVICE_SUCCESS';
export const DELETE_DEVICE_FAILURE = 'DELETE_DEVICE_FAILURE';

export const TEST_DEVICE = 'TEST_DEVICE';
export const TEST_DEVICE_SUCCESS = 'TEST_DEVICE_SUCCESS';
export const TEST_DEVICE_FAILURE = 'TEST_DEVICE_FAILURE';

export const UPDATE_DEVICE = 'UPDATE_DEVICE';
export const UPDATE_DEVICE_SUCCESS = 'UPDATE_DEVICE_SUCCESS';
export const UPDATE_DEVICE_FAILURE = 'UPDATE_DEVICE_FAILURE';

export const getDevices = pageError => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/devices/`,
    method: 'GET',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [
      GET_DEVICES,
      GET_DEVICES_SUCCESS,
      pageError ? GET_DEVICES_PAGE_ERROR : GET_DEVICES_FAILURE,
    ],
  },
});

export const createDevice = device => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/devices`,
    method: 'POST',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify(device),
    types: [CREATE_DEVICE, CREATE_DEVICE_SUCCESS, CREATE_DEVICE_FAILURE],
  },
});

export const deleteDeviceById = deviceId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/devices/${deviceId}`,
    method: 'DELETE',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [DELETE_DEVICE, DELETE_DEVICE_SUCCESS, DELETE_DEVICE_FAILURE],
  },
});

export const tryBrowserPushNotification = deviceId => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/devices/${deviceId}`,
    method: 'POST',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    types: [TEST_DEVICE, TEST_DEVICE_SUCCESS, TEST_DEVICE_FAILURE],
  },
});

export const updateDeviceById = (deviceId, name, info, token) => ({
  [RSAA]: {
    endpoint: `${process.env.REACT_APP_BASE_URL}/devices/${deviceId}`,
    method: 'PATCH',
    credentials: 'include',
    headers: withAuth({'Content-Type': 'application/json'}),
    body: JSON.stringify({name: name, info: info, token: token}),
    types: [UPDATE_DEVICE, UPDATE_DEVICE_SUCCESS, UPDATE_DEVICE_FAILURE],
  },
});
