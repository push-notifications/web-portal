import React, {useEffect} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Header, Divider, Segment} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import {Helmet} from 'react-helmet';

import DevicesList from 'devices/components/DevicesList';
import * as devicesActions from 'devices/actions/devices';
import ErrorComponent from 'utils/error-component';

const TITLE = `${process.env.REACT_APP_NAME} | Devices`;

function DevicesGlobal({devices, getDevices, loading, error}) {
  useEffect(() => {
    // Errors are blocking the page
    getDevices(true);
  }, [getDevices]);

  return error ? (
    <ErrorComponent error={error} />
  ) : (
    <div
      style={{
        width: 933,
        marginTop: 100,
        marginLeft: 'auto',
        marginRight: 'auto',
      }}
    >
      <Helmet>
        <title>{TITLE}</title>
      </Helmet>
      <Header as="h3">
        Devices List
        <Header.Subheader>
          Register your devices. Currently supported: Email, Mattermost via direct message, and
          browser push notifications.
        </Header.Subheader>
      </Header>

      <Divider />
      <Segment basic loading={loading}>
        <DevicesList devices={devices} allowDelete />
      </Segment>
    </div>
  );
}

DevicesGlobal.propTypes = {
  devices: PropTypes.arrayOf(PropTypes.object).isRequired,
  getDevices: PropTypes.func.isRequired,
  loading: PropTypes.bool,
};

export default connect(
  state => ({
    devices: state.devices.userDevices || [],
    loading: state.devices.loading,
    // Errors are blocking the page
    error: state.devices.pageError,
  }),
  dispatch =>
    bindActionCreators(
      {
        getDevices: devicesActions.getDevices,
      },
      dispatch
    )
)(DevicesGlobal);
