import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'connected-react-router';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import {register} from 'register-service-worker';

import {createBrowserHistory} from 'history';
import configureStore from './store/ConfigureStore';
// Disable for now due to conflict with push service worker
//import registerServiceWorker from './registerServiceWorker';
import App from './App';

import './index.scss';

const history = createBrowserHistory();
const store = configureStore(history);

const theme = createMuiTheme({
  palette: {
    primary: {main: '#08c'},
  },
});

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('root')
);

// Register Service Worker on homepage
// To make sure it's loaded when going to the Device management page
// Shall we filter out Safari which does not need it for push ?
try {
  register('/serviceWorker.js', {
    registrationOptions: {scope: '/'},
    ready(registration) {
      console.log('Service worker is ready.');
    },
    registered(registration) {
      console.log('Service worker is registered.');
    },
    cached(registration) {
      console.log('Service worker has been cached for offline use.');
    },
    updatefound(registration) {
      console.log('Service worker update found.');
    },
    updated(registration) {
      console.log('Service worker updated; please refresh.');
    },
    offline() {
      console.log('Service worker No internet connection found. App is running in offline mode.');
    },
    error(error) {
      console.error('Error during service worker registration:' + error.message);
    },
  });
} catch (regErr) {
  console.log('Error catched during service worker registration:' + regErr.message);
}
